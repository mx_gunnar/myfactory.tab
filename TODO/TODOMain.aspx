<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.TODOMain" codefile="TODOMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="100%" cellSpacing=0 cellPadding=0 width="100%">
<tr valign="middle">
	<td width="100%" align="center">
		<myfactory:wfLabel runat="server" ID="lblText"></myfactory:wfLabel>
	</td>
</tr>
</table>

</body>
</HTML>
