'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory International GmbH
' Component:    LoginProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for Login
'--------------------------------------------------------------------------------
' Created:      08.02.2011 10:44:50, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Data.PublicEnums
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Permissions.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab

    Partial Class LoginProcess
        Inherits System.Web.UI.Page

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Method:       Page_Load
        '--------------------------------------------------------------------------------
        ' Purpose:      Handles Page.Load event
        '--------------------------------------------------------------------------------
        ' Parameter:    
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim sRes As String = ""
            Dim sCmd As String = Request.QueryString("Cmd")


            If sCmd = "Login" Then
                sRes = msLogin()
            End If


            Response.Clear()
            Response.Expires = -1
            Response.CacheControl = "no-cache"
            Response.AddHeader("Pragma", "no-cache")

            Response.Write(sRes)
            Response.End()

        End Sub

        ''' <summary>
        ''' Get Result of Login
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msLogin() As String

            Dim sRes As String = ""
            Dim oClientInfoWeb As ClientInfo
            Dim oClientInfo As ClientInfo

            Dim sUser As String = Request.QueryString("User")
            Dim sPwd As String = Request.QueryString("Pwd")

            Dim sErr As String = ""

            '-------------- check params -----------------------------------
            If sUser = "" Then
                Return "Bitte geben Sie einen Benutzernamen an!"
            End If

            If sPwd = "" Then
                Return "Bitte geben Sie ein Kennwort an!"
            End If

            If LCase(sUser) = "webshop" Then
                Return "Diese Benutzerkennung ist nicht zugelassen"
            End If


            '-------------- create webshop clientinfo ----------------------------------
            oClientInfoWeb = ClientInfoMethods.goCreateClientInfo("Webshop", "374O356O353O", CStr(Application("AppName")))
            If oClientInfoWeb Is Nothing Then
                Return "Interner Fehler beim Anmelden des Portalusers"
            End If

            '-------------- check user ----------------------------------
            Dim rs As Recordset
            Dim sDB As String

            rs = DataMethods.grsGetDBRecordset(oClientInfoWeb, "InActive,LoginFailures,UserInitials,UserPwd", "tsUsers", _
                                             "UserName=" & DataTools.gsStr2Sql(sUser), , , wfDataSourceGlobal)
            If rs.EOF Then
                Return "Unbekannte Benutzerkennung"
            End If

            If rs("InActive").bValue Then Return "Der Benutzer ist deaktiviert"
            If rs("LoginFailures").lValue > 2 Then
                Dim sMsg As String = ""

                If Not myfactory.Sys.Main.Tools.gbLoginFailureCheck(oClientInfoWeb, sUser, sMsg) Then
                    Return sMsg
                End If
            End If

            If Not UserPwd.gbCheckUserPwd(oClientInfoWeb, rs("UserInitials").sValue, sPwd) Then
                myfactory.Sys.Main.Tools.gbAddLoginFailure(oClientInfoWeb, sUser)
                Return "Fehler bei der Anmeldung"
            End If

            Dim rsDB As Recordset
            Dim sClause As String

            sDB = Request.QueryString("Database")
            sClause = "ud.UserID=" & DataTools.gsStr2Sql(rs("UserInitials").sValue)
            If sDB <> "" Then
                sClause &= " AND ud.DatabaseName=" & DataTools.gsStr2Sql(sDB)
            End If

            rsDB = DataMethods.grsGetDBRecordset(oClientInfoWeb, "ud.DatabaseName,d.DatabaseDesc", _
                                                 "tsUserDatabases ud INNER JOIN tsDatabases d ON ud.DatabaseName=d.DatabaseName", _
                                           sClause, , , wfDataSourceGlobal)
            If rsDB.RecordCount = 0 Then Return "Diesem Benutzer wurde keine Datenbank zugeordnet"

            If rsDB.RecordCount > 1 Then
                Dim osDB As New FastString
                Dim osDB2 As New FastString
                Dim bFirst As Boolean = True

                Do Until rsDB.EOF
                    If bFirst Then
                        bFirst = False
                    Else
                        osDB.bAppend(";")
                        osDB2.bAppend(";")
                    End If
                    osDB.bAppend(rsDB(0).sValue)
                    osDB2.bAppend(rsDB(1).sValue)
                    rsDB.MoveNext()
                Loop

                Return "DB$" & osDB.sString & "$" & osDB2.sString
            End If

            '--- only one database ----
            sDB = rsDB(0).sValue

            Dim lDivisionNr As Integer = glCInt(Request.QueryString("Division"))



            '-------------- create clientinfo ----------------------------------
            Dim sErrText As String = ""

            oClientInfo = moLoginUser(sUser, sPwd, sDB, CStr(Application("AppName")), lDivisionNr, sErrText)
            If oClientInfo Is Nothing Then
                Return "Fehler beim Anmelden an der Datenbank" & " " & sErrText
            End If

            DataMethods2.glDBUpdate(oClientInfo, "tsUsers", "LoginFailures=0", "UserName=" & DataTools.gsStr2Sql(sUser), wfDataSourceGlobal)
            sRes = "OK;" & oClientInfo.sClientID


            If lDivisionNr = 0 Then
                Dim sDesc As String = ""
                Dim sField As String = ""
                Dim lDivCount As Integer = 0

                'set common permissions to "unknown" to really check
                ClientInfoMethods.gbSetCommonPermissions(oClientInfo, -1)

                rsDB = DataMethods.grsGetDBRecordset(oClientInfo, "DivisionID,DivisionDesc", "tdDivisions", , , "DivisionID")
                Do Until rsDB.EOF
                    Dim nPermission As wfEnumPermissions
                    Dim bDefault As Boolean = gbCheckDefaultPermission(oClientInfo)

                    nPermission = gnCheckCommonPermissions(oClientInfo, oClientInfo.oClientProperties.sCurrentUser, rsDB("DivisionID").lValue)
                    If nPermission = wfEnumPermissions.wfPermissionNone And bDefault Then
                        nPermission = wfEnumPermissions.wfPermissionAllowed
                    End If

                    If nPermission <> wfEnumPermissions.wfPermissionDenied Then
                        If sDesc <> "" Then
                            sDesc = sDesc & ";"
                            sField = sField & ";"
                        End If
                        sDesc = sDesc & rsDB("DivisionID").sValue
                        sField = sField & rsDB("DivisionDesc").sValue
                        lDivCount += 1
                    End If

                    rsDB.MoveNext()
                Loop

                If lDivCount = 0 Then
                    ClientLogin.gbLogoff(oClientInfo)
                    Return "Keine zul�ssige Betriebsst�tte"
                ElseIf lDivCount > 1 Then
                    ClientLogin.gbLogoff(oClientInfo)
                    Return "BS$" & sDesc & " $" & sField
                Else 'lDivCount=1
                    Call ClientInfoMethods.gbSetClientInfoSession(oClientInfo, glCInt(sDesc), oClientInfo.oClientProperties.dtBookingDate, oClientInfo.oClientProperties.lCurrentPeriod)
                    Call ClientInfoMethods.gbSetCommonPermissions(oClientInfo, CShort(myfactory.Sys.Permissions.Permissions.gnCheckCommonPermissions(oClientInfo)))

                    Call ClientLogin.gbUpdateClientInfo(oClientInfo)
                End If

            End If

            'remember
            If gbCBool(Request.QueryString("Remember")) Then

                Dim sKey As String = "wT34!Qd+12#k"
                Dim sTabUser As String = XmlFunctions.gsXMLEncrypt(sUser, sKey)
                Dim sTabPwd As String = XmlFunctions.gsXMLEncrypt(sPwd, sKey)

                sTabUser = Replace(sTabUser, "<wfXML>", "")
                sTabUser = Replace(sTabUser, "</wfXML>", "")
                sTabPwd = Replace(sTabPwd, "<wfXML>", "")
                sTabPwd = Replace(sTabPwd, "</wfXML>", "")

                Response.Cookies("wfTabUser").Value = sTabUser
                Response.Cookies("wfTabUser").Expires = DateAdd("d", 30, Date.Today)

                Response.Cookies("wfTabPwd").Value = sTabPwd
                Response.Cookies("wfTabPwd").Expires = DateAdd("d", 30, Date.Today)

                Response.Cookies("wfTabDB").Value = sDB
                Response.Cookies("wfTabDB").Expires = DateAdd("d", 30, Date.Today)

                Response.Cookies("wfTabBS").Value = lDivisionNr.ToString
                Response.Cookies("wfTabBS").Expires = DateAdd("d", 30, Date.Today)

                Response.Cookies("wfTabRemember").Value = "true"
                Response.Cookies("wfTabRemember").Expires = DateAdd("d", 30, Date.Today)
            Else
                Response.Cookies("wfTabUser").Value = ""
                Response.Cookies("wfTabPwd").Value = ""
                Response.Cookies("wfTabDB").Value = ""
                Response.Cookies("wfTabBS").Value = ""
                Response.Cookies("wfTabRemember").Value = "false"
            End If


            '--- check for active tab access
            If Not GeneralProperties.gbGetGeneralProperty(oClientInfo, "EnableTabAccess") Then
                sErrText = Dictionary.gsTranslate(oClientInfo, "Der iPad/Tablet-Zugang f�r diese Datenbank ist nicht freigegeben.")
                ClientLogin.gbLogoff(oClientInfo)
                Return sErrText
            End If

            If Not Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_Main") Then
                sErrText = Dictionary.gsTranslate(oClientInfo, "Sie haben keine Berechtigung f�r diesen Zugang.")
                ClientLogin.gbLogoff(oClientInfo)
                Return sErrText
            End If

            '--- check for pwd expire
            Dim lDays As Integer

            lDays = myfactory.Sys.Main.Tools.glGetExpireDays(oClientInfo)
            If lDays = 0 Then
                sErrText = Dictionary.gsTranslate(oClientInfo, "Ihr Kennwort ist abgelaufen, bitte melden Sie sich an der Hauptanwendung an, um es zu �ndern.")
                ClientLogin.gbLogoff(oClientInfo)
                Return sErrText
            ElseIf lDays < 11 And lDays <> -1 Then
                sErrText = Dictionary.gsTranslate(oClientInfo, "Ihr Kennwort l�uft in $1 Tagen ab, bitte melden Sie sich an der Hauptanwendung an, um es zu �ndern.")
                sErrText = Replace(sErrText, "$1", lDays)
                sRes = "PWD;" & oClientInfo.sClientID & ";" & sErrText
            End If

            oClientInfo.bTabletMode = True
            mLogoffOldClients(oClientInfo)
            DataMethods2.glDBUpdate(oClientInfo, "tsClients", "CMSUsed=11", "ClientID=" & DataTools.gsStr2Sql(oClientInfo.sClientID), wfDataSourceGlobal)

            '--- hold user mobile-platform /linux / iPhone / ... 
            Dim sPlatfrom As String = Request.QueryString("MobilePlatform")
            ClientValues.gbSetClientValue(oClientInfo, "Tab_MobilePlatformName", sPlatfrom)


            Return sRes

        End Function


        Private Function moLoginUser(ByVal sUser As String, ByVal sPwd As String, ByVal sDatabase As String, _
                        ByVal sAppName As String, ByVal lDivision As Integer, ByRef sErrorText As String) As ClientInfo

            Dim oClientInfo As ClientInfo
            Dim sQry As String

            moLoginUser = Nothing

            '---- User / Pwd --------
            oClientInfo = ClientInfoMethods.goCreateClientInfo(sUser, gsEncryptString(sPwd), sAppName)
            If oClientInfo Is Nothing Then Exit Function
            If oClientInfo.oErrors.lCount > 0 Then
                sErrorText = oClientInfo.oErrors.sErrText
                If oClientInfo.sClientID <> "" Then
                    sQry = "ClientID=" & DataTools.gsStr2Sql(oClientInfo.sClientID)
                    DataMethods2.glDBDelete(oClientInfo, "tsClients", sQry, wfEnumDataSources.wfDataSourceGlobal)
                End If
                Exit Function
            End If

            Call ClientLogin.gbInitLogin(oClientInfo)

            '---- Database -----------
            Call ClientInfoMethods.gbSetClientInfoDatabase(oClientInfo, sDatabase, sAppName)
            If oClientInfo.oErrors.lCount > 0 Then
                sErrorText = oClientInfo.oErrors.sErrText
                If oClientInfo.sClientID <> "" Then
                    sQry = "ClientID=" & DataTools.gsStr2Sql(oClientInfo.sClientID)
                    DataMethods2.glDBDelete(oClientInfo, "tsClients", sQry, wfEnumDataSources.wfDataSourceGlobal)
                End If
                Exit Function
            End If

            Call ClientLogin.gbUpdateClientInfo(oClientInfo)

            '---- Session Infos ------
            Dim dtDate As Date = Date.Today
            Dim lPeriod As Integer = 0

            If lDivision = 0 Then lDivision = 1

            Try
                If lPeriod = 0 Then lPeriod = Period.glDate2Period(oClientInfo, dtDate)
            Catch ex As System.Exception
                sErrorText = ex.Message
                If oClientInfo.sClientID <> "" Then
                    sQry = "ClientID=" & DataTools.gsStr2Sql(oClientInfo.sClientID)
                    DataMethods2.glDBDelete(oClientInfo, "tsClients", sQry, wfEnumDataSources.wfDataSourceGlobal)
                End If
                oClientInfo.Dispose()
                Exit Function
            End Try

            Call ClientInfoMethods.gbSetClientInfoSession(oClientInfo, lDivision, dtDate, lPeriod)

            '---- Additional Infos ---
            Call ClientInfoMethods.gbSetCommonPermissions(oClientInfo, CShort(Permissions.gnCheckCommonPermissions(oClientInfo)))
            Call ClientInfoMethods.gbSetEditionFlags(oClientInfo, Editions.gsGetUserEdition(oClientInfo), sAppName)
            Call ClientLogin.gbUpdateClientInfo(oClientInfo)

            oClientInfo.sAppName = sAppName
            moLoginUser = oClientInfo

        End Function

        Private Sub mLogoffOldClients(oClientInfo As ClientInfo)

            Dim frs As FastRecordset

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, "ClientID", "tsClients", _
                                                 "CMSUsed=11 AND UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser), _
                                                 , , wfDataSourceGlobal)
            Do Until frs.EOF
                ClientLogin.gbLogoff(frs(0).sValue, oClientInfo.sAppName)
                frs.MoveNext()
            Loop

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
