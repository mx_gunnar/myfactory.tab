'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    InfoMessagesData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for InfoMessagesData
'--------------------------------------------------------------------------------
' Created:      3/2/2012 10:18:22 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.InfoMessages

    Partial Class InfoMessagesData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sText As String
            Dim bHasDocuments As Boolean
            Dim sUser As String = oClientInfo.oClientProperties.sCurrentUser
            Dim sUserGroups As String
            Dim bShowHidden As Boolean

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = oClientInfo.sWebPageRoot & "/tab/modules/infomessages/infomessagesmain/InfoMessagesData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 20
            oListView.bAutoHideNavigation = True


            bShowHidden = gbCBool(oListView.sUserData)

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("m.MessagePriority", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeImage

            oListView.oCols.oAddCol("m.ShowDate", "Datum", "80", wfEnumDataTypes.wfDataTypeDate)
            oListView.oCols.oAddCol("m.MessageSubject", "Titel", "*")
            oCol = oListView.oCols.oAddCol("cmdDet", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)


            '---- Data ---------------------
            sOrder = "m.ShowDate DESC, m.MessageID DESC"

            sClause = "m.InActive=0"
            If Not bShowHidden Then
                sClause = DataTools.gsClauseAnd(sClause, "(mr.HideMessage=0 OR mr.HideMessage IS NULL)")
            End If


            sUserGroups = PermissionInfos.gsGetUserGroups(oClientInfo, "", False, True)
            If sUserGroups <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, "(EXISTS(SELECT * FROM tdInfoMessageUsers mu WHERE mu.MessageID=m.MessageID AND mu.UserName=" & DataTools.gsStr2Sql(sUser) & ")" & _
                                                " OR " & _
                                                "EXISTS(SELECT * FROM tdInfoMessageUserGroups mg WHERE mg.MessageID=m.MessageID AND mg.UserGroupName IN (" & sUserGroups & ")))")
            Else
                sClause = DataTools.gsClauseAnd(sClause, "EXISTS(SELECT * FROM tdInfoMessageUsers mu WHERE mu.MessageID=m.MessageID AND mu.UserName=" & DataTools.gsStr2Sql(sUser) & ")")
            End If

            sFields = "m.MessageID AS RowID, m.MessagePriority, m.ShowDate, m.MessageSubject, m.MessageText, mr.MessageRead" & _
                        ", (SELECT COUNT(*) FROM tdDocumentReferences dr WHERE dr.TypeID=43 AND dr.ReferenceID=m.MessageID) AS DocCount"
            sTables = "tdInfoMessages m" & _
                        " LEFT JOIN tdInfoMessageReads mr ON m.MessageID=mr.MessageID AND mr.UserName=" & DataTools.gsStr2Sql(sUser)

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)

                If rs("MessagePriority").lValue = 2 Then
                    oRow.Value(0) = "image_Prio_high.gif"
                ElseIf rs("MessagePriority").lValue = 0 Then
                    oRow.Value(0) = "image_Prio_low.gif"
                End If

                oRow.Value(1) = rs("ShowDate").dtValue
                oRow.Value(2) = rs("MessageSubject").sValue

                'read style
                If rs("MessageRead").bValue Then
                    oRow.bLineTop = True
                Else
                    oRow.bGroupStyle = True
                End If

                bHasDocuments = (rs("DocCount").lValue > 0)

                sText = rs("MessageText").sValue
                If sText <> "" Or bHasDocuments Then
                    If Len(sText) > 500 Then
                        sText = Left(sText, 500) & "..."
                    End If
                    oRow = oListView.oRows.oAddRow(rs(0).sValue & "_T")
                    oRow.Value(2) = sText

                    If bHasDocuments Then oRow.Value(0) = "image_Attachment.gif"
                    oRow.bSetEmpty(3)

                End If

                rs.MoveNext()
            Loop

            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
