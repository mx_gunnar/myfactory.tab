﻿// InfoMessagesMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshMainList()
}

function mRefreshMainList()
{
    var sUserData = '0';

    if (document.all.chkShowHidden.checked)
        sUserData = '-1';

    gListViewSetUserData('lstMain', sUserData)
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.srcElement.id == 'chkShowHidden')
        mRefreshMainList();
}

function mOnListViewClick(sListView, sItemID)
{
    var sURL = '../InfoMessageDetails/InfoMessageDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&MessageID=' + sItemID;

    document.location = sURL;
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL = '../InfoMessageDetails/InfoMessageDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&MessageID=' + sItemID;

    document.location = sURL;
}

