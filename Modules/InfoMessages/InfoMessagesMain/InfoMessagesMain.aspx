<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.InfoMessages.InfoMessagesMain" codefile="InfoMessagesMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="98%" style="table-layout:fixed;" cellSpacing=4 cellPadding=0 width="99%">
<tr height="25px">
	<td width="400px">
        <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/infomessages/infomessagesmain/dlginfomessagesmain.xml"></myfactory:wfXmlDialog>
	</td>
    <td width="*">&nbsp;</td>
</tr>
<tr height="*" valign="top">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="InfoMessagesData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
