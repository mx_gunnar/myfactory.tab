<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.InfoMessages.InfoMessageDetailsMain" codefile="InfoMessageDetailsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="100%" cellSpacing=1 cellPadding=1 width="100%">
<tr height="25px">
	<td>
        <myfactory:wfMenu runat="server" ID="mnuMain" sMenuName="MainMenuGeneral"></myfactory:wfMenu>
	</td>
</tr>
<tr height="250px">
	<td>
        <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/infomessages/infomessagedetails/dlginfomessagedetailsmain.xml"></myfactory:wfXmlDialog>
	</td>
</tr>
<tr height="*">
	<td>
        <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="InfoMessagesDocListData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
