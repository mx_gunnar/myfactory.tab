﻿//InfoMessageDetailsMain.js (c) myfactory 2012

function mOnLoad()
{
    mRefreshList(); 
}

function mRefreshList()
{
    gListViewSetUserData('lstMain', msPageParams[0])
    gListViewLoadPage('lstMain', 1);
}

function mOnError()
{
    gAlert(msTerms[0]);
    mOnClose();
}

function mOnSetDirty()
{
    var sURL;
    var sRes;

    if (event.srcElement.id == 'chkHideMessage')
    {
        sURL = msWebPageRoot +
                '/ie50/base/info/infomessages/infomessagedetails/infomessagedetailsprocess.aspx' +
                '?Cmd=Hide' +
                '&MessageID=' + msPageParams[0] +
                '&Set=' + document.all.chkHideMessage.checked;

        sRes = gsCallServerMethod(sURL, '');
        if (sRes != 'OK;')
            gAlert(sRes);
    }

    if (event.srcElement.id == 'chkMessageRead')
    {
        sURL = msWebPageRoot +
                '/ie50/base/info/infomessages/infomessagedetails/infomessagedetailsprocess.aspx' +
                '?Cmd=Read' +
                '&MessageID=' + msPageParams[0] +
                '&Set=' + document.all.chkMessageRead.checked;

        sRes = gsCallServerMethod(sURL, '');
        if (sRes != 'OK;')
            gAlert(sRes);
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    gOpenEntityMethod('Documents', 'Edit', sItemID)
}

function mOnListViewClick(sListView, sItemID)
{
    mOpenDocument(sItemID);
}

function mOpenDocument(lDocumentID)
{
    if (lDocumentID == 0) return;

    //------ check permission to read ----------
    if (lDocumentID != '')
    {
        var sPerm = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' + 'document_Process.aspx?DocumentID=' + lDocumentID + '&Method=CheckPermissionRead', '');
        if (sPerm != ';-1')
        {
            alert(msTerms[0]);
            return;
        };
    }
    else
        return;

    //------ get link and open document --------
    var sLink = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' +
					'document_Process.aspx?DocumentID=' + lDocumentID +
					'&Method=GetDocumentLink&LocalMode=-1', '');
    if (sLink.substr(0, 1) != ';')
    {
        alert(sLink);
        return;
    }
    sLink = sLink.substr(1);
    if (sLink != '')
        window.open(sLink, '', 'menubar=no,fullscreen=no,scrollbars=yes,location=no,toolbar=no,resizable=yes,status=yes,left=50,top=50,width=600,height=400')
    //------------------------------------------	
}
