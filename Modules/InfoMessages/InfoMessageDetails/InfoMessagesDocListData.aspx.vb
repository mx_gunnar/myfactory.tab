'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    DocumentsListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for DocumentsListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.InfoMessages

    Partial Class InfoMessagesDocListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim lMessageID As Integer

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "InfoMessagesDocListData.aspx"
            oListView.lPageSize = 20
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            lMessageID = glCInt(oListView.sUserData)

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("DocumentDesc", "Dokumente", "*")

            oCol = oListView.oCols.oAddCol("CmdDoc", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            Dim sSubClause As String

            sOrder = "CreationDate DESC, DocumentID DESC"

            sFields = "tdDocuments.DocumentID,DocumentDesc"
            sTables = "tdDocuments" & _
                        " LEFT JOIN tsDocumentTypes ON tdDocuments.DocumentType = tsDocumentTypes.DocumentTypeID" & _
                        " INNER JOIN tdDocumentReferences dr ON tdDocuments.DocumentID = dr.DocumentID"

            '---- clause ---------
            sClause = "dr.TypeID=43 AND dr.ReferenceID=" & lMessageID

            '---- permissions ----
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Documents")
            If sSubClause <> "" Then sClause = DataTools.gsClauseAnd(sClause, sSubClause)


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
