'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    InfoMessageDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for InfoMessageDetailsMain
'--------------------------------------------------------------------------------
' Created:      3/2/2012 11:07:07 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.InfoMessages

    Partial Class InfoMessageDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lMessageID As Integer = glCInt(Request.QueryString("MessageID"))

            Me.sTitle = "Nachrichtendetails"

            ReDim Me.asTerms(0)
            Me.asTerms(0) = "Fehler beim �ffnen der Nachricht."

            ReDim Me.asPageParams(0)
            Me.asPageParams(0) = lMessageID.ToString

            '----- check, if user should see message ----
            Dim sUser As String = oClientInfo.oClientProperties.sCurrentUser
            Dim sUserGroups As String
            Dim sClause As String = "m.MessageID=" & lMessageID

            sUserGroups = PermissionInfos.gsGetUserGroups(oClientInfo, "", False, True)
            If sUserGroups <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, "(EXISTS(SELECT * FROM tdInfoMessageUsers mu WHERE mu.MessageID=m.MessageID AND mu.UserName=" & DataTools.gsStr2Sql(sUser) & ")" & _
                                                " OR " & _
                                                "EXISTS(SELECT * FROM tdInfoMessageUserGroups mg WHERE mg.MessageID=m.MessageID AND mg.UserGroupName IN (" & sUserGroups & ")))")
            Else
                sClause = DataTools.gsClauseAnd(sClause, "EXISTS(SELECT * FROM tdInfoMessageUsers mu WHERE mu.MessageID=m.MessageID AND mu.UserName=" & DataTools.gsStr2Sql(sUser) & ")")
            End If

            If Not DataMethods.gbDBExists(oClientInfo, "MessageID", "tdInfoMessages m", sClause) Then
                Me.sOnLoad = "mOnError()"
                Return
            End If

            '----- set message values -----------
            Dim rs As Recordset

            rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", _
                                               "tdInfoMessages m" & _
                                                " LEFT JOIN tdInfoMessageReads mr ON m.MessageID=mr.MessageID AND mr.UserName=" & DataTools.gsStr2Sql(sUser), _
                                               "m.MessageID=" & lMessageID)
            If Not rs.EOF Then
                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
            End If


            Me.sOnLoad = "mOnLoad()"


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
