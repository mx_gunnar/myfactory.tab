<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Opps.OppsMain" codefile="OppsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="98%" cellSpacing=2 cellPadding=2 style="table-layout:fixed;" width="99%">
<tr height="30px">
	<td width="300px">
		<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Opps/Oppsmain/dlgOppsmain.xml"></myfactory:wfXmlDialog>
	</td>
	<td width="*">&nbsp;</td>
</tr>
<tr height="*">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="OppsListData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
