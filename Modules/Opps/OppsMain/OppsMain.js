﻿// OppsMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    gListViewSetUserData('lstMain', document.all.txtSearch.value);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL;

    if (sColID == 'cmdOpp') 
    {
        sURL = '../OppsDetails/OppsDetailsMain.aspx' +
                    '?ClientID=' + msClientID +
                    '&RecordID=' + sItemID;

        document.location = sURL;
    }

    if (sColID == 'cmdAddr')
    {
        sURL = '../../Addresses/AddressDetails/AddressDetailsMain.aspx' +
                    '?ClientID=' + msClientID +
                    '&RecordID=' + gsListViewGetItemUserData(sListView, sItemID);

        document.location = sURL;
    }
}

