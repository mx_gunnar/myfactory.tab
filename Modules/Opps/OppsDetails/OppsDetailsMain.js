﻿// OppsMain.js (c) myfactory 2011

function mOnLoad()
{
}

function mOnOK()
{
    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sURL = 'OppsDetailsProcess.aspx?Cmd=Save'

    var sRes = gsCallServerMethod(sURL, sValues);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mNavigateToList();
}

function mOnCancel()
{
        mNavigateToList();
}

function mNavigateToList()
{
    var sURL;

    if (msPageParams[0] == 'AddressOpps')
    {
        sURL = '../../Addresses/Opps/OppsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + msPageParams[1];
    }
    else
    {
        sURL = '../OppsMain/OppsMain.aspx' +
                '?ClientID=' + msClientID;
    }

    document.location = sURL;
}


function cmdEntityAddresse_OnClick() 
{
    var sPath = msWebPageRoot + "/" + msTerms[0] + "&ClientID=" + msClientID;
    document.location = sPath;
}