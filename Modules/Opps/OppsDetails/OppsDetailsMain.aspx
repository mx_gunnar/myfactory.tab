<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Opps.OppsDetailsMain" codefile="OppsDetailsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable cellSpacing=2 cellPadding=2 width="100%">
<tr height="200px">
	<td width="*">
		<form name="frmMain">
			<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Opps/OppsDetails/dlgOppsDetails.xml" sDataSource="dsoData"></myfactory:wfXmlDialog>
		</form>
	</td>
	<td width="*">&nbsp</td>
</tr>
<tr height="25px">
	<td width="300px" align="right">
		<myfactory:wfButton runat="server" ID="cmdOK" sOnClick="mOnOK()" sText=" OK "></myfactory:wfButton>
		<myfactory:wfButton runat="server" ID="cmdCancel" sOnClick="mOnCancel()" sText="Abbrechen"></myfactory:wfButton>
	</td>
	<td width="*">&nbsp</td>
</tr>
</table>

</body>
</HTML>
