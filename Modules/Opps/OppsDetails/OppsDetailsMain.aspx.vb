'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    OppsDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for OppsDetailsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.BusinessTasks.Resources.ToolFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Opps

    Partial Class OppsDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim sType As String = Request.QueryString("Type")
            Dim rs As Recordset
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))
            Dim lEntityCmdAddressID As Integer

            If lRecordID <> 0 Then

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   " o.OpportunityID, o.OpportunityDesc, o.OpportunityDescText, " & _
                                                   " o.OpportunityDate, o.RepresentativeID, a.Matchcode, " & _
                                                   " o.OrderSum, o.OpportunityRating, o.CurrUnit, o.SalesPhaseID, " & _
                                                   " o.AddressID", _
                                                   "tdSalesOpportunities o" & _
                                                        " INNER JOIN tdAddresses a ON o.AddressID=a.AddressID", _
                                                   "OpportunityID=" & lRecordID)
            Else
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   " a.Matchcode, a.AddressID, " & _
                                                        DataTools.gsStr2Sql(Currencies.gsCurrUnitInternal(oClientInfo, -1)) & " AS CurrUnit, " & _
                                                        DataTools.gsStr2Sql(Now.ToShortDateString) & " AS OpportunityDate", _
                                                   "tdAddresses a", _
                                                   "AddressID=" & lAddressID)

            End If

            If Not rs.EOF Then
                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                lEntityCmdAddressID = rs("AddressID").lValue
            End If

            Me.sOnLoad = "mOnLoad()"
            Me.gAddScriptLink("wfDlgParams.js", True)

            ReDim Me.asPageParams(2)
            ReDim Me.asTerms(1)

            Me.asPageParams(0) = sType
            Me.asPageParams(1) = lAddressID.ToString


            Dim sCmdEntity As String
            sCmdEntity = DataMethods.gsGetDBValue(oClientInfo, "EditCmdTab", "tsEntities", "EntityName='Addresses'", , myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)
            sCmdEntity = sCmdEntity.Replace("$1", lEntityCmdAddressID.ToString)
            Me.asTerms(0) = sCmdEntity


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
