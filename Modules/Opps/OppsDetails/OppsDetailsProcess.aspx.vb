'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    OppssDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for OppsDetailsProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Opps

    Partial Class OppsDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Save"
                    Return msSaveOppsData(oClientInfo)

                Case Else
                    Return "Unknown Cmd: " & sCmd
            End Select

            Return "TODO"

        End Function

        ''' <summary>
        ''' Save Opps Data
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msSaveOppsData(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRoot As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oRoot.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            '--------- check values ----------------
            Dim oDyn As New DynamicObject
            Dim lOppID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtOpportunityID"))
            Dim rs As Recordset
            Dim sXml As String

            sXml = XmlFunctions.gsXmlConvertFromDialog(oRoot.xml)
            DynamicObjectTools.gbInit(oDyn, sXml)

            If oDyn.sProp("OpportunityDesc") = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie eine Bezeichnung f�r die Auftragschance an.")
            If oDyn.sProp("OpportunityRating") = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie eine Bewertung f�r die Auftragschance an.")

            oDyn.Prop("OrderSumInternal") = Currencies.gcCurrConvert(oClientInfo, oDyn.cProp("OrderSum"), Date.Today, oDyn.sProp("CurrUnit"))

            If lOppID = 0 Then
                Dim sFields As String = ""
                Dim sValues As String = ""

                lOppID = RecordID.glGetNextRecordID(oClientInfo, "tdSalesOpportunities")
                oDyn.Prop("OpportunityID") = lOppID
                oDyn.Prop("CreationDate") = Date.Now
                oDyn.Prop("CreationUser") = oClientInfo.oClientProperties.sCurrentUser
                oDyn.Prop("DivisionNr") = oClientInfo.oClientProperties.lDivisionNr
                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdSalesOpportunities", "1=2")

                DynamicObjectTools.gbBuildInsertExpression(oClientInfo, oDyn, rs, "", sFields, sValues)
                DataMethods2.glDBExecute(oClientInfo, "INSERT INTO tdSalesOpportunities(" & sFields & ") VALUES (" & sValues & ")")

            Else
                Dim sQry As String = ""

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdSalesOpportunities", "OpportunityID=" & lOppID)
                If rs.EOF Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Daten.")

                DynamicObjectTools.gbBuildUpdateExpression(oClientInfo, oDyn, rs, "", sQry)
                If sQry <> "" Then DataMethods2.glDBUpdate(oClientInfo, "tdSalesOpportunities", sQry, "OpportunityID=" & lOppID)
            End If

            Return "OK;"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
