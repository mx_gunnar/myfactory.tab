'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressNewMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AddressNewMain
'--------------------------------------------------------------------------------
' Created:      19.04.2013 11:42:20, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressNewMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad


            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))
            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = Str(lAddressID)

            If lAddressID > 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "AddressID, Matchcode, Name1, Name2, Street, Street2, Country, PostalCode, City, PhoneNr, MobilePhone, Homepage, EMail, FaxNr, '0' as IsNewCustomer ", _
                                                   "tdAddresses", "AddressID=" & lAddressID)

                If Not rs.EOF Then

                    If DataMethods.gbDBExists(oClientInfo, "CustomerID", "tdCustomers", "AddressID=" & lAddressID & " AND InActive=0") Then
                        rs("IsNewCustomer").Value = "-1"
                        Me.asPageParams(1) = "-1"
                        Me.dlgMainCustomer.oDialog.oField("IsNewCustomer").oMember.Prop("VALUE") = "-1"
                    End If

                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)

                End If
            End If


            Me.gAddScriptLink("wfDlgParams.js", True)
            Me.sOnLoad = "mOnLoad()"


            'check if customer-checkbox is allowed (setting, no permission)
            If Not Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_AddressCreateCustomer", "", "Adresse als Kunde anlegen") Then
                Me.asPageParams(2) = "0"
            Else
                Me.asPageParams(2) = "-1"
            End If

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
