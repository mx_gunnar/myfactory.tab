'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for AddressNewProcess
'--------------------------------------------------------------------------------
' Created:      19.04.2013 11:43:51, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Tools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String




            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))

            Select Case sCmd
                Case "New"
                    Return msAddNewAddress(oClientInfo)
                Case "Change"
                    Return msChangeAddress(oClientInfo, lAddressID)
                Case Else
                    Return "ERR;unknown command"
            End Select

        End Function


        Private Function msAddNewAddress(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode

            Dim sIsNewCustomer As String = Request.QueryString("IsCustomer")



            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectSingleNode("//DlgParams")
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '----------------------------------------



            '--------- validate dialog params ---------
            If XmlFunctions.gsGetXMLText(oRootNew.xml, "txtName1") = "" Then
                Return Dictionary.gsTranslate(oClientInfo, "Das Feld 'Name' darf nicht leer sein.")
            End If
            '------------------------------------------


            '------------- insert into db -------------
            Dim lNewAddressID As Integer
            Dim sMsg As String = String.Empty
            Dim sFields As String = String.Empty
            Dim sValues As String = String.Empty

            'create new address number
            Dim sNewAddressNumber As String = BaseNumberFormat.gsFormatBaseNumberNew(oClientInfo, "*", "Addresses")
            Dim oNodeAdressNumber As XmlNode = oXml.createElement("txtAddressNumber")
            oNodeAdressNumber.text = sNewAddressNumber
            oRootNew.appendChild(oNodeAdressNumber)

            ' -- create new address by template-address
            Dim lTemplateAddress As Integer = GeneralProperties.glGetGeneralProperty(oClientInfo, "TabTemplateAddress")
            If lTemplateAddress > 0 Then

                ' create new address by template-address

                ' clone original address-record
                lNewAddressID = myfactory.BusinessTasks.CRM.Main.FastAddresses.glCloneAddress(oClientInfo, lTemplateAddress)

                ' update data by xml
                Dim sUpdate As String = String.Empty
                Dim oDyn As New DynamicObject
                Dim rsOriginal As Recordset = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdAddresses", "AddressID=" & lNewAddressID)

                If Not DynamicObjectTools.gbInit(oDyn, XmlFunctions.gsXmlConvertFromDialog(oRootNew.xml)) Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Anlegen der neuen Adresse ist ein Fehler aufgetreten")
                End If

                If Not DynamicObjectTools.gbBuildUpdateExpression(oClientInfo, oDyn, rsOriginal, "AddressID, CreationDate, CreationUser", sUpdate) Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Anlegen der neuen Adresse ist ein Fehler aufgetreten")
                End If

                If Not String.IsNullOrEmpty(sUpdate) AndAlso DataMethods2.glDBUpdate(oClientInfo, "tdAddresses", sUpdate, "AddressID=" & lNewAddressID) <> 1 Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Anlegen der neuen Adresse ist ein Fehler aufgetreten")
                End If

            Else

                ' create new address without template-address

                lNewAddressID = RecordID.glGetNextRecordID(oClientInfo, "tdAddresses")
                If Not TableDefFunctions.gbBuildInsertExpression(oClientInfo, oRootNew.xml, oClientInfo.sAppPath & "\ie50\base\Addresses\tableaddresses.xml", _
                                                                            sFields, sValues, sMsg, True) Then
                    Return sMsg
                End If

                sFields = "AddressID, CreationDate, CreationUser, " & sFields
                sValues = lNewAddressID & "," & Date.Now.gs2Sql & ", " & oClientInfo.oClientProperties.sCurrentUser.gs2Sql & ", " & sValues

                If DataMethods2.glDBExecute(oClientInfo, "INSERT INTO tdAddresses (" & sFields & ") VALUES (" & sValues & ")") <> 1 Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Anlegen der neuen Adresse ist ein Fehler aufgetreten")
                End If

            End If
            ' -------------------------------------------




            '----------- create new customer -------------
            If sIsNewCustomer = "true" AndAlso lNewAddressID > 0 Then
                If myfactory.BusinessTasks.CRM.Main.FastAddresses.glCopyTemplateCustomer(oClientInfo, lNewAddressID) = 0 Then
                    Return Dictionary.gsTranslate(oClientInfo, "Adresse erfolgreich angelegt! \nFehler beim Anlegen als neuer Kunde!")
                End If
            End If


            Return "OK;" & lNewAddressID

        End Function

        Private Function msChangeAddress(ByVal oClientInfo As ClientInfo, ByVal lAddressID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode
            Dim sIsNewCustomer As String = Request.QueryString("IsCustomer")


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '----------------------------------------


            '--------- validate dialog params ---------
            If XmlFunctions.gsGetXMLText(oRootNew.xml, "txtName1") = "" Then
                Return Dictionary.gsTranslate(oClientInfo, "Das Feld 'Name' darf nicht leer sein.")
            End If
            '------------------------------------------


            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\base\Addresses\tableaddresses.xml", sQry, sMsg, True) Then
                Return sMsg
            End If


            If Not String.IsNullOrEmpty(sQry) Then
                If DataMethods2.glDBUpdate(oClientInfo, "tdAddresses", sQry, "tdAddresses.AddressID=" & lAddressID) <> 1 Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Speichern der �nderung ist ein Fehler aufgetreten")
                End If
            End If


            '------------- create new customer ------------            
            If sIsNewCustomer = "true" Then
                If Not DataMethods.gbDBExists(oClientInfo, "CustomerID", "tdCustomers", "AddressID=" & lAddressID) Then
                    If myfactory.BusinessTasks.CRM.Main.FastAddresses.glCopyTemplateCustomer(oClientInfo, lAddressID) = 0 Then
                        Return Dictionary.gsTranslate(oClientInfo, "Adresse erfolgreich ge�ndert! \nFehler beim Anlegen als neuer Kunde!")
                    End If
                End If
            End If

            Return "OK;" & lAddressID

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
