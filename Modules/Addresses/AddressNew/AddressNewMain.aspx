<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.AddressNewMain" CodeFile="AddressNewMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="2" cellpadding="2" width="98%" style="table-layout: fixed;">
        <tr valign="top" height="200px">
            <td colspan="2">
                <form name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/AddressNew/dlgAddressNew.xml">
                </myfactory:wfXmlDialog>
                </form>
            </td>
        </tr>
        <tr id="tdAddressCustomer" valign="top" height="10px">
            <td width="300px"> 
                <myfactory:wfXmlDialog runat="server" ID="dlgMainCustomer" sDialog="/tab/modules/Addresses/AddressNew/dlgAddressNewCustomer.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr height="*">
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="bottom">
            <td align="right" colspan="2">
                <myfactory:wfButton runat="server" ID="cmdNew" sOnClick="mOnOK()" sText=" OK ">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdEdit" sOnClick="mOnNavigateBack()" sText="Abbrechen">
                </myfactory:wfButton>
            </td>
        </tr>
        <tr height="10px">
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</body>
</html>
