﻿
var sOriginalValues = "";

function mOnLoad() 
{

    if (msPageParams[0] != "0") 
    {
        sOriginalValues = gsXMLDlgParams(frmMain, '', false);
    }


    if (msPageParams[1] == "-1") 
    {
        // is already customer
        document.getElementById("chkIsNewCustomer").disabled = true;
    }

    if (msPageParams[2] == "0") 
    {
        document.getElementById("tdAddressCustomer").style.display = "none";
    }
}

function mOnOK() 
{

    // check matchcode and set it new if its emtpy  
    mSetContactMatchcode();


    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sCmd = "";
    var sRes = "";
    var sURL = "";

    var lAddressID = "-1";

    // change or new
    if (msPageParams[0] != "0") 
    {
        // change address
        sCmd = "Change";
        sURL = 'AddressNewProcess.aspx?Cmd=' + sCmd + '&AddressID=' + msPageParams[0] + '&IsCustomer=' + document.all.chkIsNewCustomer.checked;
        sRes = gsCallServerMethod(sURL, sOriginalValues + ';' + sValues);
    }
    else 
    {
        // new address
        sCmd = "New";
        sURL = 'AddressNewProcess.aspx?Cmd=' + sCmd + '&AddressID=' + msPageParams[0] + '&IsCustomer=' + document.all.chkIsNewCustomer.checked;
        sRes = gsCallServerMethod(sURL, sValues);        
    }


    // handle sRes (show Error or navigate)
    if (sRes.substr(0, 3) != 'OK;') 
    {
        alert(sRes);
    }
    else 
    {
        lAddressID = sRes.split(';')[1];
        mOnNavigateBack(lAddressID);
    }
}



function mSetContactMatchcode() 
{
    var sMatchcode = document.all.txtMatchcode.value;

    if (sMatchcode == '') 
    {
        // create new matchcode
        sMatchcode = document.all.txtName1.value + ", " + document.all.txtCity.value;
        document.all.txtMatchcode.value = sMatchcode;
    }
}





function mOnNavigateBack(sAddressID) 
{
    var sAdrID = '';
    sAdrID = sAddressID;

    if (!sAdrID) 
    {
        if (msPageParams[0] != '0') 
        {
            sAdrID = msPageParams[0];
        }
    }
    
    if (sAdrID)
    {
        var sURL = '../AddressDetails/AddressDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sAdrID;

        document.location = sURL;
    }
    else 
    {
        var sURL = '../AddressesMain/AddressesMain.aspx' +
                '?ClientID=' + msClientID;

        document.location = sURL;
    }
}

