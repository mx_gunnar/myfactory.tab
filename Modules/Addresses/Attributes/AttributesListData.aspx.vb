'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    AttributesListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AttributesListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Base.Attributes
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AttributesListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim sValues As String
            Dim oCol As AspListViewCol
            Dim lAddressID, lEntityID As Integer
            Dim bEditMode As Boolean

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AttributesListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("AttributeDesc", "Kennzeichen", "200")
            oCol = oListView.oCols.oAddCol("AttributeValue", "Wert", "*")
            oCol.bHtml = True

            oCol = oListView.oCols.oAddCol("AttributeType", "Type", "*")
            oCol.bHidden = True

            '---- Data ---------------------

            If oListView.sUserData.Split(";"c).Length < 3 Then
                Return False
            End If

            lAddressID = glCInt(oListView.sUserData.Split(";"c)(0))
            lEntityID = glCInt(oListView.sUserData.Split(";"c)(1))
            bEditMode = oListView.sUserData.Split(";"c)(2) = "-1"

            sOrder = "tdAttributeGroups.AttributeGroupTree, tdAttributeGroups.AttributeGroupDesc, tdAttributes.SortKey, tdAttributes.AttributeDesc"

            sFields = "AttributeGroup,AttributeName,AttributeDesc,AttributeType,FieldEntity"
            sTables = "tdAttributes LEFT JOIN tdAttributeGroups ON " & _
             "tdAttributes.AttributeGroup=tdAttributeGroups.AttributeGroupName "

            sClause = "EXISTS (SELECT * FROM tdAttributeTargets " & _
               " WHERE tdAttributeTargets.AttributeName=tdAttributes.AttributeName " & _
               "   AND tdAttributeTargets.AttributeEntity=" & lEntityID.ToString & ")"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
              sFields, sTables, sClause, , sOrder)

            Dim oRow As AspListViewRow
            Dim sOrderGroup As String = "-2"

            'oListView.oRows.SetRecordset(rs, True)
            Do While Not rs.EOF
                If sOrderGroup <> rs.Item("AttributeGroup").sValue Then
                    'display header (group name)
                    sOrderGroup = rs.Item("AttributeGroup").sValue
                    oRow = oListView.oRows.oAddRow("GR:" & rs.Item("AttributeGroup").sValue)
                    oRow.bGroupStyle = True

                    If sOrderGroup = "" Then
                        oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "keine Gruppenzuordnung")
                    Else
                        oRow.sValue(0) = Dictionary.gsTranslateUserLanguage(oClientInfo, _
                                            DataMethods.gsGetDBValue(oClientInfo, "AttributeGroupDesc", "tdAttributeGroups", _
                                                                     "AttributeGroupName=" & DataTools.gsStr2Sql(sOrderGroup)))
                    End If
                    oRow.lColType(0) = wfEnumAspListViewColTypes.wfColTypeText
                    oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeText
                    oRow.lColType(2) = wfEnumAspListViewColTypes.wfColTypeText
                    oRow.lColType(3) = wfEnumAspListViewColTypes.wfColTypeText
                End If
                oRow = oListView.oRows.oAddRow(rs.Item("AttributeName").sValue)

                oRow.sValue(0) = Dictionary.gsTranslateUserLanguage(oClientInfo, rs.Item("AttributeDesc").sValue)

                ' is col type entity reference? then get text!
                If rs("AttributeType").lValue <> 9 Then
                    oRow.sValue(1) = AttributeFields.gsGetAttributeValue(oClientInfo, lEntityID, lAddressID, rs.Item("AttributeName").sValue)
                Else
                    ' decode entity
                    Dim lEntityRecordId As Integer = glCInt(AttributeFields.gsGetAttributeValue(oClientInfo, lEntityID, lAddressID, rs.Item("AttributeName").sValue))
                    Dim sEntityName As String = DataMethods.gsGetDBValue(oClientInfo, "FieldEntity", "tdAttributes", "AttributeName=" & rs.Item("AttributeName").sValue.gs2Sql())

                    oRow.sValue(1) = Entities.gsGetEntityDesc(oClientInfo, sEntityName, lEntityRecordId, wfEnumEntityDescType.wfEntityDescTypeAuto)
                End If

                If Not bEditMode Then

                    If rs.Item("AttributeType").lValue = 4 Then
                        If oRow.sValue(1) = "-1" Then
                            oRow.sValue(1) = Dictionary.gsTranslate(oClientInfo, "Ja")
                        End If
                    End If

                    rs.MoveNext()
                    Continue Do

                End If

                Select Case rs.Item("AttributeType").lValue
                    Case 0
                        'string
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 1
                        'zahl
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 2
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 3
                        'date
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 4
                        'checkbox
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeCheckBox
                    Case 5
                        'combo
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeListCombo
                        sValues = msGetComboValues(oClientInfo, rs.Item("AttributeName").sValue)
                        oRow.sComboData(1) = sValues
                        oRow.sComboSrc(1) = sValues
                    Case 6
                        'website
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 7
                        'text
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeText
                    Case 8
                        'text with format
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeInput
                    Case 9
                        'entity reference
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeDefault

                    Case Else
                        oRow.lColType(1) = wfEnumAspListViewColTypes.wfColTypeDefault
                End Select

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


        Private Function msGetComboValues(ByVal oClientInfo As ClientInfo, ByVal sAttribute As String) As String

            Dim rs As Recordset
            Dim sList As String = ""

            rs = DataMethods.grsGetDBRecordset(oClientInfo, "ListValue", "tdAttributesLists", "AttributeName=" & DataTools.gsStr2Sql(sAttribute))
            Do While Not rs.EOF
                sList = sList & ";" & rs.Item("ListValue").sValue
                rs.MoveNext()
            Loop
            rs.Close()

            'sList = Mid(sList, 2)
            Return sList

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
