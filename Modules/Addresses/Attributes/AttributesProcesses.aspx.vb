'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AttributesProcesses.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for AttributesProcesses
'--------------------------------------------------------------------------------
' Created:      17.07.2012 12:15:28, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Xml
Imports myfactory.BusinessTasks.Base.Updates.wfIBaseUpdate
Imports myfactory.BusinessTasks.Base.Attributes

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AttributesProcesses
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim sAttributeName As String = Request.QueryString("AttributeName")
            Dim sNewValue As String = Request.QueryString("NewValue")
            Dim lRecrodID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lEntityID As Integer = glCInt(Request.QueryString("EntityID"))

            Select Case sCmd
                Case "ChangeValue"
                    Return msChangeValue(oClientInfo, lRecrodID, lEntityID, sAttributeName, sNewValue)
            End Select

            Return "ERR; Unknown command"

        End Function


        Private Function msChangeValue(ByVal oClientInfo As ClientInfo, ByVal lAddressID As Integer, ByVal lEntityID As Integer, ByVal sRecordID As String, ByVal sNewValue As String) As String

            If Not AttributeFields.gbSetAttributeValue(oClientInfo, lEntityID, lAddressID, sRecordID, sNewValue) Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim Speichern des Attributes")
            End If

            Return "OK; chaned"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
