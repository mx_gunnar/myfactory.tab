﻿// SalesContactsMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList("0");
}

function mRefreshList(bEditMode) 
{
    // [0] = recordID // [1] = entityID
    var sParams = msPageParams[0] + ";" + msPageParams[1] + ";" + bEditMode;

    gListViewSetUserData('lstAttributes', sParams)
    gListViewLoadPage('lstAttributes', -1);
}

function mOnEdit()
{
    document.getElementById("cmdEdit").style.visibility = "hidden";
    mRefreshList("-1");
}

function mOnListViewChkClick(sListView, sColID, sItemID)
{
    mSetNewValueToDB(sColID, sItemID, gbListViewGetChecked(sListView, sColID, sItemID));
}

function mOnListViewInputChange(sListView, sColID, sItemID, sNewValue)
{
    mSetNewValueToDB(sColID, sItemID, sNewValue);
}

function mOnListViewCboChange(sListView, sColID, sItemID, sNewValue)
{
    mSetNewValueToDB(sColID, sItemID, sNewValue);
}

function mSetNewValueToDB(sColID, sItemID, sNewValue)
{
    var sRes;
    var sURL = 'AttributesProcesses.aspx?Cmd=ChangeValue';
    sURL = sURL + "&AttributeName=" + sItemID;
    sURL = sURL + "&RecordID=" + msPageParams[0];
    sURL = sURL + "&EntityID=" + msPageParams[1];
    sURL = sURL + "&NewValue=" + sNewValue;
       
    sRes = gsCallServerMethod(sURL, sNewValue);

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
}


function mOnBack() 
{
    var sURL = '../AddressContactDetails/AddressContactDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&ContactID=' + msPageParams[0] +
                '&AddressID=' + msPageParams[2];

    document.location = sURL;
}