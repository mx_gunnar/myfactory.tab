'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    AttributesMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AttributesMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AttributesMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lContactPartnerID As Integer = glCInt(Request.QueryString("ContactPartnerID"))
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))
            Dim sType As String = Request.QueryString("Type")
            Dim lProductID, lSupportCaseID As Integer
            Dim lAttributeID As Integer = lRecordID

            ReDim Me.asPageParams(3)

            'Entity ID for Attributes
            If lContactPartnerID <> 0 Then
                Me.asPageParams(1) = "1050"
                lAttributeID = lContactPartnerID
                Me.asPageParams(2) = DataMethods.gsGetDBValue(oClientInfo, "AddressID", "tdContacts", "ContactID=" & lContactPartnerID)

            ElseIf lProjectID > 0 Then
                Me.asPageParams(1) = "6100"
                lAttributeID = lProjectID

            ElseIf sType = "Products" Then
                Me.asPageParams(1) = "2000"
                lProductID = lRecordID
                lAttributeID = lRecordID

            ElseIf sType = "SupportCases" Then
                Me.asPageParams(1) = "7600"
                lSupportCaseID = glCInt(Request.QueryString("CaseID"))
                lAttributeID = lSupportCaseID

            Else
                Me.asPageParams(1) = "1000"

            End If

            If lContactPartnerID <> 0 Then

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "ContactID, Matchcode", _
                                                   "tdContacts", "ContactID=" & lContactPartnerID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                Me.asPageParams(0) = lAttributeID.ToString

            ElseIf lProjectID <> 0 Then

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "ProjectID, (ProjectNumber  + ', ' + Matchcode) AS Matchcode", _
                                                   "tdProjects", "ProjectID=" & lProjectID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                Me.asPageParams(0) = lAttributeID.ToString

            ElseIf lProductID <> 0 Then

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "ProductID, Matchcode", _
                                                   "tdProducts", "ProductID=" & lProductID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                Me.asPageParams(0) = lAttributeID.ToString

            ElseIf lSupportCaseID <> 0 Then

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "CaseDesc as Matchcode", _
                                                   "tdSupportCases", "CaseID=" & lSupportCaseID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                Me.asPageParams(0) = lAttributeID.ToString


            ElseIf lAttributeID <> 0 Then

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "AddressID, Matchcode", _
                                                   "tdAddresses", "AddressID=" & lAttributeID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                Me.asPageParams(0) = lAttributeID.ToString

            End If

            Me.sOnLoad = "mOnLoad()"



            If lContactPartnerID <> 0 Then
                trNavi.Style.Add("display", "none")
                cmdBack.bHidden = False

            ElseIf lProjectID <> 0 Then

                '-------- write navi for Projects ---------------
                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProjectDetails", "../../../", "ProjectID=" & lProjectID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition,PartnerID,ModuleID", _
                                                       "tsTabModules", _
                                                       "ParentModule='ProjectDetails' AND ModuleName<>'Projects_Attributes'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)
                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "ProjectID=" & lAttributeID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString

            ElseIf lProductID <> 0 Then

                '-------- write navi for Products ---------------
                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProductDetails", "../../../", "ProductID=" & lProductID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition,PartnerID,ModuleID", _
                                                       "tsTabModules", _
                                                       "ParentModule='ProductDetails' AND ModuleName<>'Products_Attributes'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)
                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString

            ElseIf lSupportCaseID <> 0 Then

                '-------- write navi for SupportCases ---------------
                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "SupportCasesDetailsMain", "../../../", "CaseID=" & lSupportCaseID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition,PartnerID,ModuleID", _
                                                       "tsTabModules", _
                                                       "ParentModule='SupportCaseDetaisMain' AND ModuleName<>'Service_Attributes'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)
                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "CaseID=" & lSupportCaseID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString


            Else
                '-------- write navi for Address ---------------
                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAttributeID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition,PartnerID,ModuleID", _
                                                       "tsTabModules", _
                                                       "ParentModule='AddressDetails' AND ModuleName<>'Addresses_Attributes'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)
                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAttributeID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString
            End If



        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
