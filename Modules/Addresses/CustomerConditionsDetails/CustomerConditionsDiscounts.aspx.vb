'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerConditionsDiscounts.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for CustomerConditionsDiscounts
'--------------------------------------------------------------------------------
' Created:      22.04.2013 14:55:59, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class CustomerConditionsDiscounts
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String


            Dim asParams As String() = oListView.sUserData.Split("_"c)

            If asParams.Length <> 2 Then
                Return False
            End If

            Dim lCustomerID As Integer = glCInt(asParams(0))
            Dim lProductID As Integer = glCInt(asParams(1))


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "CustomerConditionsDiscounts.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 15
            oListView.bAutoHideNavigation = False
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("Quantity", "ab Menge", "*", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("Discount", "Rabatt in % ", "150px", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"

            Else
                ' default sort-order
                sOrder = "Quantity"
            End If


            sClause = sClause.gsClauseAnd("Quantity > 0 ")
            sClause = sClause.gsClauseAnd("ProductId = " & lProductID)
            sClause = sClause.gsClauseAnd("CustomerID = " & lCustomerID)

            sFields = "DiscountID AS RowID,Quantity,Discount"
            sTables = "tdProductCustomerDiscounts"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
