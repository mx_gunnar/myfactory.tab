'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerConditionsDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for CustomerConditionsDetailsMain
'--------------------------------------------------------------------------------
' Created:      22.04.2013 14:55:10, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class CustomerConditionsDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Request.QueryString("CustomerProductID")
            Me.asPageParams(1) = Request.QueryString("AddressID")

            Dim lProductID As Integer = glCInt(Request.QueryString("CustomerProductID").Split("_"c)(1))
            Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("VALUE") = DataMethods.gsGetDBValue(oClientInfo, "Matchcode", "tdProducts", "ProductID=" & lProductID)

            Me.sOnLoad = "mOnLoad();"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
