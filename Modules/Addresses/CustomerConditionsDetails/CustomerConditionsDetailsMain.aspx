<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.CustomerConditionsDetailsMain" CodeFile="CustomerConditionsDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="99%" height="97%">
        <tr height="30px">
            <td width="600px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/CustomerConditionsDetails/dlgCustomerConditionsDetails.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr height="*" valign="top">
            <td colspan="2">
                <table width="100%" height="100%">
                    <tr>
                        <td width="50%">
                            <myfactory:wfListView runat="server" ID="lstPrices" sListViewDataPage="CustomerConditionsPrices.aspx">
                            </myfactory:wfListView>
                        </td>
                        <td width="50%">
                            <myfactory:wfListView runat="server" ID="lstDiscounts" sListViewDataPage="CustomerConditionsDiscounts.aspx">
                            </myfactory:wfListView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="50px" valign="bottom">
            <td align="right" colspan="2">
                <myfactory:wfButton ID="cmdBack" runat="server" sText="Zur�ck" sOnClick="mNavigateBack();">
                </myfactory:wfButton>
            </td>
        </tr>
    </table>
</body>
</html>
