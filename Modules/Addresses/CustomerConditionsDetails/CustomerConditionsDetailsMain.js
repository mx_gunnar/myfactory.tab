﻿

function mOnLoad() 
{
    
    mRefreshPrices();
    mRefreshDiscounts();
}


function mRefreshPrices() 
{
    gListViewSetUserData('lstPrices', msPageParams[0])
    gListViewLoadPage('lstPrices', -1);
}

function mRefreshDiscounts() 
{
    gListViewSetUserData('lstDiscounts', msPageParams[0])
    gListViewLoadPage('lstDiscounts', -1);

}


function mNavigateBack() 
{
    var sURL = '../CustomerConditions/CustomerConditionsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + msPageParams[1];

    document.location = sURL;
}