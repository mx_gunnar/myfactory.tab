<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.AddressesMain" codefile="AddressesMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="100%" cellSpacing=2 cellPadding=2 width="98%">
<tr height="30px">
	<td width="700px">
		<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/Addressesmain/dlgAddressesmain.xml"></myfactory:wfXmlDialog>
	</td>
	<td width="*">&nbsp;</td>
</tr>
<tr height="500px">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="AddressesListData.aspx"></myfactory:wfListView>
	</td>
</tr>
<tr height="*"><td>&nbsp;</td></tr>
</table>

</body>
</HTML>
 