'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressesMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AddressesMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		17.09.20123, ABuhleier, PostalCode Filter implemented
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressesMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_Addresses_Filter", False)
            Me.dlgMain.oDialog.oField("Search").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Addresses_Search", False)
            Me.dlgMain.oDialog.oField("PostalCodeFrom").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Addresses_PostalCodeFrom", False)
            Me.dlgMain.oDialog.oField("PostalCodeTo").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Addresses_PostalCodeTo", False)

            Me.dlgMain.oDialog.oField("Country").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Addresses_Country", False)
            Me.dlgMain.oDialog.oField("CustomerGroup").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Addresses_CustomerGroup", False)

            Me.sOnLoad = "mOnLoad()"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
