﻿// AddressesMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    sData = document.all.cboFilter.value +
                ';' + document.all.cboCustomerGroup.value +
                ';' + document.all.cboCountry.value +
                ';' + document.all.txtSearch.value +
                ';' + document.all.txtPostalCodeFrom.value +
                ';' + document.all.txtPostalCodeTo.value;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();

}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL = '../AddressDetails/AddressDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sItemID;

    document.location = sURL;
}

function mOnListViewClick(sListView, sItemID)
{
    var sURL = '../AddressDetails/AddressDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sItemID;

    document.location = sURL;
}

