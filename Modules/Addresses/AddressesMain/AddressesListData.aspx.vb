'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressesListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AddressesListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		17.09.20123, ABuhleier, PostalCode Filter implemented
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressesListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AddressesListData.aspx"
            oListView.lPageSize = 11
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True)

            oListView.oCols.oAddCol("CustomerNumber", "Kunde", "65", , , True)

            oListView.oCols.oAddCol("PostalCode", "PLZ", "40", , , True)
            oListView.oCols.oAddCol("City", "Ort", "140", , , True)
            oListView.oCols.oAddCol("Street", "Stra�e", "140", , , True)
            oListView.oCols.oAddCol("PhoneNr", "Telefon", "100")

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            Dim asData As String()
            Dim lFilter As Integer
            Dim sSearch As String = ""
            Dim sPostalCodeFrom As String = ""
            Dim sPostalCodeTo As String = ""
            Dim sCountry As String = ""
            Dim sCustomerGroup As String = ""

            asData = Split(oListView.sUserData & ";;;;;;", ";")
            If UBound(asData) > 2 Then
                lFilter = glCInt(asData(0))
                sCustomerGroup = asData(1)
                sCountry = asData(2)
                sSearch = asData(3)
                sPostalCodeFrom = asData(4)
                sPostalCodeTo = asData(5)
            End If

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_Filter", lFilter, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_Search", sSearch, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_PostalCodeFrom", sPostalCodeFrom, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_PostalCodeTo", sPostalCodeTo, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_Country", sCountry, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Addresses_CustomerGroup", sCustomerGroup, False)

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "Matchcode"
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            sClause = "tdAddresses.InActive=0"
            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "tdAddresses.AddressID AS RowID, Matchcode " & _
                ", (SELECT MAX(CustomerNumber) from tdCustomers WHERE tdCustomers.AddressID=tdAddresses.AddressID AND tdCustomers.InActive=0) AS CustomerNumber " & _
                ", PostalCode, City, Street, PhoneNr"

            sTables = "tdAddresses "

            '---- clause ---------
            If lFilter = 2 Then
                sClause = DataTools.gsClauseAnd(sClause, "IsFavorite=-1")
            ElseIf lFilter = 1 Then
                sClause = DataTools.gsClauseAnd(sClause, " EXISTS(SELECT * FROM tdAddressUserSettings " & _
                                            "WHERE UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
                                            " AND IsMyFavorit=-1 " & _
                                            " AND tdAddressUserSettings.AddressID=tdAddresses.AddressID) ")


            End If

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "(Matchcode like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name1 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name2 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR City like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR EXISTS(SELECT * FROM tdCustomers WHERE tdCustomers.AddressID=tdAddresses.AddressID AND tdCustomers.InActive=0 AND tdCustomers.CustomerNumber like " & (sSearch & "%").gs2Sql & ")" & _
                                                            " OR EXISTS(SELECT * FROM tdContacts WHERE tdAddresses.AddressID=tdContacts.AddressID AND tdContacts.Matchcode like " & DataTools.gsStr2Sql(sSearch & "%") & ")" & _
                                                            ")")
            End If

            If sPostalCodeFrom <> "" Then
                sClause = sClause.gsClauseAnd("PostalCode >=" & sPostalCodeFrom.gs2Sql)
            End If

            If sPostalCodeTo <> "" Then
                sClause = sClause.gsClauseAnd("PostalCode <=" & (sPostalCodeTo & "99999").gs2Sql)
            End If


            If sCountry <> "" Then sClause = DataTools.gsClauseAnd(sClause, "tdAddresses.Country=" & DataTools.gsStr2Sql(sCountry))
            If sCustomerGroup <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, "EXISTS(SELECT CustomerID FROM tdCustomers " & _
                                                            " WHERE tdCustomers.AddressID=tdAddresses.AddressID" & _
                                                            "   AND tdCustomers.CustomerGroup=" & DataTools.gsStr2Sql(sCustomerGroup) & ")")
            End If

            '---- permissions ----
            Dim sSubClause As String

            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Addresses")
            If sSubClause <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
