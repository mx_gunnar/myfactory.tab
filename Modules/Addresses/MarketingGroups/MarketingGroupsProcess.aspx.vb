'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    MarketingGroupsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for MarketingGroupsProcess
'--------------------------------------------------------------------------------
' Created:      17.07.2013 12:26:23, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Main.User

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class MarketingGroupsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sMethod, sMarketingGroup As String
            Dim lAddressID As Integer
            Dim lValue As Integer
            Dim sOpenGroups, sQry, sClause As String
            Dim sChilds, asChilds() As String
            Dim sParents, asParents() As String
            Dim l As Integer

            sMethod = Request.QueryString("Method")

            sMarketingGroup = Request.QueryString("MarketingGroup")
            lAddressID = glCInt(Request.QueryString("AddressID"))

            If sMethod = "SetValue" Then
                lValue = glCInt(gbCBool(Request.QueryString("Value")))

                If lValue = 0 Then
                    sClause = "AddressID=" & lAddressID & " AND MarketingGroup=" & DataTools.gsStr2Sql(sMarketingGroup)
                    DataMethods2.glDBDelete(oClientInfo, "tdMarketingGroupMembers", sClause)

                    'erase all child entries
                    sChilds = msGetChilds(oClientInfo, sMarketingGroup, True)
                    If sChilds <> "" Then
                        DataMethods2.glDBDelete(oClientInfo, "tdMarketingGroupMembers", "AddressID=" & lAddressID & " AND MarketingGroup IN(" & sChilds & ")")
                    End If
                Else

                    '------ set entry -------------
                    sClause = "AddressID=" & lAddressID & " AND MarketingGroup=" & DataTools.gsStr2Sql(sMarketingGroup)

                    If Not DataMethods.gbDBExists(oClientInfo, "AddressID", "tdMarketingGroupMembers", sClause) Then
                        sQry = "INSERT INTO tdMarketingGroupMembers(AddressID,MarketingGroup) " & _
                                    " VALUES(" & _
                                        lAddressID & "," & DataTools.gsStr2Sql(sMarketingGroup) & ")"

                        DataMethods2.glDBExecute(oClientInfo, sQry)
                    End If

                    'set all child entries
                    sChilds = msGetChilds(oClientInfo, sMarketingGroup, False)
                    asChilds = Split(sChilds, ",")

                    If UBound(asChilds) > 0 Then 'group has childs - open the tree
                        sOpenGroups = gsGetUserPreference(oClientInfo, "ShowMarketingGroups", False)

                        If InStr(, ";" & sOpenGroups & ";", sMarketingGroup) = 0 Then
                            sOpenGroups = sOpenGroups & sMarketingGroup & ";"
                            gbSetUserPreference(oClientInfo, "ShowMarketingGroups", sOpenGroups, False)
                        End If
                    End If

                    For l = 0 To UBound(asChilds)
                        sMarketingGroup = asChilds(l)
                        If sMarketingGroup <> "" Then
                            sClause = "AddressID=" & lAddressID & " AND MarketingGroup=" & DataTools.gsStr2Sql(sMarketingGroup)

                            If Not DataMethods.gbDBExists(oClientInfo, "AddressID", "tdMarketingGroupMembers", sClause) Then
                                sQry = "INSERT INTO tdMarketingGroupMembers(AddressID,MarketingGroup) " & _
                                            " VALUES(" & _
                                                lAddressID & "," & DataTools.gsStr2Sql(sMarketingGroup) & ")"

                                DataMethods2.glDBExecute(oClientInfo, sQry)
                            End If
                        End If
                    Next

                    'set all parent entries
                    sParents = msGetParents(oClientInfo, sMarketingGroup)
                    asParents = Split(sParents, ",")

                    For l = 0 To UBound(asParents)
                        sMarketingGroup = asParents(l)
                        If sMarketingGroup <> "" Then
                            sClause = "AddressID=" & lAddressID & " AND MarketingGroup=" & DataTools.gsStr2Sql(sMarketingGroup)

                            If Not DataMethods.gbDBExists(oClientInfo, "AddressID", "tdMarketingGroupMembers", sClause) Then
                                sQry = "INSERT INTO tdMarketingGroupMembers(AddressID,MarketingGroup) " & _
                                            " VALUES(" & _
                                                lAddressID & "," & DataTools.gsStr2Sql(sMarketingGroup) & ")"

                                DataMethods2.glDBExecute(oClientInfo, sQry)
                            End If
                        End If

                    Next

                End If


            ElseIf sMethod = "Toggle" Then

                sOpenGroups = gsGetUserPreference(oClientInfo, "ShowMarketingGroups", False)
                If InStr(, ";" & sOpenGroups & ";", sMarketingGroup) <> 0 Then
                    sOpenGroups = Replace(";" & sOpenGroups & ";", ";" & sMarketingGroup & ";", ";")
                Else
                    sOpenGroups = sOpenGroups & sMarketingGroup & ";"
                End If
                gbSetUserPreference(oClientInfo, "ShowMarketingGroups", sOpenGroups, False)

            Else
                Return "Unknown Method"
            End If

            Return "OK"

        End Function

        ''' <summary>
        ''' Get Child Groups
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="sMarketingGroup"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msGetChilds(ByVal oClientInfo As ClientInfo, ByVal sMarketingGroup As String, _
                                     ByVal bSqlFormat As Boolean) As String

            Dim sTree As String
            Dim rs As Recordset
            Dim bNext As Boolean
            Dim osStr As New FastString

            sTree = DataMethods.gsGetDBValue(oClientInfo, "MarketingGroupTree", _
                                             "tdMarketingGroups", _
                                             "MarketingGroupName=" & DataTools.gsStr2Sql(sMarketingGroup))
            If sTree <> "" Then
                sTree &= "%"

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "MarketingGroupName", _
                                             "tdMarketingGroups", _
                                             "MarketingGroupTree Like " & DataTools.gsStr2Sql(sTree))
                Do Until rs.EOF
                    If bNext Then osStr.bAppend(",")

                    If bSqlFormat Then
                        osStr.bAppend(DataTools.gsStr2Sql(rs("MarketingGroupName").sValue))
                    Else
                        osStr.bAppend(rs("MarketingGroupName").sValue)
                    End If

                    bNext = True
                    rs.MoveNext()
                Loop
            End If

            Return osStr.sString

        End Function

        ''' <summary>
        ''' get parent marketing groups
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="sMarketingGroup"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msGetParents(ByVal oClientInfo As ClientInfo, ByVal sMarketingGroup As String) As String

            Dim sTree As String
            Dim asTree() As String
            Dim l As Integer
            Dim osStr As New FastString
            Dim bNext As Boolean
            Dim sGroup As String
            Dim sGroupTree As String = ""

            sTree = DataMethods.gsGetDBValue(oClientInfo, "MarketingGroupTree", _
                                 "tdMarketingGroups", _
                                 "MarketingGroupName=" & DataTools.gsStr2Sql(sMarketingGroup))

            If sTree = "" Then Return ""

            asTree = Split(sTree, ".")
            For l = 0 To UBound(asTree)

                sGroupTree = sGroupTree & asTree(l) & "."
                sGroup = DataMethods.gsGetDBValue(oClientInfo, "MarketingGroupName", _
                                                "tdMarketingGroups", _
                                                "MarketingGroupTree=" & DataTools.gsStr2Sql(sGroupTree))

                If sGroup <> "" Then
                    If bNext Then osStr.bAppend(",")
                    osStr.bAppend(sGroup)

                    bNext = True
                End If

            Next

            Return osStr.sString

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
