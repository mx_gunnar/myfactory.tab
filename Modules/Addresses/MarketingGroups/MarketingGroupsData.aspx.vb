'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    MarketingGroupsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for MarketingGroupsData
'--------------------------------------------------------------------------------
' Created:      17.07.2013 10:10:02, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================


Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Main.User
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class MarketingGroupsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim lAddressID, l As Integer

            Dim bAdd As Boolean
            Dim sTree As String = ""
            Dim sOpenGroups As String = ""
            Dim sPrevHidden As String = ""
            Dim sPre As String = ""

            lAddressID = glCInt(oListView.sUserData)

            '---- ListView Properties ------
            oListView.bPageNavigation = False
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.sListViewDataPage = "MarketingGroupsData.aspx"

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("cmdToggle", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.bNoPrint = True

            oCol = oListView.oCols.oAddCol("chkUse", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeCheckBox

            'oCol = oListView.oCols.oAddCol("cmdEntity", "", "20")
            'oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            'oCol.sContextMenuName = "MarketingGroups"
            'oCol.bEntityContextMenu = True
            'oCol.bNoPrint = True

            oListView.oCols.oAddCol("MarketingGroupDesc", "Klassifizierung", "*")


            '---- Data ---------------------
            sOrder = "IsNull(MarketingGroupTree,'zzz'),MarketingGroupDesc"

            sClause = ""

            If lAddressID = 0 Then Return True

            sOpenGroups = gsGetUserPreference(oClientInfo, "ShowMarketingGroups", False)

            rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                "*", "tdMarketingGroups", sClause, , sOrder)

            Do Until rs.EOF
                bAdd = True
                sTree = rs("MarketingGroupTree").sValue

                If sPrevHidden <> "" Then
                    If Left(sTree, Len(sPrevHidden)) = sPrevHidden Then
                        bAdd = False
                    Else
                        sPrevHidden = ""
                    End If
                End If

                If bAdd Then
                    oRow = oListView.oRows.oAddRow(rs("MarketingGroupName").sValue)

                    If sTree = "" Then
                        oRow.lColType(0) = wfEnumAspListViewColTypes.wfColTypeText
                        sPre = ""
                    Else
                        sPre = ""
                        For l = 1 To (Len(sTree) \ 5) - 1
                            sPre = sPre & " --- "
                        Next

                        If DataMethods.gbDBExists(oClientInfo, "MarketingGroupName", "tdMarketingGroups", _
                                            "MarketingGroupTree like " & (sTree & "%.%").gs2Sql) _
                        Then
                            If InStr(, ";" & sOpenGroups & ";", rs("MarketingGroupName").sValue) <> 0 Then
                                oRow.sValue(0) = "-"
                            Else
                                oRow.sValue(0) = "+"
                                sPrevHidden = sTree
                            End If
                        Else
                            oRow.lColType(0) = wfEnumAspListViewColTypes.wfColTypeText
                        End If

                    End If

                    If DataMethods.gbDBExists(oClientInfo, "AddressID", "tdMarketingGroupMembers", _
                                        "AddressID=" & lAddressID & _
                                        " AND MarketingGroup=" & DataTools.gsStr2Sql(rs("MarketingGroupName").sValue)) _
                    Then
                        oRow.Value(1) = -1
                    End If


                    oRow.sValue(2) = sPre & Dictionary.gsTranslateUserLanguage(oClientInfo, rs("MarketingGroupDesc").sValue)

                    'oRow.sValue(2) = rs("MarketingGroupName").sValue

                End If

                rs.MoveNext()
            Loop
            rs.Close()

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
