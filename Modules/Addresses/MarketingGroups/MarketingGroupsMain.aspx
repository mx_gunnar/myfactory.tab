<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.MarketingGroupsMain" CodeFile="MarketingGroupsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" style="table-layout: fixed;"
        width="98%" height="97%">
        <tr height="20px">
            <td >
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/MarketingGroups/dlgMarketingGroups.xml"
                    sDataSource="dsoData">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr height="*" valign="top">
            <td>
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="MarketingGroupsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td align="right">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
