﻿

function mOnLoad() 
{
    gListViewLoadPage('lstMain', -1);
}



function mOnListViewBtnClick(sListView, sColID, sItemID) {
    var sURL = "MarketingGroupsProcess.aspx" +
					"?Method=Toggle" +
					"&AddresssID=" + gsListViewGetUserData(sListView) +
					"&MarketingGroup=" + gsEncodeStringURL(sItemID);
    var sRes = gsCallServerMethod(sURL, "");
    if (sRes != 'OK') {
        alert(sRes);
    }
    else {
        gListViewLoadPage('lstMain', -1);
    }
}


function mOnListViewChkClick(sListView, sColID, sItemID) {
    var sURL = "MarketingGroupsProcess.aspx" +
					"?Method=SetValue" +
					"&AddressID=" + gsListViewGetUserData(sListView) +
					"&MarketingGroup=" + gsEncodeStringURL(sItemID) +
					"&Value=" + gbListViewGetChecked(sListView, sColID, sItemID);
    var sRes = gsCallServerMethod(sURL, "");
    if (sRes != 'OK') {
        alert(sRes);
    }
    else {
        var lPos = glListViewGetScrollTop(sListView);
        mRefreshList();
        gListViewSetScrollTop(sListView, lPos)
    }
}