<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.SalesContactDetailsMain" codefile="SalesContactDetailsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable cellSpacing=2 cellPadding=2 width="100%" height="97%">
<tr height="*" valign="top">
	<td>
		<form name="frmMain">
			<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/SalesContactDetails/dlgSalesContactDetails.xml" sDataSource="dsoData"></myfactory:wfXmlDialog>
		</form>
	</td>
</tr>
<tr height="200px" valign="bottom">
	<td align="right">
		<myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px"></myfactory:wfLabel>
	</td>
</tr>
</table>

</body>
</HTML>
