'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesContactDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesContactDetailsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class SalesContactDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lAddressID As Integer

            If lRecordID <> 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "a.AddressID, a.Matchcode, sc.ContactDate, sc.ContactDesc" & _
                                                   ", sc.ContactDescText, scr.ResultDesc, sct1.TopicDesc, sct2.TypeDesc" & _
                                                   ", c.Matchcode as CMatchcode, a2.Matchcode as RepMatchcode, du.UserName", _
                                                   "tdSalesContacts sc " & _
                                                        " INNER JOIN tdAddresses a ON sc.AddressID=a.AddressID " & _
                                                        " LEFT JOIN tdRepresentatives r ON sc.RepresentativeID=r.RepresentativeID" & _
                                                        " LEFT JOIN tdAddresses a2 ON r.AddressID=a2.AddressID" & _
                                                        " LEFT JOIN tdContacts c ON sc.ContactPartner=c.ContactID" & _
                                                        " LEFT JOIN tsDatabaseUsers du ON sc.UserName=du.UserInitials" & _
                                                        " LEFT JOIN tdSalesContactResults scr ON sc.ContactResult=scr.ResultID" & _
                                                        " LEFT JOIN tdSalesContactTopics sct1 ON sc.ContactTopic=sct1.TopicID" & _
                                                        " LEFT JOIN tdSalesContactTypes sct2 ON sc.ContactType=sct2.TypeID", _
                                                        "sc.ContactID=" & lRecordID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                    lAddressID = rs("AddressID").lValue
                End If
            End If

            '-------- write navi ---------------
            Me.lblNaviSub.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "Addresses_SalesContacts", "../../../", "RecordID=" & lAddressID)

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
