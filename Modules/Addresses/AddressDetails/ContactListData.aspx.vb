'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ContactListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ContactListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class ContactListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ContactListData.aspx"
            oListView.lPageSize = 3
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("AddrTitle", "Anrede", "50")
            oListView.oCols.oAddCol("LastName", "Nachname", "80")
            oListView.oCols.oAddCol("FirstName", "Vorname", "80")

            oListView.oCols.oAddCol("ContactPositionDesc", "Position", "100")
            oListView.oCols.oAddCol("ContactDepartmentDesc", "Abteilung", "100")

            oListView.oCols.oAddCol("PhoneNr", "Telefon", "120")
            oListView.oCols.oAddCol("MobilePhone", "Mobil", "120")
            oCol = oListView.oCols.oAddCol("EMail", "E-Mail", "*")


            oCol = oListView.oCols.oAddCol("Mail", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonEMail)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("Det", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("Del", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sToolTip = "Inaktiv setzen"
            oCol.sWidth = "50"


            '---- Data ---------------------
            Dim lAddressID As Integer

            lAddressID = glCInt(oListView.sUserData)


            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "MainContact, LastName, Firstname"
            End If

            If oListView.sOrderCol = "LastName" Then sOrder &= ", FirstName"

            sFields = "ContactID AS RowID, AddrTitle, LastName, FirstName" & _
                        " ,ContactPositionDesc, ContactDepartmentDesc" & _
                        " ,PhoneNr, MobilePhone, EMail, '' as cmdMail, '...' AS Det, 'x' AS Del , MainContact"
            sTables = "tdContacts c " & _
                        " LEFT JOIN tdContactPositions cp ON c.Position=cp.ContactPositionName " & _
                        " LEFT JOIN tdContactDepartments cd ON c.Department=cd.ContactDepartmentName "

            '---- clause ---------
            sClause = "AddressID=" & lAddressID & " AND InActive=0"


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            Dim oRow As AspListViewRow
            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                If rs("MainContact").bValue Then

                    oRow.bRowBold = True
                End If

                rs.MoveNext()
            Loop
            rs.Close()

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
