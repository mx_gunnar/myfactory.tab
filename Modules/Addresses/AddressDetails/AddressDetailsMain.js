﻿// AddressDetailsMain.js (c) myfactory 2010


function mOnLoad()
{
    if (msPageParams[1] == '-1') 
    {
        document.getElementById("tdEditAddress").style.display = "none";
    }
    
    mRefreshContactList();


}

function mRefreshContactList()
{
    gListViewLoadPage('lstContacts', -1);
}

function mOnListViewClick(sListView, sItemID)
{
    mEditContact(sItemID);
}

function cmdAddressEdit_OnClick() 
{

    var sURL = '../AddressNew/AddressNewMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[0];

    document.location = sURL;
}

function cmdOpenHomepage_OnClick()
{
    gOpenHomepageInBrowser(document.all.txtHomepage.value);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == "Del") 
    {
        mDeleteContact(sItemID);
    }
    else if (sColID == "Det") 
    {
        mEditContact(sItemID);
    }
    else if (sColID == "Mail") 
    {
        mNavigateToNewMail(sItemID);
    }
}


function mNavigateToNewMail(SalesContactID) 
{
    var sURL = '../../Email/EMailNew/EMailNewMain.aspx' +
                '?ClientID=' + msClientID + "&MailID=0&ContactID=" + SalesContactID + "&AddressID=" + msPageParams[0];

    document.location = sURL;
}


function mDeleteContact(sContactID)
{
    var sQuestion = msTerms[0];
    var sQuestionResult = window.confirm(sQuestion);

    if (sQuestionResult == false)
        return;

    var sURL = '../AddressContactDetails/AddressContactProcess.aspx?Cmd=delete&ContactID=' + sContactID;
    var sRes = gsCallServerMethod(sURL);

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mRefreshContactList();
}

function mEditContact(sContactID)
{
    var sURL = '../AddressContactDetails/AddressContactDetailsMain.aspx' +
            '?ClientID=' + msClientID +
            '&AddressID=' + msPageParams[0] +
            '&ContactID=' + sContactID;

    document.location = sURL;
}

function mNewContact()
{
    var sURL = '../AddressContactDetails/AddressContactDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[0] +
                '&ContactID=-1';

    document.location = sURL;
}

function mNewAppointment() 
{
    var sURL = '../../Appointments/AppointmentDetails/AppointmentDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[0];
    
    document.location = sURL;
}

function mOnSaveAsPdf() {
    if (msPageParams[2] != '')
        window.open(msPageParams[2] + '&ClientID=' + msClientID, '', 'menubar=no,fullscreen=no,scrollbars=yes,location=no,toolbar=no,resizable=yes,status=yes,left=50,top=50,width=600,height=400');
}

function mNewTask() {
    
    var sURL = '../../Tasks/TaskDetails/TasksDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[0];

    document.location = sURL;
}