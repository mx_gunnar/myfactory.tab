'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for TasksDetailsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))

            If lRecordID <> 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "AddressID, Matchcode, Name1, Name2, Street, Street2, Country, PostalCode, City, PhoneNr, MobilePhone, Homepage, EMail, FaxNr, (SELECT MAX(CustomerNumber) FROM tdCustomers C WHERE C.AddressID=tdAddresses.AddressID) AS CustomerNr", _
                                                   "tdAddresses", "AddressID=" & lRecordID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If

                Me.lstContacts.sUserData = lRecordID.ToString
            End If

            Me.sOnLoad = "mOnLoad()"

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lRecordID)

            Dim sInfoPage As String = msGetInfoPage(oClientInfo, lRecordID)

            If String.IsNullOrEmpty(sInfoPage) Then
                Me.cmdCreatePDF.bHidden = True
            Else
                Me.asPageParams(2) = sInfoPage
            End If


            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='AddressDetails'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString
            osHTML.bAppend("</nobr>")



            ReDim Me.asTerms(1)
            Me.asTerms(0) = "M�chten Sie diesen Ansprechpartner wirklich inaktiv setzen?"

            Dim bMailAllowed As Boolean = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_AddressCreateTask", _
                                                           "", "Neue Aufgabe aus Adresse heraus anlegen")

            If Not bMailAllowed Then Me.cmdNewTask.bHidden = True

            If Not Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_AddressEdit", "", "Adressen bearbeiten") Then
                Me.asPageParams(1) = "-1"
            End If


        End Sub

        ''' <summary>
        ''' get customer (default) or supplier info page  ----
        ''' </summary>
        ''' <param name="oClientInfo">ClientInfo</param>
        ''' <param name="lAddressID">AddressID</param>
        ''' <returns>customer (default) or supplier info page string</returns>
        ''' <remarks></remarks>
        Private Function msGetInfoPage(ByVal oClientInfo As ClientInfo, ByVal lAddressID As Integer) As String

            Dim sClause As String = "AddressID=" & lAddressID
            sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers"))

            Dim lId As Integer = DataMethods.glGetDBValue(oClientInfo, "CustomerID", "tdCustomers", sClause)
            Dim sRoot As String = oClientInfo.sWebPageRoot

            If lId > 0 Then
                Return sRoot & "/ie50/sales/info/CustomerInfo/CustomerInfoProcess.aspx?Method=SaveAsPdf&RecordID=" & lId
            Else
                sClause = "AddressID=" & lAddressID
                sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Suppliers"))
                lId = DataMethods.glGetDBValue(oClientInfo, "SupplierID", "tdSuppliers", sClause)
                If lId > 0 Then
                    Return sRoot & "/ie50/Purchase/info/Supplierinfo/SupplierInfoProcess.aspx?Method=SaveAsPdf&RecordID=" & lId
                End If
            End If

            Return String.Empty

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
