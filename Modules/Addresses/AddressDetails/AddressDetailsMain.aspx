<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.AddressDetailsMain" CodeFile="AddressDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="99%" height="97%" style="table-layout:fixed">
        <tr height="200px">
            <td>
                <form name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/AddressDetails/dlgAddressDetails.xml"
                    sDataSource="dsoData">
                </myfactory:wfXmlDialog>
                </form>
            </td>
        </tr>
         
        <tr height="150px">
            <td >
                <myfactory:wfListView runat="server" ID="lstContacts" sListViewDataPage="ContactListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="50px" valign="top">
            <td>
                <table width="100%">
                    <tr>

                        <td align="left" width="120px" id="tdEditAddress">
                            <myfactory:wfButton runat="server" ID="cmdEditAddress" sStyle="width: 150px;" sOnClick="cmdAddressEdit_OnClick();" sText="Adresse bearbeiten">  
                            </myfactory:wfButton>
                        </td>


                        <td align="left" width="120px">
                            <myfactory:wfButton runat="server" ID="cmdNewTask" sStyle="width: 120px;" sOnClick="mNewTask()" sText="Neue Aufgabe">  
                            </myfactory:wfButton>
                        </td>
                        <td align="left" width="120px">
                            <myfactory:wfButton runat="server" ID="cmdNewAppointment" sStyle="width: 120px;" sOnClick="mNewAppointment()" sText="Neuer Termin">  
                            </myfactory:wfButton>
                        </td>
                        
                        <td align="left" width="120px">
                            <myfactory:wfButton runat="server" ID="cmdCreatePDF" sStyle="width: 120px;" sOnClick="mOnSaveAsPdf()" sText="Quickreport">
                            </myfactory:wfButton>
                        </td>

                        <td align="right">
                            <myfactory:wfButton runat="server" ID="cmdNew" sOnClick="mNewContact()" sText=" Neu ">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi" >
	        <td align="right">
		        <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete"></myfactory:wfLabel>
	        </td>
        </tr>
    </table>
</body>
</html>
