﻿

function mOnLoad() 
{
    if (msPageParams[0] == '0') 
    {
        alert(msTerms[0]);
    }
    else 
    {
        mRefreshList();
    }
}


function mRefreshList() 
{

    var sParams = msPageParams[0]; // CustomerID
    sParams = sParams + ';' + document.all.cboProductGroup.value;
    sParams = sParams + ';' + document.all.txtSearch.value;
    
     gListViewSetUserData('lstMain', sParams)
    gListViewLoadPage('lstMain', -1);

}


function mOnListViewClick(sListView, sItemID) 
{
    var bHasChilds = gsListViewGetItemUserData(sListView, sItemID);
    if (bHasChilds == "-1") 
    {
        mNavigateToDetails(msPageParams[0] + "_" + sItemID);
    }
}


function mOnListViewBtnClick(sListView, sColID, sItemID) 
{
    mNavigateToDetails(msPageParams[0] + "_" + sItemID);
}


function mNavigateToDetails(sCustomerProductID) 
{
    var sURL = '../CustomerConditionsDetails/CustomerConditionsDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[1] + 
                '&CustomerProductID=' + sCustomerProductID;

    document.location = sURL;
}


function mOnSetDirty() 
{
    if (event.type == "change")
        mRefreshList();
 }