'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerConditionsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for CustomerConditionsMain
'--------------------------------------------------------------------------------
' Created:      22.04.2013 11:56:34, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class CustomerConditionsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddressID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lCustomerID As Integer = 0

            If lAddressID > 0 Then
                Dim rsCustomer As Recordset
                Dim sClause As String = "Inactive=0 AND AddressID=" & lAddressID

                sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers"))

                rsCustomer = DataMethods.grsGetDBRecordset(oClientInfo, "CustomerID", "tdCustomers", sClause, , "CustomerNumber desc")
                If Not rsCustomer.EOF Then
                    lCustomerID = rsCustomer(0).lValue
                End If
                rsCustomer.Close()
            End If

            Me.dlgMain.oDialog.oField("Search").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Adr_CustomerConditions_ProductSearch", False)
            Me.dlgMain.oDialog.oField("ProductGroup").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Adr_CustomerConditions_ProductGroup", False)

            ReDim Me.asTerms(1)
            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "Diese Adresse ist kein Kunde")

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lCustomerID)
            Me.asPageParams(1) = Str(lAddressID)

            Me.sOnLoad = "mOnLoad();"

            Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("VALUE") = DataMethods.gsGetDBValue(oClientInfo, "Matchcode", "tdAddresses", "AddressID=" & lAddressID)

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAddressID))

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition,PartnerID,ModuleID", _
                                                   "tsTabModules", _
                                                   "ParentModule='AddressDetails' AND ModuleName<>'Addresses_CustomerConditions'", _
                                                   , "ShowIndex", _
                                                   myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAddressID))

                frs.MoveNext()
            Loop
            osHTML.bAppend("</nobr>")
            Me.lblNaviSub.sTranslatedText = osHTML.sString



        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
