'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerConditionsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for CustomerConditionsData
'--------------------------------------------------------------------------------
' Created:      22.04.2013 11:56:44, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class CustomerConditionsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim oRow As AspListViewRow
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim lCountPrices, lCountDiscounts As Integer

            Dim lCustomerID As Integer = 0 'glCInt(oListView.sUserData)
            Dim sProductGroup As String = ""
            Dim sSearch As String = ""

            Dim asParams As String() = oListView.sUserData.Split(";"c)

            If asParams.Length > 2 Then
                lCustomerID = glCInt(asParams(0))
                sProductGroup = asParams(1)
                sSearch = asParams(2)
            End If

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Adr_CustomerConditions_ProductGroup", sProductGroup, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Adr_CustomerConditions_ProductSearch", sSearch, False)


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "CustomerConditionsData.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 12
            oListView.bAutoHideNavigation = False

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProductNumber", "ArtikelNr", "100", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Matchcode", "*", , , True, False)
            oListView.oCols.oAddCol("ReferenceNumber", "Bestellnummer", "150", , , True, False)
            oListView.oCols.oAddCol("Price", "Preis", "100", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("CurrUnit", "WKZ", "50", , , False, False)
            oListView.oCols.oAddCol("Discount", "Rabatt %", "100", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            Dim sSubSelectPrcie As String = " (SELECT Price FROM tdProductCustomerPrices PCP WHERE PCP.Quantity=0 AND PCP.ProductID = P.ProductID AND PCP.CustomerID = " & lCustomerID & ") AS Price "
            Dim sSubSelectDicsount As String = " (SELECT Discount FROM tdProductCustomerDiscounts PCD WHERE PCD.Quantity=0 AND PCD.ProductID = P.ProductID AND PCD.CustomerID = " & lCustomerID & ") AS Discount "


            sFields = "P.ProductID AS RowID, P.ProductNumber, P.Matchcode, PC.CurrUnit , PC.ReferenceNumber, " & sSubSelectPrcie & ", " & sSubSelectDicsount
            sTables = "tdProducts P INNER JOIN tdProductCustomers PC ON PC.ProductID = P.ProductID "

            sClause = sClause.gsClauseAnd("PC.CustomerID = " & lCustomerID)

            If Not String.IsNullOrEmpty(sSearch) Then
                sSearch = sSearch.Replace("*", "%")
                Dim sSearchValue As String = (sSearch & "%").gs2Sql()
                sClause = sClause.gsClauseAnd("(P.Matchcode like " & sSearchValue & _
                                              " OR P.Name1 like " & sSearchValue & _
                                              " OR P.Name2 like " & sSearchValue & _
                                              " OR P.Productnumber like " & sSearchValue & ")")

            End If

            If Not String.IsNullOrEmpty(sProductGroup) Then
                sClause = sClause.gsClauseAnd("P.ProductGroup=" & sProductGroup.gs2Sql())
            End If


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs("RowID").sValue)

                oRow.Value(0) = rs("ProductNumber").sValue
                oRow.Value(1) = rs("Matchcode").sValue
                oRow.Value(2) = rs("ReferenceNumber").sValue
                oRow.Value(3) = rs("Price").cValue
                oRow.Value(4) = rs("CurrUnit").sValue
                oRow.Value(5) = rs("Discount").cValue

                lCountPrices = DataMethods.glGetDBCount(oClientInfo, "tdProductCustomerPrices", "CustomerID=" & lCustomerID & " AND ProductID= " & rs("RowID").lValue & " AND Quantity > 0")
                lCountDiscounts = DataMethods.glGetDBCount(oClientInfo, "tdProductCustomerDiscounts", "CustomerID=" & lCustomerID & " AND ProductID= " & rs("RowID").lValue & " AND Quantity > 0")

                If lCountPrices > 0 Or lCountDiscounts > 0 Then
                    oRow.sUserData = "-1"
                Else
                    oRow.bSetEmpty(6)
                End If

                rs.MoveNext()
            Loop

            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
