﻿
var sOriginalValues = "";

function mOnLoad()
{
    // hold original values if 'edit'
    if (msPageParams[0] != "-1")
    {
        sOriginalValues = gsXMLDlgParams(frmMain, '', false);

        // set image source
        if (msPageParams[2] != "")
        {
            document.getElementById("imgContact").src = msPageParams[2];
        }
        else
        {
            mHideContaceImage();
        }
    }
    else
    {
        mHideContaceImage();
    }
}

function mHideContaceImage()
{
    document.getElementById("imgContact").style.visibility = "hidden";
}

function mOnOk()
{

    var sCmd = "edit";
    var sRes = "";

    if (msPageParams[0] == "-1")
    {
        sCmd = "new";

        // set required values to hidden dialog parameters
        document.all.txtAddressID.value = msPageParams[1];
        mSetContactMatchcode();

        var sValues = gsXMLDlgParams(frmMain, '', false);
        var sURL = 'AddressContactProcess.aspx?Cmd=' + sCmd;

        sRes = gsCallServerMethod(sURL, sValues);
    }
    else
    {
        mSetContactMatchcode();

        var sValues = gsXMLDlgParams(frmMain, '', false);
        var sURL = 'AddressContactProcess.aspx?Cmd=' + sCmd;

        sRes = gsCallServerMethod(sURL, sOriginalValues + ';' + sValues);
    }


    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mOnCancel();
}

function mOnCancel()
{
    var sURL = '../AddressDetails/AddressDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + msPageParams[1];

    document.location = sURL;
}

function mSetContactMatchcode()
{
    var sMatchcode = document.all.txtLastName.value;
    if (document.all.txtFirstName.value != '')
    {
        sMatchcode = sMatchcode + ", " + document.all.txtFirstName.value;
    }

    document.all.txtMatchcode.value = sMatchcode;
}


function mOnAttributesClick() 
{
    var sURL = '../Attributes/AttributesMain.aspx' +
                '?ClientID=' + msClientID +
                '&ContactPartnerID=' + msPageParams[0];

    document.location = sURL;
}