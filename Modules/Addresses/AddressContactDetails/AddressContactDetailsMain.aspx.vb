'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressContactDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AddressContactDetailsMain
'--------------------------------------------------------------------------------
' Created:      11.07.2012 10:08:07, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressContactDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim sAddressID As String = Me.Request.QueryString("AddressID")
            Dim sContactID As String = Me.Request.QueryString("ContactID")

            ' if sContactID is -1 --> new contact! otherwise edit existing contact
            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = sContactID
            Me.asPageParams(1) = sAddressID



            If sContactID <> "-1" Then

                Dim sFields As String
                Dim rs As Recordset
                Dim sClause As String = "ContactID=" & sContactID.gs2Sql()
                Dim sTables As String


                sFields = "*"

                sTables = "tdContacts"

                rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause)

                Me.dlgContact.sValues = DataTools.gsRecord2Xml(rs)


                '---------- load image links ---------
                Dim lImageLink As Integer
                Dim sTablesDoc As String = "tdDocumentReferences DR" & _
                                            " INNER JOIN tdDocuments D ON D.DocumentID=DR.DocumentID" & _
                                            " INNER JOIN tsDocumentTypes DT ON DT.DocumentTypeID=D.DocumentType"
                'DR.TypeID=39  for Contact
                lImageLink = DataMethods.glGetDBValue(oClientInfo, "DR.DocumentID", sTablesDoc, "DR.ReferenceID=" & sContactID.gs2Sql & " AND DR.TypeID=39 AND DT.DocumentType='Image'")

                Me.asPageParams(2) = myfactory.Sys.Document.DocDocumentsT.gsGetDocumentLink(oClientInfo, False, lImageLink)
            Else
                Me.cmdContactAttributes.bHidden = True
            End If


            Me.gAddScriptLink("wfDlgParams.js", True)
            Me.sOnLoad = "mOnLoad();"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
