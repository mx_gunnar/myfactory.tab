'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressContactProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for AddressContactProcess
'--------------------------------------------------------------------------------
' Created:      11.07.2012 10:07:45, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.BusinessTasks.Base.Updates.wfIBaseUpdate


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressContactProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Me.Request.QueryString("Cmd")

            Select Case sCmd
                Case "edit"

                    Return msEditContact(oClientInfo)

                Case "new"

                    Return msNewContact(oClientInfo)

                Case "delete"

                    Return msDeleteContact(oClientInfo)
            End Select

            Return "ERR; unknown command"

        End Function

        Private Function msEditContact(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode

            Dim sContactID As String

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            sContactID = XmlFunctions.gsGetXMLText(oRootOrig.xml, "txtContactID")

            If XmlFunctions.gsGetXMLText(oRootNew.xml, "txtLastname") = "" Then Return Dictionary.gsTranslate(oClientInfo, "Das Feld 'Nachname' darf nicht leer sein.")


            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\CRM\Base\Addresses\tablecontacts.xml", sQry, sMsg, True) Then
                Return sMsg
            End If

            If sQry <> "" Then
                DataMethods2.glDBUpdate(oClientInfo, "tdContacts", sQry, "tdContacts.ContactID=" & sContactID)
            End If

            Return "OK; edit"
        End Function

        Private Function msNewContact(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectSingleNode("//DlgParams")
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            If XmlFunctions.gsGetXMLText(oRootNew.xml, "txtLastname") = "" Then Return Dictionary.gsTranslate(oClientInfo, "Das Feld 'Nachname' darf nicht leer sein.")

            '------------- insert db --------------
            Dim sTables As String = ""
            Dim sFields As String = "ContactID"
            Dim sValues As String = ""
            Dim sMsg As String = ""
            Dim sQry As String
            Dim lContactID As Integer
            Dim sAddressID As String = XmlFunctions.gsGetXMLText(oRootNew.xml, "txtAddressID")

            If Not TableDefFunctions.gbBuildInsertExpression(oClientInfo, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\CRM\Base\Addresses\tablecontacts.xml", _
                                                             sFields, sValues, sMsg, True) Then
                Return sMsg
            End If

            lContactID = RecordID.glGetNextRecordID(oClientInfo, "tdContacts")

            sQry = "INSERT INTO tdContacts(ContactID," & _
                "AddressID, " & _
                "CreationDate, " & _
                sFields & ") " & _
                "VALUES (" & lContactID & "," & _
                sAddressID.gs2Sql() & "," & _
                Date.Now.gs2Sql() & "," & _
                sValues & ")"

            DataMethods2.glDBExecute(oClientInfo, sQry)

            Return "OK; new"

        End Function

        Private Function msDeleteContact(ByVal oClientInfo As ClientInfo) As String

            Dim sContactID As String = Request.QueryString("ContactID")
            Dim sUpdate As String = "InActive=-1"

            '------------- delete db -------------   (don't realy delete! set inActive = true! )
            If DataMethods2.glDBUpdate(oClientInfo, "tdContacts", sUpdate, "ContactID=" & sContactID) <> 1 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim L�schen des Ansprechpartners.")
            End If

            Return "OK; delete"
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
