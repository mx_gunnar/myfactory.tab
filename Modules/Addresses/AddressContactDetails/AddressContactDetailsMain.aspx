<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.AddressContactDetailsMain" CodeFile="AddressContactDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" style="table-layout:fixed;" cellpadding="2" width="99%" height="97%">
        <tr height="355px" valign="top">
            <td colspan="2">
                <form name="frmMain">
                <myfactory:wfXmlDialog ID="dlgContact" runat="server" sDialog="/tab/modules/Addresses/AddressContactDetails/dlgAddressContactDetails.xml" />
                </form>
            </td>
        </tr>
        <tr height="*" valign="top">
            <td align=left>
                <img id="imgContact" width="170" height="170" runat="server" />
            </td>
            <td align="right">
                <myfactory:wfButton ID="cmdContactAttributes" sText="Kennzeichen" sOnClick="mOnAttributesClick();" runat="server"></myfactory:wfButton>
            </td>
        </tr>
        <tr valign="bottom">
            <td align="right" colspan="2">
                <myfactory:wfButton runat="server" ID="cmdNew" sOnClick="mOnOk()" sText=" OK ">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdEdit" sOnClick="mOnCancel()" sText="Abbrechen">
                </myfactory:wfButton>
            </td>
        </tr>
    </table>
</body>
</html>
