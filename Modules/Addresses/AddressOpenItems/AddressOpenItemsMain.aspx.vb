'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressOpenItemsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AddressOpenItemsMain
'--------------------------------------------------------------------------------
' Created:      12.07.2012 10:45:21, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressOpenItemsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim sAddressID As String = Me.Request.QueryString("RecordID")

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = sAddressID

            Me.sOnLoad = "mOnLoad();"


            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & sAddressID))

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition,PartnerID,ModuleID", _
                                                   "tsTabModules", _
                                                   "ParentModule='AddressDetails' AND ModuleName<>'Addresses_OpenItems'", _
                                                   , "ShowIndex", _
                                                   wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & sAddressID))

                frs.MoveNext()
            Loop
            osHTML.bAppend("</nobr>")
            Me.lblNaviSub.sTranslatedText = osHTML.sString


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
