﻿

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sParams = msPageParams[0];

    gListViewSetUserData('lstOpenItems', sParams);
    gListViewLoadPage('lstOpenItems', -1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mNavigateToDetails(sOpenItemId)
{
    var sParams = "&RecordID=" + msPageParams[0];
    sParams = sParams + "&OpenItemId=" + sOpenItemId;

    var sURL = '../AddressOpenItemsDetails/AddressOpenItemsDetailMain.aspx' +
                '?ClientID=' + msClientID + sParams;

    document.location = sURL;
}