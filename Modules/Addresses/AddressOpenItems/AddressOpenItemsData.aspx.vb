'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressOpenItemsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AddressOpenItemsData
'--------------------------------------------------------------------------------
' Created:      12.07.2012 10:45:48, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressOpenItemsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim lAddressID As Integer = glCInt(oListView.sUserData)
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AddressOpenItemsData.aspx"
            oListView.bAutoHideNavigation = False
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10

            '---- Columns ------------------
            oListView.oCols.oAddCol("OpenItemNr", "OP-Nr", "100", , , True, False)
            oListView.oCols.oAddCol("initialDate", "OP-Datum", "100", wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("DueDate", "F�lligkeit", "100", wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("ReminderLevel", "MS", "40", , , True, False)
            oListView.oCols.oAddCol("SumInvoicedInternal", "Rechnung", "*", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("SumPayedInternal", "Zahlung", "120", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("SumOpen", "Rest", "120", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("CurrUnit", "WKZ", "55", , , False, False)

            oCol = oListView.oCols.oAddCol("CmdDetTurnover", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            sClause = "c.AddressID=" & lAddressID
            sClause = sClause.gsClauseAnd(Replace(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers"), "tdCustomers.", "c."))
            If Not DataMethods.gbDBExists(oClientInfo, "CustomerID", "tdCustomers c", sClause) Then Return True

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "DueDate, OpenItemID"
            End If

            sClause = DataTools.gsClauseAnd(sClause, "op.Completed=0 AND op.WaitState=0 AND op.DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)

            sTables = "tdOpenItemsDebtMain op" & _
                            " INNER JOIN tdCustomers c ON op.CustomerID=c.CustomerID" & _
                            " INNER JOIN tdAddresses a ON c.AddressID=a.AddressID "

            sFields = "OpenItemId as RowId, " & _
                    "OpenItemNr, " & _
                    "InitialDate, " & _
                    "DueDate, " & _
                    "op.ReminderLevel, " & _
                    "SumInvoicedInternal, " & _
                    "SumPayedInternal, " & _
                    "(SumInvoicedInternal-SumPayedInternal) AS SumOpen, " & _
                    "CurrUnit, " & _
                    "'...' AS Det"


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            '-------------- sum row ---------
            Dim rsSum As Recordset
            rsSum = DataMethods.grsGetDBRecordset(oClientInfo, "SUM(SumInvoicedInternal), SUM(SumPayedInternal), SUM(SumInvoicedInternal-SumPayedInternal), MIN(CurrUnit)", sTables, _
                                                   sClause, "op.customerid", "op.customerid")

            If Not rsSum.EOF Then
                oRow = oListView.oRows.oAddRow("-1")
                oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Gesamtsumme")
                oRow.Value(4) = rsSum(0).sValue
                oRow.Value(5) = rsSum(1).sValue
                oRow.Value(6) = rsSum(2).sValue
                oRow.Value(7) = rsSum(3).sValue
                oRow.bGroupStyle = True
            End If

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
