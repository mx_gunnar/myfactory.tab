<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.AddressOpenItemsMain" codefile="AddressOpenItemsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" style="table-layout:fixed;" cellspacing="2" cellpadding="2" width="99%" height="97%">

        <tr  valign="top">
            <td>
                <myfactory:wfListView runat="server" ID="lstOpenItems" sListViewDataPage="AddressOpenItemsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi" >
	        <td align="right" >
		        <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete"></myfactory:wfLabel>
	        </td>
        </tr>
    </table>
</body>
</html>