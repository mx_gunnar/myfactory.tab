﻿// SalesContactsMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshContactList();
}

function mRefreshContactList()
{
    gListViewLoadPage('lstSalesContacts', -1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{ 
    
    if (sColID == 'CmdDel')
    {
        if (window.confirm(msTerms[0]) == false)
            return;
    
        var sURL = '../SalesContactNew/SalesContactNewProcess.aspx?Cmd=Delete&RecordID=' + sItemID;

        var sRes = gsCallServerMethod(sURL);

        if (sRes.substr(0, 3) != 'OK;')
        {
            alert(sRes);
            return;
        }
        mRefreshContactList();

    }
    else if (sColID == 'CmdDet')
    {
        var sURL = '../SalesContactNew/SalesContactNewMain.aspx' +
                '?ClientID=' + msClientID +
                '&ContactID=' + sItemID +
                '&ProjectID=' + msPageParams[1] + 
                '&RecordID=' + msPageParams[0];

        document.location = sURL;
    }
}

function mOnListViewClick(sListView, sItemID)
{
    var sURL = '../SalesContactNew/SalesContactNewMain.aspx' +
                '?ClientID=' + msClientID +
                '&ContactID=' + sItemID +
                '&ProjectID=' + msPageParams[1] + 
                '&RecordID=' + msPageParams[0];

    document.location = sURL;
}
