'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesContactsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesContactsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class SalesContactsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            'record id = address id
            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))

            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))


            ReDim Me.asPageParams(2)
            ReDim Me.asTerms(1)


            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "M�chten Sie diesen Kontakt wirklich l�schen?")

            If lRecordID <> 0 Then
                Me.asPageParams(0) = Str(lRecordID)

                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "AddressID, Matchcode", _
                                                   "tdAddresses", "AddressID=" & lRecordID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If

                Me.lstSalesContacts.sUserData = lRecordID.ToString & ";0"

            ElseIf lProjectID <> 0 Then
                Me.asPageParams(1) = lProjectID.ToString

                Dim rs As Recordset = DataMethods.grsGetDBRecordset(oClientInfo, "(ProjectNumber + ', ' + Matchcode) AS Matchcode", "tdProjects", "ProjectID=" & lProjectID)
                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                Me.lstSalesContacts.sUserData = "0;" & lProjectID.ToString

            End If

            Me.gAddScriptLink("../../../Home/ModuleMain.js")
            Me.sOnLoad = "touchScrollX('lblNaviSub');mOnLoad();"





















            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean


            If lProjectID > 0 Then

                '-------- write navi for projects ---------------
                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProjectDetails", "../../../", "ProjectID=" & lProjectID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                            "ModuleName,Edition,PartnerID,ModuleID", _
                            "tsTabModules", _
                            "ParentModule='ProjectDetails' AND ModuleName <> 'Projects_Contacts' ", _
                            , "ShowIndex", wfDataSourceSystem)

                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "ProjectID=" & lProjectID))

                    frs.MoveNext()
                Loop

                osHTML.bAppend("</nobr>")

                Me.lblNaviSub.sTranslatedText = osHTML.sString
                Dim lProjectAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "tdCustomers.AddressID", "tdProjects INNER JOIN tdCustomers ON tdCustomers.CustomerID=tdProjects.CustomerId WHERE tdProjects.ProjectID = " & lProjectID)
                Me.lblNaviLeft.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "Addresses_SalesContacts_New", "../../../", "RecordID=" & lProjectAddressID & "&ProjectID=" & lProjectID)
            Else

                '-------- write navi  for addresses ---------------
                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lRecordID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition", _
                                                       "tsTabModules", _
                                                       "ParentModule='AddressDetails' AND ModuleName<>'Addresses_SalesContacts'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")

                Me.lblNaviSub.sTranslatedText = osHTML.sString
                Me.lblNaviLeft.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "Addresses_SalesContacts_New", "../../../", "RecordID=" & lRecordID)
            End If


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
