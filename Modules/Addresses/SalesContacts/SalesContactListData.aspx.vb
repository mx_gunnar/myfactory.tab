'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesContactListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesContactListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class SalesContactListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SalesContactListData.aspx"
            oListView.lPageSize = 9
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("ContactDate", "Datum", "70", wfEnumDataTypes.wfDataTypeDate)
            oListView.oCols.oAddCol("ContactDesc", "Betreff", "*")

            oListView.oCols.oAddCol("TopicDesc", "Thema", "150")
            oListView.oCols.oAddCol("TypeDesc", "Art", "120")

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDel", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50"


            '---- Data ---------------------
            Dim lAddressID As Integer
            Dim lProjectID As Integer

            Dim sParams As String = oListView.sUserData & ";;;"
            Dim asParams As String() = sParams.Split(";"c)

            lAddressID = glCInt(asParams(0))
            lProjectID = glCInt(asParams(1))


            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "ContactDate"
                oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            If oListView.sOrderCol = "LastName" Then sOrder &= ", FirstName"

            sFields = "ContactID AS RowID, ContactDate, ContactDesc, TopicDesc, TypeDesc, '', '', UserName "
            sTables = "tdSalesContacts sc " & _
                        " LEFT JOIN tdSalesContactTypes sct1 ON sc.ContactType = sct1.TypeID " & _
                        " LEFT JOIN tdSalesContactTopics sct2 ON sc.ContactTopic = sct2.TopicID "

            '---- clause ---------
            If lAddressID > 0 Then
                sClause = "AddressID=" & lAddressID
            ElseIf lProjectID > 0 Then
                sClause = "ProjectID=" & lProjectID
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)
            Dim bCanDelete As Boolean = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_AddressDeleteSalesContact", "", "Kontakt zu einer Adresse l�schen")

            Dim oRow As AspListViewRow
            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                If rs("ContactDate").dtValue < Date.Now.AddDays(-3) Or _
                    Not bCanDelete Or _
                    rs("UserName").sValue <> oClientInfo.oClientProperties.sCurrentUser Then

                    oRow.bSetEmpty(5)
                End If

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
