<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.SalesContactNewMain" codefile="SalesContactNewMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable cellSpacing=2 cellPadding=2 width="98%" height="97%" style="table-layout:fixed;" >
<tr height="*" valign="top">
	<td>
		<form name="frmMain">
			<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/SalesContactNew/dlgSalesContactNew.xml" sDataSource="dsoData"></myfactory:wfXmlDialog>
		</form>
	</td>
</tr>
<tr height="200px" valign="bottom">
	<td align="right">
		<myfactory:wfButton runat="server" ID="cmdOK" sOnClick="mOnOK()" sText="OK"></myfactory:wfButton>
		<myfactory:wfButton runat="server" ID="cmdCancel" sOnClick="mOnCancel()" sText="Abbrechen"></myfactory:wfButton>
	</td>
</tr>
</table>

</body>
</HTML>
