'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesContactNewMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesContactNewMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.FrontendSystem.AspDialog

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class SalesContactNewMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            'lRecordID = Address ID
            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lContactID As Integer = glCInt(Request.QueryString("ContactID"))
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))

            ReDim Me.asPageParams(2)


            If lContactID <> 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdSalesContacts", "ContactID=" & lContactID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)

                End If

                '----------- check user grants ---------
                Dim bCanEdit As Boolean = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_AddressChangeSalesContact", "", "Kontakt zu einer Adresse bearbeiten")

                'allow edit when: own contact / not older then 3 days / got permission
                If Not bCanEdit Or _
                    rs("ContactDate").dtValue < Date.Now.AddDays(-3) Or _
                    rs("UserName").sValue <> oClientInfo.oClientProperties.sCurrentUser Then

                    Me.cmdOK.bHidden = True

                End If
                '---------------------------------------

                Dim sMatchcode As String = ""

                If lRecordID = 0 AndAlso lProjectID <> 0 Then
                    sMatchcode = DataMethods.gsGetDBValue(oClientInfo, "tdAddresses.Matchcode", "tdProjects P INNER JOIN tdCustomers C ON C.CustomerID=P.CustomerID INNER JOIN tdAddresses ON tdAddresses.AddressID=C.AddressID", "P.ProjectID=" & lProjectID)
                Else
                    sMatchcode = DataMethods.gsGetDBValue(oClientInfo, "Matchcode", "tdAddresses", "AddressID=" & lRecordID)
                End If


                Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("VALUE") = sMatchcode

                Me.asPageParams(0) = Str(lContactID)
                Me.sOnLoad = "mOnLoad();"

            ElseIf lRecordID <> 0 Then

                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "a.AddressID, a.Matchcode", _
                                                   "tdAddresses a", _
                                                        "a.AddressID=" & lRecordID)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If

                Me.dlgMain.oDialog.oField("ContactPartner").oMember.Prop("Params") = lRecordID

            End If

            If lProjectID > 0 Then
                Me.dlgMain.oDialog.oField("ProjectID").oMember.Prop("Value") = lProjectID.ToString
                Me.asPageParams(1) = lProjectID.ToString
            End If

            Me.gAddScriptLink("wfDlgParams.js", True)

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
