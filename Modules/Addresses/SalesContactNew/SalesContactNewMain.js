﻿//SalesContactNewMain.js (c) myfactory 2011

var sOrigValues;
function mOnLoad()
{
    sOrigValues = gsXMLDlgParams(frmMain, '', false);
}

function mOnOK()
{
    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sURL = 'SalesContactNewProcess.aspx?Cmd=Save'

    if (msPageParams[0] != '')
    {
        sURL = sURL + '&RecordID=' + msPageParams[0];
        sValues = sValues + sOrigValues;
    }

    var sRes = gsCallServerMethod(sURL, sValues);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mOnCancel();
}

function mOnCancel()
{
    var sURL = "";

    sURL = '../SalesContacts/SalesContactsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + document.all.txtAddressID.value;

    if (document.all.txtProjectID.value != "" && document.all.txtProjectID.value != "0")
    {   

        sURL = '../SalesContacts/SalesContactsMain.aspx' +
                '?ClientID=' + msClientID +
                '&ProjectID=' + document.all.txtProjectID.value;
    }


    document.location = sURL;

}
