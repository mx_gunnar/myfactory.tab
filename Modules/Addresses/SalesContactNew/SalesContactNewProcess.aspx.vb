'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesContactNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SalesContactNewProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		28.09.2012 - ABuhleier      logic for tdVisits implemented
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

Imports myfactory.BusinessTasks.Base.Updates.wfIBaseUpdate
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.CRM.Sales.SalesContact

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class SalesContactNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lContactID As Integer = glCInt(Request.QueryString("RecordID"))

            Select Case sCmd
                Case "Delete"
                    If lContactID > 0 Then
                        Return msDeleteContact(oClientInfo, lContactID)
                    End If

                Case "Save"
                    If lContactID > 0 Then
                        Return msSaveChanges(oClientInfo, lContactID)
                    Else

                        Return msSaveNewContact(oClientInfo)
                    End If

                Case Else
                    Return "Unknown Cmd: " & sCmd
            End Select

            Return "TODO"

        End Function


        Private Function msDeleteContact(ByVal oClientInfo As ClientInfo, ByVal lContactID As Integer) As String

            If DataMethods2.glDBDelete(oClientInfo, "tdSalesContacts", "ContactID=" & lContactID) < 1 Then
                Return "ERR; no delete occurs"
            End If


            '--- delete tdVisits entry (if exists)
            DataMethods2.glDBDelete(oClientInfo, "tdVisits", "SalesContactID=" & lContactID)


            Return "OK; delete"
        End Function

        Private Function msSaveChanges(ByVal oClientInfo As ClientInfo, ByVal lContactID As Integer) As String
            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode
            Dim lTaskID As Integer
            Dim lAddressID As Integer

            Dim oRootOriginal As XmlNode

            '--------- get dialog params ----------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oRootNew.selectNodes("//DlgParams")(0)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOriginal = oRootNew.selectNodes("//DlgParams")(1)
            If oRootOriginal Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '---------------------------------------

            '--------- check values ----------------
            Dim sDesc As String
            Dim dtTaskDueDate As Date

            sDesc = XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtContactDesc")
            If sDesc = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Betreff an.")

            dtTaskDueDate = gdtCDate(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtContactDate"))
            If Not gbDate(dtTaskDueDate) Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie ein g�ltiges Datum an.")
            '--------------------------------------


            Dim sUpdate As String = ""
            Dim sMSG As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOriginal.xml, oRootNew.xml, oClientInfo.sAppPath & _
                        "\ie50\CRM\Base\Addresses\TableSalesContacts.xml", _
                        sUpdate, sMSG, True) Then
                sMSG = BaseUpdateData.gsGetUpdateErrorMessage(oClientInfo, "tdSalesContacts", sMSG)
                Return sMSG
            End If

            If sUpdate = "" Then
                Return "OK; no changes"
            End If

            If DataMethods2.glDBUpdate(oClientInfo, "tdSalesContacts", sUpdate, "ContactID=" & lContactID) < 1 Then
                Return "ERR; no update occurs"
            End If


            '---- sUpdate contains new SalesContactType ?  --> then check if its needed to create new tdVisits entry
            If sUpdate.Contains("ContactType") Then

                Dim lContactType As Integer = glCInt(XmlFunctions.gsGetXMLText(oRootNew.xml, "cboContactType"))

                If lContactType > 0 Then
                    Dim sQry As String

                    If DataMethods.gbGetDBValue(oClientInfo, "IsVisit", "tdSalesContactTypes", "TypeID=" & lContactType) Then

                        ' create new tdVisit entry 
                        sQry = "INSERT INTO tdVisits (SalesContactID, IsPlanned) VALUES (" & lContactID & ", 0)"
                        DataMethods2.glDBExecute(oClientInfo, sQry)
                    Else

                        ' remvoe existing tdVisist entry
                        DataMethods2.glDBDelete(oClientInfo, "tdVisits", "SalesContactID=" & lContactID)
                    End If
                End If
            End If

            'if sUpdate contains "ContactResult" than call glCreateBasicTaskFromSalesContact
            If sUpdate.Contains("ContactResult") Then

                lAddressID = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdSalesContacts", "ContactID=" & lContactID)
                lTaskID = glCreateBasicTaskFromSalesContact(oClientInfo, lContactID, lAddressID)

            End If

            '----------------------------------


            Return "OK; update"

        End Function


        ''' <summary>
        ''' Save Task Data
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msSaveNewContact(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRoot As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oRoot.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            '--------- check values ----------------
            Dim sDesc As String
            Dim dtTaskDueDate As Date
            Dim lSalesContactID As Integer

            sDesc = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtContactDesc")
            If sDesc = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Betreff an.")

            dtTaskDueDate = gdtCDate(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtContactDate"))
            If Not gbDate(dtTaskDueDate) Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie ein g�ltiges Datum an.")

            Dim sMsg As String = ""
            Dim sFields As String = ""
            Dim sValues As String = ""
            Dim sQry As String
            Dim lTaskID As Integer
            Dim lAddressID As Integer

            If Not TableDefFunctions.gbBuildInsertExpression(oClientInfo, oRoot.xml, _
                            oClientInfo.sAppPath & "\ie50\CRM\Base\Addresses\TableSalesContacts.xml", _
                            sFields, sValues, sMsg, True) Then
                sMsg = BaseUpdateData.gsGetUpdateErrorMessage(oClientInfo, "tdSalesContacts", sMsg)
                Return sMsg
            End If

            '--- create insert statement ---
            If sFields <> "" Then

                lSalesContactID = RecordID.glGetNextRecordID(oClientInfo, "tdSalesContacts")

                sQry = "INSERT INTO tdSalesContacts(ContactID,UserName," & sFields & ") " & _
                    "VALUES (" & lSalesContactID & _
                    "," & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
                    "," & sValues & ")"

                If DataMethods2.glDBExecute(oClientInfo, sQry) > 0 Then

                    'create task
                    lAddressID = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdSalesContacts", "ContactID=" & lSalesContactID)
                    lTaskID = glCreateBasicTaskFromSalesContact(oClientInfo, lSalesContactID, lAddressID)

                    '------- add tsVisits entry if tdSalesContactTypes.IsVisit = -1 -----------
                    Dim lContactType As Integer = glCInt(XmlFunctions.gsGetXMLText(oRoot.xml, "cboContactType"))
                    If lContactType > 0 Then

                        If DataMethods.gbGetDBValue(oClientInfo, "IsVisit", "tdSalesContactTypes", "TypeID=" & lContactType) Then
                            sQry = "INSERT INTO tdVisits (SalesContactID, IsPlanned) VALUES (" & lSalesContactID & ", 0)"

                            DataMethods2.glDBExecute(oClientInfo, sQry)
                        End If
                    End If
                    '----------------------------------

                End If
            End If

            Return "OK; new"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
