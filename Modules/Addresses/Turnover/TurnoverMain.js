﻿// TurnoverMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshTurnoverList();
}

function mRefreshTurnoverList() 
{
    var sData = msPageParams[0] + ';' + document.all.chkAllDivisions.checked;
    gListViewSetUserData('lstTurnover', sData);
    gListViewLoadPage('lstTurnover', -1);
}

function mOnSetDirty() {
    mRefreshTurnoverList();
}
 