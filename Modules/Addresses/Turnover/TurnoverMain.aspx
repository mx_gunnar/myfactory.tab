<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.TurnoverMain" codefile="TurnoverMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable cellSpacing=2 cellPadding=2 style="table-layout:fixed;" width="99%" height="97%">
<tr height="50px">
	<td >
		<form name="frmMain">
			<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/Turnover/dlgTurnover.xml" sDataSource="dsoData"></myfactory:wfXmlDialog>
		</form>
	</td>
</tr>
<tr height="*">
	<td>
		<myfactory:wfListView runat="server" ID="lstTurnover" sListViewDataPage="TurnoverListData.aspx"></myfactory:wfListView>
	</td>
</tr>
        <tr height="120px" valign="bottom" id="trNavi" >
	        <td align="right" width="*">
		        <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete"></myfactory:wfLabel>
	        </td>
        </tr>
</table>

</body>
</HTML>
