'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TurnoverListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for TurnoverListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class TurnoverListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim sClause As String = ""
            Dim sClauseLY As String = ""
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim bRevenue As Boolean

            bRevenue = myfactory.Sys.Permissions.Permissions.gbCheckTaskPermission(oClientInfo, "SalesRevenue")

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "TurnoverListData.aspx"
            oListView.lPageSize = 12
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            Dim lAddressID As Integer
            Dim bCustomer, bSupplier As Boolean
            Dim bAllDivisions As Boolean
            Dim sDivisionClause As String = ""

            lAddressID = glCInt(oListView.sUserData.Split(";"c)(0))
            bAllDivisions = gbCBool(oListView.sUserData.Split(";"c)(1))

            If Not bAllDivisions Then
                sDivisionClause = "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr
            End If

            sClause = "AddressID=" & lAddressID
            sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers"))
            If DataMethods.gbDBExists(oClientInfo, "CustomerID", "tdCustomers", sClause) Then bCustomer = True

            If Not bCustomer Then
                sClause = "AddressID=" & lAddressID
                sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Suppliers"))
                If DataMethods.gbDBExists(oClientInfo, "SupplierID", "tdSuppliers", sClause) Then bSupplier = True
            End If

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("Period", "Periode", "*")
            oCol.bHtml = True

            If bSupplier Then
                oListView.oCols.oAddCol("Turnover", "Einkaufsumsatz", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
                oListView.oCols.oAddCol("Turnover_VJ", "Einkaufsumsatz (VJ)", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            Else
                oListView.oCols.oAddCol("Turnover", "Umsatz", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
                oListView.oCols.oAddCol("Turnover_VJ", "Umsatz (VJ)", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            End If

            oCol = oListView.oCols.oAddCol("Revenue", "Roherl�s", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            If Not bRevenue Or Not bCustomer Then oCol.bHidden = True

            oCol = oListView.oCols.oAddCol("Revenue_VJ", "Roherl�s (VJ)", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            If Not bRevenue Or Not bCustomer Then oCol.bHidden = True

            oListView.oCols.oAddCol("CurrUnit", "Wkz", "50", , wfEnumAligns.wfAlignCenter)


            '---- Data ---------------------
            If Not bCustomer And Not bSupplier Then Return True


            Dim lCurrYear As Integer = glCInt(oClientInfo.oClientProperties.lCurrentPeriod / 1000)
            Dim lYear As Integer
            Dim l, lPeriod, lPeriodLY As Integer   'LY = Last Year
            Dim sCurrUnit As String

            lYear = lCurrYear - oListView.lPage + 1

            sCurrUnit = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            If bSupplier Then
                sTables = "tdStatPurchaseSuppliers s " & _
                            " INNER JOIN tdSuppliers c ON s.SupplierID=c.SupplierID"
            Else
                sTables = "tdStatSalesCustomers s " & _
                            " INNER JOIN tdCustomers c ON s.CustomerID=c.CustomerID"
            End If

            For l = 1 To 12
                lPeriod = lYear * 1000 + l
                lPeriodLY = (lYear - 1) * 1000 + l

                oRow = oListView.oRows.oAddRow(lPeriod.ToString)
                oRow.sValue(0) = Period.gsGetPeriodCaption(oClientInfo, lPeriod) & "<br>&nbsp;"


                If bSupplier Then
                    sClause = "c.AddressID=" & lAddressID & " AND PurchasePeriod=" & lPeriod
                Else
                    sClause = "c.AddressID=" & lAddressID & " AND SalesPeriod=" & lPeriod
                End If
                sClause = sClause.gsClauseAnd(sDivisionClause)

                oRow.Value(1) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClause)


                '------ last year -------
                If bSupplier Then
                    sClauseLY = "c.AddressID=" & lAddressID & " AND PurchasePeriod=" & lPeriodLY
                Else
                    sClauseLY = "c.AddressID=" & lAddressID & " AND SalesPeriod=" & lPeriodLY
                End If
                sClauseLY = sClauseLY.gsClauseAnd(sDivisionClause)

                oRow.Value(2) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClauseLY)
                '---------------

                If bCustomer Then oRow.Value(3) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Revenue)", sTables, sClause)

                '--------- last year -----------
                If bCustomer Then oRow.Value(4) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Revenue)", sTables, sClauseLY)
                '----------------------

                oRow.Value(5) = sCurrUnit
            Next

            oRow = oListView.oRows.oAddRow("-1")
            oRow.bGroupStyle = True
            oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Summe") & "<br>&nbsp;"

            If bSupplier Then
                sClause = "c.AddressID=" & lAddressID & " AND PurchasePeriod>=" & lYear & "000 AND PurchasePeriod<=" & lYear & "999"
            Else
                sClause = "c.AddressID=" & lAddressID & " AND SalesPeriod>=" & lYear & "000 AND SalesPeriod<=" & lYear & "999"
            End If
            sClause = sClause.gsClauseAnd(sDivisionClause)

            If bSupplier Then
                sClauseLY = "c.AddressID=" & lAddressID & " AND PurchasePeriod>=" & lYear - 1 & "000 AND PurchasePeriod<=" & lYear - 1 & "999"
            Else
                sClauseLY = "c.AddressID=" & lAddressID & " AND SalesPeriod>=" & lYear - 1 & "000 AND SalesPeriod<=" & lYear - 1 & "999"
            End If
            sClauseLY = sClauseLY.gsClauseAnd(sDivisionClause)

            oRow.Value(1) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClause)
            oRow.Value(2) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClauseLY)

            If bCustomer Then oRow.Value(3) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Revenue)", sTables, sClause)
            If bCustomer Then oRow.Value(4) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Revenue)", sTables, sClauseLY)

            oRow.Value(5) = sCurrUnit

            oListView.lRecordCountTotal = 12 * DataMethods.glGetDBCount(oClientInfo, "tdPeriods", "Period<" & lCurrYear & "999", "DISTINCT(Period/1000)")

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
