'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    OppsListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for OppsListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses.Opps

    Partial Class OppsListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "OppsListData.aspx"
            oListView.lPageSize = 8
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("s.OpportunityDesc", "Chance", "120", , , True)

            oCol = oListView.oCols.oAddCol("cmdOpp", "", "40", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oListView.oCols.oAddCol("s.OpportunityDate", "Datum", "40", wfEnumDataTypes.wfDataTypeDate, , True)
            oListView.oCols.oAddCol("s.OrderSumInternal", "Wert", "40", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True)
            oListView.oCols.oAddCol("sr.RatingValue", "%", "40", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True)
            oListView.oCols.oAddCol("s.OpportunityDescText", "Memo", "200")
            oListView.oCols.oAddCol("ar.Matchcode", "Vertreter", "100", , , True)

            '---- Data ---------------------
            Dim lAddressID As Integer
            Dim sClause2 As String

            lAddressID = glCInt(oListView.sUserData)

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "s.OpportunityDate"
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            '-- default clause ---
            sClause = "s.DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr & _
                        " AND s.Completed=0"

            sClause2 = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "SalesOpportunities")
            If sClause2 <> "" Then
                sClause2 = Replace(sClause2, "tdSalesOpportunities.", "s.")
                sClause = DataTools.gsClauseAnd(sClause, sClause2)
            End If

            sClause2 = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Addresses")
            If sClause2 <> "" Then
                sClause2 = Replace(sClause2, "tdAddresses.", "a.")
                sClause = DataTools.gsClauseAnd(sClause, sClause2)
            End If

            '--------------------
            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "s.OpportunityID, s.OpportunityDesc, '...', s.OpportunityDate, " & _
                        "s.OrderSumInternal, sr.RatingValue, '' AS DescText " & _
                        ",ar.Matchcode" & _
                        ",s.OpportunityDescText"

            sTables = "tdSalesOpportunities s " & _
                        " INNER JOIN tdAddresses a ON s.AddressID=a.AddressID " & _
                        " LEFT JOIN tdSalesOpportunityRatings sr ON s.OpportunityRating=sr.RatingID " & _
                        " LEFT JOIN tdRepresentatives r ON s.RepresentativeID=r.RepresentativeID " & _
                        " LEFT JOIN tdAddresses ar ON r.AddressID=ar.AddressID "


            '---- clause ---------

            sClause = DataTools.gsClauseAnd(sClause, _
                                " s.AddressID=" & lAddressID)

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)

                rs("DescText").Value = Left(rs("OpportunityDescText").sValue, 200)
                oRow.SetRecord(rs, True)

                rs.MoveNext()
            Loop
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
