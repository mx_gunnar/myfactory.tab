﻿// OppsMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    gListViewLoadPage('lstMain', 1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL;

    if (sColID == 'cmdOpp') 
    {
        sURL = '../../Opps/OppsDetails/OppsDetailsMain.aspx' +
                    '?ClientID=' + msClientID +
                    '&RecordID=' + sItemID +
                    '&Type=AddressOpps' + 
                    '&AddressID=' + document.all.txtAddressID.value;

        document.location = sURL;
    }

}

function mOnListViewClick(sListView, sItemID)
{
   var sURL = '../../Opps/OppsDetails/OppsDetailsMain.aspx' +
                    '?ClientID=' + msClientID +
                    '&RecordID=' + sItemID +
                    '&Type=AddressOpps' +
                    '&AddressID=' + document.all.txtAddressID.value;

    document.location = sURL;
}
