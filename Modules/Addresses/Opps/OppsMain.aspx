<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Addresses.Opps.OppsMain" codefile="OppsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="97%" cellSpacing=2 cellPadding=2 width="99%">
<tr height="20px">
	<td colspan="2">
			<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/Opps/dlgOpps.xml" sDataSource="dsoData"></myfactory:wfXmlDialog>
	</td>
</tr>
<tr height="*">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="OppsListData.aspx"></myfactory:wfListView>
	</td>
</tr>
<tr height="120px" valign="bottom" >
	<td width="200px">
		<myfactory:wfLabel runat="server" ID="lblNaviLeft" sStyle="margin-left:10px;margin-bottom:30px"></myfactory:wfLabel>
	</td>
	<td align="right" width="*">
		<myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeft" sText="<"></myfactory:wfLabel>
		<myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
		<myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNavi"></myfactory:wfLabel>
	</td>
</tr>
</table>

</body>
</HTML>
