﻿var sEditCmd;

function mOnLoad()
{
    mRefreshLstMain();
}

function mRefreshLstMain() 
{
    // hide Entity-CMD-Button (show it when something is selected)
    document.getElementById("cmdEntityEdit").style.visibility = 'hidden';

    var sParams = msPageParams[0] + ';' + msGetParameters();

    gListViewSetUserData('lstOrderedProducts', sParams);
    gListViewLoadPage('lstOrderedProducts', -1);
}

function msGetParameters()
{
    var sParams;

    sParams = document.all.txtDateFrom.value;
    sParams = sParams + ';' + document.all.txtDateTo.value;
    sParams = sParams + ';' + document.all.chkAllDivisions.checked;
    sParams = sParams + ';' + document.all.txtSearch.value;

    return sParams;
}

function mOnSetDirty()
{
    if (event.type == 'change' || event.type == 'click')
    {
        mRefreshLstMain();
    }
}

function mOnEntityEdit() 
{

    if (sEditCmd != "") 
    {
        document.location = sEditCmd;
    }
}

function mOnListViewNavigate(sListView, lPage) 
{
    document.getElementById("cmdEntityEdit").style.visibility = 'hidden';
}

function mOnListViewClick(sListView, sItemID) 
{
    sEditCmd = "";
    // get product id
    var sProductID = gsListViewGetItemUserData(sListView, sItemID);

    // create edit command
    sEditCmd = msTerms[0];
    sEditCmd = sEditCmd.replace('$1', sProductID);
    sEditCmd = msWebPageRoot + "/" + sEditCmd + "&ClientID=" + msClientID;

    // show Product-Entity-Cmd-Button
    document.getElementById("cmdEntityEdit").style.visibility = 'visible';

}