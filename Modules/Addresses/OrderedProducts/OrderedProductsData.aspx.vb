'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    OrderedProductsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for OrderedProductsData
'--------------------------------------------------------------------------------
' Created:      06.07.2012 10:18:47, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class OrderedProductsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================
        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "OrderedProductsData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("SalDate", "Datum", "80", , , True, False)
            oListView.oCols.oAddCol("ONr", "Belegnr", "80", , , True, False)
            oListView.oCols.oAddCol("PNr", "Artikelnr", "80", , , True, False)
            oListView.oCols.oAddCol("ProdMatchcode", "Kurzbezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("PosQuantity", "Menge", "60", , wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("PosUnit", "ME", "50", , , True, False)
            oListView.oCols.oAddCol("PosPrice", "Einzelpreis", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("CurrencyUnit", "Wkz", "40", , , True, False)
            oListView.oCols.oAddCol("HistPriceUnit", "Pe", "40", , wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("HistPosDiscount", "Rabatt", "60", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("HistPosIntPrice", "Umsatz (" & myfactory.Sys.Main.Currencies.gsCurrUnitInternal(oClientInfo, -1) & ")", "120", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)

            '---- Data ----- 

            sClause = msGetClause(oClientInfo, oListView.sUserData)

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "Hist.SalesDate desc "
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "" & _
                    "Hist.SalesDate as SalDate, " & _
                    "OrderNumber as ONr, " & _
                    "P.ProductNumber AS PNr," & _
                    "P.Matchcode as ProdMatchcode, " & _
                    "HistPos.Quantity as PosQuantity, " & _
                    "HistPos.Unit as PosUnit, " & _
                    "HistPos.Price as PosPrice, " & _
                    "CASE HistPos.OrderPosCombID WHEN 0 THEN Hist.CurrencyUnit ELSE NULL END AS CurrencyUnit, " & _
                    "HistPos.PriceUnit as HistPriceUnit, " & _
                    "HistPos.Discount as HistPosDiscount, " & _
                    "HistPos.InternalPrice as HistPosIntPrice, " & _
                    "P.ProductID AS ProductID"

            sTables = " tdStatSalesOrderHistory AS Hist " & _
                    " INNER JOIN tdStatSalesOrderPosHistory AS HistPos ON Hist.OrderID = HistPos.OrderID" & _
                    " INNER JOIN tdProducts AS P ON HistPos.ProductID = P.ProductID" & _
                    " INNER JOIN tdSalesOrderTypes AS Type ON Hist.OrderType = Type.OrderType " & _
                    " INNER JOIN tdCustomers C ON Hist.CustomerID=C.CustomerID "

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)


            Dim l As Integer = 0

            Do Until rs.EOF
                l = l + 1

                oRow = oListView.oRows.oAddRow(Str(l))
                'oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, False)
                oRow.sUserData = rs("ProductID").sValue

                rs.MoveNext()
            Loop

            rs.Close()

            'oListView.oRows.SetRecordset(rs, True)
            'rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function

        Private Function msGetClause(ByVal oClientInfo As ClientInfo, ByVal sParams As String) As String

            Dim sClause As String = ""
            Dim asParams As String() = sParams.Split(";"c)

            Dim sAddressID As String = "-1"
            Dim dtFromDate As Date
            Dim dtToDate As Date
            Dim sAllDivisions As String
            Dim sSearchText As String

            If asParams.Length > 4 Then
                sAddressID = asParams(0)
                dtFromDate = gdtCDate(asParams(1))
                dtToDate = gdtCDate(asParams(2))
                sAllDivisions = asParams(3)
                sSearchText = asParams(4)
            Else
                Return "1=2"
            End If

            If gbDate(dtFromDate) Then
                sClause = sClause.gsClauseAnd("Hist.SalesDate >= " & dtFromDate.gs2Sql())
            End If

            If gbDate(dtToDate) Then
                sClause = sClause.gsClauseAnd("Hist.SalesDate <= " & dtToDate.gs2Sql())
            End If

            If sAllDivisions = "false" Then
                sClause = sClause.gsClauseAnd("Hist.DivisionNr = " & oClientInfo.oClientProperties.lDivisionNr)
            End If

            If sSearchText <> "" Then
                sSearchText = sSearchText.Replace("*", "%")
                Dim sSearchValue As String = (sSearchText & "%").gs2Sql()
                sClause = sClause.gsClauseAnd("(Matchcode like " & sSearchValue & _
                                              " OR Name1 like " & sSearchValue & _
                                              " OR Name2 like " & sSearchValue & _
                                              " OR Productnumber like " & sSearchValue & ")")
            End If



            '--------------- Add Permission Clause --------------------
            Dim sPermissionClause As String
            sPermissionClause = myfactory.Sys.Permissions.EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Products")
            If sPermissionClause <> "" Then
                sPermissionClause = Replace(sPermissionClause, "tdProducts.", "P.")
                sClause = sClause.gsClauseAnd(sPermissionClause)
            End If
            '-----------------------------------------------------------


            sClause = sClause.gsClauseAnd("Type.StateOutput IN (4,6,7)")
            sClause = sClause.gsClauseAnd("C.AddressID=" & sAddressID.gs2Sql())

            Return sClause

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
