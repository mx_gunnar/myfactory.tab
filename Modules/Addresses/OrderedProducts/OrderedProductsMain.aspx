<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Addresses.OrderedProductsMain" CodeFile="OrderedProductsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="99%" height="97%">
        <tr height="50px" valign="top">
            <td width="700px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Addresses/OrderedProducts/dlgOrderedProducts.xml" />
            </td>
            <td width="*">
            </td>
        </tr>
        <tr height="*">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstOrderedProducts" sListViewDataPage="OrderedProductsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="30px">
            <td>
                <myfactory:wfButton ID="cmdEntityEdit" sStyle="width: 150px;" runat="server" sOnClick="mOnEntityEdit();">
                </myfactory:wfButton>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td align="right" width="*" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
