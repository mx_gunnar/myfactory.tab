'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AddressOpenItemsDetailsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AddressOpenItemsDetailsData
'--------------------------------------------------------------------------------
' Created:      12.07.2012 12:56:32, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Addresses

    Partial Class AddressOpenItemsDetailsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim lOpenPosId As Integer = glCInt(oListView.sUserData)
            Dim sWKZ As String
            Dim oRow As AspListViewRow

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AddressOpenItemsDetailsData.aspx"
            oListView.bAutoHideNavigation = True
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 15


            '---- Columns ------------------
            oListView.oCols.oAddCol("DokumentNr", "DokumentNr", "*", , , True, False)
            oListView.oCols.oAddCol("DocumentDate", "Datum", "120", wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("InvoicedInternal", "Rechnung", "120", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("PayedInternal", "Zahlung", "120", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("CurrUnitExternal", "WKZ", "60", , , True, False)

            '---- Data ---------------------
            sWKZ = myfactory.Sys.Main.Currencies.gsCurrUnitInternal(oClientInfo, -1)

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "BookingDate"
            End If

            sFields = "OpenItemSubID AS RowID, " & _
                    "DocumentNr, " & _
                    "DocumentDate, " & _
                    "InvoicedInternal, " & _
                    "PayedInternal, " & _
                    "'' as WKZ"

            sTables = "tdOpenItemsDebtSub"

            sClause = sClause.gsClauseAnd("OpenItemID=" & lOpenPosId.gs2Sql())

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs("RowID").sValue)
                oRow.SetRecord(rs, True)
                oRow.Value(4) = sWKZ
                rs.MoveNext()
            Loop

            rs.Close()



            '---------- sum rows ----------

            rs = DataMethods.grsGetDBRecordset(oClientInfo, "SUM(InvoicedInternal), SUM(PayedInternal)", sTables, sClause, "OpenItemId")

            If Not rs.EOF Then
                Dim cSumInvoice, cSumPayed As Decimal

                cSumInvoice = rs(0).cValue
                cSumPayed = rs(1).cValue
                rs.Close()

                oRow = oListView.oRows.oAddRow("-1")
                oRow.bGroupStyle = True
                oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Summe")
                oRow.Value(2) = gsFormatNumber(cSumInvoice, 2)
                oRow.Value(3) = gsFormatNumber(cSumPayed, 2)
                oRow.Value(4) = sWKZ

                oRow = oListView.oRows.oAddRow("-2")
                oRow.bGroupStyle = True
                oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Summe offen")
                oRow.Value(2) = gsFormatNumber(cSumInvoice - cSumPayed, 2)
                oRow.Value(4) = sWKZ
            End If

            '--------------------------------

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
