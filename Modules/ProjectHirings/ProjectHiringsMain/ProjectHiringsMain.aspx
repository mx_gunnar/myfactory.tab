<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.ProjectHirings.ProjectHiringsMain" CodeFile="ProjectHiringsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" style="table-layout: fixed;" cellspacing="2" cellpadding="2"
        width="98%" height="97%">
        <tr valign="top">
            <td width="99%">
                <myfactory:wfListView ID="lstMain" runat="server" sListViewDataPage="ProjectHiringsData.aspx" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td width="200px" id="trNaviLeft">
                <myfactory:wfLabel runat="server" ID="lblNaviLeft" sStyle="margin-left:10px;margin-bottom:30px">
                </myfactory:wfLabel>
            </td>
            <td align="right" width="*" id="trNaviRight">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeft" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNavi">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
