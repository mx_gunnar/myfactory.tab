'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectHiringsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProjectHiringsData
'--------------------------------------------------------------------------------
' Created:      20.07.2012 16:51:42, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.ProjectHirings

    Partial Class ProjectHiringsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim sSubText As String
            Dim lProjectID As Integer = glCInt(oListView.sUserData)

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProjectHiringsData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.lPageSize = 15

            '---- Columns ------------------
            oListView.oCols.oAddCol("TimeTypeDesc", "Zeittyp", "120", , , True, False)
            oListView.oCols.oAddCol("EntityDesc", "Bezug", "*", , , False, False)
            oListView.oCols.oAddCol("WorkingDate", "Tag", "120", , , True, False)
            oListView.oCols.oAddCol("StartTime", "Von", "80", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeTime, , True, False)
            oListView.oCols.oAddCol("EndTime", "Bis", "80", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeTime, , True, False)
            oListView.oCols.oAddCol("AmUn", "Menge", "80", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeString, wfEnumAligns.wfAlignRight, True, False)

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDel", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50"

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "WorkingDate desc, StartTime desc, EndTime desc"
            End If

            sFields = "EntryID AS RowID," & _
                        "TimeTypeDesc, " & _
                        "'' as EntityDesc," & _
                        "WorkingDate,  " & _
                        "StartTime, " & _
                        "EndTime, " & _
                        "Amount," & _
                        "'...' AS Det, " & _
                        "'x' as Del, " & _
                        "tdHiringTimes.EntityID as hiddenEntityID," & _
                        "RecordID as hiddenRecordID, " & _
                        "Unit as hiddenUnit"

            sTables = "tdHiringTimes " & _
                "INNER JOIN tdHiringTimeTypes ON tdHiringTimeTypes.TimeTypeID=tdHiringTimes.TimeTypeID " & _
                "LEFT OUTER JOIN tdResources ON tdResources.ResourceID=tdHiringTimes.ResourceID"


            If lProjectID > 0 Then
                sClause = "((tdHiringTimes.EntityID=6100 AND RecordID=" & lProjectID & ") OR tdHiringTimes.EntityID=3100)"
            Else
                sClause = "(tdHiringTimes.EntityID=6100 OR tdHiringTimes.EntityID=3100)"
            End If
            sClause = sClause.gsClauseAnd("tdResources.UserInitials=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
            sClause = sClause.gsClauseAnd("WorkingDate >= " & Date.Now.AddDays(-5).gs2Sql)

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF

                If rs("hiddenEntityID").lValue = 3100 Then
                    sSubText = DataMethods.gsGetDBValue(oClientInfo, "SO.OrderNumber + ', ' + SOP.PosNumber + ', ' + SOP.Name1", _
                                "tdSalesOrderPos SOP INNER JOIN tdSalesOrders SO ON SO.OrderID=SOP.OrderID",
                                "SOP.OrderPosID=" & rs("hiddenRecordID").lValue)
                Else
                    sSubText = Entities.gsGetEntityDesc(oClientInfo, rs("hiddenEntityID").lValue, rs("hiddenRecordID").lValue, wfEnumEntityDescType.wfEntityDescTypeAuto)
                End If

                rs("EntityDesc").Value = sSubText

                'get TimeUnit (default time unit: h )
                Dim sUnit As String = rs("hiddenUnit").sValue
                If sUnit = "" Then
                    sUnit = "h"
                End If

                rs("Amount").Value = gsFormatNumber(rs("Amount").cValue, 2) & sUnit

                rs.MoveNext()
            Loop

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
