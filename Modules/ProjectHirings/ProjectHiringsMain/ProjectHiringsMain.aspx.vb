'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectHiringsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProjectHiringsMain
'--------------------------------------------------------------------------------
' Created:      20.07.2012 16:51:07, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.ProjectHirings

    Partial Class ProjectHiringsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad()"

            ReDim Me.asPageParams(1)
            ReDim Me.asTerms(1)
            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "M�chten Sie diesen Eintrag wirklich l�schen?")

            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))
            Me.asPageParams(0) = lProjectID.ToString

            If lProjectID <> 0 Then

                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                '-------- write navi for projects ---------------
                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProjectDetails", "../../../", "ProjectID=" & lProjectID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                            "ModuleName,Edition,PartnerID,ModuleID", _
                            "tsTabModules", _
                            "ParentModule='ProjectDetails' AND ModuleName <> 'Projects_HiringTimes' ", _
                            , "ShowIndex", myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)

                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "ProjectID=" & lProjectID))

                    frs.MoveNext()
                Loop

                Me.lblNaviSub.sTranslatedText = osHTML.sString
                osHTML.bAppend("</nobr>")

                Me.lblNaviSub.sTranslatedText = osHTML.sString
                Me.lblNaviLeft.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "Projects_HiringTimes_New", "../../../", "ProjectID=" & lProjectID)

            End If


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
