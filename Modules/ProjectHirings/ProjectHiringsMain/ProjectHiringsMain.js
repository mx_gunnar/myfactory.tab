﻿
function mOnLoad()
{

    if (msPageParams[0] == '' || msPageParams[0] == '0')
    {
        document.getElementById("trNavi").style.display = "none"
    }

    mRefreshLstMain();
}

function mRefreshLstMain()
{
    gListViewSetUserData('lstMain', msPageParams[0]);
    gListViewLoadPage('lstMain', -1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == 'CmdDet')
    {
        mNavigateToDetails(sItemID);
    }
    else if (sColID == 'CmdDel')
    {
        mOnDelete(sItemID);
    }
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mOnDelete(sItemID)
{
    if (window.confirm(msTerms[0]) == false)
    {
        return;
    }

    var sURL = '../ProjectHiringNew/ProjectHiringsNewProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=Delete&EntryID=" + sItemID;
    sRes = gsCallServerMethod(sURL, '');
    
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }
    else
    {
        mRefreshLstMain();
    }
} 

function mNavigateToDetails(sEntryID)
{

    var sURL = '../ProjectHiringNew/ProjectHiringsNew.aspx' +
                '?ClientID=' + msClientID +
                '&ProjectID=' + msPageParams[0] +
                '&EntryID=' + sEntryID;

    document.location = sURL;
}
