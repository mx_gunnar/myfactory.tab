﻿var mOiginalValues;


function mOnLoad() 
{
    if (msPageParams[0] != '0') 
    {
        mOiginalValues = gsXMLDlgParams(frmMain, '', false);
    }

    mRefreshStandardPrice();
}


function mOnOK() 
{
    var sURL = 'ProjectHiringAddOnsProcess.aspx';
    var sRes;
    var sNewValues;

    if (msPageParams[1] == '0') 
    {
        sURL = sURL + "?Cmd=SaveNew&HiringTimeEntryID=" + msPageParams[0];
        sNewValues = gsXMLDlgParams(frmMain, '', false);

        sRes = gsCallServerMethod(sURL, sNewValues);
    }
    else {
        sURL = sURL + "?Cmd=SaveChange&AddOnEntryID=" + msPageParams[1];
        sNewValues = gsXMLDlgParams(frmMain, '', false);

        sRes = gsCallServerMethod(sURL, mOiginalValues + ';' + sNewValues);
    }

    var bOK = mHandleResult(sRes);

    if (bOK) {
        mNavigateBack();
    }
}

function mHandleResult(sRes) {

    if (sRes.substr(0, 3) != 'OK;') {
        alert(sRes.replace('ERR;', ''));
        return false;
    }
    return true;
}


function mNavigateBack()
 {
     var sURL = '../ProjectHiringNew/ProjectHiringsNew.aspx?ClientID=' + msClientID + '&EntryID=' + msPageParams[0];

    document.location = sURL;
}

function mOnSetDirty() 
{
    if ("cboAddOnID" == event.srcElement.id) 
    {
        mRefreshStandardPrice();
    }
}

function mRefreshStandardPrice() {

    var sPrice = '';

    if (msPageParams[0] != '0') {
        
        var AddOnEntryID = document.all.cboAddOnID.value;
        var sURL = 'ProjectHiringAddOnsProcess.aspx?Cmd=StandardPrice&HiringTimeEntryID=' + msPageParams[0] + '&AddOnEntryID=' + AddOnEntryID;
        sPrice = gsCallServerMethod(sURL, '');
    }

    document.all.txtStandardPrice.value = sPrice;
}