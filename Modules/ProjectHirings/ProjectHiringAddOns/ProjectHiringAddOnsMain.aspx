<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.ProjectHirings.ProjectHiringAddOnsMainaspx" CodeFile="ProjectHiringAddOnsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="100%" height="97%">
        <tr valign="top" height="200px">
            <td>
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/ProjectHirings/ProjectHiringAddOns/DlgProjectHiringAddOns.xml" />
                </form>
            </td>
        </tr>

        <tr height="120px" valign="bottom">
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdOK" sOnClick="mOnOK()" runat="server" sText=" OK " />
                        </td>
                        <td>
                            <myfactory:wfButton ID="cmdCancel" sOnClick="mNavigateBack()" runat="server" sText="Abbrechen" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</body>
</html>
