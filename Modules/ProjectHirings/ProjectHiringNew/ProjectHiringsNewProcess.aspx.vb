'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectHiringsNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for ProjectHiringsNewProcess
'--------------------------------------------------------------------------------
' Created:      23.07.2012 11:12:08, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.ProjectHirings

    Partial Class ProjectHiringsNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================


        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Me.Request.QueryString("Cmd")
            Dim lEntryID As Integer = glCInt(Me.Request.QueryString("EntryID"))
            Dim lProjectID As Integer = glCInt(Me.Request.QueryString("ProjectID"))

            Select Case sCmd
                Case "New"
                    Return msSaveNewEntry(oClientInfo)
                Case "Change"
                    Return msChangeEntry(oClientInfo, lEntryID)
                Case "Delete"
                    Return msDeleteEntry(oClientInfo, lEntryID)
                Case "GetSalesOrderPosCombo"
                    Return msGetSalesOrderPosCombo(oClientInfo, lProjectID)
                Case "GetProjectDataCombo"
                    Dim sProjectSearch As String = Me.Request.QueryString("ProjectSearch")
                    Return msGetProjectDataCombo(oClientInfo, sProjectSearch)
            End Select

            Return "ERR; unknown command"

        End Function


        Private Function msGetProjectDataCombo(ByVal oClientInfo As ClientInfo, ByVal sProjectSearch As String) As String

            Dim sTables As String
            Dim sFields As String
            Dim sClause As String = ""
            Dim sNames As String = ""
            Dim sValues As String = ""
            Dim sSearch As String = sProjectSearch
            Dim rs As Recordset


            If GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabProjectHiringCboWithCustomer", False) Then
                sFields = "ProjectID,ProjectNumber + ', ' + LEFT(p.Matchcode,30) + ', ' + LEFT(a.Matchcode, 30) as Name"
            Else
                sFields = "ProjectID,ProjectNumber + ', ' + LEFT(p.Matchcode,30)  as Name"
            End If



            sTables = "tdProjects p INNER JOIN tdCustomers c ON p.CustomerID=c.CustomerID " & _
                                "INNER JOIN tdAddresses a ON c.AddressID=a.AddressID"

            If Not String.IsNullOrEmpty(sSearch) Then

                sSearch = sSearch.Replace("*", "%")
                sSearch = "%" & sSearch & "%"
                sSearch = sSearch.gs2Sql

                sClause = sClause.gsClauseOr("p.Matchcode like " & sSearch)
                sClause = sClause.gsClauseOr("p.ProjectNumber like " & sSearch)
                sClause = sClause.gsClauseOr("a.Matchcode like " & sSearch)
                sClause = "(" & sClause & ")"
            End If
            sClause = sClause.gsClauseAnd("p.Inactive=0")

            rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause, , "ProjectNumber")

            While Not rs.EOF
                sValues = sValues & ";" & rs("ProjectID").sValue.Replace(";", ".")
                sNames = sNames & ";" & rs("Name").sValue.Replace(";", ".")
                rs.MoveNext()
            End While

            sValues = sValues.Replace("|", " ")
            sNames = sNames.Replace("|", " ")

            Return sValues & "|" & sNames

        End Function


        Private Function msGetSalesOrderPosCombo(ByVal oClientInfo As ClientInfo, ByVal lProjectID As Integer) As String

            Dim sTables As String
            Dim sFields As String
            Dim sClause As String
            Dim sNames As String = ""
            Dim sValues As String = ""
            Dim rs As Recordset

            sFields = "SOP.OrderPosID, " & _
                        "SOP.Name1, " & _
                        "SO.OrderNumber,  " & _
                        "SOP.PosNumber"

            sTables = "tdSalesOrderPos SOP " & _
                        "INNER JOIN tdSalesOrders SO ON SO.OrderID=SOP.OrderID " & _
                        "INNER JOIN tdSalesOrderTypes SOT ON SOT.OrderType=SO.OrderType " & _
                        "INNER JOIN tdSalesOrderMainPos SOMP ON SOMP.MainPositionID=SOP.MainPosID"

            sClause = "SO.ProjectID=" & lProjectID
            sClause = sClause.gsClauseAnd("SOMP.Completed1=0")
            sClause = sClause.gsClauseAnd("SOMP.State1>State2")
            sClause = sClause.gsClauseAnd("SOT.OrderType=2")

            rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause, , "SOP.Name1")

            Do Until rs.EOF

                sValues = sValues & ";" & rs(0).sValue.Replace(";", ".")

                'show only Name1 (Name1 + Number + PosNr is to long for iPhone)
                sNames = sNames & ";" & rs(1).sValue.Replace(";", ".")

                'sNames = sNames & ";" & rs(2).sValue.Replace(";", ".") & _
                '    ", " & rs(3).sValue.Replace(";", ".") & _
                '    ", " & rs(1).sValue.Replace(";", ".")

                rs.MoveNext()
            Loop

            sValues = sValues.Replace("|", " ")
            sNames = sNames.Replace("|", " ")

            Return sValues & "|" & sNames

        End Function

        Private Function msSaveNewEntry(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '------------------------------------



            '-------- correct time values? ----------
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Startzeit ung�ltig")
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Endzeit ung�ltig")

            Dim dtDateStart As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime"))
            Dim dtDateend As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime"))

            If dtDateend <= dtDateStart Then
                Return Dictionary.gsTranslate(oClientInfo, "Die Endzeit muss gr��er als die Startzeit sein")
            End If
            '-----------------------------------



            '------- get additional information for calculation -----------------
            Dim lTimeTypeId As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboTimeTypeID"))

            Dim lProjectID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboProjectID"))
            Dim lOrderPosID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboSalesOrderPosID"))

            If lProjectID = 0 Then Return Dictionary.gsTranslate(oClientInfo, "Sie m�ssen ein Projekt angeben")

            Dim tsAmount As TimeSpan = New TimeSpan(dtDateend.Ticks - dtDateStart.Ticks)
            '-------------------------------------------------------------------



            '-------------- build insert statement -----------------
            Dim sFields As String = ""
            Dim sValues As String = ""
            Dim sMsg As String = ""

            If Not TableDefFunctions.gbBuildInsertExpression(oClientInfo, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\hrm\hiring\tableHiringTimes.xml", _
                                                             sFields, sValues, sMsg, True) Then
                Return sMsg
            End If

            sFields = "RecordID, " & sFields & ", EntryID, Amount, UserInitials, ResourceID, IsRunning,  IsDayToDay, EntryType, EmployeeID, HiringID, EntityID"

            If lOrderPosID <> 0 Then
                sValues = lOrderPosID & "," & sValues
            Else
                If lProjectID <> 0 Then
                    sValues = lProjectID & "," & sValues
                End If
            End If

            Dim lNewEntryID As Integer = RecordID.glGetNextRecordID(oClientInfo, "tdHiringTimes")
            sValues = sValues & ", " & lNewEntryID
            sValues = sValues & ", " & msGetFormatedAmount(oClientInfo, lTimeTypeId, tsAmount)
            sValues = sValues & ", " & oClientInfo.oClientProperties.sCurrentUser.gs2Sql
            sValues = sValues & ", " & DataMethods.glGetDBValue(oClientInfo, "ResourceID", "tdResources", "UserInitials=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql).gs2Sql
            sValues = sValues & ", 2,  0, 0, 0, 0,"

            If lOrderPosID <> 0 Then
                sValues = sValues & "3100" 'sales order pos entity
            Else
                sValues = sValues & "6100" 'project entity
            End If

            If DataMethods2.glDBExecute(oClientInfo, "INSERT INTO tdHiringTimes (" & sFields & ") VALUES(" & sValues & ")", myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceData) = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim Speichern")
            End If

            Return "OK;" & lNewEntryID
        End Function


        Private Function msChangeEntry(ByVal oClientInfo As ClientInfo, ByVal lEntryID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '-----------------------------------



            '-------- correct time values? ----------
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Startzeit ung�ltig")
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Endzeit ung�ltig")

            Dim dtDateStart As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime"))
            Dim dtDateend As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime"))

            If dtDateend <= dtDateStart Then
                Return Dictionary.gsTranslate(oClientInfo, "Die Endzeit muss gr��er als die Startzeit sein")
            End If
            '----------------------------------------



            '--------- get additional informations for validation and calculation --------
            Dim lProjectID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboProjectID"))
            Dim lOrderPosID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboSalesOrderPosID"))

            If lProjectID = 0 Then Return Dictionary.gsTranslate(oClientInfo, "Sie m�ssen ein Projekt angeben")

            Dim lProjectIDOriginal As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootOrig, "cboProjectID"))
            Dim lOrderPosIDOriginal As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootOrig, "cboSalesOrderPosID"))

            Dim lTimeTypeId As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboTimeTypeID"))

            Dim tsAmount As TimeSpan = New TimeSpan(dtDateend.Ticks - dtDateStart.Ticks)
            '------------------------------------------------------------------------------



            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\hrm\hiring\tableHiringTimes.xml", sQry, sMsg, True) Then
                Return sMsg
            End If

            If lOrderPosID <> lOrderPosIDOriginal And lOrderPosID <> 0 Then

                If sQry <> "" Then
                    sQry = sQry & ", "
                End If

                sQry = sQry & "RecordID=" & lOrderPosID
                sQry = sQry & ", EntityID=3100"

            Else

                If lProjectID <> lProjectIDOriginal Or lOrderPosID = 0 Then

                    If sQry <> "" Then
                        sQry = sQry & ", "
                    End If

                    sQry = sQry & "RecordID=" & lProjectID
                    sQry = sQry & ", EntityID=6100"

                End If

            End If

            'calculate new amount in correct unit!
            If sQry <> "" Then
                Dim sAmount = msGetFormatedAmount(oClientInfo, lTimeTypeId, tsAmount)
                sQry = sQry & ",Amount=" & sAmount.gs2Sql
            End If

            If sQry = "" Then
                Return "OK; no changes"
            End If

            If DataMethods2.glDBUpdate(oClientInfo, "tdHiringTimes", sQry, "EntryID=" & lEntryID) = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim Speichern")
            End If

            Return "OK; updated for id: " & lEntryID
        End Function


        Private Function msDeleteEntry(ByVal oClientInfo As ClientInfo, ByVal lEntryID As Integer) As String

            If DataMethods2.glDBDelete(oClientInfo, "tdHiringTimes", "EntryID=" & lEntryID) <> 1 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim L�schen")
            End If

            Return "OK; delete"
        End Function


        Private Function msGetFormatedAmount(ByVal oClientInfo As ClientInfo, ByVal lTimeTypeID As Integer, ByVal timeSpanAmount As TimeSpan) As String

            Dim cAmount = timeSpanAmount.TotalHours
            Dim lFactor = DataMethods.glGetDBValue(oClientInfo, "UnitFactor", "tdHiringTimeTypes HTT INNER JOIN tdTimeUnits TU ON HTT.Unit=TU.UnitName", "HTT.TimeTypeID=" & lTimeTypeID)

            If lFactor > 0 Then
                cAmount = timeSpanAmount.TotalSeconds / lFactor
            End If

            Return gsFormatNumber(cAmount, 2).Replace(",", ".")
        End Function
    End Class

End Namespace

'================================================================================
'================================================================================
