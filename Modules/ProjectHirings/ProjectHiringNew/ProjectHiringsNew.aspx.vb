'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectHiringsNew.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProjectHiringsNew
'--------------------------------------------------------------------------------
' Created:      23.07.2012 10:13:43, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem.AspMobileControls

Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.Sys.Permissions


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.ProjectHirings

    Partial Class ProjectHiringsNew
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lEntryID As Integer = glCInt(Request.QueryString("EntryID"))
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))


            ReDim Me.asPageParams(4)
            Me.gAddScriptLink("wfDlgParams.js", True)

            Me.asPageParams(3) = lProjectID.ToString

            '------------ laod data ----------
            If lEntryID <> 0 Then
                Dim sFields As String
                Dim rs As Recordset
                Dim sClause As String = "EntryID=" & lEntryID.gs2Sql()
                Dim sTables As String
                Dim sValues As String

                sFields = "'0' as SalesOrderPosID, '' as ProjectID, *"
                sTables = "tdHiringTimes "

                rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause)

                rs("StartTime").Value = gsGetTimeFromDate(rs("StartTime").dtValue)
                rs("EndTime").Value = gsGetTimeFromDate(rs("EndTime").dtValue)

                If rs("EntityID").lValue = 3100 Then

                    Dim lSalesOrderPosID As Integer = rs("RecordID").lValue
                    If lProjectID = 0 Then
                        lProjectID = DataMethods.glGetDBValue(oClientInfo, "ProjectID", "tdSalesOrderPos INNER JOIN tdSalesOrders ON tdsalesorders.OrderID = tdSalesOrderPos.OrderID", "tdSalesOrderPos.OrderPosId=" & lSalesOrderPosID)
                    End If

                    rs("SalesOrderPosID").Value = lSalesOrderPosID
                    rs("ProjectID").Value = lProjectID

                    Me.asPageParams(2) = lSalesOrderPosID.ToString

                Else
                    rs("ProjectID").Value = rs("RecordID").lValue
                End If

                sValues = DataTools.gsRecord2Xml(rs)

                Me.asPageParams(0) = lEntryID.ToString
                Me.asPageParams(1) = rs("ActivityID").sValue
                Me.dlgMain.sValues = sValues
            Else
                If GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabProjectHiringCboWithCustomer", False) Then
                    Me.dlgMain.oDialog.oField("ProjectID").oMember.Prop("SRC") = "Projects3"
                Else
                    Me.dlgMain.oDialog.oField("ProjectID").oMember.Prop("SRC") = "Projects2"
                    Me.dlgMain.oDialog.oField("ProjectID").oMember.Prop("CLAUSE") = "tdProjects.Inactive=0"
                End If

            End If
            '---------------------------------

            Me.sOnLoad = "mOnLoad()"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
