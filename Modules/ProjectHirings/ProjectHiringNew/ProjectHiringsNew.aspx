<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.ProjectHirings.ProjectHiringsNew" CodeFile="ProjectHiringsNew.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" style="table-layout:fixed;" cellpadding="2" width="98%" height="97%">
        <tr valign="top" height="200px">
            <td width="98%"  style="overflow-x: auto;" >
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/ProjectHirings/ProjectHiringNew/dlgProjectHiringsNew.xml" />
                </form>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr height="35px" valign="top">
            <td>
                <myfactory:wfButton runat="server" ID="cmdNewAddOn" sOnClick="mOnNewAddOn();" sText="Zulage hinzufügen" sStyle="Width: 160px;"></myfactory:wfButton>
            </td>
        </tr>
        <tr>
            <td>
                <myfactory:wfListView runat="server" ID="lstAddOns" sListViewDataPage="ProjectHiringsNewAddOnData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom">
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdOK" sOnClick="mOnOK()" runat="server" sText=" OK " />
                        </td>
                        <td>
                            <myfactory:wfButton ID="cmdCancel" sOnClick="mNavigateToList()" runat="server" sText="Abbrechen" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
