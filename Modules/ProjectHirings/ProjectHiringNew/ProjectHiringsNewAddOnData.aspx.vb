'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectHiringsNewAddOnData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProjectHiringsNewAddOnData
'--------------------------------------------------------------------------------
' Created:      07.02.2013 17:41:46, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.ProjectHirings

    Partial Class ProjectHiringsNewAddOnData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================
        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sCurrUnit As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProjectHiringsNewAddOnData.aspx"
            oListView.lPageSize = 5
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True


            '---- Columns ------------------
            oListView.oCols.oAddCol("AddOnDesc", "Zulage", "*", , , False, False)
            oListView.oCols.oAddCol("Amount", "Menge", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("BaseUnit", "ME", "50", , , False, False)
            oListView.oCols.oAddCol("Price", "Preis", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("TotalPrice", "Gesamt", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("CurrUnit", "", "50", , , False, False)

            oCol = oListView.oCols.oAddCol("CmdDetail", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDelete", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50"

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If


            sFields = "HTA.EntryID AS RowID, " & _
                        "HA.AddOnDesc, " & _
                        "HTA.Amount, " & _
                        "P.BaseUnit, " & _
                        "HTA.PriceNew, " & _
                        "'666.31' AS TotalPrice, " & _
                        "'CHF' AS CurrUnit, " & _
                        "'' as CmdDetails, " & _
                        "'x' as CmdDelete, " & _
                        "HA.AddOnID"


            sTables = "tdHiringTimeAddOns HTA INNER JOIN tdHiringAddONs HA ON HA.AddOnID=HTA.AddOnId INNER JOIN tdProducts P on P.ProductID=HA.ProductId"
            sClause = "HTA.HiringTimeEntryID=" & oListView.sUserData.gs2Sql

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Dim cPrice As Decimal

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs("RowID").sValue)
                oRow.SetRecord(rs, True)

                If rs("PriceNew").sValue = "" Then
                    'get default price
                    cPrice = mcGetStandardPrice(oClientInfo, glCInt(oListView.sUserData), rs("AddOnID").lValue)
                Else
                    cPrice = rs("PriceNew").cValue
                End If

                oRow.Value(3) = cPrice
                oRow.Value(4) = cPrice * rs("Amount").cValue
                oRow.Value(5) = sCurrUnit

                rs.MoveNext()
            Loop
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


        Private Function mcGetStandardPrice(ByVal oClientInfo As ClientInfo, ByVal lProjectHiringEntryID As Integer, ByVal lAddOnID As Integer) As Decimal

            '----------- get default price ---------------
            Dim lProductID As Integer = DataMethods.glGetDBValue(oClientInfo, "ProductID", "tdHiringAddOns", "AddOnID=" & lAddOnID.gs2Sql)
            Dim lCustomerID, lTaxBusinessPartner As Integer
            Dim cDefaultPrice As Decimal
            Dim lEntityID, lRecordID As Integer
            Dim sCurrUnit As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)

            lEntityID = DataMethods.glGetDBValue(oClientInfo, "EntityID", "tdHiringTimes", "EntryID=" & lProjectHiringEntryID)
            lRecordID = DataMethods.glGetDBValue(oClientInfo, "RecordID", "tdHiringTimes", "EntryID=" & lProjectHiringEntryID)


            Select Case lEntityID
                Case 1100 'customers
                    lCustomerID = lRecordID
                Case 3100
                    'get cutomer from salesorder
                    lCustomerID = DataMethods.glGetDBValue(oClientInfo, "tdProjects.CustomerID", _
                            "tdProjects,tdsalesorders,tdSalesOrderPos", _
                            "tdSalesOrderPos.OrderID=tdSalesOrders.OrderID AND tdProjects.ProjectID=tdsalesorders.ProjectID AND tdSalesOrderPos.OrderPosID=" & lRecordID)
                Case 6100
                    'get cutomer from project
                    lCustomerID = DataMethods.glGetDBValue(oClientInfo, "CustomerID", "tdProjects", "ProjectID=" & lRecordID)

            End Select

            If lCustomerID <> 0 Then
                lTaxBusinessPartner = DataMethods.glGetDBValue(oClientInfo, "Taxation", "tdCustomers", "CustomerID=" & lCustomerID)

                If lEntityID = 6100 Then
                    cDefaultPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                                    lCustomerID, _
                                    lProductID, 1, sCurrUnit, False, _
                                    oClientInfo.oClientProperties.dtBookingDate, _
                                    lTaxBusinessPartner, 2, lRecordID)
                Else
                    cDefaultPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                                    lCustomerID, _
                                    lProductID, _
                                    1)
                End If

            End If

            Return cDefaultPrice

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
