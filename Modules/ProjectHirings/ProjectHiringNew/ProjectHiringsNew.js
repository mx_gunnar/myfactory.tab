﻿
var mOiginalValues;

function mOnLoad()
{

    // got project id ?  set project and refresh sales order positions
    if (msPageParams[3] != '' && msPageParams[3] != '0')
    {
        gSetCboValue(document.getElementById("cboProjectID"), msPageParams[3]);
        mRefreshSalesOrderPosList();
    }

    mRefreshActivitys();

    // entry id not empty?
    if (msPageParams[0] != '')
    {
        mRefreshSalesOrderPosList();

        // set activity id
        if (msPageParams[1] != '')
        {
            gSetCboValue(document.getElementById("cboActivityID"), msPageParams[1]);
        }

        // set sales-order-pos id
        if (msPageParams[2] != '' && msPageParams[2] != '0')
        {
            gSetCboValue(document.getElementById("cboSalesOrderPosID"), msPageParams[2]);
        }

        // hold original values
        mOiginalValues = gsXMLDlgParams(frmMain, '', false);


        // load AddOn list
        mRefreshAddOnList();
    }
}

function mRefreshAddOnList()
{
    // load AddOn list
    gListViewSetUserData('lstAddOns', msPageParams[0]);
    gListViewLoadPage('lstAddOns', 1);

}

function mRefreshProjectDataCombo()
{
    var sProjectSearch = document.all.txtProjectSearch.value;

    var sURL = 'ProjectHiringsNewProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=GetProjectDataCombo&ProjectSearch=" + gsEncodeStringURL(sProjectSearch);
    sRes = gsCallServerMethod(sURL, '');

    gClearCombobox2(document.getElementById("cboProjectID"));

    var sValues = sRes.split("|")[0];
    var sNames = sRes.split("|")[1];

    var asValues = sValues.split(";");
    var asNames = sNames.split(";");

    for (i = 0; i < asValues.length; i++)
    {
        var sValue = asValues[i];
        var sName;
        try
        {
            sName = asNames[i];
        }
        catch (Ex)
        {
            sName = " ";
        }

        gCreateNewCboOption(document.getElementById("cboProjectID"), sValue, sName, i, false);
    }
}

function mOnSetDirty()
{
    if (event.srcElement.id == 'cboTimeTypeID')
    {
        mRefreshActivitys();
    }
    else if (event.srcElement.id == 'cboProjectID')
    {
        mRefreshSalesOrderPosList();
    }
    else if (event.srcElement.id == 'txtProjectSearch')
    {
        mRefreshProjectDataCombo();
    }
}

function mRefreshSalesOrderPosList()
{
    var ProjectID = document.all.cboProjectID.value;
    // get values and descriptions from server process

    var sURL = 'ProjectHiringsNewProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=GetSalesOrderPosCombo&ProjectID=" + ProjectID;
    sRes = gsCallServerMethod(sURL, '');

    gClearCombobox2(document.getElementById("cboSalesOrderPosID"));

    var sValues = sRes.split("|")[0]
    var sNames = sRes.split("|")[1]

    for (i = 0; i < sValues.split(";").length; i++)
    {
        var sValue = sValues.split(";")[i];
        var sName;
        try
        {
            sName = sNames.split(";")[i];
        }
        catch (Ex)
        {
            sName = " ";
        }

        gCreateNewCboOption(document.getElementById("cboSalesOrderPosID"), sValue, sName, i, false);
    }
}


function mRefreshActivitys()
{
    var TimeTypeValue = document.all.cboTimeTypeID.value;

    gRefreshDataCombo('cboActivityID', 'ProjectActivitiesActive', TimeTypeValue, false, '');
}

function mOnOK()
{
    var sURL = 'ProjectHiringsNewProcess.aspx';
    var sRes;
    var sNewValues;
    var lOrderPosID = document.all.cboSalesOrderPosID.value;

    if (msPageParams[0] != '')
    {
        sURL = sURL + "?Cmd=Change&&EntryID=" + msPageParams[0];

        if (lOrderPosID != '' && lOrderPosID != '0')
        {
            sURL = sURL + "&OrderPosID=" + lOrderPosID;
        }

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, mOiginalValues + ';' + sNewValues);

    }
    else
    {
        sURL = sURL + "?Cmd=New";

        if (lOrderPosID != '' && lOrderPosID != '0')
        {
            sURL = sURL + "&OrderPosID=" + lOrderPosID;
        }

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, sNewValues);
    }

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }
    else
    {
        mNavigateToList();
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{

    if (sColID == 'CmdDetail')
    {
        mNavigateToAddOnDetails(sItemID);
    }
    if (sColID == 'CmdDelete')
    {
        mOnDeleteAddOn(sItemID);
    }
}

function mOnDeleteAddOn(sAddOnEntryID)
{

    var sURL = '../ProjectHiringAddOns/ProjectHiringAddOnsProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=Delete&AddOnEntryID=" + sAddOnEntryID;
    sRes = gsCallServerMethod(sURL, '');

    mHandleResult(sRes);
    mRefreshAddOnList();
}

function mHandleResult(sRes)
{

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes.replace('ERR;', ''));
        return false;
    }
    return true;
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToAddOnDetails(sItemID);
}

function mOnNewAddOn()
{

    var lHiringEntryID = '';

    if (msPageParams[0] == '')
    {
        // need to save first
        var sURL = 'ProjectHiringsNewProcess.aspx';
        var sRes;
        var sNewValues;
        var lOrderPosID = document.all.cboSalesOrderPosID.value;

        sURL = sURL + "?Cmd=New";

        if (lOrderPosID != '' && lOrderPosID != '0')
        {
            sURL = sURL + "&OrderPosID=" + lOrderPosID;
        }

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, sNewValues);

        if (mHandleResult(sRes))
        {
            lHiringEntryID = sRes.split(";")[1];
        }
        else
        {
            return;
        }
    }
    else
    {
        lHiringEntryID = msPageParams[0];
    }

    var sURLAddOn = '../ProjectHiringAddOns/ProjectHiringAddOnsMain.aspx?ClientID=' + msClientID + '&HiringEntryID=' + lHiringEntryID;
    document.location = sURLAddOn;
}

function mNavigateToAddOnDetails(sAddOnEntryID)
{
    var sURL = '../ProjectHiringAddOns/ProjectHiringAddOnsMain.aspx?ClientID=' + msClientID + '&AddOnEntryID=' + sAddOnEntryID + '&HiringEntryID=' + msPageParams[0];

    document.location = sURL;
}

function mNavigateToList()
{
    var sURL = '../ProjectHiringsMain/ProjectHiringsMain.aspx?ClientID=' + msClientID + '&ProjectID=' + msPageParams[3];

    document.location = sURL;
}
