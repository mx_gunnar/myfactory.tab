<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.AppointmentGroup.AppointmentGroupMain" CodeFile="AppointmentGroupMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" style="table-layout: fixed;" height="98%" cellspacing="2"
        cellpadding="2" width="99%">
        <tr height="50px" valign="top">
            <td width="99%">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/AppointmentGroup/AppointmentGroupMain/dlgAppointmentGroup.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <div id="divAppointments" runat="server">
                </div>
            </td>
        </tr>
        <tr height="*" valign="bottom">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfButton sText="Zur�ck" runat="server" ID="cmdResourceBack"  sOnClick="OnResBack();">
                            </myfactory:wfButton>
                        </td>
                        <td class="tdResourceHeader">
                            <label runat="server" ID="lblResourceInfo"></label>
                        </td>
                        <td>
                            <myfactory:wfButton sText="Weiter" runat="server" ID="cmdResourceForward" sOnClick="OnResForward();">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
