﻿
var lPage = 1;
var lTotalCount = 0;
var lPageSize = 0;

function mOnLoad() {

    if (msPageParams[0] != '0') {
        lPage = parseInt(msPageParams[0]);
    }
    mLoadTable();
}

function mLoadTable() 
{
    var sURL = 'AppointmentGroupProcess.aspx?Cmd=Table&ResourceGroup=' + document.all.cboResourceGrp.value;
    sURL = sURL + '&GoToDate=' + document.all.txtGoToDate.value;
    sURL = sURL + '&Page=' + lPage;

    var sRes = gsCallServerMethod(sURL, '');
    if (sRes.substr(0, 4) == 'ERR;') {
        alert(sRes);
        return; 
    }

    document.all.divAppointments.innerHTML = sRes.split("|")[1];

    // 12 1 5
    var sInfo = sRes.split("|")[0];
    lTotalCount = sInfo.split(";")[0];
    lPageSize = sInfo.split(";")[2];

    mRefreshResourceCount();
}


function mOnSetDirty() 
{
    mLoadTable();
}


function mRefreshResourceCount() {

    var _lPage = parseInt(lPage);
    var _lTotalCount = parseInt(lTotalCount);
    var _lPageSize = parseInt(lPageSize);

    var lFrom = 1;
    if (_lPage > 1)
        lFrom = ((_lPage - 1) * _lPageSize) + 1;

    var lTo = 1;

    lTo = _lPage * _lPageSize;

    if (lTo > _lTotalCount)
        lTo = _lTotalCount;

    document.all.lblResourceInfo.innerText = lFrom + " - " + lTo + " von " + _lTotalCount;

    // set navi button posibilities
    if (_lPage == 1)
        document.getElementById("cmdResourceBack").disabled = true;
    else
        document.getElementById("cmdResourceBack").disabled = false;

    if (lTo == _lTotalCount)
        document.getElementById("cmdResourceForward").disabled = true;
    else
        document.getElementById("cmdResourceForward").disabled = false;

}

function mOnAppointmentClick(appointmentID, resourceID) 
{
    var sURL = '../../Appointments/AppointmentDetails/AppointmentDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AppointmentPage=' + lPage +
                '&ResourceID=' + resourceID +
                '&AppointmentDate=' + document.all.txtGoToDate.value +
                '&RecordID=' + appointmentID;

    document.location = sURL;
}


function cmdForward_OnClick() 
{
    mChangeDate(7);
}

function cmdBack_OnClick() 
{
    mChangeDate(-7);
}

function mChangeDate(days)
{
    var sURL = 'AppointmentGroupProcess.aspx?Cmd=ChangeDate&Date=' + document.all.txtGoToDate.value;
    sURL = sURL + '&Amount=' + days;

    document.all.txtGoToDate.value = gsCallServerMethod(sURL, '');
    mLoadTable();
}

function OnResBack()
{
    lPage = lPage - 1;
    mLoadTable();
 }

function OnResForward() 
{
    lPage = lPage + 1;
    mLoadTable();
}


function mAddNewAppointment(resourceID) {


    var sURL = '../../Appointments/AppointmentDetails/AppointmentDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&AppointmentPage=' + lPage +
                '&ResourceID=' + resourceID +
                '&AppointmentDate=' + document.all.txtGoToDate.value;
 
    document.location = sURL;
}