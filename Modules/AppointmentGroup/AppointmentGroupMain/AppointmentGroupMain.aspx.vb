'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AppointmentGroupMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AppointmentGroupMain
'--------------------------------------------------------------------------------
' Created:      17.05.2013 11:54:19, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.AppointmentGroup

    Partial Class AppointmentGroupMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad
            Me.sOnLoad = "mOnLoad()"

            Dim lPage As Integer = glCInt(Request.QueryString("Page"))
            Dim sDate As String = Request.QueryString("Date")

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = lPage.ToString

            If gbDate(sDate) Then
                Me.dlgMain.oDialog.oField("GoToDate").oMember.Prop("Value") = gdtCDate(sDate)
            Else
                'appointment view always from monday - sunday ? 
                If GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabAppointmentGroupFromMonday", False) Then
                    Me.dlgMain.oDialog.oField("GoToDate").oMember.Prop("Value") = Date.Today.AddDays((Date.Now.DayOfWeek - 1) * -1)
                End If
            End If

            Me.dlgMain.oDialog.oField("ResourceGrp").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_AppointmentGroup_ResourceGrp", False)


        End Sub



    End Class

End Namespace

'================================================================================
'================================================================================
