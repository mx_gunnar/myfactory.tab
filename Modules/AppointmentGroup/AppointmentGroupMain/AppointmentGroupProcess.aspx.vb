'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AppointmentGroupProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for AppointmentGroupProcess
'--------------------------------------------------------------------------------
' Created:      17.05.2013 12:28:09, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.AppointmentGroup

    Partial Class AppointmentGroupProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Table"
                    Return mDrawAppointmentTable(oClientInfo, Request.QueryString("ResourceGroup"), Request.QueryString("GoToDate"), glCInt(Request.QueryString("Page")))
                Case "ChangeDate"
                    Return mChangeDate(Request.QueryString("Date"), Request.QueryString("Amount"))
            End Select

            Return "ERR;unknown cmd"

        End Function

        Private Function mChangeDate(ByVal sDate As String, ByVal sAmount As String) As String

            Dim lAmount As Integer = glCInt(sAmount)
            Dim dtDate As Date = Date.Now

            If gbDate(sDate) Then
                dtDate = gdtCDate(sDate)
            End If

            Return dtDate.AddDays(lAmount).ToShortDateString()
        End Function

        Private Function mDrawAppointmentTable(ByVal oClientInfo As ClientInfo, ByVal sResourceGroup As String, ByVal sGoToDate As String, ByVal lPage As Integer) As String

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_AppointmentGroup_ResourceGrp", sResourceGroup, False)


            Dim rsResources As Recordset = Nothing
            Dim rsAppointments As Recordset = Nothing
            Dim dtGoToDate As Date = Date.Now
            Dim sTable As String = "tdResources"
            Dim sFields As String = "ResourceID, ResourceDesc"
            Dim sClause As String = "ResourceGroupID=" & sResourceGroup.gs2Sql & " AND InActive=0"
            Dim bColorToggle As Boolean = True
            Dim lVisibleDayCount As Integer = 7
            Dim lPageSize As Integer = 5
            Dim cColumnWidth As Double = 100 / lVisibleDayCount
            Dim sClassTDHeader As String = ""
            Dim lResourceCount As Integer = 0

            sClause = sClause.gsClauseAnd(EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Resources"))

            If gbDate(sGoToDate) Then
                dtGoToDate = gdtCDate(sGoToDate)
            End If

            If Not String.IsNullOrEmpty(sResourceGroup) Then
                rsResources = DataMethods.grsGetDBRecordsetPage(oClientInfo, lPage, lPageSize, sFields, sTable, sClause)
            End If

            If rsResources Is Nothing Then
                Return "keine Resourcen gefunden"
            End If


            '============= load appointments for all found resources by only one db-select (performance) ==========
            Dim sResourceIDs As String = "("
            Do Until rsResources.EOF
                If sResourceIDs.Length > 1 Then
                    sResourceIDs = sResourceIDs & ","
                End If
                sResourceIDs = sResourceIDs & rsResources("ResourceID").sValue
                rsResources.MoveNext()
            Loop
            rsResources.MoveFirst()
            sResourceIDs = sResourceIDs & ")"

            Dim sOrderAppointments As String = "MIN(TaskDate), MIN(TaskTimeFrom)"
            Dim sFieldsAppointments As String = "TaskID, ResourceID, MIN(TaskDate) AS TaskDate, MAX(TaskDueDate) AS TaskDueDate, MIN(TaskDesc) AS TaskDesc, MIN(TaskLocation) AS TaskLocation, MIN(IsPrivate) AS Private"
            Dim sTablesAppointments As String = "tsTasks INNER JOIN tdResourceAllocation ON tdResourceAllocation.AllocationPlanID=tsTasks.ResourcePlanningID"
            Dim sClauseAppointments As String = "ResourceID IN " & sResourceIDs
            sClauseAppointments = DataTools.gsClauseAnd(sClauseAppointments, "TaskDueDate>=" & DataTools.gsDate2Sql(dtGoToDate))
            sClauseAppointments = DataTools.gsClauseAnd(sClauseAppointments, "TaskDate<=" & DataTools.gsDate2Sql(dtGoToDate.AddDays(lVisibleDayCount)))
            rsAppointments = DataMethods.grsGetDBRecordset(oClientInfo, sFieldsAppointments, sTablesAppointments, sClauseAppointments, "TaskID, ResourceID", sOrderAppointments)


            '============= build table ===============
            Dim ofsTable As New FastString
            ofsTable.bAppend("<table width=""100%""  cellpedding=""0"" cellspacing=""1""  >")
            ofsTable.bAppend("<tr >")
            For iCol As Integer = 0 To lVisibleDayCount - 1 Step 1
                If iCol = 0 Then
                    ofsTable.bAppend("<td class=""tdResourceHeader"">")
                    ofsTable.bAppend("Resource")
                    ofsTable.bAppend("</td>")
                End If
                ofsTable.bAppend("<td class=""tdResourceHeader"">")
                ofsTable.bAppend(dtGoToDate.AddDays(iCol).ToString("dd.MM.yy ddd"))
                ofsTable.bAppend("</td>")
            Next
            ofsTable.bAppend("</tr>")

            Do Until rsResources.EOF

                Dim bFullAccess = ItemApprovals.gbCheckItemPermission(oClientInfo, 9500, rsResources("ResourceID").lValue, Permissions.wfEnumPermissions.wfPermissionAllowed)
                Dim bReadOnlyAccess = ItemApprovals.gbCheckItemPermission(oClientInfo, 9500, rsResources("ResourceID").lValue, Permissions.wfEnumPermissions.wfPermissionReadOnly)

                If bColorToggle Then
                    ofsTable.bAppend("<tr class=""trResourceRow1"">")
                Else
                    ofsTable.bAppend("<tr class=""trResourceRow2"">")
                End If

                If Not bFullAccess AndAlso Not bReadOnlyAccess Then
                    rsResources.MoveNext()
                    Continue Do
                End If

                bColorToggle = Not bColorToggle

                For iCol As Integer = 0 To lVisibleDayCount - 1 Step 1

                    If iCol = 0 Then

                        If Not bFullAccess And Not bReadOnlyAccess Then
                            sClassTDHeader = "disabledTDHead"
                        Else
                            sClassTDHeader = ""
                        End If

                        ofsTable.bAppend("<td style=""padding:5px;"" class=""" & sClassTDHeader & """  width=" & cColumnWidth & "%>" & "<p>" & rsResources("ResourceDesc").sValue & "</p>")

                        If bFullAccess Then
                            ofsTable.bAppend("<div onclick=""mAddNewAppointment(" & rsResources("ResourceID").sValue & ")"" class=""divAppointmentNew"">+ " & Dictionary.gsTranslate(oClientInfo, "Neu") & "</div>")
                        End If

                        ofsTable.bAppend("</td>")
                    End If

                    ofsTable.bAppend("<td style=""vertical-align:top;"" width=" & cColumnWidth & "%>")

                    If bFullAccess Or bReadOnlyAccess Then

                        'add all appointments for this day
                        Do Until rsAppointments.EOF
                            If rsAppointments("ResourceID").lValue = rsResources("ResourceID").lValue Then

                                If (rsAppointments("TaskDate").dtValue.Date = dtGoToDate.AddDays(iCol).Date Or
                                    rsAppointments("TaskDueDate").dtValue.Date = dtGoToDate.AddDays(iCol).Date) Or
                                    (rsAppointments("TaskDueDate").dtValue.Date > dtGoToDate.AddDays(iCol).Date AndAlso rsAppointments("TaskDate").dtValue.Date < dtGoToDate.AddDays(iCol).Date) Then

                                    ofsTable.bAppend("<div class=""divAppointment"" ")
                                    If (bReadOnlyAccess Or bFullAccess) AndAlso Not rsAppointments("Private").bValue Then
                                        ofsTable.bAppend("onclick=""mOnAppointmentClick(" & rsAppointments("TaskID").lValue & "," & rsResources("ResourceID").sValue & ");"" ")
                                    End If
                                    ofsTable.bAppend(">")

                                    ofsTable.bAppend(rsAppointments("TaskDate").dtValue.ToString("HH:mm") & "-" & rsAppointments("TaskDueDate").dtValue.ToString("HH:mm"))
                                    ofsTable.bAppend("<br />")
                                    ofsTable.bAppend("<label class=""lblAppointmentDesc"">")

                                    If rsAppointments("Private").bValue Then
                                        ofsTable.bAppend("- " & Dictionary.gsTranslate(oClientInfo, "Privat") & " -")
                                    Else
                                        ofsTable.bAppend(rsAppointments("TaskDesc").sValue)
                                    End If

                                    ofsTable.bAppend("<label/>")
                                    ofsTable.bAppend("</div>")
                                End If
                            End If
                            rsAppointments.MoveNext()
                        Loop
                        rsAppointments.MoveFirst()

                    End If

                    ofsTable.bAppend("</td>")
                Next

                ofsTable.bAppend("</tr>")
                rsResources.MoveNext()
            Loop
            rsAppointments.Close()
            rsResources.Close()
            ofsTable.bAppend("</table>")


            'get total-record count
            Dim lTotalCount As Integer = 0
            Dim rsCount As FastRecordset = DataMethods.gfrsGetFastRecordset(oClientInfo, "ResourceID", sTable, sClause)
            Do Until rsCount.EOF

                Dim bFullAccess = ItemApprovals.gbCheckItemPermission(oClientInfo, 9500, rsCount(0).lValue, Permissions.wfEnumPermissions.wfPermissionAllowed)
                Dim bReadOnlyAccess = ItemApprovals.gbCheckItemPermission(oClientInfo, 9500, rsCount(0).lValue, Permissions.wfEnumPermissions.wfPermissionReadOnly)
                If Not bFullAccess AndAlso Not bReadOnlyAccess Then
                    rsCount.MoveNext()
                    Continue Do
                End If
                lTotalCount = lTotalCount + 1
                rsCount.MoveNext()
            Loop
            rsCount.Close()

            Return lTotalCount & ";" & lPage & ";" & lPageSize & "|" & ofsTable.sString

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
