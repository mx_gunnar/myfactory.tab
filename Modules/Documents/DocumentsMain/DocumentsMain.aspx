<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Documents.DocumentsMain" CodeFile="DocumentsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">

    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server"></myfactory:wfPageHeader>
</head>


<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" style="table-layout: fixed;" cellspacing="2" cellpadding="2" width="99%">
        <tr height="30px">
            <td width="700px" id="tdDlgMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/documents/documentsmain/DlgDocumentsMain.xml"></myfactory:wfXmlDialog>
            </td>
            <td width="*">&nbsp;</td>
        </tr>
        <tr height="*" valign="top">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="DocumentsListData.aspx"></myfactory:wfListView>
            </td>
        </tr>

        <tr valign="top" height="40px">
            <td colspan="2">
                <table width="100%" height="100%">
                    <tr>                       
                        <td align="right" width="180px">
                            <myfactory:wfButton ID="cmdNewDocument" sOnClick="mNewDocument();" sStyle="width: 150px;" sText="Neues Dokument" Visible="false"
                                runat="server">
                            </myfactory:wfButton>
                        </td>                    
                    </tr>
                </table>
            </td>
        </tr>

        <tr height="120px" valign="bottom" id="trNavi">

            <td align="right" width="*" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<"></myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete"></myfactory:wfLabel>
            </td>
        </tr>
    </table>

</body>
</html>
