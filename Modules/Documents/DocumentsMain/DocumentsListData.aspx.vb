'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    DocumentsListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for DocumentsListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		14.03.2013;  ABuhleier;  Doc-Reference Clause expanded (search all documents (Customer, Supplier, Representative....) for the AddressID)
'               7/28/2014, JSchweighart: clause for support case documents added                
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Documents

    Partial Class DocumentsListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sAddClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "DocumentsListData.aspx"
            oListView.lPageSize = 12
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True


            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("CreationDate", "Datum", "80", wfEnumDataTypes.wfDataTypeDate, , True)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("DocumentDesc", "Bezeichnung", "*", , , True)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("DocumentExtension", "Typ", "80", , , True)
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("CmdMail", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonEMail)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDoc", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            Dim sSubClause As String
            Dim asData As String()
            Dim lFilter As Integer
            Dim sSearch As String = ""
            Dim lRecordID As Integer = 0
            Dim lProjectID As Integer = 0
            Dim lSupportCaseID As Integer = 0

            asData = Split(oListView.sUserData & ";;;;;;;;", ";")
            If UBound(asData) > 0 Then
                lFilter = glCInt(asData(0))
                sSearch = asData(1)
                lRecordID = glCInt(asData(2))
                lProjectID = glCInt(asData(3))
                lSupportCaseID = glCInt(asData(4))

            End If

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "D.CreationDate"
                oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            If sOrder = "CreationDate" Then
                sOrder &= ", D.DocumentID"
            ElseIf sOrder = "D.CreationDate DESC" Then
                sOrder &= ", D.DocumentID DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "D.DocumentID AS RowID,D.CreationDate,D.DocumentDesc,tsDocumentTypes.DocumentExtension"
            sTables = "tdDocuments D" & _
                        " LEFT JOIN tsDocumentTypes ON D.DocumentType = tsDocumentTypes.DocumentTypeID"

            '---- clause ---------
            sClause = "SystemDoc=0"

            If lFilter = 2 Then 'favorites
                sClause = DataTools.gsClauseAnd(sClause, "IsFavorite=-1")
            ElseIf lFilter = 1 Then 'my favorites
                sClause = DataTools.gsClauseAnd(sClause, " EXISTS(SELECT * FROM tdDocumentsUserSettings " & _
                                                            "WHERE UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
                                                            " AND IsMyFavorit=-1 " & _
                                                            " AND tdDocumentsUserSettings.DocumentID=D.DocumentID) ")
            End If

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "D.DocumentDesc like " & DataTools.gsStr2Sql(sSearch & "%"))
            End If


            '---- permissions ----
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Documents")
            If sSubClause <> "" Then
                sSubClause = sSubClause.Replace("tdDocuments.", "D.")
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If


            'if lRecordID (AddressID) is set -> find all Doc-References for this Address!
            If lRecordID > 0 Then

                sTables = sTables & " INNER JOIN tdDocumentReferences DR ON DR.DocumentID=D.DocumentID"
                sAddClause = "DR.TypeID=1 AND ReferenceID=" & lRecordID.gs2Sql

                Dim rsAddressClause As FastRecordset
                Dim lReferenceTypeID As Integer
                Dim sIDs As String


                ' OR Customer
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "CustomerID", "tdCustomers", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Customers'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                ' OR Supplier
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "SupplierID", "tdSuppliers", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Suppliers'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                ' OR Prospects
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "ProspectID", "tdProspects", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Prospects'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                ' OR Employees
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "EmployeeID", "tdEmployees", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Employees'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                ' OR Representatives
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Representatives'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                ' OR Competitors
                rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "CompetitorID", "tdCompetitors", "AddressID=" & lRecordID)
                lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Competitors'")
                sIDs = "0"
                Do Until rsAddressClause.EOF
                    sIDs = sIDs & "," & rsAddressClause(0).sValue
                    rsAddressClause.MoveNext()
                Loop
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")


                If GeneralProperties.gbGetGeneralProperty(oClientInfo, "ShowContactDocumentsInList") Then

                    ' OR Contacts
                    rsAddressClause = DataMethods.gfrsGetFastRecordset(oClientInfo, "ContactID", "tdContacts", "AddressID=" & lRecordID)
                    lReferenceTypeID = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Contracts'")
                    sIDs = "0"
                    Do Until rsAddressClause.EOF
                        sIDs = sIDs & "," & rsAddressClause(0).sValue
                        rsAddressClause.MoveNext()
                    Loop
                    sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID IN (" & sIDs & ")")

                End If

                sClause = sClause.gsClauseAnd(sAddClause)

            End If


            ' build clause if a project id is set
            If lProjectID > 0 Then
                sTables = sTables & " INNER JOIN tdDocumentReferences DR ON DR.DocumentID=D.DocumentID"
                Dim lReferenceTypeID As Integer = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='Projects'")
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID =" & lProjectID & "")

                sClause = sClause.gsClauseAnd(sAddClause)
            End If

            'build clause for support case id not null
            If lSupportCaseID > 0 Then

                sTables += " INNER JOIN tdDocumentReferences DR ON DR.DocumentID=D.DocumentID"
                Dim lReferenceTypeID As Integer = DataMethods.glGetDBValue(oClientInfo, "TypeID", "tsDocumentReferenceTypes", "EntityName='SupportCases'")
                sAddClause = sAddClause.gsClauseOr("DR.TypeID=" & lReferenceTypeID & " AND ReferenceID =" & lSupportCaseID & "")

                sClause = sClause.gsClauseAnd(sAddClause)

            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
