'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    DocumentsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for DocumentsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		7/28/2014, JSchweighart: refactored build bottom navigation logic into msBuildBottomNavigation()
'               7/28/2014, JSchweighart: build bottom navigation for support case documents added
'               8/20/2014, JSchweighart: parameters for document upload added
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Documents

    Partial Class DocumentsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim osHTML As New FastString
            Dim lAddressID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))
            Dim lSupportCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lDocumentReferenceID As Integer
            Dim lDocumentTypeID As Integer

            If lAddressID > 0 Then

                'build list view filter
                Dim sAddressDesc = DataMethods.gsGetDBValue(oClientInfo, "Matchcode", "tdAddresses", "AddressID=" & lAddressID.gs2Sql)
                Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("Value") = sAddressDesc
                Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = 3

                'show "New Document" button
                Me.cmdNewDocument.Visible = True

                'build bottom navigation
                osHTML = msBuildBottomNavigation("AddressDetails", "Address_Documents", lAddressID)

                'set values for tdDocumentReferences
                lDocumentReferenceID = lAddressID
                lDocumentTypeID = 1 'DocumentReferenceType "Addresses"

            End If

            If lProjectID > 0 Then

                'build list view filter
                Dim sProjectDesc As String = DataMethods.gsGetDBValue(oClientInfo, "ProjectNumber + ', ' + Matchcode", "tdProjects", "ProjectID=" & lProjectID.gs2Sql)
                Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("Value") = sProjectDesc
                Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = 3

                'show "New Document" button
                Me.cmdNewDocument.Visible = True

                'build bottom navigation
                osHTML = msBuildBottomNavigation("ProjectDetails", "Project_Documents", lProjectID)

                'set values for tdDocumentReferences
                lDocumentReferenceID = lProjectID
                lDocumentTypeID = 4 'DocumentReferenceType "Projects"

            End If

            If lSupportCaseID > 0 Then

                'build liest view filter
                Dim sSupportCaseDesc = DataMethods.gsGetDBValue(oClientInfo, "CaseNumber + ', ' + CaseDesc", "tdSupportCases", "CaseID=" & lSupportCaseID.gs2Sql)
                Me.dlgMain.oDialog.oField("Matchcode").oMember.Prop("Value") = sSupportCaseDesc
                Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = 3

                'show "New Document" button
                Me.cmdNewDocument.Visible = True

                'build bottom navigation
                osHTML = msBuildBottomNavigation("SupportCase", "SupportCases_Documents", lSupportCaseID)

                'set values for tdDocumentReferences
                lDocumentReferenceID = lSupportCaseID
                lDocumentTypeID = 10 'DocumentReferenceType "SupportCases"

            End If


            ReDim Me.asPageParams(5)
            Me.asPageParams(0) = Str(lAddressID)
            Me.asPageParams(1) = Str(lProjectID)
            Me.asPageParams(2) = Str(lSupportCaseID)
            Me.asPageParams(3) = Str(lDocumentReferenceID)
            Me.asPageParams(4) = Str(lDocumentTypeID)

            Me.sOnLoad = "mOnLoad()"

            Me.lblNaviSub.sTranslatedText = osHTML.sString

        End Sub

        ''' <summary>
        ''' build bottom navigation for a given module
        ''' </summary>
        ''' <param name="sParentModule">parent module name</param>
        ''' <param name="sModulName">module name</param>
        ''' <param name="lRecordID">documents foreign key</param>
        ''' <returns>TODO: there might be no need for any return value</returns>
        ''' <remarks></remarks>
        Private Function msBuildBottomNavigation(ByVal sParentModule As String, ByVal sModulName As String, ByVal lRecordID As Integer) As FastString

            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean
            Dim sRecordIDName As String = "RecordID"

            If sModulName = "SupportCases_Documents" Then sRecordIDName = "CaseID"

            osHTML.bAppend("<nobr>")

            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, sParentModule, "../../../", "RecordID=" & lRecordID))
            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition,PartnerID,ModuleID", _
                                                   "tsTabModules", _
                                                   String.Format("ParentModule='{0}' AND ModuleName<>'{1}'", sParentModule, sModulName), _
                                                   , "ShowIndex", _
                                                   wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", String.Format("{0}={1}", sRecordIDName, lRecordID)))
                frs.MoveNext()
            Loop

            osHTML.bAppend("</nobr>")

            Return osHTML

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
