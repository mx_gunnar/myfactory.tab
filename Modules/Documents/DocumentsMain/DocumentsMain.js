﻿// DocumentsMain.js (c) myfactory 2010

function mOnLoad() {

    if (msPageParams[0] == '0' && msPageParams[1] == '0' && msPageParams[2] == '0')
    {
        document.getElementById("tdlabMatchcode").style.display = "none";
        document.getElementById("tdMatchcode").style.display = "none";
        document.getElementById("trNavi").style.display = "none";
    }

    mRefreshList()
}

function mRefreshList()
{
    var sData;

    sData = document.all.cboFilter.value +
                ';' + document.all.txtSearch.value +
                ';' + msPageParams[0] +
                ';' + msPageParams[1] +
                ';' + msPageParams[2];

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnListViewClick(sListView, sItemID)
{
    mOpenDocument(sItemID);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == 'CmdMail')
    {
        // open new email with this documant as attachment
        mNavigateToNewMail(sItemID)
    }

    if (sColID == 'CmdDoc')
    {
        mOpenDocument(sItemID);
    }
}

function mNavigateToNewMail(DocID)
{
    var sURL = '../../Email/EMailNew/EMailNewMain.aspx' +
                '?ClientID=' + msClientID + "&MailID=0&DocumentID=" + DocID;

    document.location = sURL;
}

function mOpenDocument(lDocumentID)
{
    window.resizeTo(10, 10);

    //------ check permission to read ----------
    if (lDocumentID != '')
    {
        var sPerm = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' + 'document_Process.aspx?DocumentID=' + lDocumentID + '&Method=CheckPermissionRead', '');
        if (sPerm != ';-1')
        {
            if (sPerm != ';0')
                alert(sPerm);
            else
                alert(msTerms[3]);
            return;
        };
    }
    else
        return;

    //------ get link and open document --------
    var sLink = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' +
					'document_Process.aspx?DocumentID=' + lDocumentID +
					'&Method=GetDocumentLink&LocalMode=-1', '');
    if (sLink.substr(0, 1) != ';')
    {
        alert(sLink);
        return;
    }
    sLink = sLink.substr(1);
    if (sLink != '')
        window.open(sLink, '', 'menubar=no,fullscreen=no,scrollbars=yes,location=no,toolbar=no,resizable=yes,status=yes,left=50,top=50,width=600,height=400')
    //------------------------------------------	
}

function mNewDocument()
{
    var sDocumentReferenceID = msPageParams[3];
    var sDocumentTypeID = msPageParams[4];
    var sUrl = '../DocumentNew/DocumentNewMain.aspx?ClientID=' + msClientID;

    sUrl += '&DocumentReferenceID=' + sDocumentReferenceID;
    sUrl += '&DocumentTypeID=' + sDocumentTypeID

    document.location = sUrl;
}