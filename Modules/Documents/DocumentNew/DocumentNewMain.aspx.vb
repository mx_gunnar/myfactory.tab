'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    DocumentNewMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for DocumentNewMain
'--------------------------------------------------------------------------------
' Created:      10.07.2014 17:21:03, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		8/19/2014, JSchweighart: document description added
'               8/20/2014, JSchweighart: parameters for document upload added
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Documents

    Partial Class DocumentNewMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim sDocumentReferenceID As String = (Request.QueryString("DocumentReferenceID"))
            Dim sDocumentTypeID As String = (Request.QueryString("DocumentTypeID"))
            Dim sDocumentDesc As String = String.Format("{0}: {1}", Dictionary.gsTranslate(oClientInfo, "Upload vom"), DateAndTime.Now)

            If myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "DocumentUploadAllPermissionsTab", False) = "-1" Then
                Me.dlgMain.oDialog.oField("AllPermissions").oMember.Prop("Value") = "-1"
            End If

            Me.sOnLoad = "mOnLoad();"

            'action="<%=sWebPageRoot%>/IE50/BASE/DOCUMENTS/DocumentUploadProcess.aspx?ClientID=<%=sClientID%>&amp;Private=true&amp;Type=-1"
            Dim sFormAction As String = oClientInfo.sWebPageRoot & _
                                        "/IE50/BASE/DOCUMENTS/DocumentUploadProcess.aspx?ClientID=" & oClientInfo.sClientID & _
                                        "&Private=true&Type=0" & _
                                        "&Name=" & sDocumentDesc & _
                                        "&ReferenceID=" & sDocumentReferenceID & _
                                        "&TypeID=" & sDocumentTypeID

            ReDim Me.asPageParams(5)
            Me.asPageParams(0) = sFormAction
            Me.asPageParams(1) = Dictionary.gsTranslate(oClientInfo, "Datei jetzt hochladen")
            Me.asPageParams(2) = sDocumentDesc
            Me.asPageParams(3) = sDocumentReferenceID
            Me.asPageParams(4) = sDocumentTypeID

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
