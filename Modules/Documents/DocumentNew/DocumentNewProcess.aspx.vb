'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    DocumentNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for DocumentNewProcess
'--------------------------------------------------------------------------------
' Created:      10.07.2014 17:25:35, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Documents

    Partial Class DocumentNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim bAllPermissions As Boolean = (Request.QueryString("AllPermissions") = "true")

            Select Case sCmd
                Case "SaveNewDoc"

                    Dim lDocumentID As Integer = ClientValues.glGetClientValue(oClientInfo, "DocumentUploadID")
                    If lDocumentID <= 0 Then Return Dictionary.gsTranslate(oClientInfo, "Beim Anlegen des Dokumentes ist ein Fehler aufgetreten")

                    Dim sDocumentDesc As String = Request.QueryString("DocumentDesc")
                    Dim sDocumentGroup As String = Request.QueryString("DocumentGroup")
                    bAllPermissions = (Request.QueryString("AllPermissions") = "true")

                    Try

                        oClientInfo.oDataConn.gbBeginTrans(oClientInfo)

                        DataMethods2.glDBUpdate(oClientInfo, "tdDocuments", "DocumentDesc=" & sDocumentDesc.gs2Sql & ",DocumentGroup=" & sDocumentGroup.gs2Sql, "DocumentID=" & lDocumentID)

                        'set permissions for document
                        If bAllPermissions Then
                            myfactory.Sys.Permissions.ItemApprovals.gbSetItemApproval(oClientInfo, 6000, lDocumentID, _
                                myfactory.Sys.Permissions.Permissions.wfEnumPermissions.wfPermissionAllowed, "AL001", -1)
                            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "DocumentUploadAllPermissionsTab", "-1", False)
                        Else
                            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "DocumentUploadAllPermissionsTab", "0", False)
                        End If

                        oClientInfo.oDataConn.gbCommitTrans(oClientInfo)

                    Catch ex As Exception

                        oClientInfo.oDataConn.gbRollbackTrans(oClientInfo)

                        Return Dictionary.gsTranslate(oClientInfo, "Beim Speichern der �nderungen ist ein Fehler augetreten.")

                    End Try

                    Return "OK;"

            End Select

            Return "unknown cmd"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
