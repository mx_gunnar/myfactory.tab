﻿

function mOnLoad()
{
    document.getElementById("txtNewUpload").addEventListener("change", mEnableUpload, false);

    document.all.frmDocUpload.action = msPageParams[0];
    document.all.cmdNewSubmit.style.display = "none";

}

function mEnableUpload(event)
{
    document.getElementById("tdDetails").style.display = "none";
    document.all.cmdNewSubmit.style.display = "";
}

function mOnSubmit()
{    
    document.all.cmdNewSubmit.style.display = "none";
    document.getElementById("tdDetails").style.display = "";
    document.getElementById("txtDocumentDesc").value = msPageParams[2];
}

function mOnUploadToSever()
{
    document.all.frmDocUpload.submit();
}

function mOnSave()
{
    var sParams = "";
    var bAllPermissions = document.getElementById("chkAllPermissions").checked;

    sParams = sParams + "&DocumentGroup=" + document.all.cboDocumentGroup.value;
    sParams = sParams + "&DocumentDesc=" + gsEncodeStringURL(document.all.txtDocumentDesc.value);
    sParams += '&AllPermissions=' + bAllPermissions;

    var sURL = "DocumentNewProcess.aspx?Cmd=SaveNewDoc";
    sURL = sURL + sParams;
    var sRes = gsCallServerMethod(sURL, '');

    if (sRes.substr(0, 3) != 'OK;')
    {
        gAlert(sRes);
    }
    else
    {
        mNavigateBack();
    }
}

function mNavigateBack()
{
    var sURL = "";
    var sDocumentReferenceID = msPageParams[3]
    var sDocumentTypeID = msPageParams[4]

    switch (sDocumentTypeID)
    {
        case "1": //Addresses

            sURL = '../DocumentsMain/DocumentsMain.aspx?ClientID=' + msClientID;
            sURL += '&RecordID=' + sDocumentReferenceID;

            break;

        case "4": //Projects

            sURL = '../DocumentsMain/DocumentsMain.aspx?ClientID=' + msClientID;
            sURL += '&ProjectID=' + sDocumentReferenceID;

            break;

        case "10": //SupportCases

            sURL = '../DocumentsMain/DocumentsMain.aspx?ClientID=' + msClientID;
            sURL += '&CaseID=' + sDocumentReferenceID;

            break;

        default: //Document Module

            sURL = '../DocumentsMain/DocumentsMain.aspx?ClientID=' + msClientID;
    }

    document.location = sURL;
}

