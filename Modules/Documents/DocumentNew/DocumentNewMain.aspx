<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Documents.DocumentNewMain" CodeFile="DocumentNewMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">

    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server"></myfactory:wfPageHeader>
</head>


<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">

    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" style="table-layout: fixed;" width="98%">
        <tr valign="bottom" height="100px">
            <td>
                <form id="frmDocUpload" name="frmDocUpload" method="POST" enctype="multipart/form-data" target="fraHidden">
                    <myfactory:wfXmlDialog runat="server" ID="dlgHeader" sDialog="/tab/modules/documents/documentNew/dlgDocNewHeader.xml">
                    </myfactory:wfXmlDialog>
                </form>
            </td>
        </tr>
        <tr valign="top" height="*">
            <td id="tdDetails" style="display: none;">
                <table width="100%" height="100%">
                    <tr valign="top">
                        <td height="10px">
                            <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/documents/documentNew/dlgDocumentNew.xml"></myfactory:wfXmlDialog>
                        </td>
                    </tr>
                    <tr valign="top" height="10px">
                        <td align="right" valign="top">
                            <myfactory:wfButton ID="cmdSave" runat="server" sOnClick="mOnSave();" sText="Speichern"></myfactory:wfButton>
                        </td>
                    </tr>
                    <tr height="*">
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>
</html>
