<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Finance.FinanceMain" codefile="FinanceMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id="ctlBody" leftMargin="0" topMargin="0" scroll="no" runat="server">
<table class="borderTable" height="100%" cellSpacing="2" cellPadding="2" width="99%">
<tr height="150px" id="trAccounts" runat="server">
	<td>
		<myfactory:wfListView runat="server" ID="lstAccounts" sListViewDataPage="AccountListData.aspx"></myfactory:wfListView>
	</td>
</tr>
<tr id="trPos" height="*">
	<td width="50%">
		<myfactory:wfListView runat="server" ID="lstOpenItemsDebt" sListViewDataPage="OpenItemDebtListData.aspx"></myfactory:wfListView>
	</td>
	<td width="50%">
		<myfactory:wfListView runat="server" ID="lstOpenItemsCred" sListViewDataPage="OpenItemCredListData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
