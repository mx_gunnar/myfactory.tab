'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AccountListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions
Imports myfactory.Sys.Tools.StringFunctions

Imports myfactory.BusinessTasks.Accounting.Main
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Finance

    Partial Class OpenItemDebtListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        Private masCaptions() As String
        Private masUserData() As String
        Private macValues() As Decimal

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim l As Integer
            Dim sCurrUnit As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = False
            oListView.sListViewDataPage = "AccountListData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            mInitData(oClientInfo)

            '---- Columns ------------------
            sCurrUnit = Currencies.gsCurrUnitInternal(oClientInfo, -1)

            oCol = oListView.oCols.oAddCol("Caption", "Offene Posten Kunden", "*")
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("Value", sCurrUnit, "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("cmdDebtDetails", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            For l = 0 To UBound(masCaptions)
                oRow = oListView.oRows.oAddRow(CStr(l))
                oRow.sValue(0) = masCaptions(l)
                oRow.Value(1) = macValues(l)

                oRow.sUserData = masUserData(l)
            Next

            Return True

        End Function

        '================================================================================
        ' Method:       mInitData
        '--------------------------------------------------------------------------------
        ' Purpose:      Init Timespan Values
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub mInitData(ByVal oClientInfo As ClientInfo)

            Dim s, sCaptions, sClause, asData() As String
            Dim l As Integer

            '---- ListView Row Captions ----
            sCaptions = Dictionary.gsTranslateKeepApos(oClientInfo, "alle")
            s = Dictionary.gsTranslateKeepApos(oClientInfo, "seit >$1 Tagen")
            sCaptions += ";" & Replace(s, "$1", 150)
            sCaptions += ";" & Replace(s, "$1", 90)
            sCaptions += ";" & Replace(s, "$1", 60)
            sCaptions += ";" & Replace(s, "$1", 30)
            sCaptions += ";" & Replace(s, "$1", 15)
            sCaptions += ";" & Replace(s, "$1", 5)
            sCaptions += ";" & Dictionary.gsTranslateKeepApos(oClientInfo, "heute")
            s = Dictionary.gsTranslateKeepApos(oClientInfo, "in <$1 Tagen")
            sCaptions += ";" & Replace(s, "$1", 5)
            sCaptions += ";" & Replace(s, "$1", 15)
            sCaptions += ";" & Replace(s, "$1", 30)
            sCaptions += ";" & Replace(s, "$1", 60)
            sCaptions += ";" & Replace(s, "$1", 90)
            sCaptions += ";" & Replace(s, "$1", 150)
            sCaptions += ";" & Dictionary.gsTranslateKeepApos(oClientInfo, "sp�ter")

            masCaptions = Split(sCaptions, ";")

            '---- UserData ----
            s = ",;" & ",-151;" & "-150,-91;" & "-90,-61;" & "-60,-31;" & "-30,-16;" & "-15,-6;" & "-5,0;" & _
                    "1,4;" & "5,14;" & "15,29;" & "30,59;" & "60,89;" & "90,149;" & "150,"
            masUserData = Split(s, ";")

            '---- Values -----
            ReDim macValues(UBound(masUserData))

            For l = 0 To UBound(masUserData)
                sClause = "Completed=0 AND WaitState=0 AND DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr
                asData = Split(masUserData(l), ",")
                If asData(0) <> "" Then
                    sClause = sClause & " AND DueDate>=" & DataTools.gsDate2Sql(Date.Today.AddDays(glCInt(asData(0))))
                End If
                If asData(1) <> "" Then
                    sClause = sClause & " AND DueDate<=" & DataTools.gsDate2Sql(Date.Today.AddDays(glCInt(asData(1))))
                End If

                macValues(l) = DataMethods.gcGetDBValue(oclientinfo, _
                                    "SUM(SumInvoicedInternal-SumPayedInternal)", _
                                    "tdOpenItemsDebtMain", sClause)
            Next

        End Sub



    End Class

End Namespace

'================================================================================
'================================================================================
