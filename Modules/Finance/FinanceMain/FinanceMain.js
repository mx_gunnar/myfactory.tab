﻿// FinanceMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshLists();
}

function mRefreshLists()
{
    gListViewLoadPage('lstAccounts', 1);
    gListViewLoadPage('lstOpenItemsDebt', 1);
    gListViewLoadPage('lstOpenItemsCred', 1);
}

function mOnListViewClick(sListView, sItemID)
{
    mDetails(sListView, sItemID)
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    mDetails(sListView, sItemID)
}

function mDetails(sListView, sItemID)
{
    var sData, sURL;

    sData = gsListViewGetItemUserData(sListView, sItemID);

    if (sListView == 'lstOpenItemsDebt')
    {
        sURL = '../OpenItemListDebt/OpenItemListDebtMain.aspx' +
                '?ClientID=' + msClientID +
                '&Data=' + sData;

        document.location = sURL;

    }

    if (sListView == 'lstOpenItemsCred')
    {
        sURL = '../OpenItemListCred/OpenItemListCredMain.aspx' +
                '?ClientID=' + msClientID +
                '&Data=' + sData;

        document.location = sURL;

    }
}
