'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AccountListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Accounting.Main
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Finance

    Partial Class AccountListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim cBalance As Decimal = 0
            Dim cBalanceSum As Decimal = 0
            Dim sCurrUnit As String

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AccountListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("AccountNumber", "Konto", "60")
            oListView.oCols.oAddCol("AccountDesc", "Bezeichnung", "*")
            oListView.oCols.oAddCol("Balance", "Saldo", "70", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("CurrUnit", "Wkz", "40", , wfEnumAligns.wfAlignCenter)

            '---- Data ---------------------

            sOrder = "AccountNumber"

            sFields = "AccountID AS RowID, AccountNumber, AccountDesc, 0 AS Balance"
            sTables = "tdImpersonalAccounts "

            '---- clause ---------
            Dim bShow As Boolean
            Dim lPosCounter As Integer

            sClause = "IsFavorite=-1 AND AccountType=4"

            sCurrUnit = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                sFields, sTables, sClause, , sOrder)
            Do Until rs.EOF

                cBalance = Balance.gcGetAccountBalanceInternal(oClientInfo, _
                                                                    rs(0).lValue, _
                                                                    wfEnumAccountTypes.wfEnumAccountTypeImpersonal, _
                                                                    oClientInfo.oClientProperties.lDivisionNr, _
                                                                    Date.Today)
                cBalanceSum += cBalance

                lPosCounter += 1
                bShow = (lPosCounter <= (oListView.lPage * oListView.lPageSize) And lPosCounter > ((oListView.lPage - 1) * oListView.lPageSize))

                If bShow Then
                    oRow = oListView.oRows.oAddRow(rs(0).sValue)
                    oRow.SetRecord(rs, True)
                    oRow.Value(2) = cBalance
                    oRow.Value(3) = sCurrUnit

                End If
                rs.MoveNext()
            Loop

            oRow = oListView.oRows.oAddRow("-1")
            oRow.Value(1) = Dictionary.gsTranslate(oClientInfo, "Summe")
            oRow.Value(2) = cBalanceSum
            oRow.Value(3) = sCurrUnit
            oRow.bGroupStyle = True

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
