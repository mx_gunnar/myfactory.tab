<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Finance.OpenItemListDebtMain" codefile="OpenItemListDebtMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="100%" cellSpacing=2 cellPadding=2 width="100%">
<tr height="*">
	<td>
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="OpenItemListDebtData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
