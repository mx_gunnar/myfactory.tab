'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    OpenItemListCredData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for OpenItemListCredData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions
Imports myfactory.Sys.Tools.StringFunctions

Imports myfactory.BusinessTasks.Accounting.Main
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Finance

    Partial Class OpenItemListCredData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder, sTables As String
            Dim sClause As String = ""
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim sUserData, asData() As String

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "OpenItemListCredData.aspx"
            oListView.bTabletMode = True
            oListView.lPageSize = 20
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("SupplierNumber", "Lieferant", "100", , , True)
            oListView.oCols.oAddCol("Matchcode", "Matchcode", "*", , , True)
            oListView.oCols.oAddCol("OpenItemNr", "OP-Nr.", "100", , , True)
            oCol = oListView.oCols.oAddCol("cmdOP", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.bHidden = True

            oListView.oCols.oAddCol("DueDate", "F�llig", "100", wfEnumDataTypes.wfDataTypeDate, , True)
            oListView.oCols.oAddCol("SumInvoicedInternal", "Rechnung", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True)
            oListView.oCols.oAddCol("SumPayedInternal", "Zahlung", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True)
            oListView.oCols.oAddCol("SumOpen", "Offen", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True)

            oListView.oCols.oAddCol("ReminderLevel", "MS", "45", wfEnumDataTypes.wfDataTypeInt, wfEnumAligns.wfAlignCenter, True)

            '---- Data ---------------------
            sUserData = oListView.sUserData
            If sUserData = "" Then Return True

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "SumOpen DESC"
            End If

            For Each oFilter In oListView.oFilters
                If oFilter.sID = "SumOpen" Then
                    sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause("(SumInvoicedInternal - SumPayedInternal)"))
                Else
                    sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
                End If
            Next

            sClause = DataTools.gsClauseAnd(sClause, "Completed=0 AND WaitState=0 AND op.DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
            If sUserData = "OpenItems" Then
                sUserData = ","

                asData = Split(sUserData, ",")
            Else
                asData = Split(sUserData, ",")
            End If

            If asData(0) <> "" Then
                sClause = sClause & " AND DueDate>=" & DataTools.gsDate2Sql(Date.Today.AddDays(glCInt(asData(0))))
            End If
            If asData(1) <> "" Then
                sClause = sClause & " AND DueDate<=" & DataTools.gsDate2Sql(Date.Today.AddDays(glCInt(asData(1))))
            End If

            sTables = "tdOpenItemsCredMain op" & _
                            " INNER JOIN tdSuppliers s ON op.SupplierID=s.SupplierID" & _
                            " INNER JOIN tdAddresses a ON s.AddressID=a.AddressID "

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                "OpenItemID, s.SupplierNumber, a.Matchcode, OpenItemNr, '...', " & _
                                "DueDate,SumInvoicedInternal,SumPayedInternal," & _
                                "(SumInvoicedInternal-SumPayedInternal) AS SumOpen" & _
                                ",op.ReminderLevel", _
                                sTables, sClause, , sOrder)



            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
