﻿
function mOnLoad() 
{
    mRefreshList();
}

function mRefreshList() 
{
    var sParams = document.all.txtdtFrom.value + ";" + document.all.txtdtTo.value;

    gListViewSetUserData('lstMain', sParams);
    gListViewLoadPage('lstMain', 1);
}

function GoBack()
{
    var sURL = '../RepresentativesMain/RepresentativesMain.aspx' +
                '?ClientID=' + msClientID;

    document.location = sURL;
}

function mOnSetDirty() 
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID) 
{

    var sURL = '../../Products/ProductDetails/ProductDetailsMain.aspx' +
                '?ClientID=' + msClientID + '&RecordID=' + sItemID;

    document.location = sURL;
}
