'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    RepresentativeTopProductData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for RepresentativeTopProductData
'--------------------------------------------------------------------------------
' Created:      28.06.2013 11:44:04, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Representatives

    Partial Class RepresentativeTopProductData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim sOrder As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            Dim lIndex As Integer = 0
            Dim lRank As Integer

            Dim sCurrUnitInternal As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            Dim sdtFrom As String = oListView.sUserData.Split(";"c)(0)
            Dim sdtTo As String = oListView.sUserData.Split(";"c)(1)

            Dim dtFrom As Date = Date.Now
            If gbDate(sdtFrom) Then
                dtFrom = gdtCDate(sdtFrom)
            End If
            Dim dtTo As Date = Date.Now
            If gbDate(sdtTo) Then
                dtTo = gdtCDate(sdtTo)
            End If

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.sListViewDataPage = "RepresentativeTopProductData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("Position", "Platz", "50", , , False, False)
            oListView.oCols.oAddCol("ProductNumber", "Artikelnr", "100", , , False, False)

            oCol = oListView.oCols.oAddCol("cmdCustomer", "Kunde", "100", , , False, False)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "70px"

            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , False, False)
            oListView.oCols.oAddCol("SumQuantity", "Anzahl", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("BaseUnit", "BME", "60", , , False, False)
            oListView.oCols.oAddCol("SumTurnover", "Umsatz", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("CurrUnit", "WKZ", "60", , , False, False)

            '---- Data ---------------------
            Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
            Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)

            If lRepresentativeID = 0 Then
                lRepresentativeID = -1
            End If

            sFields = "MIN(P.ProductNumber) AS ProductNumber, MIN(P.Matchcode) As Matchcode, SUM(Quantity) as SumQuantity, SUM(Turnover) AS SumTurnover, MIN(P.BaseUnit) AS BaseUnit, MIN(P.ProductID) AS ProductID"
            sTables = "tdStatSalesDetail SSD INNER JOIN tdProducts P ON P.ProductID=SSD.ProductID"

            sClause = sClause.gsClauseAnd("RepresentativeID=" & lRepresentativeID)
            sClause = sClause.gsClauseAnd("SalesDate>=" & dtFrom.gs2Sql & " AND SalesDate<=" & dtTo.gs2Sql)


            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "SumQuantity desc"
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, "P.ProductID", sOrder)
            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs("ProductID").sValue)
                lIndex = lIndex + 1

                If oListView.lPage > 1 Then
                    lRank = lIndex + ((oListView.lPage - 1) * oListView.lPageSize)
                Else
                    lRank = lIndex
                End If

                oRow.Value(0) = lRank
                oRow.Value(1) = rs("ProductNumber").sValue
                oRow.Value(2) = "..."
                oRow.Value(3) = rs("Matchcode").sValue
                oRow.Value(4) = rs("SumQuantity").cValue
                oRow.Value(5) = " " & rs("BaseUnit").sValue
                oRow.Value(6) = rs("SumTurnover").cValue
                oRow.Value(7) = " " & sCurrUnitInternal

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause, "distinct P.ProductID")

            Return True
        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
