'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    RepresentativeTopProductMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for RepresentativeTopProductMain
'--------------------------------------------------------------------------------
' Created:      28.06.2013 11:43:56, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem.AspSystem

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Representatives

    Partial Class RepresentativeTopProductMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad()"
            Dim sPeriod As String = Request.QueryString("Period")

            Dim dtDateFrom As Date
            Dim dtDateTo As Date

            If sPeriod.Length = 4 Then ' only a year, not a period
                dtDateFrom = New Date(glCInt(sPeriod), 1, 1)
                dtDateTo = dtDateFrom.AddYears(1).AddDays(-1)
            Else
                dtDateFrom = Period.gdtPeriod2Date(oClientInfo, glCInt(sPeriod))
                dtDateTo = dtDateFrom.AddMonths(1).AddDays(-1)
            End If


            Me.dlgMain.oDialog.oField("dtFrom").oMember.Prop("VALUE") = dtDateFrom.ToShortDateString()
            Me.dlgMain.oDialog.oField("dtTo").oMember.Prop("VALUE") = dtDateTo.ToShortDateString()



        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
