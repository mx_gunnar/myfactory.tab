<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Representatives.RepresentativeTopProductMain" CodeFile="RepresentativeTopProductMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" style="table-layout: fixed;" cellspacing="2"
        cellpadding="2" width="98%">
        <tr height="30px">
            <td width="500px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Representatives/RepresentativesTopProduct/dlgRepresentativeTopProduct.xml" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr height="*" valign="top">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="RepresentativeTopProductData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr valign="bottom" height="50px">
            <td align="right" colspan="2">
                <myfactory:wfButton ID="cmdBack" sOnClick="GoBack();" sText="Zur�ck" runat="server">
                </myfactory:wfButton>
            </td>
        </tr>
    </table>
</body>
</html>
