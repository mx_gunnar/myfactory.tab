'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    RepresentativesMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for RepresentativesMain
'--------------------------------------------------------------------------------
' Created:      26.04.2013 10:33:46, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Representatives

    Partial Class RepresentativesMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad


            Me.sOnLoad = "mOnLoad();"

            Dim lAddressID = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
            Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)

            If lRepresentativeID <> 0 Then

                Dim sClause As String = ""
                Dim cValue As Decimal
                Dim sCurrUnitInternal As String = " " & Currencies.gsCurrUnitInternal(oClientInfo, -1)

                sClause = "SalesDate>=" & DataTools.gsDate2Sql(Date.Now) & _
                            " AND SalesDate<" & DataTools.gsDate2Sql(Date.Now.AddDays(1))
                sClause = DataTools.gsClauseAnd(sClause, "Combinationsmark<2")
                sClause = DataTools.gsClauseAnd(sClause, "RepresentativeID=" & lRepresentativeID)
                sClause = DataTools.gsClauseAnd(sClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)

                cValue = DataMethods.gcGetDBValue(oClientInfo, _
                            "SUM(Turnover)", "tdStatSalesDetail", sClause)

                Me.dlgMain.oDialog.oField("TurnoverToday").oMember.Prop("Value") = gsFormatNumber(cValue, 2) & sCurrUnitInternal

            Else
                Me.dlgMain.oDialog.oField("TurnoverToday").oMember.Prop("TYPE") = "LABEL"
                Me.dlgMain.oDialog.oField("TurnoverToday").oMember.Prop("Value") = Dictionary.gsTranslate(oClientInfo, "Sie sind in der Datenbank nicht als Vertreter hinterlegt")

            End If

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
