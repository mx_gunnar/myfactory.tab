﻿


function mOnLoad() 
{
    mRefreshList();
}

function mRefreshList() 
{    
    gListViewLoadPage('lstMain', 1);
}




function mOnListViewBtnClick(sListView, sColID, sItemID) 
{
    var sURL = "";


    if (sColID == "TopCustomers") 
    {
        sURL = '../RepresentativesTopCustomer/RepresentativeTopCustomerMain.aspx' +
                '?ClientID=' + msClientID +
                '&Period=' + sItemID;
    }
    else if (sColID == "TopProducts") 
    {
        sURL = '../RepresentativesTopProduct/RepresentativeTopProductMain.aspx' +
                '?ClientID=' + msClientID +
                '&Period=' + sItemID;
    }


    document.location = sURL;

}
