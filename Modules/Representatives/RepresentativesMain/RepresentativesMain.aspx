<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Representatives.RepresentativesMain" CodeFile="RepresentativesMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="2" cellpadding="2" width="98%">
        <tr valig="top" height="30px" >
            <td width="500px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Representatives/RepresentativesMain/dlgRepresentative.xml"
                    sDataSource="dsoData">
                </myfactory:wfXmlDialog>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr valign="top">
            <td width="98%" colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="RepresentativesData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="*">
            <td>
                &nbsp;
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>
