'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    RepresentativesData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for RepresentativesData
'--------------------------------------------------------------------------------
' Created:      26.04.2013 12:25:27, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem.AspSystem

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Representatives

    Partial Class RepresentativesData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim sClause As String = ""
            Dim sClauseLY As String = ""
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "RepresentativesData.aspx"
            oListView.lPageSize = 12
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            Dim lAddressID As Integer
            Dim bAllDivisions As Boolean
            Dim sDivisionClause As String = ""

            lAddressID = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
            Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)
            bAllDivisions = gbCBool(oListView.sUserData)

            If Not bAllDivisions Then
                sDivisionClause = "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr
            End If


            If lRepresentativeID = 0 Then
                lRepresentativeID = -1 'set an invalid Representative ID
            End If

            ' add representative clause
            sDivisionClause = sDivisionClause.gsClauseAnd("RepresentativeID=" & lRepresentativeID)
            sDivisionClause = sDivisionClause.gsClauseAnd("Combinationsmark<2")

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("Period", "Periode", "*")
            oCol.bHtml = True

            oListView.oCols.oAddCol("Turnover", "Umsatz", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("Turnover_VJ", "Umsatz (VJ)", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("CurrUnit", "Wkz", "50", , wfEnumAligns.wfAlignCenter)

            oCol = oListView.oCols.oAddCol("TopCustomers", "Top Kunden", "100")
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sText = "Top Kunden"
            oCol.sWidth = "100px"

            oCol = oListView.oCols.oAddCol("TopProducts", "Top Artikel", "100")
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sText = "Top Artikel"
            oCol.sWidth = "100px"

                '---- Data ---------------------
            Dim lCurrYear As Integer = glCInt(oClientInfo.oClientProperties.lCurrentPeriod / 1000)
            Dim lYear As Integer
            Dim l, lPeriod, lPeriodLY As Integer   'LY = Last Year
            Dim sCurrUnit As String

            lYear = lCurrYear - oListView.lPage + 1

            sCurrUnit = Currencies.gsCurrUnitInternal(oClientInfo, -1)

            sTables = "tdStatSalesDetail"

            For l = 1 To 12
                lPeriod = lYear * 1000 + l
                lPeriodLY = (lYear - 1) * 1000 + l

                oRow = oListView.oRows.oAddRow(lPeriod.ToString)
                oRow.sValue(0) = Period.gsGetPeriodCaption(oClientInfo, lPeriod) & ""  'remove this new row ?

                sClause = "SalesPeriod=" & lPeriod
                sClause = sClause.gsClauseAnd(sDivisionClause)

                oRow.Value(1) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClause)

                '------ last year -------

                sClauseLY = "SalesPeriod=" & lPeriodLY
                sClauseLY = sClauseLY.gsClauseAnd(sDivisionClause)

                oRow.Value(2) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClauseLY)
                '---------------

                oRow.Value(3) = sCurrUnit
            Next

            oRow = oListView.oRows.oAddRow(lYear.ToString)
            oRow.bGroupStyle = True
            oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Summe") & ""



            sClause = "SalesPeriod>=" & lYear & "000 AND SalesPeriod<=" & lYear & "999"
            sClause = sClause.gsClauseAnd(sDivisionClause)

            sClauseLY = "SalesPeriod>=" & lYear - 1 & "000 AND SalesPeriod<=" & lYear - 1 & "999"
            sClauseLY = sClauseLY.gsClauseAnd(sDivisionClause)

            oRow.Value(1) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClause)
            oRow.Value(2) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", sTables, sClauseLY)

            oRow.Value(3) = sCurrUnit

            oListView.lRecordCountTotal = 12 * DataMethods.glGetDBCount(oClientInfo, "tdPeriods", "Period<" & lCurrYear & "999", "DISTINCT(Period/1000)")

            Return True

        End Function



        'Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
        '    ByVal oListView As AspListView) As Boolean

        '    Dim sClause As String = ""
        '    Dim oRow As AspListViewRow
        '    Dim sCurrUnit As String
        '    Dim sCurrUnitInternal As String
        '    Dim cValue As Decimal
        '    Dim dtToday As Date
        '    Dim dtLast As Date
        '    Dim dtFirst As Date
        '    Dim sDivisionRepresentativeClause As String = ""
        '    Dim oCol As AspListViewCol

        '    Dim bAllDivisions As Boolean = gbCBool(oListView.sUserData)
        '    Dim lRepresentativeID As Integer = 0

        '    dtToday = Date.Today

        '    '---- ListView Properties ------
        '    oListView.bPageNavigation = True
        '    oListView.sListViewDataPage = "RepresentativesData.aspx"
        '    oListView.bTabletMode = True
        '    oListView.bTabletScrollMode = True
        '    oListView.bAutoHideNavigation = True


        '    '---- Columns ------------------
        '    oListView.oCols.oAddCol("Desc", "Zeitraum", "*", , , False, False)
        '    oListView.oCols.oAddCol("Turnover", "Umsatz", "200", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
        '    oListView.oCols.oAddCol("CurrUnit", "WKZ", "50", , , False, False)

        '    '---- Data ---------------------
        '    Dim lEmployeeAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
        '    lRepresentativeID = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lEmployeeAddressID)

        '    If lRepresentativeID = 0 Then
        '        lRepresentativeID = -1
        '    End If


        '    sCurrUnitInternal = " " & Currencies.gsCurrUnitInternal(oClientInfo, -1)
        '    sCurrUnit = sCurrUnitInternal

        '    sDivisionRepresentativeClause = "Combinationsmark<2"
        '    sDivisionRepresentativeClause = sDivisionRepresentativeClause.gsClauseAnd("RepresentativeID=" & lRepresentativeID)

        '    If Not bAllDivisions Then
        '        sDivisionRepresentativeClause = DataTools.gsClauseAnd(sDivisionRepresentativeClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
        '    End If


        '    'Today
        '    dtFirst = dtToday
        '    dtLast = dtToday.AddDays(1)

        '    sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
        '                " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
        '    sClause = DataTools.gsClauseAnd(sClause, sDivisionRepresentativeClause)

        '    cValue = DataMethods.gcGetDBValue(oClientInfo, _
        '                "SUM(Turnover)", "tdStatSalesDetail", sClause)
        '    cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

        '    oRow = oListView.oRows.oAddRow("1")
        '    oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz heute")
        '    oRow.Value(1) = cValue
        '    oRow.Value(2) = sCurrUnit


        '    'Month
        '    dtFirst = DateSerial(Year(dtToday), Month(dtToday), 1)
        '    dtLast = dtToday.AddDays(1)

        '    sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
        '                " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
        '    sClause = DataTools.gsClauseAnd(sClause, sDivisionRepresentativeClause)

        '    cValue = DataMethods.gcGetDBValue(oClientInfo, _
        '                "SUM(Turnover)", "tdStatSalesDetail", sClause)
        '    cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

        '    oRow = oListView.oRows.oAddRow("2")
        '    oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz Monat")
        '    oRow.Value(1) = cValue
        '    oRow.Value(2) = sCurrUnit



        '    'year
        '    Dim dtYearFirstDate As Date
        '    Dim lYear As Integer = oClientInfo.oClientProperties.lCurrentPeriod \ 1000

        '    dtYearFirstDate = Period.gdtPeriod2Date(oClientInfo, glCInt(lYear & "001"))


        '    dtFirst = dtYearFirstDate
        '    dtLast = dtToday.AddDays(1)

        '    sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
        '                " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
        '    sClause = DataTools.gsClauseAnd(sClause, sDivisionRepresentativeClause)

        '    cValue = DataMethods.gcGetDBValue(oClientInfo, _
        '                "SUM(Turnover)", "tdStatSalesDetail", sClause)
        '    cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

        '    oRow = oListView.oRows.oAddRow("3")
        '    oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz Jahr")
        '    oRow.Value(1) = cValue
        '    oRow.Value(2) = sCurrUnit



        '    Return True

        'End Function


    End Class

End Namespace

'================================================================================
'================================================================================
