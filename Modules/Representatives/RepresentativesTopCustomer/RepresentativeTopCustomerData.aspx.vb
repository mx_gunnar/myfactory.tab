'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    RepresentativeTopCustomerData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for RepresentativeTopCustomerData
'--------------------------------------------------------------------------------
' Created:      27.06.2013 17:10:20, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Representatives

    Partial Class RepresentativeTopCustomerData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim sOrder As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            Dim lIndex As Integer = 0
            Dim lRank As Integer

            Dim sCurrUnitInternal As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            Dim sdtFrom As String = oListView.sUserData.Split(";"c)(0)
            Dim sdtTo As String = oListView.sUserData.Split(";"c)(1)

            Dim dtFrom As Date = Date.Now
            If gbDate(sdtFrom) Then
                dtFrom = gdtCDate(sdtFrom)
            End If
            Dim dtTo As Date = Date.Now
            If gbDate(sdtTo) Then
                dtTo = gdtCDate(sdtTo)
            End If

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.sListViewDataPage = "RepresentativeTopCustomerData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("Position", "Platz", "50", , , False, False)
            oListView.oCols.oAddCol("CustomerNumber", "Kundennr", "100", , , False, False)

            oCol = oListView.oCols.oAddCol("cmdCustomer", "Kunde", "100", , , False, False)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "70px"

            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , False, False)
            oListView.oCols.oAddCol("Sum", "Summe", "200", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("CurrUnit", "WKZ", "60", , , False, False)

            '---- Data ---------------------
            Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
            Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)

            If lRepresentativeID = 0 Then
                lRepresentativeID = -1
            End If

            sFields = "MIN(C.CustomerNumber) AS CustomerNumber , MIN(A.Matchcode) As Matchcode, SUM(Turnover) as Sum, MIN(A.AddressID) AS AddressID"
            sTables = "tdStatSalesDetail SSD INNER JOIN tdCustomers C ON C.CustomerID=SSD.CustomerID INNER JOIN tdAddresses A ON A.AddressID=C.AddressID"

            sClause = sClause.gsClauseAnd("RepresentativeID=" & lRepresentativeID)
            sClause = sClause.gsClauseAnd("SalesDate>=" & dtFrom.gs2Sql & " AND SalesDate<=" & dtTo.gs2Sql)

            sOrder = "Sum desc"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, "C.CustomerID", sOrder)
            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs("AddressID").sValue)
                lIndex = lIndex + 1

                If oListView.lPage > 1 Then
                    lRank = lIndex + ((oListView.lPage - 1) * oListView.lPageSize)
                Else
                    lRank = lIndex
                End If

                oRow.Value(0) = lRank
                oRow.Value(1) = rs("CustomerNumber").sValue
                oRow.Value(2) = "..."
                oRow.Value(3) = rs("Matchcode").sValue
                oRow.Value(4) = rs("Sum").cValue
                oRow.Value(5) = " " & sCurrUnitInternal

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause, "distinct C.CustomerID")

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
