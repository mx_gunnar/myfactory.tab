﻿

function mOnLoad()
{
    mRefreshListEMail();
    mRefreshListCases();
}


function mRefreshListEMail()
{
    gListViewSetUserData('lstMails', document.all.txtMailsSince.value);
    gListViewLoadPage('lstMails', 1);

}

function mRefreshListCases()
{
    //gListViewSetUserData('lstCases', '');
    gListViewLoadPage('lstCases', 1);
}

function mOnSetDirty()
{
    if (event.type == "change")
    {
        if (event.srcElement.id == "txtMailsSince")
        {
            mRefreshListEMail();
        }
    }
}


function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sListView == 'lstCases')
    {
        mNavigateToCases(sItemID);
    }
    else
    {
        var bOnlyNotCompleted = false;
        var sFrom = document.all.txtMailsSince.value;
        if (sItemID == "-1" || sItemID == "-2")
        {
            if (sItemID == "-1")
            {
                sFrom = "";
            }

            bOnlyNotCompleted = true;
        }

        mNavigateToEMails(bOnlyNotCompleted, sFrom);
    }
}

function mOnListViewClick(sListView, sItemID)
{
    if (sListView == 'lstCases')
    {
        mNavigateToCases(sItemID);
    }
    else
    {
        var bOnlyNotCompleted = false;
        var sFrom = document.all.txtMailsSince.value;
        if (sItemID == "-1" || sItemID == "-2")
        {
            if (sItemID == "-1")
            {
                sFrom = "";
            }

            bOnlyNotCompleted = true;
        }

        mNavigateToEMails(bOnlyNotCompleted, sFrom);
    }
}

function mNavigateToEMails(bOnlyNotCompleted, sFromDate)
{
    var sURL = '../SupportEMails/SupportEMailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&FromDate=' + sFromDate +
                '&OnlyNotCompleted=' + bOnlyNotCompleted;

    document.location = sURL;
}

function mNavigateToCases(lState)
{
    var sURL = '../SupportCases/SupportCasesMain.aspx' +
                '?ClientID=' + msClientID +
                '&State=' + lState;

    document.location = sURL;
}