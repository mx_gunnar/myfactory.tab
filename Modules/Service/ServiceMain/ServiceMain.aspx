<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Service.ServiceMain" CodeFile="ServiceMain.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="0" cellpadding="2" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="10px">
            <td>
                <myfactory:wfXmlDialog ID="dlgEMails" runat="server" sDialog="/tab/Modules/Service/ServiceMain/dlgServiceEMails.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td width="10px;">
                &nbsp;
            </td>
            <td>
                <myfactory:wfXmlDialog ID="dlgCases" runat="server" sDialog="/tab/Modules/Service/ServiceMain/dlgServiceCases.xml">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <myfactory:wfListView runat="server" ID="lstMails" sListViewDataPage="ServiceMainEMailData.aspx">
                </myfactory:wfListView>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <myfactory:wfListView runat="server" ID="lstCases" sListViewDataPage="ServiceMainCasesData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <!-- bottom menue  -->
        <tr height="120px" valign="bottom">
            <td colspan="3">
                <table width="100%" height="100%">
                    <td width="200px" id="trNaviLeft">
                        <myfactory:wfLabel runat="server" ID="lblNaviLeft" sStyle="margin-left:10px;margin-bottom:30px">
                        </myfactory:wfLabel>
                    </td>
                    <td align="right">
                        <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                        </myfactory:wfLabel>
                    </td>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
