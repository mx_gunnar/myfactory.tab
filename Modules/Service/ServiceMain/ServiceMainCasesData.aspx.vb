'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ServiceMainCasesData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ServiceMainCasesData
'--------------------------------------------------------------------------------
' Created:      01.10.2013 09:54:12, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class ServiceMainCasesData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sGroup As String
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol
            Dim sME As String = Dictionary.gsTranslate(oClientInfo, "Stk")

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.bAutoHideNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.sListViewDataPage = "ServiceMainCasesData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("CaseState", "F�lle", "*", , , False, False)
            oListView.oCols.oAddCol("Count", "Anzahl", "100", , wfEnumAligns.wfAlignRight, False, False)
            oCol = oListView.oCols.oAddCol("Details", "Details", "100", , , , )
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)

            '---- Data ---------------------
            sFields = "CaseState AS RowID, MIN(CaseStateDesc),COUNT(CaseID)"
            sTables = "tdSupportCases SC INNER JOIN tdSupportCaseStates SCS ON SCS.CaseStateID=SC.CaseState"
            sOrder = "COUNT(CaseID) desc"
            sGroup = "CaseState"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, sGroup, sOrder)


            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)
                oRow.Value(1) = rs(2).sValue & "&nbsp;&nbsp;" & sME & "&nbsp;&nbsp;"
                rs.MoveNext()
            Loop

            'sum row
            oRow = oListView.oRows.oAddRow("-1")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Gesamt")
            oRow.Value(1) = DataMethods.glGetDBCount(oClientInfo, "tdSupportCases") & "&nbsp;&nbsp;" & sME & "&nbsp;&nbsp;"
            oRow.bGroupStyle = True

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
