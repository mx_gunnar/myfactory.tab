'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ServiceMainEMailData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ServiceMainEMailData
'--------------------------------------------------------------------------------
' Created:      01.10.2013 09:54:24, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class ServiceMainEMailData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean


            Dim sDateClause As String = ""
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sFromDate As String = ""
            Dim dtFromDate As Date
            Dim sME As String = Dictionary.gsTranslate(oClientInfo, "Stk")

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ServiceMainEMailData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '------ User Filter Data ----------
            sFromDate = oListView.sUserData

            '---- Columns ------------------
            oListView.oCols.oAddCol("Description", "EMails", "*", , , False, False)
            oListView.oCols.oAddCol("Count", "Anzahl", "100", , wfEnumAligns.wfAlignRight, False, False)

            oCol = oListView.oCols.oAddCol("CmdDetails", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)

            '---- Data ---------------------
            sTables = " tdSupportCasePos SCP"
            sClause = " SCP.Completed=0 AND SCP.BySupportMail=-1 "

            '---------- Clause ---------------
            If Not gbDate(sFromDate) Then
                dtFromDate = Date.Now
            Else
                dtFromDate = gdtCDate(sFromDate)
            End If

            If Not String.IsNullOrEmpty(sFromDate) Then
                sDateClause = sDateClause.gsClauseAnd("SCP.PosDate >= " & dtFromDate.gs2Sql)
            End If

            'row: nicht erledigt insgesamt
            oRow = oListView.oRows.oAddRow("-1")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Nicht erledigt gesamt")
            oRow.Value(1) = DataMethods.glGetDBCount(oClientInfo, sTables, sClause) & "&nbsp;&nbsp;" & sME & "&nbsp;&nbsp;"

            'row: nicht erledigt seit datum
            oRow = oListView.oRows.oAddRow("-2")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Nicht erledigt seit") & " " & dtFromDate.Date
            oRow.Value(1) = DataMethods.glGetDBCount(oClientInfo, sTables, sClause.gsClauseAnd(sDateClause)) & "&nbsp;&nbsp;" & sME & "&nbsp;&nbsp;"

            'row: gesamt seit datum
            oRow = oListView.oRows.oAddRow("-3")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Gesamt seit") & " " & dtFromDate.Date
            oRow.Value(1) = DataMethods.glGetDBCount(oClientInfo, sTables, sDateClause.gsClauseAnd("SCP.MailID IS NOT NULL")) & "&nbsp;&nbsp;" & sME & "&nbsp;&nbsp;"

            oListView.lRecordCountTotal = 3

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
