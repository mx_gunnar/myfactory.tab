'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SupportCaseDetailsProcess
'--------------------------------------------------------------------------------
' Created:      02.10.2013 10:18:02, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.BusinessTasks.CRM.Main
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Tools.StringFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String


            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "GetDepartmentUserCbo"
                    Return msGetDepartmentUserCbo(oClientInfo, glCInt(Request.QueryString("DepartmentID")))
                Case "SaveCase"
                    Return msSaveSupportCase(oClientInfo, glCInt(Request.QueryString("CaseID")))
                Case "CreateCase"
                    Return msCreateNewCase(oClientInfo, glCInt(Request.QueryString("AddressID")))
                Case "ChangeCaseAddress"
                    Return msChangeSupportCaseAddress(oClientInfo, glCInt(Request.QueryString("CaseID")), glCInt(Request.QueryString("AddressID")))
                Case Else
                    Return "unknown command"
            End Select

            Return "unknown command"

        End Function


        Private Function msChangeSupportCaseAddress(ByVal oClientInfo As ClientInfo, ByVal lCaseID As Integer, ByVal lAddressID As Integer) As String

            Dim oCase As New SupportCase

            If lCaseID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln des Supportfalls")
            End If

            ' load case
            If Not oCase.gbLoad(oClientInfo, lCaseID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Supportfalls")
            End If

            ' set new address
            If Not SupportCaseFunctions.gbSetAddress(oClientInfo, oCase, lAddressID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen der neuen Adresse")
            End If

            ' save case
            If Not oCase.gbSave(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Der Supportfall konnte nicht gespeichert werden!") & Environment.NewLine & oClientInfo.oErrors.sErrText()
            End If

            Return "OK;"
        End Function


        Public Function msCreateNewCase(ByVal oClientInfo As ClientInfo, ByVal lAddressID As Integer) As String

            If Not lAddressID > 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Adresse")
            End If

            Dim oCase As New SupportCase

            If Not oCase.gbNew(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Initialisieren des Supportfalles!")
            End If

            oCase.oMembers.Prop("CaseDesc") = Dictionary.gsTranslate(oClientInfo, "Neuer Supportfall")

            If Not SupportCaseFunctions.gbSetAddress(oClientInfo, oCase, lAddressID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen der Adresse!")
            End If

            If Not oCase.gbSave(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Der Supportfall konnte nicht gespeichert werden!") & vbCrLf() & oClientInfo.oErrors.sErrText()
            End If

            Return "OK;" & oCase.oMembers.sProp("CaseID")

        End Function

        Public Function msSaveSupportCase(ByVal oClientInfo As ClientInfo, ByVal lCaseID As Integer) As String

            Dim sXml As String = String.Empty
            Dim oXml As New XmlDocument
            Dim oNode As XmlNode = Nothing
            Dim oCase As New SupportCase

            If lCaseID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln des Supportfalls")
            End If

            If Not oCase.gbLoad(oClientInfo, lCaseID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Supportfalls")
            End If

            If oXml.text.Length = 0 Then
                If Not oXml.load(Request) Then
                    oClientInfo.LogSysEvent(oXml.parseError.reason)
                    Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen des Xml!")
                End If
            End If

            ' convert from dialog xml
            sXml = XmlFunctions.gsXmlConvertFromDialog(oXml.xml)

            If Not oXml.loadXML(sXml) Then
                oClientInfo.oErrors.bAdd(oXml.parseError.reason)
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen des Xml!")
            End If

            ' set new values to case
            If Not oCase.gbSetXml(oClientInfo, oXml.selectSingleNode("Values/Params/DlgParams").xml) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen der Werte!")
            End If

            ' save case
            If Not oCase.gbSave(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Der Supportfall konnte nicht gespeichert werden!") & Environment.NewLine & oClientInfo.oErrors.sErrText()
            End If

            Return "OK;"

        End Function


        Private Function msGetDepartmentUserCbo(ByVal oClientInfo As ClientInfo, ByVal lDepartmentID As Integer) As String

            Dim sIDs As String = ""
            Dim sNames As String = ""

            Dim frs As FastRecordset

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, "SDU.UserName, DBU.UserName", "tdSupportDepartmentUsers SDU INNER JOIN tsDataBaseUsers DBU ON DBU.UserInitials=SDU.UserName", "DepartmentID=" & lDepartmentID)

            Do Until frs.EOF
                sIDs = sIDs & ";" & frs(0).sValue.Replace(";", " ")
                sNames = sNames & ";" & frs(1).sValue.Replace(";", " ")
                frs.MoveNext()
            Loop
            frs.Close()

            sIDs = sIDs.Replace("|", "_")
            sNames = sNames.Replace("|", "_")

            Return sIDs & "|" & sNames

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
