<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Service.SupportCaseDetailsMain" CodeFile="SupportCaseDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="100px">
            <td>
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/TAB/Modules/SErvice/SupportCaseDetails/dlgSupportCaseDetails.xml" />
                </form>
            </td>
        </tr>
        <tr valign="top" height="40px">
            <td>
                <table width="100%" height="100%">
                    <tr>
						<td align="left" width="180px" id="tdToProject">
                            <myfactory:wfButton ID="cmdToProject" sText="Zum Projekt" sStyle="width: 150px;" runat="server"
                                sOnClick="mToProject();">
                            </myfactory:wfButton>
                        </td>
                        <td align="left" width="180px">
                            <myfactory:wfButton ID="cmdToAdress" sText="Zur Adresse" sStyle="width: 150px;" runat="server"
                                sOnClick="mToAddress();">
                            </myfactory:wfButton>
                        </td>
                        <td align="left" width="180px">
                            <myfactory:wfButton ID="cmdEmail" sOnClick="mOnMail();" sStyle="width: 150px;" sText="E-Mail schreiben"
                                runat="server">
                            </myfactory:wfButton>
                        </td>
                        <td align="left" >
                            <myfactory:wfButton ID="cmdSupportCaseActionsNew" sText="Neue Aktion" sStyle="width: 150px;" runat="server"
                                sOnClick="mSupportCaseActionsNew();">
                            </myfactory:wfButton>
                        </td>  
                        <td align="right" width="120px">
                            <myfactory:wfButton runat="server" ID="CmdOK" sText="OK" sOnClick="mOnOK();">
                            </myfactory:wfButton>
                        </td>
                        <td align="right" width="120px">
                            <myfactory:wfButton runat="server" ID="CmdCancel" sText="Abbrechen" sOnClick="mOnCancel();">
                            </myfactory:wfButton>
                        </td>                     
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td>
                <table width="100%" height="100%">
                    <tr>
                        <td width="*" id="trNaviLeft">
                            <myfactory:wfLabel runat="server" ID="lblNaviLeft" sStyle="margin-left:10px;margin-bottom:30px">
                            </myfactory:wfLabel>
                        </td>
                        <td align="right">
                            <%--<myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeft" sText="<"></myfactory:wfLabel>
                            <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>--%>
                            <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNavi"></myfactory:wfLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
