'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseDetailsMain
'--------------------------------------------------------------------------------
' Created:      02.10.2013 10:17:49, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lCasePosID As Integer = glCInt(Request.QueryString("CasePosID"))
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))

            If lCaseID = 0 AndAlso lCasePosID > 0 Then
                lCaseID = DataMethods.glGetDBValue(oClientInfo, "CaseID", "tdSupportCasePos", "PosID=" & lCasePosID)
            End If

            ReDim Me.asTerms(1)
			ReDim Me.asTranslatedTerms(1)
            ReDim Me.asPageParams(4)
            Me.asPageParams(0) = lCaseID.ToString
            Me.asPageParams(2) = lCasePosID.ToString
            Me.asPageParams(3) = lAddressID.ToString

            Me.gAddScriptLink("wfDlgParams.js", True)
            Me.sOnLoad = "mOnLoad()"

			Me.asTranslatedTerms(0) = "Dem Supportfall ist kein Projekt zugeordnet."

            If lCasePosID > 0 Then
                ' from E-Mails
                Me.asPageParams(1) = "EMails"
            Else
                ' from Support Cases
                Me.asPageParams(1) = "Cases"
            End If


            Dim sNavigateBackCmd As String = ClientValues.gsGetClientValue(oClientInfo, "Tab_NavigateURL_SupportCaseDetailsMain")
            If Not String.IsNullOrEmpty(sNavigateBackCmd) Then
                Me.asPageParams(1) = "Custom"
                Me.asTerms(0) = sNavigateBackCmd
            End If


            If lCaseID > 0 Then
                'fill dialog

                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, "tdSupportCases.*, A.Matchcode AS AddressMatchcode, p.ProjectNumber, p.Matchcode AS ProjectMatchcode", _
					"tdSupportCases INNER JOIN tdAddresses A ON A.AddressID=tdSupportCases.AddressID LEFT JOIN tdProjects p ON p.ProjectID=tdSupportCases.ProjectID", _
					"CaseID=" & lCaseID)

                If Not rs.EOF Then
                    Me.dlgMain.oDialog.oField("ContactID").oMember.Prop("PARAMS") = rs("AddressID").lValue
                End If

                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)

                'if support case is closed, hide new action button
                If rs("CompletedDate").sValue <> "" Then
                    Me.lblNaviLeft.Visible = False
                End If

                Me.cmdSupportCaseActionsNew.Visible = False

            End If

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='SupportCaseDetaisMain' AND ModuleName<>'SupportCasesDetailsMain'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "CaseID=" & lCaseID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString

            Me.lblNaviLeft.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "SupportCaseActionNew", "../../../", "CaseID=" & lCaseID)

            osHTML.bAppend("</nobr>")

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
