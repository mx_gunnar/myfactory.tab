﻿
function mOnLoad()
{

 //   if (msPageParams[0] && msPageParams[0] != "" && msPageParams[0] != "-1")
 //   {
       // sOriginalValues = gsXMLDlgParams(frmMain, '', false);
   // }

    if (document.getElementById('txtProjectID').value == '0' || document.getElementById('txtProjectID').value == '')
        document.getElementById('tdToProject').style.display = 'none';

    
    mRefreshHandlingUserCbo(document.all.cboSupportDepartment.value);
    gSetCboValue(document.getElementById("cboHandlingUserSelect"), document.all.txtHandlingUser.value);
}



function mOnSetDirty()
{
    if (event.type == "change")
    {
        if (event.srcElement.id == "cboSupportDepartment")
        {
            mRefreshHandlingUserCbo(document.all.cboSupportDepartment.value);
        }

        if (event.srcElement.id == "cboHandlingUserSelect")
        {
            document.all.txtHandlingUser.value = document.all.cboHandlingUserSelect.value;
        }
    }
}


/// Methode to refill the "Handling User" Combobox after the Support-Department changed
function mRefreshHandlingUserCbo(lDepartmentID)
{
    var sURL = 'SupportCaseDetailsProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=GetDepartmentUserCbo&DepartmentID=" + lDepartmentID;
    sRes = gsCallServerMethod(sURL, '');

    gClearCombobox2(document.getElementById("cboHandlingUserSelect"));

    var sValues = sRes.split("|")[0];
    var sNames = sRes.split("|")[1];

    var asValues = sValues.split(";");
    var asNames = sNames.split(";");

    for (i = 0; i < asValues.length; i++)
    {
        var sValue = asValues[i]; 
        var sName;
        try
        {
            sName = asNames[i];
        }
        catch (Ex)
        {
            sName = " ";
        }

        gCreateNewCboOption(document.getElementById("cboHandlingUserSelect"), sValue, sName, i, false);
    }
}


function mOnMail()
{
    var sURL = "../SupportEMailNew/SupportEMailNew.aspx?ClientID=" + msClientID + "&CaseID=" + msPageParams[0] + "&CasePosID=" + msPageParams[2];
    document.location = sURL;
}

function mOnOK()
{
    // save case
    var sURL = 'SupportCaseDetailsProcess.aspx';
    sURL = sURL + "?Cmd=SaveCase&CaseID=" + msPageParams[0];
    var sXML = gsXMLDlgParams(frmMain, '', false);
    var sRes = gsCallServerMethod(sURL, sXML);

    if (sRes.substr(0, 3) == "OK;")
    {
        mNavigateBack();
    }
    else
    {
        alert(sRes);
    }
}

function mOnCancel()
{
    mNavigateBack();
}

function mNavigateBack()
{
    var sURL = "";

    if (msPageParams[1] == "EMails")
    {
        sURL = "../SupportEMails/SupportEMailsMain.aspx?ClientID=" + msClientID;
    }
    else if (msPageParams[1] == "Cases")
    {
        sURL = "../SupportCases/SupportCasesMain.aspx?ClientID=" + msClientID;
        sURL = sURL + "&RecordID=" + msPageParams[3];
    }
    else if (msPageParams[1] == "Custom")
    {
        sURL = msTerms[0];

        // Clear the Page Navigation ClientValue for this Page
        gClearCacheValue("Tab_NavigateURL_SupportCaseDetailsMain");
    }

    document.location = sURL;
}



function cmdSelectNewAddress_OnClick()
{
    var sURL = "../SupportCaseNew/SupportCaseNew.aspx?ClientID=" + msClientID;
    sURL = sURL + "&Origin=SelectCaseAddress";
    sURL = sURL + "&CaseID=" + msPageParams[0];

    document.location = sURL;
}

function mToAddress()
{
    var sAddressID = document.all.txtAddressID.value;   

    var sURL = "../../Addresses/AddressDetails/AddressDetailsMain.aspx?ClientID=" + msClientID + "&RecordID=" + sAddressID;
    document.location = sURL;
}

function mToProject()
{
    var sProjectID = document.all.txtProjectID.value;   
	if(sProjectID != "0") {
		var sURL = "../../Projects/ProjectsDetails/ProjectDetailsMain.aspx?ClientID=" + msClientID + "&ProjectID=" + sProjectID;
		document.location = sURL;
	} else {
		gInfoMsg(msTranslatedTerms[0]);
	}
}

