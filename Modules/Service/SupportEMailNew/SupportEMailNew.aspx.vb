'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportEMailNew.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportEMailNew
'--------------------------------------------------------------------------------
' Created:      07.10.2013 15:52:14, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportEMailNew
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lCasePosID As Integer = glCInt(Request.QueryString("CasePosID"))
            Dim lMailTemplateID As Integer = glCInt(Request.QueryString("MailTemplateID"))

            ReDim Me.asPageParams(3)
            ReDim Me.asTerms(1)
            Me.asPageParams(0) = lCaseID.ToString
            Me.asPageParams(1) = lMailTemplateID.ToString
            Me.asPageParams(2) = lCasePosID.ToString

            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "Bitte w�hlen Sie eine EMail Vorlage aus")

            Dim sText As String = ClientValues.gsGetClientValue(oClientInfo, "TabSupportEmailReplyText")
            sText = sText.Replace("<br />", Environment.NewLine)
            Me.dlgMain.oDialog.oField("ReplyText").oMember.Prop("VALUE") = sText


            Me.sOnLoad = "mOnLoad();"
        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
