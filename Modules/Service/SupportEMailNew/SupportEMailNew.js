﻿
function mOnLoad()
{
    mRefreshList();


    if (msPageParams[1] != "0")
    {
        gListViewItemSelect('lstMain', msPageParams[1])
    }

}

function mRefreshList()
{
    gListViewLoadPage('lstMain', 1);
}

function mOnCancel()
{
    gClearCacheValue("TabSupportEmailReplyText");

    var sURL = "../SupportCaseDetails/SupportCaseDetailsMain.aspx?ClientID=" + msClientID + "&CaseID=" + msPageParams[0] + "&CasePosID=" + msPageParams[2];
    document.location = sURL;
}

function mOnSend()
{
    var sMailTemplateID = gsListViewGetSelection('lstMain');
    if (!sMailTemplateID || sMailTemplateID == "")
    {
        alert(msTerms[0]);
        return;
    }

    // hold Mail-Placeholder-Text (Reply Text) in ClientValues
    var sValue = document.all.txtReplyText.value;
    sValue = sValue.split("\n").join("<br />");
    gSetCacheValue("TabSupportEmailReplyText", sValue);

    var sURL = "SupportEMailNewProcess.aspx?Cmd=Send&CaseID=" + msPageParams[0] + "&MailTemplateID=" + sMailTemplateID;
    var sRes = gsCallServerMethod(sURL, '');

    if (sRes.substr(0, 3) == "OK;")
    {
        // clear cache value after send mail if send was OK
        gClearCacheValue("TabSupportEmailReplyText");
        mOnCancel();
    }
    else
    {
        alert(sRes);
    }    

    // clear cache value after mail send
    gClearCacheValue("TabSupportEmailReplyText");

}

function mOnPreview()
{
    var sMailTemplateID = gsListViewGetSelection('lstMain');
    if (!sMailTemplateID || sMailTemplateID == "")
    {
        alert(msTerms[0]);
        return;
    }

    // hold Mail-Placeholder-Text (Reply Text) in ClientValues
    var sValue = document.all.txtReplyText.value;

    sValue = sValue.split("\n").join("<br />");
    
    gSetCacheValue("TabSupportEmailReplyText", sValue);

    // open Preview URL
    var sURL = "SupportEMailNewPreview.aspx?ClientID=" + msClientID;
    sURL = sURL + "&CaseID=" + msPageParams[0];
    sURL = sURL + "&MailTemplateID=" + sMailTemplateID;
    sURL = sURL + "&CasePosID=" + msPageParams[2];
    document.location = sURL;
}