'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportEMailNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SupportEMailNewProcess
'--------------------------------------------------------------------------------
' Created:      07.10.2013 16:38:58, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.BusinessTasks.CRM.Main
Imports myfactory.Sys.Main
Imports myfactory.BusinessTasks.Base.MailTools
Imports myfactory.Sys.Email

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportEMailNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lMailTemplateID As Integer = glCInt(Request.QueryString("MailTemplateID"))

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Send"
                    Return msSendMail(oClientInfo, lCaseID, lMailTemplateID)
                Case Else
                    Return "unknown command"
            End Select

            Return "unknown command"

        End Function


        Private Function msSendMail(ByVal oClientInfo As ClientInfo, ByVal lCaseID As Integer, ByVal lMailTemplateID As Integer) As String

            '---- data parameter
            Dim sFrom As String
            Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdSupportCases", "CaseID=" & lCaseID)
            Dim lContactID As Integer = DataMethods.glGetDBValue(oClientInfo, "ContactID", "tdSupportCases", "CaseID=" & lCaseID)

            sFrom = DataMethods.gsGetDBValue(oClientInfo, "MailTemplateFrom", "tdMailTemplates", "MailTemplateID=" & lMailTemplateID)
            If sFrom = "" Then sFrom = "support@myfactory.com"

            '-----------------------   generate mail preview  -----------------
            Dim sHeaderText As String = ""
            Dim sReplyText As String = ""
            sReplyText = ClientValues.gsGetClientValue(oClientInfo, "TabSupportEmailReplyText")

            Dim sMailContent As String
            Dim sSubject As String = ""
            Dim sEMailSender As String = ""
            Dim oTo As New EMailAddress

            oTo.lAddressID = lAddressID
            oTo.lContactID = lContactID
            oTo.SetEMail(oClientInfo)
            oTo.SetEMailAlias(oClientInfo)

            sMailContent = MailTools.gsGetTemplateText(oClientInfo, lMailTemplateID, oTo, sSubject, "")

            SupportCaseTools.gbReplacePlaceholders(oClientInfo, sMailContent, lCaseID)
            sMailContent = sMailContent.Replace("$SupportReplyMobile$", sReplyText)

            SupportCaseTools.gbReplacePlaceholders(oClientInfo, sSubject, lCaseID)

            sHeaderText = Dictionary.gsTranslate(oClientInfo, "Von") & ": " & sFrom
            sHeaderText = sHeaderText & "<br />"
            sHeaderText = sHeaderText & Dictionary.gsTranslate(oClientInfo, "An") & ": " & oTo.sEMail
            sHeaderText = sHeaderText & "<br />" & Dictionary.gsTranslate(oClientInfo, "Betreff") & ": " & sSubject


            '--------- send mail ---------    
            Dim bOK As Boolean = MailTools.gbSaveMail(oClientInfo, 0, 0, sFrom, oTo.sEMail, "", "", sSubject, sMailContent, 0, True)

            If bOK Then
                Return "OK;"
            Else
                Return Dictionary.gsTranslate(oClientInfo, "Beim Senden der E-Mail ist ein Fehler aufgetreten")
            End If

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
