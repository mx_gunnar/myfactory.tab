<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Service.SupportEMailNew" CodeFile="SupportEMailNew.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server"> 
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr>
            <td valign="top" height="300">
                <myfactory:wfListView ID="lstMain" sListViewDataPage="SupportEMailNewData.aspx" runat="server">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="4px"><td>&nbsp;</td></tr>
        <tr height="*" valign="top">
            <td>
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/Tab/Modules/Service/SupportEMailNew/dlgSupportEMailNew.xml">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr valign="bottom">
            <td>
                <table width="100%" height="100%">
                    <tr>
                    <td>&nbsp;</td>
                        <td align="right" width="120px">
                            <myfactory:wfButton ID="cmdPreview" sOnClick="mOnPreview()" sText="Vorschau" runat="server">
                            </myfactory:wfButton>
                        </td>
                        <td align="right" width="120px">
                            <myfactory:wfButton ID="cmdSend" sOnClick="mOnSend()" sText="Senden" runat="server">
                            </myfactory:wfButton>
                        </td>
                        <td align="right" width="120px">
                            <myfactory:wfButton ID="cmdCancel" sOnClick="mOnCancel()" sText="Abbrechen" runat="server">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
