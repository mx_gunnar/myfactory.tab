﻿


function mOnBack()
{
    var sURL = "SupportEMailNew.aspx?ClientID=" + msClientID;
    sURL = sURL + "&CaseID=" + msPageParams[0];
    sURL = sURL + "&MailTemplateID=" + msPageParams[1];
    sURL = sURL + "&CasePosID=" + msPageParams[2];

    document.location = sURL;
}

function mOnSend()
{
    var sURL = "SupportEMailNewProcess.aspx?Cmd=Send&CaseID=" + msPageParams[0] + "&MailTemplateID=" + msPageParams[1];
    var sRes = gsCallServerMethod(sURL, '');

    if (sRes.substr(0, 3) == "OK;")
    {
        // clear cache value after send mail if send was OK
        gClearCacheValue("TabSupportEmailReplyText");
        mNavigateToCaseDetails();
    } 
    else
    {
        alert(sRes);
    }
}

function mOnCancel()
{
    gClearCacheValue("TabSupportEmailReplyText");
    mNavigateToCaseDetails();

}

function mNavigateToCaseDetails()
{
    var sURL = "../SupportCaseDetails/SupportCaseDetailsMain.aspx?ClientID=" + msClientID;
    sURL = sURL + "&CaseID=" + msPageParams[0];
    sURL = sURL + "&CasePosID=" + msPageParams[2];

    document.location = sURL;
}