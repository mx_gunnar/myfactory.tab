'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportEMailNewData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SupportEMailNewData
'--------------------------------------------------------------------------------
' Created:      07.10.2013 15:52:24, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.StringFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportEMailNewData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportEMailNewData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.lPageSize = 5

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("MailTEmplateDesc", "EMail Vorlage", "*", , , True, False)
            oListView.oCols.oAddCol("MailTemplateFrom", "EMail Von", "200", , , True, False)
            oListView.oCols.oAddCol("MailTemplateGroupDesc", "Vorlagengruppe", "200", , , True, False)

            ' with this button the rows are double-hight
            'oCol = oListView.oCols.oAddCol("CmdSelect", "", "20", , , False, False)
            'oCol.SetDefaultButton(myfactory.FrontendSystem.AspTools.wfEnumAspListViewButtonTypes.wfButtonUp)
            'oCol.sButtonImage = "image_tab_select.png"
            'oCol.sToolTip = "Selektieren"


            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "MailTEmplateDesc"
            End If

            sClause = sClause.gsClauseAnd("ShowMobileSupportMail=-1")
            sFields = "MailTemplateID AS RowID, MailTEmplateDesc,MailTemplateFrom,MailTemplateGroupDesc"
            sTables = "tdMailTemplates MT LEFT JOIN tdMailTemplateGroups MTG ON mTg.MailTemplateGroupName=Mt.MailTemplateGroupName"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                rs.MoveNext()
            Loop
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
