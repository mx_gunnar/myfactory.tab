<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Base.SupportEMailNewPreview" CodeFile="SupportEMailNewPreview.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr height="5px" valign="top">
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="top" height="45px">
            <td>
                <div runat="server" id="DivMailTo" style="width: 90%; height: 45px; padding-left: 20px; font-size: 8pt; color: #000; padding-right: 20px; ">
                </div>
            </td>
        </tr>
        <tr valign="top" height="2px">
        <td></td></tr>
        <tr valign="top" height="*">
            <td>
                <div runat="server" id="DivMailContent" style="width: 90%; height: 100%; overflow-y: auto;
                    overflow-x: auto; padding: 20px; background-color: WhiteSmoke;">
                    Inhalt der EMail wird geladen... 
                </div>
            </td>
        </tr>
        <tr valign="bottom" height="120px">
            <td>
                <table width="100%" height="100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td width="120px">
                            <myfactory:wfButton ID="cmdSend" sText="Senden" runat="server" sOnClick="mOnSend();">
                            </myfactory:wfButton>
                        </td>
                        <td width="120px">
                            <myfactory:wfButton ID="cmdBack" sText="Zur�ck" runat="server" sOnClick="mOnBack();">
                            </myfactory:wfButton>
                        </td>
                        <td width="120px">
                            <myfactory:wfButton ID="cmdCancel" sText="Abbrechen" runat="server" sOnClick="mOnCancel();">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
