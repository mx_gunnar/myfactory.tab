'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportEMailNewPreview.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportEMailNewPreview
'--------------------------------------------------------------------------------
' Created:      08.10.2013 12:23:44, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.BusinessTasks.Base.MailTools
Imports myfactory.Sys.Email
Imports myfactory.BusinessTasks.CRM.Main
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Base

    Partial Class SupportEMailNewPreview
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            '---- page parameter
            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lMailTemplateID As Integer = glCInt(Request.QueryString("MailTemplateID"))
            Dim lCasePosID As Integer = glCInt(Request.QueryString("CasePosID"))

            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = lCaseID.ToString
            Me.asPageParams(1) = lMailTemplateID.ToString
            Me.asPageParams(2) = lCasePosID.ToString


            '---- data parameter
            Dim sFrom As String
            Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdSupportCases", "CaseID=" & lCaseID)
            Dim lContactID As Integer = DataMethods.glGetDBValue(oClientInfo, "ContactID", "tdSupportCases", "CaseID=" & lCaseID)

            sFrom = DataMethods.gsGetDBValue(oClientInfo, "MailTemplateFrom", "tdMailTemplates", "MailTemplateID=" & lMailTemplateID)
            If sFrom = "" Then sFrom = "support@myfactory.com"

            '-----------------------   generate mail preview  -----------------
            Dim sHeaderText As String = ""
            Dim sReplyText As String = ""
            sReplyText = ClientValues.gsGetClientValue(oClientInfo, "TabSupportEmailReplyText")


            Dim sMailContent As String
            Dim sSubject As String = ""
            Dim sEMailSender As String = ""
            Dim oTo As New EMailAddress

            oTo.lAddressID = lAddressID
            oTo.lContactID = lContactID
            oTo.SetEMail(oClientInfo)
            oTo.SetEMailAlias(oClientInfo)

            sMailContent = MailTools.gsGetTemplateText(oClientInfo, lMailTemplateID, oTo, sSubject, "")

            SupportCaseTools.gbReplacePlaceholders(oClientInfo, sMailContent, lCaseID)
            sMailContent = sMailContent.Replace("$SupportReplyMobile$", sReplyText)

            '--------- set Text / To to HTML ------------
            SupportCaseTools.gbReplacePlaceholders(oClientInfo, sSubject, lCaseID)

            sHeaderText = Dictionary.gsTranslate(oClientInfo, "Von") & ": " & sFrom
            sHeaderText = sHeaderText & "<br />"
            sHeaderText = sHeaderText & Dictionary.gsTranslate(oClientInfo, "An") & ": " & oTo.sEMail
            sHeaderText = sHeaderText & "<br />" & Dictionary.gsTranslate(oClientInfo, "Betreff") & ": " & sSubject

            Me.DivMailTo.InnerHtml = sHeaderText
            Me.DivMailContent.InnerHtml = sMailContent










            ' so sende ich das dann:         

            'bOK = MailTools.gbSaveMail(oClientInfo, _
            '            0, _
            '            0, _
            '            sFrom, _
            '            sTo, _
            '            "", _
            '            "", _
            '            sSubject, _
            '            sMailContent, _
            '            0, _
            '            True, sContactDesc, sContactType, sContactTopic, sContactResult, _
            '            sDocIDs)




        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
