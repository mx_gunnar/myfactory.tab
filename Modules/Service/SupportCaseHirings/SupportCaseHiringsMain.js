﻿var sProcessPage = msWebPageRoot + '/ie50/crm/support/SupportCase/HiringTimes/pos/SupportCaseHiringTimePosProcess.aspx';
var mlst = 'lstMain';
var mlCaseID = msPageParams[0];
var mlCurrentPosID;
var mlPage = msPageParams[1];

function mOnLoad() {
    gListViewSetUserData(mlst, mlCaseID);

    if (!mlPage) mlPage = -1;

    gListViewLoadPage(mlst, mlPage);
}


function mOnListViewClick(sListView, sID) {
    mlCurrentPosID = sID;
    var sXml = gsCallServerMethod(sProcessPage + '?Cmd=GetPosXml&EntryID=' + sID);
    // refresh details
    gbSetDlgParamsXML('dsoDetails', sXml, false);
}

function mOnListViewBtnClick(sListView, sColID, sItemID) {

    mlCurrentPosID = sItemID;

    switch (sColID) {
        case 'CmdDetail':
            mOnEdit(sItemID);
            break
        case 'CmdDelete':
            mOnDelete(sItemID);
            break
        default:
            mSaveInputValue(sColID, sItemID, '');
            break
    }
}

function mOnListViewInputChange(sListView, sColID, sItemID, sNewValue) {

    mlCurrentPosID = sItemID;

    switch (sColID) {
        case 'tdHiringTimes.EntryDesc':
            sNewValue = gsEncodeStringURL(sNewValue);
            break
        default:
            break
    }
    mSaveInputValue(sColID, sItemID, sNewValue);
}

function mSaveInputValue(sColID, sItemID, sNewValue) {
    var sURL = sProcessPage + '?Cmd=UpdateList&EntryID=' + sItemID + '&ColID=' + sColID + '&Value=' + sNewValue;
    var sRes = gsCallServerMethod(sURL);

    if (sRes == 'OK') {
        mRefresh(sItemID);
    } else {
        gAlert(sRes);
    }
}

function mbCheckAnyRunning() {

    var sURL = sProcessPage + '?Cmd=CheckAnyRunning';
    var sInfo = gsCallServerMethod(sURL);

    if ((sInfo != "OK") && !gbConfirmYesNo(sInfo)) {
        return false;
    }

    return true
}

function mOnNew() {

    if (!mbCheckAnyRunning()) {
        return
    }

    var sURL = sProcessPage + '?Cmd=NewEntry&CaseID=' + mlCaseID;
    var sRes = gsCallServerMethod(sURL);

    if (sRes.substr(0, 3) == 'OK;') {

        var asRes = sRes.split(';');

        var sItemID = asRes[1];

        if (sItemID == '0') {

            var lPage = glListViewGetPage(mlst);

            // navitate to detail page
            sURL = '../SupportCaseHiringsDetails/SupportCaseHiringsDetailsMain.aspx' +
                    '?ClientID=' + msClientID + '&EntryID=' + sItemID + '&Cmd=New&CaseID=' + mlCaseID + '&lPage=' + lPage;

            document.location = sURL;
            
        } else {
            mRefresh(sItemID);
        }

    } else {
        gAlert(sRes);
    }

}

function mOnEdit(sItemID) {

    var lPage = glListViewGetPage(mlst);

    // navitate to detail page
    var sURL = '../SupportCaseHiringsDetails/SupportCaseHiringsDetailsMain.aspx?ClientID=' +
                msClientID + '&Cmd=Edit&CaseID=' + mlCaseID + '&EntryID=' + sItemID + '&lPage=' + lPage;

    document.location = sURL;
}


function mOnDelete(lPosID) {

    if (lPosID != '') {

        if (gbConfirmYesNo(msTerms[0])) {

            var sURL = sProcessPage + '?Cmd=Delete&EntryID=' + lPosID;
            var sRes = gsCallServerMethod(sURL);

            if (sRes.substr(0, 3) == 'OK') {
                mRefresh(0);
            } else {
                gAlert(sRes);
            }

        }
    }
}

function mRefresh(sReturn) {

    if (sReturn) {
        mlCurrentPosID = sReturn;
    }

    var lPage = glListViewGetPage(mlst);

    gListViewLoadPage(mlst, lPage);

    if ((mlCurrentPosID) && (mlCurrentPosID.length > 0)) {
        gListViewItemSelect(mlst, mlCurrentPosID);
        gListViewScrollIntoView(mlst, mlCurrentPosID, true);
        mOnListViewClick(mlst, mlCurrentPosID);
    }

}

function mOnSetDirty() {
    if (event.type == 'change') {
        switch (event.srcElement.id) {
            case 'txtMemo':

                var sValues = gsXMLDlgParams(document.all.frmMain, '', true);

                var sURL = sProcessPage + '?Cmd=UpdateMemo';
                var sRes = gsCallServerMethod(sURL, sValues);

                if (sRes == 'OK') {

                } else {
                    gAlert(sRes);
                }

                break
        }
    }
}

