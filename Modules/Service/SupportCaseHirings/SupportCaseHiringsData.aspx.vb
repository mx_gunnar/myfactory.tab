'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseHiringsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SupportCaseHiringsData
'--------------------------------------------------------------------------------
' Created:      25.05.2016 16:31:29, APriemyshev
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseHiringsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================


        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                ByVal oListView As AspListView) As Boolean

            '---- ListView Properties ------

            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportCaseHiringsData.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------

            Dim oCol As AspListViewCol

            oCol = oListView.oCols.oAddCol("WorkingDate", "Tag", "120", , , True, False) '0

            oCol = oListView.oCols.oAddCol("StartTime", "Von", "80", wfEnumDataTypes.wfDataTypeTime, , True, False) '1

            oCol = oListView.oCols.oAddCol("EndTime", "Bis", "80", wfEnumDataTypes.wfDataTypeTime, , True, False) '2

            oCol = oListView.oCols.oAddCol("Amount", "Menge (h)", "80", , wfEnumAligns.wfAlignLeft, True, False) '3

            oCol = oListView.oCols.oAddCol("EntryDesc", "Bezeichnung", "*", , wfEnumAligns.wfAlignLeft, True, False) '4

            oCol = oListView.oCols.oAddCol("TimeTypeDesc", "Zeittyp", "120", , wfEnumAligns.wfAlignLeft, True, False) '5

            oCol = oListView.oCols.oAddCol("isCalculated", "Abgerechnet", "120", , wfEnumAligns.wfAlignLeft, True, False) '6
            oCol.bHidden = True

            oCol = oListView.oCols.oAddCol("CmdStop", "Stop", "40", , wfEnumAligns.wfAlignCenter) '7
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.sCtlHeight = "29"
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDetail", "", "20", , wfEnumAligns.wfAlignCenter) '8
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDelete", "L�.", "20", , wfEnumAligns.wfAlignCenter) '9
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("image", "", "20", , wfEnumAligns.wfAlignCenter) '10
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeImage

            '---- Data ---------------------

            Dim sOrder As String = oListView.sOrderCol

            If sOrder = "" Then
                sOrder = "WorkingDate asc, StartTime asc"
            ElseIf sOrder = "isCalculated" Then
                sOrder = "isnull(isCalculated, 0)"
            End If

            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            Dim lCaseID As Integer = glCInt(oListView.sUserData)

            'Dim sYes As String = Dictionary.gsTranslate(oClientInfo, "Ja")
            'Dim sNo As String = Dictionary.gsTranslate(oClientInfo, "Nein")

            Dim sClause As String = "tdHiringTimes.EntityID=7600 AND sc.CaseID=" & lCaseID


            '----- permissions -----
            Dim sPermClause As String

            sPermClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "SupportCases")

            sClause = sClause.gsClauseAnd(sPermClause)

            Dim oFields As New StringBuilder
            oFields.Append("tdHiringTimes.EntryID AS RowID, WorkingDate, StartTime, EndTime, Amount,")
            oFields.Append("tdHiringTimes.EntryDesc,")
            oFields.Append("TimeTypeDesc, IsCalculated,")

            oFields.Append(Dictionary.gsTranslate(oClientInfo, "Stop").gs2Sql)

            oFields.Append(",'...','x',")
            oFields.Append("CASE WHEN EndTime IS NULL THEN '/images/button_record.gif' ELSE '/images/button_done.gif' END")

            Dim sTables As String = "tdHiringTimes" & _
                        " INNER JOIN tdSupportCases sc on sc.CaseID= tdHiringTimes.RecordID" & _
                        " LEFT JOIN tdHiringTimeTypes ON tdHiringTimeTypes.TimeTypeID=tdHiringTimes.TimeTypeID"

            Dim rs As Recordset = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                oFields.ToString, sTables, sClause, , sOrder)

            Dim cTotalAmount As Decimal = 0

            Dim oRow As AspListViewRow

            Do Until rs.EOF

                cTotalAmount += rs("Amount").cValue

                oRow = oListView.oRows.oAddRow(rs(0).sValue)

                oRow.SetRecord(rs, True)

                If rs("IsCalculated").bValue Then
                    'oRow.Value(6) = sYes
                    oRow.bSetEmpty(8)
                    oRow.bSetEmpty(9)
                    oRow.lColType(5) = wfEnumAspListViewColTypes.wfColTypeText
                    oRow.bSetEmpty(7)

                Else
                    'oRow.Value(6) = sNo
                End If

                If rs("EndTime").sValue <> "" Then
                    oRow.bSetEmpty(7)
                End If

                rs.MoveNext()
            Loop

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            If oListView.lRecordCountTotal > 0 Then
                'add summe
                oRow = oListView.oRows.oAddRow("TotalAmount")
                oListView.lRecordCountTotal += 1
                oRow.bRowBold = True

                oRow.sValue(2) = Dictionary.gsTranslate(oClientInfo, "Summe:")
                oRow.sValue(3) = gsCStr(cTotalAmount)
                oRow.lColType(4) = wfEnumAspListViewColTypes.wfColTypeText
                oRow.bSetEmpty(7)
                oRow.bSetEmpty(8)
                oRow.bSetEmpty(9)

            End If

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
