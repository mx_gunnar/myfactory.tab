<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls"  %>

<%@ Page Inherits="ASP.Tab.Service.SupportCaseHiringsMain" CodeFile="SupportCaseHiringsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="2" width="98%"
        style="table-layout: fixed;">
        <tr valign="top">
            <td colspan="2">
                <myfactory:wfListView ID="lstMain" runat="server" sListViewDataPage="SupportCaseHiringsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <!-- bottom menue  -->
        <tr height="120px" valign="bottom">
            <td colspan="2">
                <table width="100%" height="100%">
                    <tr>
                        <td width="200px" id="trNaviLeft">
                            <myfactory:wfLabel runat="server" ID="lblNaviLeft" sStyle="margin-left:10px;margin-bottom:30px">
                            </myfactory:wfLabel>
                        </td>
                        <td align="right" width="*">               
                            <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNavi">
                            </myfactory:wfLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
