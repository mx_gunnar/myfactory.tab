'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseHiringsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseHiringsMain
'--------------------------------------------------------------------------------
' Created:      26.05.2016 16:31:29, APriemyshev
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseHiringsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim sEntryID As String = Request.QueryString("EntryID")
            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lPage As Integer = glCInt(Request.QueryString("lPage"))

            If lPage = 0 Then
                lPage = -1
            End If

            Me.sOnLoad = "mOnLoad();"

            'for when the CaseID is empty (when certain navigation paths are taken)
            If lCaseID = 0 AndAlso sEntryID <> "" Then
                Dim sTables As String = "tdHiringTimes INNER JOIN tdSupportCases sc on sc.CaseID= tdHiringTimes.RecordID"
                lCaseID = DataMethods.glGetDBValue(oClientInfo, "CaseID", sTables, "EntityID=7600 AND EntryID=" & glCInt(sEntryID))
            End If

            '-------- write navi ---------------
            Dim osHTML As New FastString

            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")

            Dim frs As FastRecordset = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", "tsTabModules", _
                        "ParentModule='SupportCase' OR ParentModule='SupportCase_Hirings'", _
                        , "ShowIndex", wfDataSourceSystem)

            Do Until frs.EOF

                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)

                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", _
                "CaseID=" & lCaseID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString

            osHTML.bAppend("</nobr>")

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = lCaseID.ToString
            Me.asPageParams(1) = lPage.ToString

            ReDim Me.asTerms(1)
            Me.asTerms(0) = "M�chten Sie diesen Datensatz wirklich l�schen?"
            Me.asTerms(1) = "Dieser Eintrag ist bereits abgerechnet und kann deshalb nicht gel�scht werden."

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
