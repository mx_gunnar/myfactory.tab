'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseHiringsDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseHiringsDetailsMain
'--------------------------------------------------------------------------------
' Created:      25.05.2016 16:31:29, APriemyshev
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.BusinessTasks.Resources.ToolFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseHiringsDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad
            Dim rs As Recordset = Nothing

            '------ Dialog Params --------
            Me.sOnLoad = "mOnLoad()"
            Me.gAddScriptLink("wfDlgParams.js", True)

            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim lEntryID As Integer = glCInt(Request.QueryString("EntryID"))
            Dim lPage As Integer = glCInt(Request.QueryString("lPage"))

            If lCaseID = 0 AndAlso lEntryID <> 0 Then
                Dim sTables As String = "tdHiringTimes INNER JOIN tdSupportCases sc on sc.CaseID= tdHiringTimes.RecordID"
                lCaseID = DataMethods.glGetDBValue(oClientInfo, "CaseID", sTables, "EntityID=7600 AND EntryID=" & lEntryID)
            End If

            Me.sTitle = "Erfassung der Bearbeitungszeit"

            If lEntryID = 0 Then

                Dim oParams As New QueryParams("CaseID", lCaseID)

                Me.dlgMain.oDialog.oField("CaseNumber").oMember.Prop("Value") = DataMethods.gsGetDBValuePQ(oClientInfo, "CaseNumber", "tdSupportCases", "CaseID=@CaseID", oParams)
                Me.dlgMain.oDialog.oField("CaseDesc").oMember.Prop("Value") = DataMethods.gsGetDBValuePQ(oClientInfo, "CaseDesc", "tdSupportCases", "CaseID=@CaseID", oParams)

                Dim lDefaultTimeTypeID As Integer = DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdSupportCaseGeneralProperties", "PropertyName='DefaultTimeTypeID'")
                Dim lDefaultActivityID As Integer = DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdSupportCaseGeneralProperties", "PropertyName='DefaultActivityID'")

                Me.dlgMain.oDialog.oField("TimeTypeID").oMember.Prop("Value") = lDefaultTimeTypeID
                Me.dlgMain.oDialog.oField("ActivityID").oMember.Prop("PARAMS") = lDefaultTimeTypeID
                Me.dlgMain.oDialog.oField("ActivityID").oMember.Prop("Value") = lDefaultActivityID

                Dim dtStart As Date = Date.Now

                Me.dlgMain.oDialog.oField("StartTime").oMember.Prop("Value") = dtStart.ToString("HH:mm:ss")

            Else

                Dim sClause As String = "tdHiringTimes.EntityID=7600 AND sc.CaseID=" & lCaseID & " AND EntryID=" & lEntryID
                Dim sFields As String = "EntryID,IsCalculated,RecordID,ResourceID,WorkingDate,Amount,StartTime,EndTime,TimeTypeID,ActivityID,EntryDesc,Memo,CaseNumber,CaseDesc"
                Dim sTables As String = "tdSupportCases sc INNER JOIN tdHiringTimes on sc.CaseID= tdHiringTimes.RecordID"

                rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause)

            End If

            If rs IsNot Nothing AndAlso Not rs.EOF Then
                rs("StartTime").Value = gsGetTimeFromDate(rs("StartTime").dtValue)
                rs("EndTime").Value = gsGetTimeFromDate(rs("EndTime").dtValue)
                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
            End If

            Me.dlgMain.oDialog.oField("RecordID").oMember.Prop("Value") = lCaseID

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = lCaseID.ToString
            Me.asPageParams(1) = lEntryID.ToString
            Me.asPageParams(2) = lPage.ToString

            ReDim Me.asTerms(0)
            Me.asTerms(0) = "Dieser Servicefall ist bereits geschlossen!"


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
