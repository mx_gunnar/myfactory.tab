﻿var sProcessPage = msWebPageRoot + '/ie50/crm/support/SupportCase/HiringTimes/pos/SupportCaseHiringTimePosProcess.aspx';
var mlPosID = msPageParams[1];
var mlCaseID = msPageParams[0];
var mlPage = msPageParams[2];
var msOldValues;

function mOnLoad() {

    gAddEventListener(document.getElementById('cboTimeTypeID'), 'change', mRefreshActivitys);

      if (mlPosID != 0) {
        msOldValues = gsXMLDlgParams(document.all.frmMain, '', true);
    }

}

function mOnCancel() {
    mNavigateBack();
}

function mOnSave() {
    var sValues = gsXMLDlgParams(document.all.frmMain, '', true);

    if (mlPosID != 0) {
        sValues = msOldValues + ';' + sValues;
    }

    var sRes = gsCallServerMethod(sProcessPage + '?Cmd=Save&CaseID=' + mlCaseID + '&EntryID=' + mlPosID, sValues);

    if (sRes.substr(0, 3) == 'OK;') {
        mNavigateBack();
    } else {
        gAlert(sRes);
        return
    }

}

function mRefreshActivitys() {
    var TimeTypeValue = document.all.cboTimeTypeID.value;
    gRefreshDataCombo('cboActivityID', 'ProjectActivitiesActive', TimeTypeValue, false, '');
}


function mOnSetDirty() {

    if (event.type == 'change' || event.type == 'click') {
        switch (event.srcElement.id) {
            case 'cboTimeTypeID':
                mRefreshActivitys();
                break
            default:

                if (event.srcElement.id == 'txtStartTime' || event.srcElement.id == 'txtEndTime' || event.srcElement.id == 'txtCmdStartTime' || event.srcElement.id == 'txtCmdEndTime') {

                    var sParams = gsXMLDlgParams(document.all.frmMain, '', true);
                    var sURL = sProcessPage + '?Cmd=TimeChange';

                    var sRes = gsCallServerMethod(sURL, sParams);

                    if (sRes.substr(0, 3) == 'OK;') {
                        var sReturn = sRes.substr(3);
                        document.all.item('txtAmount').value = sReturn;
                    }
                    else {
                        gAlert(sRes);
                    }
                }

                if (event.srcElement.id == 'txtAmount') {
                    var sParams = gsXMLDlgParams(document.all.frmMain, '', true);
                    var sURL = sProcessPage + '?Cmd=ChangeAmount';

                    var sRes = gsCallServerMethod(sURL, sParams);

                    if (sRes.substr(0, 3) == 'OK;') {
                        var sReturn = sRes.substr(3);
                        document.all.item('txtEndTime').value = sReturn;
                    }
                    else {
                        gAlert(sRes);
                    }

                }

                break
        }
    }
}

function mNavigateBack()
{
     var sURL = "../SupportCaseHirings/SupportCaseHiringsMain.aspx?ClientID=" + msClientID;
     sURL += "&CaseID=" + mlCaseID + "&EntryID=" + mlPosID + "&lPage=" + mlPage;
  
    document.location = sURL;
}



