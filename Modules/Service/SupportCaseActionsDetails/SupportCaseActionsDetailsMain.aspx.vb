'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseActionsDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseActionsDetailsMain
'--------------------------------------------------------------------------------
' Created:      7/21/2014, JSchweighart
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseActionsDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lSupportCaseActionID As Integer = glCInt(Request.QueryString("SupportCaseActionsID"))
            Dim lSupportCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            Dim sOrigin As String = gsCStr(Request.QueryString("Origin"))
            Dim rs As Recordset

            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = lSupportCaseActionID.ToString
            Me.asPageParams(1) = lSupportCaseID.ToString
            Me.asPageParams(2) = sOrigin

            Me.gAddScriptLink("wfDlgParams.js", True)
            Me.sOnLoad = "mOnLoad()"

            If lSupportCaseActionID > 0 Then

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "p.*, a.AddressID, a.Matchcode, a.EMail,sc.ContactID,c.EMail As ContactEMail, sc.CompletedDate", _
                            "tdSupportCasePos p" & _
                                " INNER JOIN tdSupportCases sc ON p.CaseID=sc.CaseID" & _
                                " LEFT JOIN tdAddresses a ON sc.AddressID=a.AddressID" & _
                                " LEFT JOIN tdContacts c ON sc.ContactID=c.ContactID", _
                            "p.PosID=" & lSupportCaseActionID)

            Else

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "a.AddressID, a.Matchcode, a.EMail,'' As PosDesc,'' As PosDate,'' As PosDueDate,sc.ContactID,c.EMail As ContactEMail, sc.CompletedDate", _
                            "tdSupportCases sc" & _
                                " LEFT JOIN tdAddresses a ON sc.AddressID=a.AddressID" & _
                                " LEFT JOIN tdContacts c ON sc.ContactID=c.ContactID", _
                            "sc.CaseID=" & lSupportCaseID)

                If rs("PosDate").sValue.Length = 0 Then
                    rs("PosDate").Value = DateTime.Now
                End If

                If rs("PosDueDate").sValue.Length = 0 Then
                    rs("PosDueDate").Value = DateTime.Now
                End If

                If String.IsNullOrEmpty(rs("PosDesc").sValue) Then
                    rs("PosDesc").Value = Dictionary.gsTranslate(oClientInfo, "Bearbeitung")
                End If

            End If

            Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)

            'disable save if support case is set completed
            If rs("CompletedDate").sValue <> "" Then
                Me.CmdOK.Visible = False
            End If

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
