'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseActionsDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SupportCaseActionsDetailsProcess
'--------------------------------------------------------------------------------
' Created:      7/16/2014, JSchweighart
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.BusinessTasks.CRM.Main
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Tools.StringFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseActionsDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String


            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd

                Case "SaveCase"
                    Return msSaveSupportCaseAction(oClientInfo, glCInt(Request.QueryString("SupportCaseActionID")),
                                                   glCInt(Request.QueryString("CaseID")))
                Case Else
                    Return "unknown command"
            End Select

            Return "unknown command"

        End Function

        ''' <summary>
        ''' update an existing support case action or create a new one, depending on pramaters value
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lSupportCaseActionID"></param>
        ''' <param name="lSupportCaseID"></param>
        ''' <returns>"OK;" or error message</returns>
        ''' <remarks></remarks>
        Public Function msSaveSupportCaseAction(ByVal oClientInfo As ClientInfo, ByVal lSupportCaseActionID As Integer,
                                                ByVal lSupportCaseID As Integer) As String

            Dim sXml As String = String.Empty
            Dim oXml As New XmlDocument
            Dim oNode As XmlNode = Nothing
            Dim oCase As New SupportCase
           
            If lSupportCaseActionID <> 0 AndAlso lSupportCaseID = 0 Then

                lSupportCaseID = DataMethods.glGetDBValue(oClientInfo, "CaseID", "tdSupportCasePos", "PosID=" & lSupportCaseActionID)

            End If

            'load existing support case
            If Not oCase.gbLoad(oClientInfo, lSupportCaseID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Supportfalls")
            End If

            If Not oXml.load(Request) Then
                oClientInfo.LogSysEvent(oXml.parseError.reason)
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen des Xml")
            End If

            ' convert from dialog xml
            sXml = XmlFunctions.gsXmlConvertFromDialog(oXml.xml)

            If Not oXml.loadXML(sXml) Then
                oClientInfo.oErrors.bAdd(oXml.parseError.reason)
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen des Xml")
            End If

            oNode = oXml.selectSingleNode("Values/Params/DlgParams")

            If oNode Is Nothing Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen eines XML Knotens")
            End If

            'check if support case action is new and create one accordingly
            If lSupportCaseActionID = 0 AndAlso lSupportCaseID <> 0 Then

                Dim oPos As New SupportCasePos()
                If Not oPos.gbNew(oClientInfo, oCase) Then
                    Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen der Position!")
                End If

                ' set new values to position
                For Each oChild As XmlNode In oNode.childNodes()
                    oPos.oMembers.Prop(oChild.nodeName) = oChild.text
                Next

                'save position
                If Not SupportCasePosFunctions.gbCreatePosition(oClientInfo, oPos) Then
                    Return Dictionary.gsTranslate(oClientInfo, "Die Supportfall Aktion konnte nicht gespeichert werden!") & vbCrLf() & oClientInfo.oErrors.sErrText()
                End If

                Return String.Format("OK;")

            End If

            For Each oPos As SupportCasePos In oCase.oPositions
                If oPos.oMembers.lProp("PosID") = lSupportCaseActionID Then
                    For Each oChild As XmlNode In oNode.childNodes()
                        oPos.oMembers.Prop(oChild.nodeName) = oChild.text
                    Next
                    Exit For
                End If
            Next

            If Not oCase.gbSave(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Die Aktion konnte nicht gespeichert werden.") & vbCrLf() & oClientInfo.oErrors.sErrText()
            End If

            Return "OK;"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
