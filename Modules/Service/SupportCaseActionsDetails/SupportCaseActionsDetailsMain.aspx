<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls"  %>

<%@ Page Inherits="ASP.Tab.Service.SupportCaseActionsDetailsMain" CodeFile="SupportCaseActionsDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="100px">
            <td>
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/TAB/Modules/SErvice/SupportCaseActionsDetails/dlgSupportCaseActionsDetails.xml" />
                </form>
            </td>
        </tr>
        <tr height="*">
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="bottom" height="100px">
            <td>
                <table width="100%" height="100%">
                    <tr>
                        <td align="right" width="120px">
                            <myfactory:wfButton runat="server" ID="CmdOK" sText="OK" sOnClick="mOnOK();">
                            </myfactory:wfButton>
                            <myfactory:wfButton runat="server" ID="CmdCancel" sText="Abbrechen" sOnClick="mOnCancel();">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
