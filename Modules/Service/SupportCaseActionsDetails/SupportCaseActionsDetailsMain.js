﻿/* (c) myfactory International  2014 */

function mOnLoad()
{
    
}

function mOnSetDirty()
{
    if (event.type == "change")
    {

        if (event.srcElement.id == "cboHandlingUserSelect")
        {
            document.all.txtHandlingUser.value = document.all.cboHandlingUserSelect.value;
        }
    }
}

function mOnOK()
{
    // save case
    var sURL = "SupportCaseActionsDetailsProcess.aspx";
    sURL += "?Cmd=SaveCase";
    sURL += "&SupportCaseActionID=" + msPageParams[0];
    sURL += "&CaseID=" + msPageParams[1];
    var sXML = gsXMLDlgParams(frmMain, '', false);
    var sRes = gsCallServerMethod(sURL, sXML);

    if (sRes.substr(0, 3) == "OK;")
    {
       mNavigateBack();
    }
    else
    {
        alert(sRes);
    }
}

function mOnCancel()
{
    mNavigateBack();
}

function mNavigateBack()
{
    var sURL = "";

    if (msPageParams[2] == "Details")
    {
        sURL = "../SupportCaseActions/SupportCaseActionsMain.aspx?ClientID=" + msClientID;
        sURL += "&SupportCaseActionID=" + msPageParams[0];
    }
    else
    {
        sURL = "../SupportCaseDetails/SupportCaseDetailsMain.aspx?ClientID=" + msClientID;
        sURL += "&CaseID=" + msPageParams[1];
    }

    document.location = sURL;
}

function mSupportCaseActions()
{
    var sSupportCaseID = msPageParams[0];

    var sUrl = "../SupportCaseActions/SupportCaseActionsMain.aspx?ClientID=" + msClientID;
    sUrl += "&CaseID=" + sSupportCaseID;
    document.location = sUrl;
}