'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ServiceMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ServiceMain
'--------------------------------------------------------------------------------
' Created:      23.09.2013 17:00:43, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportEMailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad();"

            Dim sOnlyNotCompleted As String = Request.QueryString("OnlyNotCompleted")
            Dim sFromDate As String = Request.QueryString("FromDate")
            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))

            ReDim Me.asPageParams(1)


            If lCaseID > 0 Then
                'show only emails for this support case
                Me.dlgMain.sDialog = "/tab/modules/Service/SupportEMails/dlgSuppotEMailsCase.xml"
                Me.dlgMain.oDialog.oField("CaseName").oMember.Prop("VALUE") = DataMethods.gsGetDBValue(oClientInfo, "CaseNumber + ', ' + CaseDesc", "tdSupportCases", "CaseID=" & lCaseID)
                Me.asPageParams(0) = lCaseID.ToString
            Else
                Me.dlgMain.oDialog.oField("From").oMember.Prop("VALUE") = sFromDate
                Me.dlgMain.oDialog.oField("OnlyNotCompleted").oMember.Prop("VALUE") = gbCBool(sOnlyNotCompleted)
            End If


            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ServiceDetails' AND ModuleName <> 'Service_EMails' ", _
                        , "ShowIndex", _
                         PublicEnums.wfEnumDataSources.wfDataSourceSystem)

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", ""))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString

            '------------------------------------

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
