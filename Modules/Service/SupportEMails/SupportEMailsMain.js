﻿


function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData = "";

    if (msPageParams[0] && msPageParams[0] != "" && msPageParams[0] != "0")
    {
        sData = msPageParams[0];
    }
    else
    {
        sData = "0;" + document.all.txtFrom.value;
        sData = sData + ";" + document.all.txtTo.value;
        sData = sData + ";" + document.all.chkOnlyNotCompleted.checked;
    }

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{

    if (event.type == "change" || event.type == "click")
    {
        mRefreshList();
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{

    if (sColID == "CmdDetails")
    {
        mNavigateToDetails(sItemID);
    }
    else if (sColID == "CmdMail")
    {
        // ToDo: open EMail in new Side
        var sMailURL = gsTemplateForm("EMail", gsListViewGetItemUserData(sListView, sItemID), true);

        window.open(sMailURL);

    }

} 

function mOnListViewClick(sListView, sItemID)
{
    //mNavigateToDetails(sItemID);
}

function mOnListViewChkClick(sListView, sColID, sItemID)
{
    if (sColID == "Completed")
    {
        var sURL = "SupportEMailsProcess.aspx?Cmd=ChangeCompleted&PosID=" + sItemID;
        sURL = sURL + "&Completed=" + gbListViewGetChecked(sListView, sColID, sItemID);

        var sRes = gsCallServerMethod(sURL, '');

        if (sRes.substr(0, 3) != "OK;")
        {
            alert(sRes.substr(4, sRes.length - 4));
        }

    }
}

function mNavigateToDetails(sPosID)
{
    // navitate to case detail page
    var sURL = '../SupportCaseDetails/SupportCaseDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&CasePosID=' + sPosID;

    document.location = sURL;
}