'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ServiceMainProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for ServiceMainProcess
'--------------------------------------------------------------------------------
' Created:      24.09.2013 12:55:53, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Email

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class ServiceMainProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lPosID As Integer = glCInt(Request.QueryString("PosID"))

            Select Case sCmd
                Case "ChangeCompleted"
                    Return msChangeCompleted(oClientInfo, lPosID, Request.QueryString("Completed"))
            End Select


            Return "unknown command"
        End Function



        Private Function msChangeCompleted(ByVal oClientInfo As ClientInfo, ByVal lPosID As Integer, ByVal sCompleted As String) As String

            Dim sUpdate As String = "Completed="
            Dim lChecked As Integer = 0
            Dim lFolderID As Integer = 0

            If sCompleted = "true" Then
                sUpdate = sUpdate & "-1"
                lChecked = -1
            Else
                sUpdate = sUpdate & "0"
            End If

            If DataMethods2.glDBUpdate(oClientInfo, "tdSupportCasePos", sUpdate, "PosID=" & lPosID) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Beim Speichern ist ein Fehler aufgetreten")
            End If


            ' move mail to completed ( or not completed ) folder
            If lChecked = -1 Then
                lFolderID = DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdSupportCaseGeneralProperties", "PropertyName='SupportMailCompletedFolder'")
            Else
                lFolderID = DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdSupportCaseGeneralProperties", "PropertyName='SupportMailUnCompletedFolder'")
            End If

            If lFolderID <> 0 Then
                Dim lMailID As Integer
                Dim oEmail As New EMail

                lMailID = DataMethods.glGetDBValue(oClientInfo, "MailID", "tdSupportCasePos", "PosID=" & lPosID)
                If oEmail.gbLoadInfo(oClientInfo, lMailID) Then
                    oEmail.gbMoveMail(oClientInfo, lFolderID)
                End If
            End If

            Return "OK;"
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
