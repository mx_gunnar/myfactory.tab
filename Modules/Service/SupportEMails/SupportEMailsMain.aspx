<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Service.SupportEMailsMain" CodeFile="SupportEMailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr valign="top">
            <td width="700px" height="20px">
                <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/tab/modules/Service/SupportEMails/dlgSupportEMails.xml" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="SupportEMailsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>

                <!-- bottom menue  -->
        <tr height="120px" valign="bottom">
            <td colspan="2">
                <table width="100%" height="100%">
                    <td align="right">
                        <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                        </myfactory:wfLabel>
                    </td>
                </table>
            </td>
        </tr>

    </table>
</body>
</html>
