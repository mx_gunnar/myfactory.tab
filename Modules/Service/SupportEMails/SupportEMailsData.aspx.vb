'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ServiceMainData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ServiceMainData
'--------------------------------------------------------------------------------
' Created:      23.09.2013 17:01:03, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportEMailsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow

            Dim sFromDate As String = ""
            Dim sToDate As String = ""
            Dim sOnlyNotCompleted As String = ""
            Dim lCaseID As Integer

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportEMailsData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True


            '------ User Filter Data ----------
            Dim asUserData As String() = (oListView.sUserData & ";;;;;").Split(";"c)
            If asUserData.Length > 3 Then
                lCaseID = glCInt(asUserData(0))
                sFromDate = asUserData(1)
                sToDate = asUserData(2)
                sOnlyNotCompleted = asUserData(3)
            End If


            '---- Columns ------------------
            oListView.oCols.oAddCol("CaseNumber", "Nummer", "80", , , True, False)
            oListView.oCols.oAddCol("AdressMatchcode", "Adresse", "150", , , True, False)

            oListView.oCols.oAddCol("CaseDesc", "Betreff", "*", , , True, False)
            oListView.oCols.oAddCol("CaseState", "Status", "100", , , True, False)
            oListView.oCols.oAddCol("PosDate", "Aufnahmedatum", "120", , , True, False)
            oCol = oListView.oCols.oAddCol("Completed", "Erledigt", "80", , wfEnumAligns.wfAlignCenter, True, False)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeCheckBox

            oCol = oListView.oCols.oAddCol("CmdMail", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonEMail)
            'oCol.sWidth = "80"
            oCol.sText = "EMail"

            oCol = oListView.oCols.oAddCol("CmdDetails", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            'oCol.sWidth = "80"
            oCol.sText = "Fall"


            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "SCP.PosDate desc"
            End If

            sFields = "SCP.PosID AS RowID,SC.CaseNumber, A.Matchcode AS AdressMatchcode, SC.CaseDesc, SCS.CaseStateDesc, SCP.PosDate, SCP.Completed,'','', SCP.MailID"

            sTables = " tdSupportCases SC" & _
                      " INNER JOIN tdAddresses A ON SC.AddressID = A.AddressID" & _
                      " INNER JOIN tdSupportCasePos SCP ON SC.CaseID = SCP.CaseID" & _
                      " LEFT JOIN tdSupportCaseStates SCS ON SC.CaseState = SCS.CaseStateID "


            'sTables = " tdSupportCases SC" & _
            '          " INNER JOIN tdAddresses A ON SC.AddressID = A.AddressID" & _
            '          " INNER JOIN tdSupportCasePos SCP ON SC.CaseID = SCP.CaseID" & _
            '          " LEFT JOIN tdSupportCaseStates SCS ON SC.CaseState = SCS.CaseStateID" & _
            '          " LEFT JOIN tdContacts CO ON SC.ContactID = CO.ContactID" & _
            '          " LEFT JOIN tdMailInfos mi ON mi.MailID=SCP.MailID" & _
            '          " LEFT JOIN tdAddressGroups AG ON A.AddressGroup=AG.AddressGroupName" & _
            '          " LEFT JOIN tdCustomers C ON C.CustomerID=(SELECT TOP 1 CustomerID FROM tdCustomers WHERE tdCustomers.AddressID=A.AddressID ORDER BY AddressID)" & _
            '          " LEFT JOIN tdCustomerGroups CG ON CG.CustomerGroupName=C.CustomerGroup" & _
            '          " LEFT JOIN tdSupportDepartments SD ON SC.SupportDepartment=SD.DepartmentID" & _
            '          " LEFT JOIN tsDatabaseUsers db1 ON SC.HandlingUser = db1.UserInitials"




            '---------- Clause ---------------
            sClause = "SCP.BySupportMail=-1"
            If lCaseID > 0 Then
                sClause = sClause.gsClauseAnd("SC.CaseID=" & lCaseID)
            Else
                If Not String.IsNullOrEmpty(sFromDate) Then
                    If gbDate(sFromDate) Then
                        sClause = sClause.gsClauseAnd("SCP.PosDate >= " & gdtCDate(sFromDate).gs2Sql)
                    End If
                End If
                If Not String.IsNullOrEmpty(sToDate) Then
                    If gbDate(sToDate) Then
                        sClause = sClause.gsClauseAnd("SCP.PosDate < " & gdtCDate(sToDate).AddDays(1).gs2Sql)
                    End If
                End If
                If sOnlyNotCompleted = "true" Then
                    sClause = sClause.gsClauseAnd("SCP.Completed = 0")
                End If
            End If


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)
                oRow.sUserData = rs("MailID").sValue
                rs.MoveNext()
            Loop
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
