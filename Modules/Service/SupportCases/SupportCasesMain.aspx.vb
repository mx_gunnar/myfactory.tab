'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCasesMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCasesMain
'--------------------------------------------------------------------------------
' Created:      01.10.2013 15:43:29, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

	Partial Class SupportCasesMain
		Inherits myfactory.FrontendSystem.AspSystem.DialogPage

		'================================================================================
		' Page Members
		'================================================================================

#Region " Vom Web Form Designer generierter Code "

		'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

		End Sub

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
			'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
			InitializeComponent()
		End Sub

#End Region

		'================================================================================
		' Private Members
		'================================================================================

		'================================================================================
		' Functions
		'================================================================================

		'================================================================================
		' Method:       PageLoad
		'--------------------------------------------------------------------------------
		' Purpose:      Inits Page
		'--------------------------------------------------------------------------------
		' Parameter:
		'--------------------------------------------------------------------------------
		' Return:       
		'================================================================================

		Private Sub Page_PageLoad() Handles MyBase.PageLoad

			Me.sOnLoad = "mOnLoad();"
			Dim sState As String = Request.QueryString("State")
			Dim lAddressID As Integer = glCInt(Request.QueryString("RecordID"))	'lAddressID
			Dim sHasParendModule As String = Request.QueryString("HasParentModule")

			If Not String.IsNullOrEmpty(sState) Then
				Me.dlgMain.oDialog.oField("State").oMember.Prop("VALUE") = sState
			End If

            Me.gAddScriptLink("wfDlgParams.js", True)

            ReDim Me.asPageParams(2)
			Me.asPageParams(0) = lAddressID.ToString
			Me.asPageParams(1) = sHasParendModule

			' empty = no default value
			' 0     = today
			' 3/+3  = today + 3 days
			' -2    = today - 2 days
			Dim sDueDateDefaultValue As String = GeneralProperties.gsGetGeneralProperty(oClientInfo, "TabSupportCaseDueDateDefaultValue", "")
			If sDueDateDefaultValue <> "" AndAlso IsNumeric(sDueDateDefaultValue) Then
				Dim dtDate As Date = Date.Today.AddDays(glCInt(sDueDateDefaultValue))
				Me.dlgMain.oDialog.oField("DueDate").oMember.Prop("VALUE") = dtDate
			End If


			'-------- write navi ---------------
			If lAddressID > 0 Then
				' --- write address navi --- 
				Dim osHTML As New FastString
				Dim frs As FastRecordset
				Dim bShow As Boolean

				osHTML.bAppend("<nobr>")
				osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAddressID))
				frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
							"ModuleName,Edition,PartnerID,ModuleID", _
							"tsTabModules", _
							"ParentModule='AddressDetails' AND ModuleName <> 'Addresses_SupportCases'", _
							, "ShowIndex", _
							myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)

				Do Until frs.EOF
					bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
					If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
					If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

					If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAddressID))

					frs.MoveNext()
				Loop

				Me.lblNaviSub.sTranslatedText = osHTML.sString
				osHTML.bAppend("</nobr>")

			Else
				' --- write support navi ---
				Dim osHTML As New FastString
				Dim frs As FastRecordset
				Dim bShow As Boolean

				frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
							"ModuleName,Edition,PartnerID,ModuleID", _
							"tsTabModules", _
							"ParentModule='ServiceDetails' AND ModuleName <> 'Service_Cases' ", _
							, "ShowIndex", _
							 myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)

				Do Until frs.EOF
					bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
					If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
					If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

					If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", ""))

					frs.MoveNext()
				Loop

				Me.lblNaviSub.sTranslatedText = osHTML.sString
				Me.lblNaviLeft.sTranslatedText = PageTools.gsWriteSubNaviLink(oClientInfo, "Service_Cases_New", "../../../", "CaseID=-1")

				Me.lblScroll1.Visible = False
				Me.lblScroll2.Visible = False

			End If


		End Sub

	End Class

End Namespace

'================================================================================
'================================================================================
