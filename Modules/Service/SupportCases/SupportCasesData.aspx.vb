'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCasesData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SupportCasesData
'--------------------------------------------------------------------------------
' Created:      01.10.2013 15:43:42, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		09.07.2014, JSchweighart
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCasesData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
            Dim asUserData As String() = (oListView.sUserData & ";;;;;;;;;;;").Split(";"c)
            Dim sSearch As String
            Dim sAddressSearch As String
            Dim lState As Integer
            Dim lAddressID As Integer
            Dim bGroupByCurrentUser As Boolean
			Dim sSupportDepartment As String
			Dim sHandlingUser As String
			Dim sDueDate As String

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportCasesData.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------

            oListView.oCols.oAddCol("CaseNumber", "Nummer", "70", , , True, False)
			oListView.oCols.oAddCol("DueDate", "F�lligkeit", "80", wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("AddressMatchcode", "Adresse", "180", , , True, False)
            oListView.oCols.oAddCol("CaseDesc", "Bezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("CaseStateDesc", "Status", "100", , , True, False)
            oListView.oCols.oAddCol("SupportChangeDate", "�nderung", "80", wfEnumDataTypes.wfDataTypeDate, , True, False)

            oCol = oListView.oCols.oAddCol("Details", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sText = "Details"
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("EMails", "EMails", "100", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"
			oCol.sText = "EMails"

            '---- Data ---------------------

            For i As Integer = 0 To asUserData.Length - 1
                asUserData(i) = msUnescape(asUserData(i))
            Next

            sAddressSearch = asUserData(0)
            sSearch = asUserData(1)
            lState = glCInt(asUserData(2))
            bGroupByCurrentUser = gbCBool(asUserData(3))
            lAddressID = glCInt(asUserData(4))		
			sSupportDepartment = asUserData(5)
			sHandlingUser = asUserData(6)
			sDueDate = asUserData(7)	
            Dim bShowCompleted As Boolean = gbCBool(asUserData(8))

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
				sOrder = "sc.DueDate DESC, SupportChangeDate DESC"
            End If

            '----- Clause --------------
            If Not String.IsNullOrEmpty(sAddressSearch) Then
                sAddressSearch = ms2SqlWildcard(sAddressSearch)
                sClause = sClause.gsClauseAnd(String.Format("(a.Matchcode like {0} OR a.AddressNumber like {0})", sAddressSearch))
            End If

            If Not String.IsNullOrEmpty(sSearch) Then
                sSearch = ms2SqlWildcard(sSearch)
                sClause = sClause.gsClauseAnd("(CaseNumber like " & sSearch & " OR CaseDesc like " & sSearch & ")")
            End If

            If lState > 0 Then
                sClause = sClause.gsClauseAnd("CaseState = " & lState)
            End If

            If bGroupByCurrentUser Then
                sClause = sClause.gsClauseAnd("HandlingUser=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser))
            End If

            If lAddressID > 0 Then
                sClause = sClause.gsClauseAnd("SC.AddressID=" & lAddressID)
            End If
			
            If Not String.IsNullOrEmpty(sSupportDepartment) Then
				sClause = sClause.gsClauseAnd("sc.SupportDepartment=" & DataTools.gsStr2Sql(sSupportDepartment))
			End If
			
			If Not String.IsNullOrEmpty(sHandlingUser) Then
				sClause = sClause.gsClauseAnd("sc.HandlingUser=" & DataTools.gsStr2Sql(sHandlingUser))
			End If
			
			If Not String.IsNullOrEmpty(sDueDate) AndAlso IsDate(sDueDate) Then
				sClause = sClause.gsClauseAnd("(sc.DueDate IS NULL OR sc.DueDate<=" & DataTools.gsDateTime2Sql(sDueDate) & ")")
			End If

            If Not bShowCompleted Then
                sClause = sClause.gsClauseAnd("sc.CompletedDate IS NULL")
            End If

            sFields = "CaseID AS RowID,CaseNumber,sc.DueDate, A.Matchcode AS AddressMatchcode,CaseDesc,  CaseStateDesc, SC.ChangeDate AS SupportChangeDate"
            sTables = "tdSupportCases SC INNER JOIN tdSupportCaseStates SCS ON SCS.CaseStateID=SC.CaseState INNER JOIN tdAddresses A ON A.AddressID=SC.AddressID"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function

        ''' <summary>
        ''' add/replace the sql wildcard character '%' if string contains a '*' char
        ''' </summary>
        ''' <param name="sInputString">user input string</param>
        ''' <returns>sql formated string for use in clauses</returns>
        Private Function ms2SqlWildcard(ByVal sInputString As String) As String

            If Not String.IsNullOrEmpty(sInputString) Then

                sInputString = sInputString.Replace("*", "%")
                sInputString = sInputString & "%"
                sInputString = sInputString.gs2Sql

                Return sInputString

            End If

            Return String.Empty

        End Function

        Private Function msUnescape(ByVal sImputString As String) As String

            Return sImputString.Replace("|$", ";")

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
