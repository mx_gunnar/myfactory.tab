﻿
function mOnLoad()
{
    // check if module has parent module
    if (msPageParams[1] == 'False')
    {
        document.all.chkGroupByCurrentUser.checked = true
    }

    mRefreshHandlingUserCbo(document.all.cboSupportDepartment.value);

    mRefreshList();
} 

function mRefreshList()
{   
    var sData = mEscape(document.all.txtAddressSearch.value);
    sData += ";" + mEscape(document.all.txtSearch.value)
    sData += ";" + mEscape(document.all.cboState.value);
    sData += ";" + (document.all.chkGroupByCurrentUser.checked);
    sData += ";" + mEscape(msPageParams[0]);
	sData += ";" + mEscape(document.all.cboSupportDepartment.value);
	sData += ";" + mEscape(document.all.cboHandlingUser.value);
	sData += ";" + mEscape(document.all.txtDueDate.value);
	sData += ";" + (document.all.chkShowCompleted.checked);

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mEscape(inputString)
{
    return outputString = gsReplace(inputString ,";", "|$");
}

function mOnSetDirty()
{
    if (event.type == "change" && event.srcElement.id == "cboSupportDepartment")
    {
        mRefreshHandlingUserCbo(document.all.cboSupportDepartment.value);
    }

    if (event.type == "change" || event.type == 'click')
    {
        mRefreshList();
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == "EMails")
    {
        // navigate to emails with case-id as parameter
        var sURL = '../SupportEMails/SupportEMailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&CaseID=' + sItemID;

        document.location = sURL;
    }
    else if (sColID == "Details")
    {
        // navitate to case detail page
        var sURL = '../SupportCaseDetails/SupportCaseDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&CaseID=' + sItemID +
                '&AddressID=' + msPageParams[0];


        document.location = sURL;
    }
}

/// Methode to refill the "Handling User" Combobox after the Support-Department changed
function mRefreshHandlingUserCbo(lDepartmentID)
{
    var sURL = '../SupportCaseDetails/SupportCaseDetailsProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=GetDepartmentUserCbo&DepartmentID=" + lDepartmentID;
    sRes = gsCallServerMethod(sURL, '');

    gClearCombobox2(document.getElementById("cboHandlingUser"));

    var sValues = sRes.split("|")[0];
    var sNames = sRes.split("|")[1];

    var asValues = sValues.split(";");
    var asNames = sNames.split(";");

    for (i = 0; i < asValues.length; i++)
    {
        var sValue = asValues[i];
        var sName;
        try
        {
            sName = asNames[i];
        }
        catch (Ex)
        {
            sName = " ";
        }

        gCreateNewCboOption(document.getElementById("cboHandlingUser"), sValue, sName, i, false);
    }
}

