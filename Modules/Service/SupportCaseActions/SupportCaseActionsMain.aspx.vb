'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseActionsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseActionsMain
'--------------------------------------------------------------------------------
' Created:      7/16/2014, JSchweighart
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseActionsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad();"
            Dim sSupportCaseActionID As String = Request.QueryString("SupportCaseActionID")
            Dim sSupportCaseID = Request.QueryString("CaseID")

            'for when the CaseID is empty (when certain navigation paths are taken)
            If sSupportCaseID = "" AndAlso sSupportCaseActionID <> "" Then
                sSupportCaseID = DataMethods.gsGetDBValue(oClientInfo, "CaseID", "tdSupportCasePos", "PosID=" & sSupportCaseActionID)
            End If

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = sSupportCaseID

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean
            Dim sRecordIDName As String = "CaseID"

            osHTML.bAppend("<nobr>")
            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='SupportCase' OR ParentModule='SupportCaseAction'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            If frs(0).sValue = "SupportCaseActionNew" Then sRecordIDName = "CaseID"

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", String.Format("{0}={1}", sRecordIDName, sSupportCaseID)))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString
            osHTML.bAppend("</nobr>")


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
