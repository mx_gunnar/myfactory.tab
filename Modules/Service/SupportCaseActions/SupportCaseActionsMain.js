﻿/* (c) myfactory International  2014 */

function mOnLoad()
{
  mRefreshList();
} 

function mRefreshList()
{
    var sData = msPageParams[0];

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == "change" || event.type == 'click')
    {
        mRefreshList();
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == "Details")
    {
        // navitate to case actions detail page
        var sURL = '../SupportCaseActionsDetails/SupportCaseActionsDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&SupportCaseActionsID=' + sItemID +
                '&Origin=' + sColID

        document.location = sURL;
    }
}