'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseActionsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SupportCaseActionsData
'--------------------------------------------------------------------------------
' Created:      7/14/2014 9:37:31 AM, JSchweighart
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseActionsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol
            Dim sTimeformat As String = "dd.MM.yyyy HH:mm "

            Dim asUserData As String() = (oListView.sUserData & ";").Split(";"c)
            Dim sSupportCaseID As String

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportCaseActionsData.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 10
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("tdSupportCasePos.PosDate", "Datum", "110", wfEnumDataTypes.wfDataTypeTime, , True, False)
            oListView.oCols.oAddCol("tdSupportCasePosTypes.TypeDesc", "Aktionstyp", "120", , , True, False)
            oListView.oCols.oAddCol("tdSupportCasePos.PosDesc", "Beschreibung", "*", , , True, False)
            oListView.oCols.oAddCol("tsDatabaseUsers.UserName", "Bearbeiter", "120", , , True, False)

            oCol = oListView.oCols.oAddCol("Details", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeOptionButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sText = "Details"
            oCol.sWidth = "50"

            '---- Data ---------------------

            'TODO: order by action date
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "tdSupportCasePos.PosDate DESC"
                oListView.sOrderCol = "tdSupportCasePos.PosDate"
                oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
            End If

            sSupportCaseID = asUserData(0)

            sFields = "tdSupportCasePos.PosID, tdSupportCasePos.PosDate, tdSupportCasePosTypes.TypeDesc, " & _
                     "tdSupportCasePos.PosDesc, tsDatabaseUsers.UserName"


            sTables = " tdSupportCasePos LEFT OUTER JOIN " & _
                      " tdSalesContactTypes ON tdSupportCasePos.SalesContactType = tdSalesContactTypes.TypeID LEFT OUTER JOIN " & _
                      " tdSupportCasePosTypes ON tdSupportCasePos.PosType = tdSupportCasePosTypes.TypeID LEFT OUTER JOIN " & _
                      " tdSupportCasePosGroups ON tdSupportCasePos.PosGroup = tdSupportCasePosGroups.PosGroupName " & _
                      " LEFT JOIN tsDatabaseUsers ON tsDatabaseUsers.UserInitials=tdSupportCasePos.UserName"

            sClause = DataTools.gsClauseAnd(sClause, "tdSupportCasePos.CaseID =" & DataTools.gsStr2Sql(sSupportCaseID))


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs("PosID").sValue)
                oRow.sValue(0) = (rs.Fields("PosDate").dtValue).ToString(sTimeformat)
                oRow.sValue(1) = rs("TypeDesc").sValue
                oRow.sValue(2) = rs("PosDesc").sValue
                oRow.sValue(3) = rs("UserName").sValue

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
