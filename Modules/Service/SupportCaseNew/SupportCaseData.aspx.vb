'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SupportCaseData
'--------------------------------------------------------------------------------
' Created:      07.10.2013 10:03:39, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim sSearch As String = oListView.sUserData

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SupportCaseData.aspx"
            oListView.bAutoHideNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10

            '---- Columns ------------------
            oListView.oCols.oAddCol("AddressNumber", "Nummer", "100", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Matchcode", "*", , , True, False)
            oListView.oCols.oAddCol("PostalCode", "PLZ", "70", , , True, False)
            oListView.oCols.oAddCol("City", "Ort", "120", , , True, False)

            oCol = oListView.oCols.oAddCol("CmdSelect", "", "", , , False, False)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonNew)

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "Matchcode"
            End If



            sClause = sClause.gsClauseAnd("InActive=0")
            If Not String.IsNullOrEmpty(sSearch) Then
                sSearch = (sSearch & "%").gs2Sql
                sClause = sClause.gsClauseAnd("(Matchcode like " & sSearch & " OR AddressNumber like " & sSearch & " OR Name1 like " & sSearch & " )")
            End If


            sFields = "AddressID as RowID, " & _
                        "AddressNumber, " & _
                        "Matchcode, " & _
                        "PostalCode, " & _
                        "City"

            sTables = "tdAddresses"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
