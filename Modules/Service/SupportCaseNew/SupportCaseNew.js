﻿

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData = document.all.txtSearch.value;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}


function mOnSetDirty()
{
    if (event.type == "change")
    {
        mRefreshList();
    }
}


function mOnCancel()
{
    // navigate back to Case Details or Service Main PAge
    if (msPageParams[1] != "" && msPageParams[1] != "0")
    {
        // back to case details
        mNavitageToCaseDetails(msPageParams[1]);
    } 
    else
    {
        //else: navigate back to service main page
        var sURL = "../ServiceMain/ServiceMain.aspx?ClientID=" + msClientID;
        document.location = sURL;
    }
}

function mOnOK()
{
    // create case

    var sAddressID = gsListViewGetSelection('lstMain');
    if (!sAddressID || sAddressID == "")
    {
        alert(msTerms[0]);
        return;
    }

    var sURL = '../SupportCaseDetails/SupportCaseDetailsProcess.aspx';

    // if origin = SelectCaseAddress
    // --> Change Address of SupportCase
    if (msPageParams[0] == "SelectCaseAddress")
    {
        sURL = sURL + "?Cmd=ChangeCaseAddress&AddressID=" + sAddressID + "&CaseID=" + msPageParams[1];
        var sRes = gsCallServerMethod(sURL, "");

        if (sRes.substr(0, 3) == "OK;")
        {
            mNavitageToCaseDetails(msPageParams[1]);
        }
        else
        {
            alert(sRes);
        }
    }
    else
    {
        // else: Create new Case with selected Address
        sURL = sURL + "?Cmd=CreateCase&AddressID=" + sAddressID;
        var sRes = gsCallServerMethod(sURL, "");

        if (sRes.substr(0, 3) == "OK;")
        {
            // navigate to details of this case
            var lCaseID = sRes.split(";")[1];

            mNavitageToCaseDetails(lCaseID);
        }
        else
        {
            alert(sRes);
        }
    }
}


function mNavitageToCaseDetails(lCaseID)
{
    var sURL = "../SupportCaseDetails/SupportCaseDetailsMain.aspx?ClientID=" + msClientID + "&CaseID=" + lCaseID;
    document.location = sURL;
    
}