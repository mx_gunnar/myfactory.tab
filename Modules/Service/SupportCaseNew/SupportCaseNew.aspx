<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Service.SupportCaseNew" CodeFile="SupportCaseNew.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr>
            <td valign="top" height="15px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/TAB/Modules/Service/SupportCaseNew/dlgSupportCaseNew.xml">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr>
            <td>
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="SupportCaseData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr valign="bottom">
            <td>
                <table width="100%" height="100%">
                    <tr>
                    <td>&nbsp;</td>
                        <td align="right" width="150px">
                            <myfactory:wfButton id="cmdOK" sOnClick="mOnOK();" sStyle="width: 150px;" sText="Fall erstellen" runat="server"></myfactory:wfButton>
                        </td>
                        <td align="right" width="120px">
                        <myfactory:wfButton id="cmdCancel" sOnClick="mOnCancel();" sText="Abbrechen" runat="server"></myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
