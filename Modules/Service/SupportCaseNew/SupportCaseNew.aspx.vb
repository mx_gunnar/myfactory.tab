'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SupportCaseNew.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SupportCaseNew
'--------------------------------------------------------------------------------
' Created:      07.10.2013 10:03:26, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Service

    Partial Class SupportCaseNew
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad();"
            ReDim Me.asTerms(1)
            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "Sie m�ssen eine Adresse ausw�hlen f�r welche der neue Supportfall erstellt werden soll")

            Dim sOrigin As String = Request.QueryString("Origin")
            Dim lCaseID As Integer = glCInt(Request.QueryString("CaseID"))
            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = sOrigin
            Me.asPageParams(1) = lCaseID.ToString


            If sOrigin = "SelectCaseAddress" Then
                Me.cmdOK.sText = Dictionary.gsTranslate(oClientInfo, "Adresse zuweisen")
                Me.dlgMain.oDialog.oField("Info").oMember.Prop("VALUE") = Dictionary.gsTranslate(oClientInfo, "Bitte w�hlen Sie eine Adresse aus, die dem Supportfall zugewiesen werden soll")
            End If


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
