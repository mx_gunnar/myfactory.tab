'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailNewMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for EMailNewMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Email
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.BusinessTasks.Base.MailTools.MailTools
Imports myfactory.FrontendApp.ASPMailTools

'================================================================================
' Class Definition
'================================================================================ 

Namespace ASP.Tab.EMail

    Partial Class EMailNewMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lType As Integer = glCInt(Request.QueryString("Type"))
            Dim lMailID As Integer = glCInt(Request.QueryString("MailID"))
            Dim lDocumentID As Integer = glCInt(Request.QueryString("DocumentID"))
            Dim lSalesContactID As Integer = glCInt(Request.QueryString("ContactID"))
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))

            ReDim Me.asPageParams(1)
            If lAddressID > 0 Then
                Me.asPageParams(0) = Str(lAddressID)
            Else
                Me.asPageParams(0) = "0"
            End If

            If lDocumentID = 0 Then
                ClientValues.gbSetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs", "")
            Else
                ClientValues.gbSetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs", lDocumentID.ToString)
            End If

            If lSalesContactID > 0 Then
                Me.dlgMain.oDialog.oField("To").oMember.Prop("Value") = DataMethods.gsGetDBValue(oClientInfo, "EMail", "tdContacts", "ContactID=" & lSalesContactID)
            End If

            If lType <> 0 And lMailID <> 0 Then
                Dim oMail As New myfactory.Sys.Email.EMail

                If Not oMail.gbLoadInfo(oClientInfo, lMailID) Then
                    Me.dlgMain.oDialog.oField("Subject").oMember.Prop("Value") = Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der E-Mail")
                Else

                    ' 3 = forward
                    If lType = 3 Then
                        ClientValues.gbSetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs", oMail.sDocumentIDs)
                    End If

                    Dim sSubject As String = oMail.sSubject
                    Dim sMailText As String = ""
                    Dim sTo As String = ""
                    Dim sCC As String = ""
                    Dim sAnswerHeader As String = ""

                    If lType = 1 Then 'reply
                        sSubject = Dictionary.gsTranslate(oClientInfo, "AW:") & " " & sSubject
                        sTo = oMail.sFrom
                    ElseIf lType = 2 Then 'replyall
                        sSubject = Dictionary.gsTranslate(oClientInfo, "AW:") & " " & sSubject
                        sTo = oMail.sFrom
                        sCC = oMail.sCC
                    ElseIf lType = 3 Then 'forward
                        sSubject = Dictionary.gsTranslate(oClientInfo, "WG:") & " " & sSubject
                    End If

                    sAnswerHeader &= "---------------------------------------------------------------------------" & vbCrLf
                    sAnswerHeader &= Dictionary.gsTranslate(oClientInfo, "Von:") & " " & oMail.sFrom & vbCrLf
                    sAnswerHeader &= Dictionary.gsTranslate(oClientInfo, "An:") & " " & oMail.sTo & vbCrLf
                    sAnswerHeader &= Dictionary.gsTranslate(oClientInfo, "CC:") & " " & oMail.sCC & vbCrLf
                    sAnswerHeader &= Dictionary.gsTranslate(oClientInfo, "Datum:") & " " & oMail.dtDate.ToString & vbCrLf
                    sAnswerHeader &= Dictionary.gsTranslate(oClientInfo, "Betreff:") & " " & oMail.sSubject & vbCrLf
                    sAnswerHeader &= "---------------------------------------------------------------------------" & vbCrLf

                    Me.dlgMain.oDialog.oField("Subject").oMember.Prop("Value") = sSubject
                    Me.dlgMain.oDialog.oField("To").oMember.Prop("Value") = sTo
                    Me.dlgMain.oDialog.oField("CC").oMember.Prop("Value") = sCC

                    Me.dlgMain.oDialog.oField("MailID").oMember.Prop("Value") = lMailID
                    Me.dlgMain.oDialog.oField("Type").oMember.Prop("Value") = lType

                    gbGetContentBody(oMail, "", sMailText, False, True)

                    If GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabMailbodyAsText") Then
                        sMailText = gsConvertMailText(sMailText, False)
                    End If

                    Me.dlgMain.oDialog.oField("MailText").oMember.Prop("Value") = vbCrLf & vbCrLf & vbCrLf & sAnswerHeader & vbCrLf & sMailText
                End If



            End If

            Me.sOnLoad = "mOnLoad()"
            Me.gAddScriptLink("wfDlgParams.js", True)

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
