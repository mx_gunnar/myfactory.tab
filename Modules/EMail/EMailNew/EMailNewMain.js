﻿// EMailNewMain.js (c) myfactory 2011

function mOnLoad()
{
    mShowSearch(false);

    OnShowAddresses();

    mRefreshAttachmentList();
}

function mOnOK()
{
    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sURL = 'EMailNewProcess.aspx?Cmd=Save'

    var sRes = gsCallServerMethod(sURL, sValues);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mNavigateToList();
}

function mOnCancel()
{
    mNavigateToList();
}

function mNavigateToList() 
{

    var sURL = "";

    if (msPageParams[0] != "0") 
    {
        sURL = '../../Addresses/AddressDetails/AddressDetailsMain.aspx' +
                '?ClientID=' + msClientID + '&RecordID=' + msPageParams[0];

    }
    else 
    {
        sURL = '../EMailMain/EMailMain.aspx' +
                '?ClientID=' + msClientID;
    }


    

    document.location = sURL;
}

function mShowSearch(bShow)
{
    var sStyle = '';

    if (bShow)
    {
        sStyle = 'visible';
    }
    else
    { 
        sStyle = 'hidden';
    }

    document.all.cmdToButton.style.visibility = sStyle;
    document.all.cmdCCButton.style.visibility = sStyle;
    document.all.cmdBCCButton.style.visibility = sStyle;
}

function cmdSearchButton_OnClick()
{
    gListViewSetUserData('lstSearch', document.all.txtSearch.value);
    gListViewLoadPage('lstSearch', 1);

    mShowSearch(true);
}

function cmdToButton_OnClick()
{
    mAddAddress(document.all.txtTo);
}

function cmdCCButton_OnClick()
{
    mAddAddress(document.all.txtCC);
}

function cmdBCCButton_OnClick()
{
    mAddAddress(document.all.txtBCC);
}

function mAddAddress(oCtl)
{
    var sItemID = gsListViewGetSelection('lstSearch');
    if (sItemID == '')
        return;

    var sAddress = gsListViewGetItemUserData('lstSearch', sItemID);
    if (sAddress == '')
        return;

    if (oCtl.value != '')
        oCtl.value = oCtl.value + ';';

    oCtl.value = oCtl.value + sAddress;
}

function mRefreshAttachmentList()
{ 
      //  gListViewSetUserData('lstDocuments', document.all.txtDocumentSearch.value);
    gListViewLoadPage('lstAttachments', 1);
}

function cmdDocSearch_OnClick()
{
    gListViewSetUserData('lstDocuments', document.all.cboFilter.value + ";" + document.all.txtDocumentSearch.value);
    gListViewLoadPage('lstDocuments', 1);
}

function OnShowAddresses()
{
    document.getElementById("trAddresses").style.display = "";
    document.getElementById("trDocuments").style.display = "none";
}

function OnShowDocuments()
{
    document.getElementById("trAddresses").style.display = "none";
    document.getElementById("trDocuments").style.display = "";
}

function cmdAddDocument_OnClick()
{
    var sSelection = gsListViewGetSelection('lstDocuments');

    var sURL = 'EMailNewProcess.aspx?Cmd=AppendAttachment&DocID=' + sSelection
    
    var sRes = gsCallServerMethod(sURL);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mRefreshAttachmentList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sListView == 'lstAttachments')
    {
        if (sColID == 'CmdDel')
        {
            mRemoveAttachment(sItemID);
        }

        if (sColID == 'CmdOpen')
        {
            mOpenDocument(sItemID);
        }
    }
}

function mRemoveAttachment(sDocID)
{
    var sURL = 'EMailNewProcess.aspx?Cmd=RemoveAttachment&DocID=' + sDocID;

    var sRes = gsCallServerMethod(sURL);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mRefreshAttachmentList();
}


function mOpenDocument(lDocumentID)
{
    window.resizeTo(10, 10);

    //------ check permission to read ----------
    if (lDocumentID != '')
    {
        var sPerm = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' + 'document_Process.aspx?DocumentID=' + lDocumentID + '&Method=CheckPermissionRead', '');
        if (sPerm != ';-1')
        {
            if (sPerm != ';0')
                alert(sPerm);
            else
                alert(msTerms[3]);
            return;
        };
    }
    else
        return;

    //------ get link and open document --------
    var sLink = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' +
					'document_Process.aspx?DocumentID=' + lDocumentID +
					'&Method=GetDocumentLink&LocalMode=-1', '');
    if (sLink.substr(0, 1) != ';')
    {
        alert(sLink);
        return;
    }
    sLink = sLink.substr(1);
    if (sLink != '')
        window.open(sLink, '', 'menubar=no,fullscreen=no,scrollbars=yes,location=no,toolbar=no,resizable=yes,status=yes,left=50,top=50,width=600,height=400')
    //------------------------------------------	
}