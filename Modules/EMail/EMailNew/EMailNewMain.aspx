<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.EMail.EMailNewMain" CodeFile="EMailNewMain.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="98%" style="table-layout: fixed;">
        <tr height="200px">
            <td width="500px" valign="top" style="overflow-x: auto; overflow-y: auto;" >
                <form name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/EMail/EMailNew/dlgEMailNew.xml"
                    sDataSource="dsoData">
                </myfactory:wfXmlDialog>
                </form>
            </td>
            <td width="*" valign="top">
                <table width="100%">
                    <tr>
                        <td align="left" width="100px">    
                            <myfactory:wfButton ID="CmdShowAddresses" runat="server" sText="Adressen" sOnClick="OnShowAddresses();">
                            </myfactory:wfButton>
                        </td>
                        <td align="left">
                            <myfactory:wfButton ID="CmdShowDocuments" runat="server" sText="Dokumente" sOnClick="OnShowDocuments();">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                    <tr id="trAddresses" width="*">
                        <td colspan="2">
                            <myfactory:wfXmlDialog  runat="server" ID="dlgSearch" sDialog="/tab/modules/EMail/EMailNew/dlgSearch.xml">
                            </myfactory:wfXmlDialog>
                        </td>
                    </tr>
                    <tr id="trDocuments" width="*">
                        <td colspan="2">
                            <myfactory:wfXmlDialog runat="server" ID="dlgDocument" sDialog="/tab/modules/EMail/EMailNew/dlgDocuments.xml">
                            </myfactory:wfXmlDialog>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="25px">
            <td width="300px" align="right">
                <myfactory:wfButton runat="server" ID="cmdOK" sOnClick="mOnOK()" sText="Senden">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdCancel" sOnClick="mOnCancel()" sText="Verwerfen">
                </myfactory:wfButton>
            </td>
            <td width="*">
                &nbsp
            </td>
        </tr>
    </table>
</body>
</html>
