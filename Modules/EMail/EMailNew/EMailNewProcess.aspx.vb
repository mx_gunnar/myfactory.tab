'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for EMailNewProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Email

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Base.General
Imports myfactory.BusinessTasks.Base.MailTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.EMail

    Partial Class EMailNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Save"
                    Return msSaveMailData(oClientInfo)

                Case "AppendAttachment"
                    Return msAppendAttachment(oClientInfo, glCInt(Request.QueryString("DocID")))

                Case "RemoveAttachment"
                    Return msRemoveAttachment(oClientInfo, glCInt(Request.QueryString("DocID")))

                Case Else
                    Return "Unknown Cmd: " & sCmd
            End Select

            Return "TODO"

        End Function

        Private Function msRemoveAttachment(ByVal oClientInfo As ClientInfo, ByVal lDocumentID As Integer) As String

            If lDocumentID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Entfernen des Dokumentes")
            End If

            Dim sDocuments As String
            sDocuments = ClientValues.gsGetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs")
            Dim sRetVal As String = ""

            For Each sDocID As String In sDocuments.Split(","c)

                If sDocID = lDocumentID.ToString Then
                    Continue For
                End If

                If sRetVal <> "" Then
                    sRetVal &= ","
                End If
                sRetVal &= sDocID

            Next

            ClientValues.gbSetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs", sRetVal)

            Return "OK; attachment removed"
        End Function

        Private Function msAppendAttachment(ByVal oClientInfo As ClientInfo, ByVal lDocumentID As Integer) As String

            If lDocumentID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Hinzuf�gen des Dokumentes")
            End If

            Dim sDocuments As String
            sDocuments = ClientValues.gsGetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs")
            If sDocuments <> "" Then
                sDocuments &= ","
            End If

            ClientValues.gbSetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs", sDocuments & lDocumentID)

            Return "OK; append attachment"
        End Function

        ''' <summary>
        ''' Save Task Data
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msSaveMailData(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRoot As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oRoot.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            '--------- check values ----------------
            Dim sFrom, sTo As String

            sFrom = XmlFunctions.gsGetXMLSubNodeText(oRoot, "cboFrom")
            If sFrom = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Absender an.")

            sTo = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTo")
            If sTo = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Empf�nger an.")


            Dim bOK As Boolean
            Dim sMailText As String

            sMailText = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtMailText")
            If InStr(sMailText, vbCrLf) <> 0 Then
                sMailText = Replace(sMailText, vbCrLf, "<br>")
            ElseIf InStr(sMailText, vbLf) <> 0 Then
                sMailText = Replace(sMailText, vbLf, "<br>")
            End If


            '-------- get mail properties! (signature / body style) -----------
            Dim rsMailProperties As Recordset
            Dim lSignatureID As Integer
            Dim sFontSize As String = ""
            Dim sFontFamily As String = ""
            Dim sMobileSignatureText As String = ""
            Dim sDivOpenTag As String = "<DIV "

            rsMailProperties = DataMethods.grsGetDBRecordset(oClientInfo, "PropertyValue , PropertyName", "tdMailDefaultProperties", _
                                                            "UserID=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql() & " AND (" & _
                                                            "PropertyName='DefaultMobilMailTempID' OR " & _
                                                            "PropertyName='MobileMailBodyFontSize' OR " & _
                                                            "PropertyName='MobileMailBodyFontFamily')")

            Do Until rsMailProperties.EOF
                If rsMailProperties("PropertyName").sValue = "DefaultMobilMailTempID" Then
                    lSignatureID = rsMailProperties("PropertyValue").lValue
                End If
                If rsMailProperties("PropertyName").sValue = "MobileMailBodyFontFamily" Then
                    sFontFamily = rsMailProperties("PropertyValue").sValue
                End If
                If rsMailProperties("PropertyName").sValue = "MobileMailBodyFontSize" Then
                    sFontSize = rsMailProperties("PropertyValue").sValue
                End If
                rsMailProperties.MoveNext()
            Loop

            If lSignatureID <> 0 Then
                sMobileSignatureText = myfactory.BusinessTasks.Base.MailTools.MailTools.gsGetSignatureText(oClientInfo, lSignatureID)
            End If

            If sFontFamily <> "" Then
                sFontFamily = "FONT-FAMILY: " & sFontFamily & "; "
            End If

            If sFontSize <> "" Then
                sFontSize = "FONT-SIZE: " & sFontSize & "; "
            End If

            If sFontFamily <> "" Or sFontSize <> "" Then
                sDivOpenTag = sDivOpenTag & "style='" & sFontFamily & sFontSize & "'"
            End If

            sDivOpenTag = sDivOpenTag & ">"

            sMailText = sDivOpenTag & sMailText & "</DIV>"

            If sMobileSignatureText <> "" Then
                sMailText = sMailText & "<br><br>"
                sMailText = sMailText & sMobileSignatureText
            End If
            '-----------------------------------------


            '------ get docids on fwd -----
            Dim sDocIDs As String = ""
            Dim lType As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtType"))

            If lType = 3 Then
                Dim lMailID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtMailID"))
                Dim oMail As New myfactory.Sys.Email.EMail

                If oMail.gbLoadInfo(oClientInfo, lMailID) Then
                    sDocIDs = oMail.sDocumentIDs
                End If
            End If
            '-------------------------------


            '---------- append all new attachments (documents) -------
            If sDocIDs <> "" Then
                sDocIDs &= ","
            End If
            sDocIDs &= ClientValues.gsGetClientValue(oClientInfo, "Tab_NewEmail_DocumentIDs")
            '--------------------------------------


            '----- get contact data -----
            Dim rs As Recordset
            Dim bCreateDirect As Boolean
            Dim lContactType, lContactTopic, lContactResult As Integer
            Dim sContactDesc As String = ""

            rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                    "PropertyName,PropertyValue", _
                    "tdMailDefaultProperties", _
                    "UserID=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser))

            Do Until rs.EOF
                Select Case rs("PropertyName").sValue
                    Case "CreateDirect"
                        bCreateDirect = rs("PropertyValue").bValue
                    Case "ContactType"
                        lContactType = rs("PropertyValue").lValue
                    Case "ContactDesc"
                        sContactDesc = rs("PropertyValue").sValue
                    Case "ContactTopic"
                        lContactTopic = rs("PropertyValue").lValue
                    Case "ContactResult"
                        lContactResult = rs("PropertyValue").lValue
                End Select
                rs.MoveNext()
            Loop

            '----------------------------
            Dim lNewMailID As Integer

            If bCreateDirect Then
                If sContactDesc = "" Then sContactDesc = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtSubject")
                sContactDesc = Left(sContactDesc, 100)

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "AddressID,ContactID", "vwEMailAddresses", "AddressID<>0 AND EMailAddress=" & DataTools.gsStr2Sql(sTo))
                If Not rs.EOF Then sTo = sTo & "$,$" & rs(0).lValue & "$,$" & rs(1).lValue

                bOK = MailTools.gbSaveMail(oClientInfo, lNewMailID, 0, sFrom, sTo, _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtCC"), _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtBCC"), _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtSubject"), _
                           sMailText, _
                           0, True, sContactDesc, lContactType.ToString, lContactTopic.ToString, lContactResult.ToString, sDocIDs)
            Else
                bOK = MailTools.gbSaveMail(oClientInfo, lNewMailID, 0, sFrom, sTo, _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtCC"), _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtBCC"), _
                           XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtSubject"), _
                           sMailText, _
                           0, True, "", "", "", "", sDocIDs)
            End If

            If bOK Then
                Return "OK;"
            Else
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Senden der Mail") & vbCrLf & oClientInfo.oErrors.sErrText
            End If

        End Function



    End Class

End Namespace

'================================================================================
'================================================================================
