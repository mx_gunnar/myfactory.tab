'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailNewSeachDocumentData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for EMailNewSeachDocumentData
'--------------------------------------------------------------------------------
' Created:      24.08.2012 14:50:08, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Base

    Partial Class EMailNewSeachDocumentData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim lFilter As Integer = glCInt(oListView.sUserData.Split(";"c)(0))
            Dim sSubClause As String
            Dim sSearch As String = oListView.sUserData.Split(";"c)(1)

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10
            oListView.sListViewDataPage = "EMailNewSeachDocumentData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("DocumentDesc", "Matchcode", "160", , , True, False)
            oListView.oCols.oAddCol("DocumentType", "Typ", "70", , , True, False)
            oListView.oCols.oAddCol("DocumentDate", "Datum", "80", , , True, False)



            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "tdDocuments.DocumentID AS RowID, tdDocuments.DocumentDesc, T.TypeDescription, tdDocuments.CreationDate "
            sTables = "tdDocuments" & _
                        " INNER JOIN tsDocumentTypes T ON T.DocumentTypeID=tdDocuments.DocumentType"

            '---- clause ---------
            sClause = "SystemDoc=0"

            If lFilter = 2 Then 'favorites
                sClause = DataTools.gsClauseAnd(sClause, "IsFavorite=-1")
            ElseIf lFilter = 1 Then 'my favorites
                sClause = DataTools.gsClauseAnd(sClause, " EXISTS(SELECT * FROM tdDocumentsUserSettings " & _
                                                            "WHERE UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
                                                            " AND IsMyFavorit=-1 " & _
                                                            " AND tdDocumentsUserSettings.DocumentID=tdDocuments.DocumentID) ")
            End If

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "DocumentDesc like " & DataTools.gsStr2Sql(sSearch & "%"))
            End If

            '---- permissions ----
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Documents")
            If sSubClause <> "" Then sClause = DataTools.gsClauseAnd(sClause, sSubClause)




            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
