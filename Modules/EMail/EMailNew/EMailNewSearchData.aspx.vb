'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailNewSearchData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for EMailNewSearchData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.EMail

	Partial Class EMailNewSearchData
		Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

		'================================================================================
		' Page Members
		'================================================================================

#Region " Vom Web Form Designer generierter Code "

		'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

		End Sub

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
			'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
			InitializeComponent()
		End Sub

#End Region

		'================================================================================
		' Private Members
		'================================================================================

		'================================================================================
		' Functions
		'================================================================================

		'================================================================================
		' Method:       bFillListView
		'--------------------------------------------------------------------------------
		' Purpose:      Fill Data into ListView
		'--------------------------------------------------------------------------------
		' Parameter:    oClientInfo     - ClientInfo
		'               oListView       - ListView Object to fill data into
		'--------------------------------------------------------------------------------
		' Return:       true/false
		'================================================================================

		Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
					ByVal oListView As AspListView) As Boolean

			Dim rs As Recordset
			Dim sOrder As String
			Dim sClause As String = ""
			Dim sFields As String
			Dim sTables As String
            Dim oRow As AspListViewRow
            Dim l As Integer = 1

			'---- ListView Properties ------
			oListView.bPageNavigation = True
			oListView.sListViewDataPage = "EMailNewSearchData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

			'---- Columns ------------------
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "160")
            oListView.oCols.oAddCol("EMail", "EMail", "150")

			'---- Data ---------------------
			Dim sSearch As String = oListView.sUserData

			sOrder = "EMailMatchcode"

			sFields = "EMailMatchcode,EMailAddress"
			sTables = "vwEMailAddresses"

			'---- clause ---------
			If sSearch <> "" Then
				sSearch = Replace(sSearch, "*", "%")
				sClause = DataTools.gsClauseAnd(sClause, "(EMailMatchcode like " & DataTools.gsStr2Sql(sSearch & "%") & " OR " & _
															"EMailAddress like " & DataTools.gsStr2Sql(sSearch & "%") & ")")
			End If

			rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
								sFields, sTables, sClause, , sOrder)
			Do Until rs.EOF
				oRow = oListView.oRows.oAddRow(l.ToString)

				oRow.Value(0) = rs(0).sValue
				oRow.Value(1) = rs(1).sValue
				oRow.sUserData = rs(1).sValue

				l += 1
				rs.MoveNext()
			Loop
			rs.Close()

			oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

			Return True

		End Function


	End Class

End Namespace

'================================================================================
'================================================================================
