'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for EMailMain
'--------------------------------------------------------------------------------
' Created:      1/31/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Main.User
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.ASPMailTools.MailEditor
Imports myfactory.BusinessTasks.Base.MailTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.EMail

    Partial Class EMailMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad()"

            Dim sSelected As String
            Dim sLastFolder As String

            MailTools.gbInsertDefaultFolder(oClientInfo)

            Call gbSetSubNodes(oClientInfo, "", Me.tvwFolder.oTreeView.Nodes, "", "", oClientInfo.oClientProperties.sCurrentUser, True)
            sSelected = gsGetUserPreference(oClientInfo, "EMailFolders", False, False)
            Me.tvwFolder.oTreeView.bSetExpandedNodes(sSelected)
            sLastFolder = gsGetUserPreference(oClientInfo, "EMailFolderLast", False, False)

            Me.tvwFolder.oTreeView.bSetExpandedNodes(sLastFolder)

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = sLastFolder

            ReDim Me.asTerms(1)
            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "M�chten Sie diese E-Mail wirklich l�schen?")

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
