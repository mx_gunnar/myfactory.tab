<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.EMail.EMailMain" CodeFile="EMailMain.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table id="tblMain" class="borderTable" style="table-layout: fixed;" height="98%" cellspacing="2"
        cellpadding="2" width="98%">
        <tr height="300px">
            <td id="tdFolder"  width="300px" rowspan="3" valign="top">
                <myfactory:wfTreeView runat="server" ID="tvwFolder" sCaption="Alle Ordner" bDrawLines="false">
                </myfactory:wfTreeView>
            </td>
            <td  id="tdList" >
                <myfactory:wfButton runat="server" ID="cmdFolder" sOnClick="mOnFolder()" sText="Ordner"
                    sStyle="width:80px">
                </myfactory:wfButton>
                <myfactory:wfListView runat="server" ID="lstMails" sListViewDataPage="MailListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="25px">
            <td id="tdButtons" style="display: none" >
                <myfactory:wfButton runat="server" ID="cmdReply" sOnClick="mOnReply()" sText="Antworten"
                    sStyle="width:80px">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdReplyAll" sOnClick="mOnReplyAll()" sText="Allen Antworten"
                    sStyle="width:120px">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdForward" sOnClick="mOnForward()" sText="Weiterleiten">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdDelete" sOnClick="mOnDelete()" sText="L�schen">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdRead" sOnClick="mOnRead()" sText="Gelesen"
                    sStyle="margin-left:100px">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdUnread" sOnClick="mOnUnRead()" sText="Ungelesen">
                </myfactory:wfButton>
            </td>
        </tr>
        <tr>
            <td  style="overflow-y:auto;" >
                <myfactory:wfIFrame runat="server" ID="fraMail" sStyle="width:100%;height:100%">
                </myfactory:wfIFrame>
            </td>
        </tr>
    </table>
</body>
</html>
