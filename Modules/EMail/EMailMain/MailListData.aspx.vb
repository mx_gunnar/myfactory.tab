'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    MailListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for MailListData
'--------------------------------------------------------------------------------
' Created:      1/31/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Base.Attributes

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.EMail

    Partial Class MailListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
			Dim sClause As String = ""
			Dim sGroup As String
            Dim sFields As String
            Dim sTables As String
            Dim oCol As AspListViewCol
			Dim bOutGoing As Boolean

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "MailListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True


			Dim lFolderID As Integer

			lFolderID = glCInt(oListView.sUserData)
			If lFolderID <> 0 Then
				Dim sType As String

				sType = DataMethods.gsGetDBValue(oClientInfo, "MailFolderType", "tdMailFolders", "MailFolderID=" & lFolderID)
				If sType = "mfOutgoing" Or sType = "mfDraft" Then bOutGoing = True
			End If

			'---- Columns ------------------
            oListView.oCols.oAddCol("MailDate", "Datum", "80", wfEnumDataTypes.wfDataTypeDateTime, , True)

            oCol = oListView.oCols.oAddCol("AttachmentCount", "", "20", , , False, False)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeImage

			oCol = oListView.oCols.oAddCol("MIN(ISNULL(AddressAlias,AddressEMail))", "Von", "150", , , True)
			If bOutGoing Then oCol.sText = "An"
			oListView.oCols.oAddCol("Subject", "Betreff", "*", , , True)

			oCol = oListView.oCols.oAddCol("CmdMail", "", "50", , wfEnumAligns.wfAlignCenter)
			oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
			oCol.sWidth = "50"

			'---- Data ---------------------

            'implement attachment icon:  as CountAttachments


			If oListView.sOrderCol = "" Then
				oListView.sOrderCol = "MailDate"
				oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
			End If

			sOrder = oListView.sOrderCol
			If sOrder <> "" Then
				If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
			End If


            sFields = "tdMailInfos.MailID AS RowID, tdMailInfos.MailDate, (select count(*) from tdMailContents where MailID=tdMailInfos.MailID and not ContentTypeName is null) AS AttachmentCount, MIN(ISNULL(a1.AddressAlias,a1.AddressEMail)), tdMailInfos.Subject,NULL, tdMailReads.ReadDate"
			sGroup = "tdMailInfos.MailID, tdMailInfos.MailDate, tdMailInfos.Subject, tdMailReads.ReadDate"

			sTables = "tdMailInfos"
			If bOutGoing Then
				sTables &= " left join tdMailAddresses a1 on a1.MailID=tdMailInfos.MailID and a1.Addresstype=1"
			Else
				sTables &= " left join tdMailAddresses a1 on a1.MailID=tdMailInfos.MailID and a1.Addresstype=0"
			End If

			sTables &= _
			   " left join tdMailReads on tdMailReads.MailID = tdMailInfos.MailID and tdMailReads.UserInitials = " & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
			   " LEFT JOIN tdMailPriorities ON tdMailPriorities.PriorityID=tdMailReads.PriorityID"

			'---- clause ---------

			Dim asFolderID() As String
			Dim lMailCategoryID As Integer
			Dim lAttributeFilterID As Integer
			Dim lSpecialCase As Integer

			asFolderID = Split(oListView.sUserData, "_")

			Select Case asFolderID(0)
				Case "-11"
					lSpecialCase = 1
				Case "-12"
					lSpecialCase = 2
				Case "-13"
					lSpecialCase = 3
					If asFolderID.Length = 1 Then
						lMailCategoryID = 0
					Else
						lMailCategoryID = glCInt(asFolderID(1))
					End If
				Case "-14"
					lSpecialCase = 4
					If asFolderID.Length = 1 Then
						lAttributeFilterID = 0
					Else
						lAttributeFilterID = glCInt(asFolderID(1))
					End If
				Case Else
					lSpecialCase = 0
			End Select


			Select Case lSpecialCase
				Case 1
					sTables = sTables & " INNER JOIN tdMailReads as ReadSpecial on " & _
					   " ReadSpecial.MailID = tdMailInfos.MailID " & _
					   " and ReadSpecial.UserInitials = " & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
					   " AND ReadSpecial.PriorityID > 0"
				Case 2
					sTables = sTables & " INNER JOIN tdMailFolderSpecialUnread as SpecialUnread on " & _
					 " SpecialUnread.MailFolderID = tdMailInfos.FolderID " & _
					 " and SpecialUnread.UserInitials = " & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser)
					sClause = "tdMailReads.ReadDate is null "
				Case 3
					If lMailCategoryID = 0 Then
						sClause = DataTools.gsClauseAnd(sClause, "exists (select tdMailAssignCategories.MailID " & _
						 " from tdMailAssignCategories " & _
						 " where tdMailAssignCategories.MailID = tdMailInfos.MailID" & _
						 " and tdMailAssignCategories.UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & ")")
					Else
						sClause = DataTools.gsClauseAnd(sClause, "exists (select tdMailAssignCategories.MailID " & _
						 " from tdMailAssignCategories " & _
						 " where tdMailAssignCategories.MailID = tdMailInfos.MailID" & _
						 " and tdMailAssignCategories.UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
						 " and tdMailAssignCategories.MailCategoryID=" & lMailCategoryID & ")")
					End If

				Case 4
					Dim rsFilter As Recordset
					Dim sFilterClause As String
					Dim oXML As New XmlDocument

					rsFilter = DataMethods.grsGetDBRecordset(oClientInfo, _
					 "*", _
					 "tdMailFolderAttributeSearch", _
					 "UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser) & _
					 " and FilterID=" & lAttributeFilterID)
					If Not rsFilter.EOF Then
						Select Case rsFilter.Item("ProcessTo").lValue
							Case 0 ' Schnellzugriff
								sTables = sTables & " INNER JOIN tdMailFolderSpecialUnread as SpecialUnread on " & _
								 " SpecialUnread.MailFolderID = tdMailInfos.FolderID " & _
								 " and SpecialUnread.UserInitials = " & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser)
								sClause = ""

							Case 1 ' Meine Postf�cher
								sTables = sTables & " INNER JOIN tdMailFolders on " & _
								 " tdMailFolders.MailFolderID = tdMailInfos.FolderID " & _
								 " and tdMailFolders.Owner = " & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser)

								sClause = ""
							Case 2 ' Gemeinsame Postf�cher
								sTables = sTables & " INNER JOIN tdMailFolders on " & _
								 " tdMailFolders.MailFolderID = tdMailInfos.FolderID " & _
								 " and tdMailFolders.Owner = '*'"
								sClause = ""
							Case 3 ' Postfach
								sClause = "FolderID=" & rsFilter.Item("MailFolderID").lValue

						End Select

						If rsFilter.Item("FilterClause").sValue <> "" Then
							If Not oXML.loadXML(rsFilter.Item("FilterClause").sValue) Then
								sFilterClause = "0=1"
							Else
								sFilterClause = AttributeFields.gsGetClause(oClientInfo, 9700, oXML.documentElement)
							End If
						Else
							sFilterClause = "0=1"
						End If
						sClause = DataTools.gsClauseAnd(sClause, sFilterClause)

					Else
						sClause = "0=1"
					End If
				Case Else
					If Not ItemApprovals.gbCheckItemPermission(oClientInfo, _
					 9800, _
					 lFolderID, _
					 Permissions.wfEnumPermissions.wfPermissionAllowed, _
					 oClientInfo.oClientProperties.sCurrentUser, _
					 0, _
					 True, _
					 True) Then
						Return True
					End If

					sClause = "FolderID=" & lFolderID
			End Select

			'----- fill listview ----
			'TODO: unread bold
			Dim oRow As AspListViewRow

			rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
			 sFields, sTables, sClause, sGroup, sOrder)

			Do Until rs.EOF
				oRow = oListView.oRows.oAddRow(rs(0).sValue)
				oRow.SetRecord(rs, True)

				If Not gbDate(rs("ReadDate").dtValue) Then
					oRow.bRowBold = True
				End If

                ' go attachments? then show image
                If rs("AttachmentCount").lValue > 0 Then
                    oRow.sValue(1) = "/images/Listview/image_Attachment.gif"
                Else
                    oRow.bSetEmpty(1)
                End If

				rs.MoveNext()
			Loop
			rs.Close()


			oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause, "DISTINCT(tdMailInfos.MailID)")

			Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
