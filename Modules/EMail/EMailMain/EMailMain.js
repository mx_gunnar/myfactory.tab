﻿// EMailMain.js (c) myfactory 2011

var msTreeNode = 0;
var msItemID = 0;
var mbFolder = true;

function mOnLoad()
{
    try
    {
        if (msPageParams[0] != '0')
        {
            mOnTreeViewSelect('tvwFolder', msPageParams[0]);
            gTreeNodeSelect('tvwFolder', msTreeNode);
        }
    }
    catch (e) { ; }

    mOnFolder();
}

function mOnFolder()
{
    if (mbFolder)
    {
        document.getElementById('tdFolder').style.display = "none";


        document.getElementById("tdList").style.overflow = "hidden";
        mbFolder = false;
    }
    else {

   
         document.getElementById('tdFolder').style.display = "";


         document.getElementById("tdList").style.overflow = "hidden";
        mbFolder = true; 
    } 
}


function mOnTreeViewSelect(sTreeView, sNodeID)
{
    var sSelected;

    if (msTreeNode == sNodeID) return;
    msTreeNode = sNodeID;

    gListViewSetUserData('lstMails', sNodeID );
    gListViewLoadPage('lstMails', 1);
    sSelected = gsGetExpandedNodes(sTreeView)
    gSetUserPreference('EMailFolders', sSelected, false, false);
    gSetUserPreference('EMailFolderLast', sNodeID, false, false);

    mRefreshPreview(0);
}


function mRefreshPreview(sItemID)
{
    var sURL;

    if (sItemID == '' || sItemID == '0')
    {
        sURL = msWebPageRoot + '/empty.htm';
        document.all.tdButtons.style.display = 'none';
    }
    else
    {
        sURL = gsTemplateForm("EMail", sItemID, true);
        document.all.tdButtons.style.display = 'block';
    }
    msItemID = sItemID;
    document.all.fraMail.src = sURL;
}

function mOnListViewClick(sListView, sItemID)
{
    mRefreshPreview(sItemID);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    mRefreshPreview(sItemID);
}

function mOnReply()
{
    mNewMail(1);
}

function mOnReplyAll()
{
    mNewMail(2);
}

function mOnForward()
{
    mNewMail(3);
}

function mOnDelete()
{ 
    if (window.confirm(msTerms[0]))
    {
        var sUrl = 'EMailMainProcess.aspx?Cmd=MailDelete&MailID=' + msItemID;
        var sRes = gsCallServerMethod(sUrl, '');
        if (sRes != 'OK;')
        {
            alert(sRes);
            return;
        }

        gListViewLoadPage('lstMails', -1);
        mRefreshPreview(0);
    }
}

function mNewMail(lType)
{
    var sURL = '../EMailNew/EMailNewMain.aspx' +
                '?ClientID=' + msClientID + 
                '&Type=' + lType +
                '&MailID=' + msItemID ;

    document.location = sURL;

}

function mOnRead()
{
    var sUrl = 'EMailMainProcess.aspx?Cmd=MailRead&MailID=' + msItemID;
    var sRes = gsCallServerMethod(sUrl, '');
    if (sRes != 'OK;')
    {
        alert(sRes);
        return;
    }
    gListViewLoadPage('lstMails', -1);
}

function mOnUnRead()
{
    var sUrl = 'EMailMainProcess.aspx?Cmd=MailUnRead&MailID=' + msItemID;
    var sRes = gsCallServerMethod(sUrl, '');
    if (sRes != 'OK;')
    {
        alert(sRes);
        return;
    }
    gListViewLoadPage('lstMails', -1);
}