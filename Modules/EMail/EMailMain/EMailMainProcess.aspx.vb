'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    EMailMainProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for EMailMainProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Email

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Base.General
Imports myfactory.BusinessTasks.Base.MailTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.EMail

	Partial Class EMailMainProcess
		Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

		'================================================================================
		' Page Members
		'================================================================================

#Region " Vom Web Form Designer generierter Code "

		'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

		End Sub

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
			'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
			InitializeComponent()
		End Sub

#End Region

		'================================================================================
		' Private Members
		'================================================================================

		'================================================================================
		' Functions
		'================================================================================

		'================================================================================
		' Method:       sResponseText
		'--------------------------------------------------------------------------------
		' Purpose:      Return Response Text
		'--------------------------------------------------------------------------------
		' Parameter:    oClientInfo         - ClientInfo
		'               sClientID           - ClientID
		'               lContentType (out)  - ContentType
		'--------------------------------------------------------------------------------
		' Return:       string  - ResponseText
		'================================================================================

		Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
					ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

			Dim sCmd As String = Request.QueryString("Cmd")

			Select Case sCmd
				Case "MailRead"
					Return msSetMailRead(oClientInfo)

				Case "MailUnRead"
					Return msSetMailUnRead(oClientInfo)

				Case "MailDelete"
					Return msMailDelete(oClientInfo)

				Case Else
					Return "Unknown Cmd: " & sCmd
			End Select

			Return "TODO"

		End Function

		Private Function msSetMailRead(ByVal oClientInfo As ClientInfo) As String

			Dim lMailID As Integer = glCInt(Request.QueryString("MailID"))

			If lMailID = 0 Then
				Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Mail")
			End If

			MailTools.gbMailReadStatus(oClientInfo, lMailID, True)
			Dim sErr As String = oClientInfo.oErrors.sErrText
			If sErr <> "" Then Return sErr

			Return "OK;"

		End Function

		Private Function msSetMailUnRead(ByVal oClientInfo As ClientInfo) As String

			Dim lMailID As Integer = glCInt(Request.QueryString("MailID"))

			If lMailID = 0 Then
				Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Mail")
			End If

			MailTools.gbMailReadStatus(oClientInfo, lMailID, False)
			Dim sErr As String = oClientInfo.oErrors.sErrText
			If sErr <> "" Then Return sErr

			Return "OK;"

		End Function

		Private Function msMailDelete(ByVal oClientInfo As ClientInfo) As String

			Dim lMailID As Integer = glCInt(Request.QueryString("MailID"))

			If lMailID = 0 Then
				Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Mail")
			End If

			Dim oEMail As New myfactory.Sys.Email.EMail
			oEMail.gbLoadInfo(oClientInfo, lMailID)
			If Not oEMail.gbDeleteInfo(oClientInfo) Then
				Return oClientInfo.oErrors.sErrText
			End If

			Return "OK;"

		End Function


	End Class

End Namespace

'================================================================================
'================================================================================
