'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AppointmentsListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for AppointmentsListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		24.07.2012 - ABuhleier -> also show Time-From and Time-To
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Appointments

    Partial Class AppointmentsListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "AppointmentsListData.aspx"
            oListView.lPageSize = 20
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("TaskDate", "von", "150", wfEnumDataTypes.wfDataTypeDateTime)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("TaskDueDate", "bis", "150", wfEnumDataTypes.wfDataTypeDateTime)
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("TaskDesc", "Bezeichnung", "*")
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("TaskLocation", "Ort", "200")
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            Dim sDate As String = ""
            Dim dtDateFrom, dtDateTo As Date

            sDate = oListView.sUserData
            dtDateFrom = gdtCDate(sDate)
            If Not gbDate(dtDateFrom) Then dtDateFrom = Date.Today
            dtDateTo = dtDateFrom.AddDays(7)

            sOrder = "MIN(TaskDate) desc, MIN(TaskTimeFrom) desc"

            sFields = "TaskID AS RowID, MIN(TaskDate), MAX(TaskDueDate) , MIN(TaskDesc), MIN(TaskLocation)"
            sTables = "tsTasks " & _
                        " INNER JOIN tdResourceAllocation ON tdResourceAllocation.AllocationPlanID=tsTasks.ResourcePlanningID"

            '---- clause ---------
            Dim lUserResourceID As Integer

            lUserResourceID = DataMethods.glGetDBValue(oClientInfo, "ResourceID", "tdResources", _
                                                "UserInitials=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentUser))

            sClause = "ResourceID=" & lUserResourceID

            sClause = DataTools.gsClauseAnd(sClause, "TaskDueDate>=" & DataTools.gsDate2Sql(dtDateFrom))
            sClause = DataTools.gsClauseAnd(sClause, "TaskDate<=" & DataTools.gsDate2Sql(dtDateTo))

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, "TaskID", sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
