﻿// AppointmentsMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    sData = document.all.txtGotoDate.value;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL = '../AppointmentDetails/AppointmentDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sItemID;

    document.location = sURL;
}

function mOnListViewClick(sListView, sItemID)
{
    var sURL = '../AppointmentDetails/AppointmentDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sItemID;

    document.location = sURL;
}

function cmdMonthPrev_OnClick()
{
    mNavigate(-30);
}

function cmdWeekPrev_OnClick()
{
    mNavigate(-7);
}

function cmdDayPrev_OnClick()
{
    mNavigate(-1);
}

function cmdMonthNext_OnClick()
{
    mNavigate(30);
}

function cmdWeekNext_OnClick()
{
    mNavigate(7);
}

function cmdDayNext_OnClick()
{
    mNavigate(1);
}

function mNavigate(lType)
{
    var sURL = 'AppointmentsProcess.aspx' +
                '?Cmd=Navigate' +
                '&Type=' + lType +
                '&Date=' + document.all.txtGotoDate.value;

    document.all.txtGotoDate.value = gsCallServerMethod(sURL, '');
    mRefreshList();
}  

