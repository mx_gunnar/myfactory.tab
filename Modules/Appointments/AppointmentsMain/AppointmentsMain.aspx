<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Appointments.AppointmentsMain" codefile="AppointmentsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable style="table-layout:fixed;" height="98%" cellSpacing=2 cellPadding=2 width="99%">
<tr height="30px">
	<td>
		<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Appointments/Appointmentsmain/dlgAppointmentsmain.xml"></myfactory:wfXmlDialog>
	</td>
</tr>
<tr height="*" valign="top">
	<td>
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="AppointmentsListData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
