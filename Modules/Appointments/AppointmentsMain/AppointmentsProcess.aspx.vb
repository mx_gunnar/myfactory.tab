'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AppointmentsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for AppointmentsProcess
'--------------------------------------------------------------------------------
' Created:      1/28/2011 3:13:35 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Appointments

    Partial Class AppointmentsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Navigate"
                    Return msNavigate(oClientInfo)
                Case Else
                    Return "Unknown Cmd:" & sCmd
            End Select


        End Function

        ''' <summary>
        ''' calc new date
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msNavigate(ByVal oClientInfo As ClientInfo) As String

            Dim dtDate As Date
            Dim lType As Integer

            dtDate = gdtCDate(Request.QueryString("Date"))
            If Not gbDate(dtDate) Then dtDate = Date.Today

            lType = glCInt(Request.QueryString("Type"))
            Select Case lType
                Case -30
                    dtDate = dtDate.AddMonths(-1)
                Case -7
                    dtDate = dtDate.AddDays(-7)
                Case -1
                    dtDate = dtDate.AddDays(-1)
                Case 1
                    dtDate = dtDate.AddDays(1)
                Case 7
                    dtDate = dtDate.AddDays(7)
                Case 30
                    dtDate = dtDate.AddMonths(1)
            End Select

            Return dtDate.ToShortDateString

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
