﻿// AppointmentDetailsMain.js (c) myfactory 2011

function mOnLoad() {

}

function mOnOK()
{
    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sURL = 'AppointmentDetailsProcess.aspx?Cmd=Save&ResourceID=' + msPageParams[3];

    var sRes = gsCallServerMethod(sURL, sValues);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mNavigateToList();
}

function mOnCancel() {
    mNavigateToList();
}

function mNavigateToList() {

    var sURL = '../AppointmentsMain/AppointmentsMain.aspx' +
                '?ClientID=' + msClientID;

    
    // if i come from Address-Details: navigate back to AddressDetails
    if (msPageParams[0] != '0')
    {
        sURL = '../../Addresses/AddressDetails/AddressDetailsMain.aspx?ClientID=' + msClientID + '&RecordID=' + msPageParams[0];
    }

    // if i come from Appointment-Overview: navigate back to correct page / date
    if (msPageParams[1] != '0') 
    {
        sURL = '../../AppointmentGroup/AppointmentGroupMain/AppointmentGroupMain.aspx?ClientID=' + msClientID;
        sURL = sURL + '&Page=' + msPageParams[1];
        sURL = sURL + '&Date=' + msPageParams[2];
    }


    document.location = sURL;
}

function mOnDelete() {
 
    var conf = window.confirm(msTerms[0]);
    if (conf == false) 
    {
        return;
    }

    var sURL = 'AppointmentDetailsProcess.aspx?Cmd=Delete&AppointmentID=' + document.all.txtTaskID.value;

    var sRes = gsCallServerMethod(sURL, '');
    if (sRes.substr(0, 3) != 'OK;') {
        alert(sRes);
        return;
    }

    mNavigateToList();
}