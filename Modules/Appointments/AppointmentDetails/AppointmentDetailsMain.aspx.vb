'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    AppointmentDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for AppointmentDetailsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspPhoneTools


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Appointment

    Partial Class AppointmentDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))

            Dim lAppointmentPage As Integer = glCInt(Request.QueryString("AppointmentPage"))
            Dim sAppointmentDate As String = Request.QueryString("AppointmentDate")
            Dim lResourceID As Integer = glCInt(Request.QueryString("ResourceID"))

            Dim sMobilePhonePlatform = ClientValues.gsGetClientValue(oClientInfo, "Tab_MobilePlatformName")

            ReDim Me.asPageParams(5)
            ReDim Me.asTerms(1)
            Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "M�chten Sie diesen Termin wirklich l�schen?")
            Me.asPageParams(0) = Str(lAddressID)
            Me.asPageParams(1) = lAppointmentPage.ToString
            Me.asPageParams(2) = sAppointmentDate
            Me.asPageParams(3) = lResourceID.ToString

            Dim bFullAccess = ItemApprovals.gbCheckItemPermission(oClientInfo, 9500, lResourceID, Permissions.wfEnumPermissions.wfPermissionAllowed)
            If Not bFullAccess Then
                Me.cmdOK.bHidden = True
                Me.cmdDelete.bHidden = True
            End If


            If lAddressID <> 0 Then
                ' new Appointment for lAddressID
                ' set some default values (location / subject / addressID)

                Dim sMatchcode As String = DataMethods.gsGetDBValue(oClientInfo, "Matchcode", "tdAddresses", "AddressID=" & lAddressID.gs2Sql)
                Dim sAddressString As String = DataMethods.gsGetDBValue(oClientInfo, "Street + ', ' + PostalCode + ' ' + City", "tdAddresses", "AddressID=" & lAddressID)
                Me.dlgMain.oDialog.oField("AddressID").oMember.Prop("Value") = lAddressID
                Me.dlgMain.oDialog.oField("TaskDesc").oMember.Prop("Value") = sMatchcode
                Me.dlgMain.oDialog.oField("TaskLocation").oMember.Prop("Value") = sAddressString
            End If


            If lRecordID <> 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "TaskID,TaskDesc,TaskDueDate AS OrigTaskDueDate, TaskDate as OrigTaskDate," & _
                                                   "TaskText,'' AS TaskDueDate, '' AS TaskTimeTo, '' AS TaskDate, '' AS TaskTimeFrom, " & _
                                                   "TaskLocation", _
                                                   "tsTasks", "TaskID=" & lRecordID)
                If Not rs.EOF Then
                    rs("TaskDueDate").Value = gsGetDate(rs("OrigTaskDueDate").dtValue)
                    rs("TaskTimeTo").Value = gsGetTimeFromDate(rs("OrigTaskDueDate").dtValue)

                    rs("TaskDate").Value = gsGetDate(rs("OrigTaskDate").dtValue)
                    rs("TaskTimeFrom").Value = gsGetTimeFromDate(rs("OrigTaskDate").dtValue)


                    'bring into correct jQueryMobile-Format
                    If sMobilePhonePlatform = "iPad" Then
                        rs("TaskDate").Value = PhoneToolFunctions.gsGetDateInMobileFormat(rs("TaskDate").dtValue)
                        rs("TaskDueDate").Value = PhoneToolFunctions.gsGetDateInMobileFormat(rs("TaskDueDate").dtValue)
                    End If


                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
            Else
                'bring into correct jQueryMobile-Format
                If sMobilePhonePlatform = "iPad" Then
                    Me.dlgMain.oDialog.oField("TaskDate").oMember.Prop("VALUE") = PhoneToolFunctions.gsGetDateInMobileFormat(Date.Now)
                    Me.dlgMain.oDialog.oField("TaskDueDate").oMember.Prop("VALUE") = PhoneToolFunctions.gsGetDateInMobileFormat(Date.Now)
                End If
            End If

            If sMobilePhonePlatform <> "iPad" Then
                If Request.Browser.Browser.Contains("Chrome") Then
                    Me.dlgMain.oDialog.oField("TaskTimeTo").oMember.Prop("INPUTTYPE") = ""
                    Me.dlgMain.oDialog.oField("TaskDueDate").oMember.Prop("INPUTTYPE") = ""

                    Me.dlgMain.oDialog.oField("TaskTimeFrom").oMember.Prop("INPUTTYPE") = ""
                    Me.dlgMain.oDialog.oField("TaskDate").oMember.Prop("INPUTTYPE") = ""
                End If
            End If

            Me.sOnLoad = "mOnLoad()"
            Me.gAddScriptLink("wfDlgParams.js", True)

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
