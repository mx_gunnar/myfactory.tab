'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for TasksDetailsProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		05.02.2013  -  ABuhleier  -  AddressID implemented
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Base.General
Imports myfactory.BusinessTasks.Resources.ToolFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Appointment

    Partial Class AppointmentDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Save"
                    Return msSaveAppointmentData(oClientInfo)
                Case "Delete"
                    Return msDeleteAppointment(oClientInfo, glCInt(Request.QueryString("AppointmentID")))
                Case Else
                    Return "Unknown Cmd: " & sCmd
            End Select

            Return "TODO"

        End Function

        Private Function msDeleteAppointment(ByVal oClientInfo As ClientInfo, ByVal lAppointmentID As Integer) As String
            Dim oAppointment As New myfactory.BusinessTasks.Base.General.Appointment

            If oAppointment.gbLoad(oClientInfo, lAppointmentID) Then
                If oAppointment.gbDel() Then
                    Return "OK;"
                Else
                    Return Dictionary.gsTranslate(oClientInfo, "Beim L�schen des Termins ist ein Fehler aufgetreten")
                End If
            End If

            Return "OK;"

        End Function

        ''' <summary>
        ''' Save Task Data
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msSaveAppointmentData(ByVal oClientInfo As ClientInfo) As String

            Dim lResourceID As Integer = glCInt(Request.QueryString("ResourceID"))

            Dim oXml As New XmlDocument
            Dim oRoot As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oRoot.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            '--------- check values ----------------
            Dim sDesc As String
            Dim dtTaskDate, dtTaskDueDate As Date
            Dim lAppointmentID As Integer
            Dim lAddressID As Integer
            Dim oAppointment As New myfactory.BusinessTasks.Base.General.Appointment

            sDesc = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskDesc")
            If sDesc = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Aufgabenbetreff an.")

            dtTaskDate = gdtCDate(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskDate"))
            If Not gbDate(dtTaskDate) Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie ein g�ltiges Beginn-Datum an.")
            dtTaskDueDate = gdtCDate(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskDueDate"))
            If Not gbDate(dtTaskDueDate) Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie ein g�ltiges End-Datum an.")

            lAppointmentID = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskID"))
            If lAppointmentID = 0 Then
                oAppointment.gbNew(oClientInfo)
            Else
                If Not oAppointment.gbLoad(oClientInfo, lAppointmentID) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Termins")
            End If


            oAppointment.oMember.Prop("TaskDesc") = sDesc
            oAppointment.oMember.Prop("TaskText") = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskText")
            oAppointment.oMember.Prop("TaskLocation") = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskLocation")


            dtTaskDate = gdtAddTime2Date(dtTaskDate, gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeFrom")))
            oAppointment.oMember.Prop("TaskDate") = dtTaskDate
            oAppointment.oMember.Prop("TaskTimeFrom") = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeFrom")).ToShortTimeString

            dtTaskDueDate = gdtAddTime2Date(dtTaskDueDate, gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeTo")))
            oAppointment.oMember.Prop("TaskDueDate") = dtTaskDueDate
            oAppointment.oMember.Prop("TaskTimeTo") = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeTo")).ToShortTimeString

            ' set AddressID
            lAddressID = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtAddressID"))
            If lAddressID > 0 Then
                oAppointment.oMember.Prop("AddressID") = lAddressID
            End If


            'set resource id
            If lAppointmentID = 0 Then
                If lResourceID > 0 Then
                    Dim sForeignUserInitials As String = DataMethods.gsGetDBValue(oClientInfo, "UserInitials", "tdResources", "ResourceID=" & lResourceID)
                    If Not String.IsNullOrEmpty(sForeignUserInitials) Then
                        oAppointment.oMember.Prop("UserName") = sForeignUserInitials
                    End If
                    Dim oAppRes As New AppointmentResource
                    oAppRes.lResourceID = lResourceID
                    oAppointment.AppointmentResources.bAdd(oAppRes)
                Else
                    oAppointment.oMember.Prop("UserName") = oClientInfo.oClientProperties.sCurrentUser
                End If
            End If

            If 0 = oAppointment.glSave(oClientInfo, False, False, False) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Speichern des Termins")

            Return "OK;"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
