'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectDetails.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProjectDetails
'--------------------------------------------------------------------------------
' Created:      16.09.2013 12:37:24, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectDetails
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = lProjectID.ToString

            Me.sOnLoad = "mOnLoad();"
            Me.gAddScriptLink("wfDlgParams.js", True)

            If lProjectID <> 0 Then
                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdProjects", "ProjectID=" & lProjectID)
                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
            End If


            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ProjectDetails' ", _
                        , "ShowIndex", PublicEnums.wfEnumDataSources.wfDataSourceSystem)

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "ProjectID=" & lProjectID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString
            osHTML.bAppend("</nobr>")

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
