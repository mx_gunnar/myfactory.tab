<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Projects.ProjectDetails" CodeFile="ProjectDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />

    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="99%" height="97%" style="table-layout:fixed">
        <tr valign="top" height="100px;">
            <td>
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Projects/ProjectsDetails/dlgProjectDetails.xml" />
                </form>
            </td>
        </tr>
        <tr valign="top">
            <td align="right">
                <myfactory:wfButton runat="server" ID="cmdSave" sText="Speichern" sOnClick="mOnSave();">
                </myfactory:wfButton>
            </td>
        </tr>
        <tr height="*">
            <td>
                &nbsp;
            </td>
        </tr>
        <tr height="130px" valign="bottom" id="trNavi">
            <td align="right">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
