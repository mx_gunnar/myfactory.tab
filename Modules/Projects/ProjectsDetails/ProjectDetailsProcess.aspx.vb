'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for ProjectDetailsProcess
'--------------------------------------------------------------------------------
' Created:      16.09.2013 12:37:36, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String


            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Save"
                    Return msSaveChanges(oClientInfo, glCInt(Request.QueryString("ProjectID")))
                Case Else
                    Return "unknown method"
            End Select

        End Function


        Private Function msSaveChanges(ByVal oClientInfo As ClientInfo, ByVal lProjectID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '----------------------------------------

            '--------- validate dialog params ---------
            If XmlFunctions.gsGetXMLText(oRootNew.xml, "txtMatchcode") = "" Then
                Return Dictionary.gsTranslate(oClientInfo, "Das Feld 'Bezeichnung' darf nicht leer sein.")
            End If
            '------------------------------------------

            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\base\Projects\tableprojects.xml", sQry, sMsg, True) Then
                Return sMsg
            End If

            If Not String.IsNullOrEmpty(sQry) Then
                If DataMethods2.glDBUpdate(oClientInfo, "tdProjects", sQry, "tdProjects.ProjectID=" & lProjectID) <> 1 Then
                    Return Dictionary.gsTranslate(oClientInfo, "Beim Speichern der �nderung ist ein Fehler aufgetreten")
                End If
            End If

            Return "OK;" & Dictionary.gsTranslate(oClientInfo, "�nderungen wurden gespeichert")

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
