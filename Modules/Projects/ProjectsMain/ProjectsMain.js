﻿



function mOnLoad()
{
    mRefreshProjectList("");
}

function mRefreshProjectList(sSearch)
{
    gListViewSetUserData('lstMain', sSearch + '$;$' + document.all.cboState.value);
    gListViewLoadPage('lstMain', 1);
}

function mOnListViewBtnClick(sListView,sColID,sItemId)
{
    mNavigateToDetails(sItemId);
}


function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
} 


function mNavigateToDetails(sProjectID)
{
    var sURL = '../ProjectsDetails/ProjectDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&ProjectID=' + sProjectID;

    document.location = sURL;
}

function mOnSetDirty()
{
    if (event.type == "change")
    {
        var sSearch = document.all.txtSearch.value;
        mRefreshProjectList(sSearch);
    }
}