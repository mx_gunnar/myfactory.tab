<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Projects.ProjectsMain" CodeFile="ProjectsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="35px">
            <td>
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Projects/ProjectsMain/dlgProjectsMain.xml" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="top" height="*">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="ProjectsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
    </table>
</body>
</html>
