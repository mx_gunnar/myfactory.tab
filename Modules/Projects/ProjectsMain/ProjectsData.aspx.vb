'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProjectsData
'--------------------------------------------------------------------------------
' Created:      16.09.2013 10:56:01, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.StringFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim oCol As AspListViewCol
            Dim sTables As String
            Dim lState As Integer
            Dim sSearch As String
            Dim asData As String() = Split(oListView.sUserData & "$;$", "$;$")

            sSearch = asData(0)
            lState = glCInt(asData(1))

            '---- ListView Properties ------

            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProjectsData.aspx"
            oListView.bAutoHideNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProjectNumber", "Nr", "80", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Bezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("ProjectState", "Status", "120", , , True, False)

            oListView.oCols.oAddCol("StartDate", "Startdatum", "100", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("EndDate", "Enddatum", "100", myfactory.Sys.Tools.DataTypeFunctions.wfEnumDataTypes.wfDataTypeDate, , True, False)

            oCol = oListView.oCols.oAddCol("Details", "Details", "50", , , False, False)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)



            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "StartDate desc, EndDate desc"
            End If


            sFields = "ProjectID AS RowID,ProjectNumber,Matchcode,PS.ProjectStateDesc, StartDate, EndDate"
            sTables = "tdProjects LEFT OUTER JOIN tdProjectStates PS ON PS.ProjectStateName = tdProjects.ProjectState"


            If Not String.IsNullOrEmpty(sSearch) Then
                sSearch = sSearch.Replace("*", "%")
                sSearch = sSearch & "%"
                sSearch = sSearch.gs2Sql

                sClause = sClause.gsClauseAnd("(ProjectNumber like " & sSearch & " OR Matchcode like " & sSearch & ")")
            End If

            If lState = 0 Then
                sClause = sClause.gsClauseAnd("tdProjects.InActive=0")
            ElseIf lState = 1 Then
                sClause = sClause.gsClauseAnd("tdProjects.InActive<>0")
            End If

            '---- permissions ----
            Dim sSubClause As String

            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Projects")
            If sSubClause <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
