'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectsFixcostsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for ProjectsFixcostsProcess
'--------------------------------------------------------------------------------
' Created:      18.09.2013 15:56:44, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Xml

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectsFixcostsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lPosID As Integer = glCInt(Request.QueryString("PosID"))
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))
            Dim lProductID As Integer = glCInt(Request.QueryString("ProductID"))

            Select Case sCmd
                Case "Delete"
                    Return msDeletePosition(oClientInfo, lPosID)
                Case "Add"
                    Return msAddPos(oClientInfo, lProjectID, lProductID)
                Case "Change"
                    Return msChangePos(oClientInfo, lPosID)
                Case "GetValueXML"
                    Return msGetValuesXML(oClientInfo, lPosID)
                Case Else
                    Return "unknown cmd"
            End Select

        End Function

        Private Function msChangePos(ByVal oClientInfo As ClientInfo, ByVal lPosID As Integer) As String

            Dim oXml As New XmlDocument
            Dim sUpdate As String = ""
            Dim oRoot As XmlNode

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oXml.documentElement.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '----------------------------------------

            sUpdate = "UseInCalc=$1$," & _
                        "Price=$2$," & _
                        "Quantity=$3$," & _
                        "ValuationPrice=$4$," & _
                        "ProjectOrderPosDesc=$5$"

            sUpdate = sUpdate.Replace("$1$", XmlFunctions.gsGetXMLSubNodeText(oRoot, "chkUseInCalc").gs2Sql)
            sUpdate = sUpdate.Replace("$2$", gcCCur(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtPrice")).gs2Sql)
            sUpdate = sUpdate.Replace("$3$", glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtQuantity")).gs2Sql)
            sUpdate = sUpdate.Replace("$4$", gcCCur(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtValuationPrice")).gs2Sql)
            sUpdate = sUpdate.Replace("$5$", XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtProjectOrderPosDesc").gs2Sql)

            If DataMethods2.glDBUpdate(oClientInfo, "tdProjectOrderPos", sUpdate, "ProjectOrderPosID =" & lPosID) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Beim Speichern ist ein Fehler aufgetreten")
            End If

            Return "OK;"
        End Function


        Private Function msAddPos(ByVal oClientInfo As ClientInfo, ByVal lProjectID As Integer, ByVal lProductID As Integer) As String

            Dim cProductPrice As Decimal
            Dim lCustomerID As Integer = 0
            Dim sInsert As String
            Dim cValuationPrice As Decimal = 0

            lCustomerID = DataMethods.glGetDBValue(oClientInfo, "CustomerID", "tdProjects", "ProjectID=" & lProjectID)

            cProductPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                                                lCustomerID, lProductID, _
                                                 1)

            Dim sClause As String = ""
            Dim bDivisionDependency As Boolean = False
            sClause = "ProductID=" & lProductID & " AND VariantID=0 "
            bDivisionDependency = DataMethods.gbGetDBValue(oClientInfo, "DivisionDependency", "tdProducts", "ProductID=" & lProductID)
            If bDivisionDependency Then
                sClause = sClause & " AND DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr
            Else
                sClause = sClause & " AND DivisionNr=0"
            End If
            cValuationPrice = DataMethods.gcGetDBValue(oClientInfo, _
                                                        "ValuationPrice", _
                                                        "tdProductStockInfo", sClause)

            sInsert = " INSERT tdProjectOrderPos (ProjectOrderPosID,ProjectID,ProductID, " & _
                        " Quantity,Price, ProjectOrderPosDesc,ValuationPrice " & _
                        " ) VALUES ( $1$,$2$,$3$,$4$,$5$,$6$,$7$ )"

            sInsert = Replace(sInsert, "$1$", RecordID.glGetNextRecordID(oClientInfo, "tdProjectOrderPos").ToString)
            sInsert = Replace(sInsert, "$2$", lProjectID.ToString)
            sInsert = Replace(sInsert, "$3$", lProductID.ToString)
            sInsert = Replace(sInsert, "$4$", "1")
            sInsert = Replace(sInsert, "$5$", cProductPrice.gs2Sql())
            sInsert = Replace(sInsert, "$6$", "''")
            sInsert = Replace(sInsert, "$7$", cValuationPrice.gs2Sql())

            If DataMethods2.glDBExecute(oClientInfo, sInsert) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Beim Hinzuf�gen des Artikels ist ein Fehler aufgetreten")
            End If

            Return "OK;"

        End Function

        Private Function msGetValuesXML(ByVal oClientInfo As ClientInfo, ByVal lPosID As Integer) As String
            Dim rs As Recordset
            Dim sXMLData As String = ""
            rs = DataMethods.grsGetDBRecordsetDirect(oClientInfo, "SELECT * FROM tdProjectOrderPos WHERE ProjectOrderPosID=" & lPosID)

            If Not rs.EOF Then
                sXMLData = myfactory.Sys.Data.DataTools.gsRecord2Xml(rs, True)
            End If
            rs.Close()

            Return sXMLData
        End Function


        Private Function msDeletePosition(ByVal oClientInfo As ClientInfo, ByVal lPosID As Integer) As String

            If DataMethods2.glDBDelete(oClientInfo, "tdProjectOrderPos", "ProjectOrderPosID=" & lPosID) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Beim L�schen ist ein Fehler aufgetreten")
            End If

            Return "OK;"
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
