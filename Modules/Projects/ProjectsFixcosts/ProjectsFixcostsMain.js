﻿var sProcessURL = 'ProjectsFixcostsProcess.aspx';


function mOnLoad()
{
    mRefreshList(1);
}


function mRefreshList(lPage)
{
    gListViewSetUserData('lstMain', msPageParams[0]);
    gListViewLoadPage('lstMain', lPage);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sListView == 'lstMain')
    {
        if (sColID == 'Delete')
        {
            if (window.confirm(msTerms[0]))
            {
                var sURL = sProcessURL + "?Cmd=Delete&PosID=" + sItemID;
                var sRes = gsCallServerMethod(sURL, '');

                if (sRes.substr(0, 4) == "ERR;")
                {
                    alert(sRes.substr(4, sRes.length - 4));
                }

                mRefreshList(glListViewGetPage('lstMain'));
            }
        }
    }
    else if (sListView == 'lstProducts')
    {
        if (sColID == 'CmdAdd')
        {
            // add product
            var sURL = sProcessURL + "?Cmd=Add&ProjectID=" + msPageParams[0];
            sURL = sURL + "&ProductID=" + sItemID;

            var sRes = gsCallServerMethod(sURL, '');

            if (sRes.substr(0, 4) == "ERR;")
            {
                alert(sRes.substr(4, sRes.length - 4));
            }

            // refresh main list
            mRefreshList(1);
        }
    }
}

function mOnListViewClick(sListView, sItemID)
{

    if (sListView == "lstMain")
    {

        var sURL = sProcessURL + "?Cmd=GetValueXML&PosID=" + sItemID;
        var sRes = gsCallServerMethod(sURL, '');

        gbSetDlgParamsXML('XMLValues', sRes, true);
    }
}

function mOnSetDirty()
{
    if (event.type == "change")
    {
        if (event.srcElement.id == "txtSearch")
        {
            gListViewSetUserData('lstProducts', document.all.txtSearch.value);
            gListViewLoadPage('lstProducts', 1);
        }
    }
}


function cmdSave_OnClick()
{
    var sSelectedItem = gsListViewGetSelection('lstMain');

    if (!sSelectedItem || sSelectedItem == "0")
    {
        return;
    }

    var sParams = gsXMLDlgParams(frmMain, '', false);

    var sURL = sProcessURL + "?Cmd=Change&PosID=" + sSelectedItem;
    var sRes = gsCallServerMethod(sURL, sParams);

    if (sRes.substr(0, 4) == "ERR;")
    {
        alert(sRes.substr(4, sRes.length - 4));
    }

    mRefreshList(glListViewGetPage('lstMain'));
    gListViewItemSelect('lstMain', sSelectedItem);
}