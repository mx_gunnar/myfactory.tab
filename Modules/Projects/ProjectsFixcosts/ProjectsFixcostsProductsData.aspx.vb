'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectFixcostsProductsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProjectFixcostsProductsData
'--------------------------------------------------------------------------------
' Created:      20.09.2013 11:31:52, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectFixcostsProductsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim sSearch As String = oListView.sUserData
            Dim sSearchClause As String = ""
            Dim oRow As AspListViewRow

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProjectsFixcostsProductsData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 3

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProductNumber", "Nr", "80", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True, False)

            oCol = oListView.oCols.oAddCol("CmdAdd", "", "20", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonNew)
            oCol.sText = "+"

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "ProductID AS RowID, ProductNumber, Matchcode, '+' as BtnAdd"
            sTables = "tdProducts"

            If Not String.IsNullOrEmpty(sSearch) Then
                sSearchClause = "(Matchcode like '$1$%' OR ProductNumber like '$1$%' OR Name1 like '$1$%')"
                sSearchClause = sSearchClause.Replace("$1$", sSearch)
                sClause = sClause.gsClauseAnd(sSearchClause)
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)



            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
