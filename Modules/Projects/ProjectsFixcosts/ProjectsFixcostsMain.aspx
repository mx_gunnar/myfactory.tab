<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Projects.ProjectsFixcostsMain" CodeFile="ProjectsFixcostsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="*">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="ProjectsFixcostsData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDataSource="XMLValues" sDialog="/tab/modules/Projects/ProjectsFixcosts/dlgProjectFixcosts.xml" />
                </form>
            </td>
            <td>
                <!-- xml dialog product search -->
                <!-- list view products -->
                <table width="100%" height="100%">
                    <tr height="10px">
                        <td width="100%">
                            <myfactory:wfXmlDialog runat="server" ID="dlgProducts" sDialog="/tab/modules/Projects/ProjectsFixcosts/dlgProducts.xml" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <myfactory:wfListView runat="server" ID="lstProducts" sListViewDataPage="ProjectsFixcostsProductsData.aspx">
                            </myfactory:wfListView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="130px" valign="bottom" id="trNavi">
            <td align="right" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
