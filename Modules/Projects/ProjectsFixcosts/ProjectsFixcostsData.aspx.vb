'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProjectFixcostsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProjectFixcostsData
'--------------------------------------------------------------------------------
' Created:      18.09.2013 15:56:11, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Projects

    Partial Class ProjectFixcostsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol
            Dim lProjectID As Integer = glCInt(oListView.sUserData)

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProjectsFixcostsData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.lPageSize = 5

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProductNumber", "ArtikelNr", "80", , , True, False)
            oListView.oCols.oAddCol("UseInCalc", "Abrechenbar", "100", , , True, False)
            oListView.oCols.oAddCol("Name1", "Name1", "*", , , True, False)
            oListView.oCols.oAddCol("Quantity", "Menge", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("BaseUnit", "ME", "50", , wfEnumAligns.wfAlignLeft, True, False)
            oListView.oCols.oAddCol("Price", "Preis", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("Total", "Gesamt", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("WKZ", "WKZ", "50", , wfEnumAligns.wfAlignLeft, True, False)


            oCol = oListView.oCols.oAddCol("Delete", "L�schen", "50", , , , )
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "POP.ProjectOrderPosID AS RowID, " & _
                        "P.ProductNumber, " & _
                        "POP.UseInCalc, " & _
                        "P.Name1, " & _
                        "POP.Quantity, " & _
                        "P.BaseUnit, " & _
                        "POP.Price, " & _
                        "'' AS Total"

            sTables = "tdProjectOrdeRPos POP INNER JOIN tdProducts P ON POP.ProductID=P.ProductID"

            If lProjectID <> 0 Then
                sClause = sClause.gsClauseAnd("POP.ProjectID=" & lProjectID)
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                If rs("UseInCalc").bValue Then
                    oRow.Value(1) = "Ja"
                Else
                    oRow.Value(1) = "Nein"
                End If

                oRow.Value(6) = rs("Price").cValue * rs("Quantity").cValue
                oRow.Value(7) = "EUR"
                rs.MoveNext()
            Loop
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
