<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Internet.InternetMain" codefile="InternetMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable style="table-layout:fixed;" height="98%" cellSpacing=2 cellPadding=2 width="99%">
<tr height="30px">
	<td width="300px">
		<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Internet/Internetmain/dlgInternetmain.xml"></myfactory:wfXmlDialog>
	</td>
	<td width="*">&nbsp;</td>
</tr>
<tr height="*" valign="top">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="InternetListData.aspx"></myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
