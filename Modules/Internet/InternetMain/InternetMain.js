﻿// InternetMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList()
}

function mRefreshList()
{
    var sData;

    sData = document.all.cboFilter.value;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL = gsListViewGetItemUserData(sListView, sItemID);

    window.open(sURL);
}

function mOnListViewClick(sListView, sItemID)
{
    var sURL = gsListViewGetItemUserData(sListView, sItemID);

    window.open(sURL);
}
