'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderContractsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrderContractsMain
'--------------------------------------------------------------------------------
' Created:      11.02.2013 12:16:57, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrderContracts

    Partial Class SalesOrderContractsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddressID As Integer = glCInt(Page.Request.QueryString("RecordID"))
            Dim lCustomerID As Integer = DataMethods.glGetDBValue(oClientInfo, "CustomerID", "tdCUstomers", "tdCustomers.AddressID=" & lAddressID.gs2Sql)

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lCustomerID)
            Me.asPageParams(1) = Str(lAddressID)

            Me.sOnLoad = "mOnLoad();"



            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAddressID))

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition,PartnerID,ModuleID", _
                                                   "tsTabModules", _
                                                   "ParentModule='AddressDetails' AND ModuleName<>'Addresses_SalesOrderContracts'", _
                                                   , "ShowIndex", _
                                                   wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAddressID))

                frs.MoveNext()
            Loop
            osHTML.bAppend("</nobr>")
            Me.lblNaviSub.sTranslatedText = osHTML.sString

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
