﻿

function mOnLoad() 
{
    mRefreshList();
}

function mRefreshList() 
{
    gListViewSetUserData('lstSalesOrderContracts', msPageParams[0]);
    gListViewLoadPage('lstSalesOrderContracts', -1);
}

function mOnListViewClick(sListView, sItemID) {
    mNavigateToDetails(sItemID);
}

function mOnListViewBtnClick(sListView, sColID, sItemID) {
    mNavigateToDetails(sItemID);
}


function mNavigateToDetails(sContractID) 
{
    var sURL = '../SalesOrderContractsDetail/SalesOrderContractsDetails.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[1] +
                '&SalesOrderContractID=' + sContractID;

    document.location = sURL;
}