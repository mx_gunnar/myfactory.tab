'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderContractsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrderContractsData
'--------------------------------------------------------------------------------
' Created:      11.02.2013 12:17:24, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrderContracts

    Partial Class SalesOrderContractsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim lCostumerID As Integer = glCInt(oListView.sUserData)


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.lPageSize = 15
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.sListViewDataPage = "SalesOrderContractsData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("Active", "Aktiv", "50", , wfEnumAligns.wfAlignCenter, False, False)
            oListView.oCols.oAddCol("ContractDesc", "Bezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("ContractNumber", "Vertragsnr", "150", , , True, False)
            oListView.oCols.oAddCol("ContractTypeDesc", "Vertragskreis", "150", , , True, False)

            oCol = oListView.oCols.oAddCol("CmdMail", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "Active, StartDate desc"
            End If

            sClause = sClause.gsClauseAnd("SOC.CustomerID=" & lCostumerID.gs2Sql)

            sFields = "SOC.ContractID AS RowID, " & _
                "CASE WHEN soc.Active=-1 THEN 'X' END," & _
                "SOC.ContractDesc, " & _
                "SOC.ContractNumber, " & _
                "SOCT.ContractTypeDesc, " & _
                "'' as CmdDetail"

            sTables = "tdSalesOrderContracts SOC LEFT OUTER JOIN tdSalesOrderContractTypes SOCT ON SOCT.ContractTypeName=SOC.ContractTypeName "

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
