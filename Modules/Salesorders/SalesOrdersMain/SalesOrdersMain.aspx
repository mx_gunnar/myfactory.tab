<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.SalesOrders.SalesOrdersMain" CodeFile="SalesOrdersMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="98%">
        <tr height="85px">
            <td width="700px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/SalesOrders/SalesOrdersmain/dlgSalesOrdersmain.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td align="right">
                <myfactory:wfButton runat="server" ID="cmdShow" sText="Anzeigen" sOnClick="mRefreshList()">
                </myfactory:wfButton>
            </td>
        </tr>
        <tr valign="top" height="*">
            <td colspan="2"  >
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="SalesOrdersListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr id="trPos" valign="top" height="250px">
            <td colspan="2"  >
                <myfactory:wfListView runat="server" ID="lstPos" sListViewDataPage="SalesOrdersPosListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi" >
	        <td align="right"   colspan="2">
		        <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">"></myfactory:wfLabel>
		        <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete"></myfactory:wfLabel>
	        </td>
        </tr>
    </table>
</body>
</html>
