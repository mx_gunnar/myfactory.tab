﻿// SalesOrdersMain.js (c) myfactory 2010

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    sData = document.all.txtDateFrom.value +
                ';' + document.all.txtDateTo.value +
                ';' + document.all.cboOrderType.value +
                ';' + document.all.chkAllDivisions.checked +
                ';' + document.all.txtSearch.value;

    // got addressid? hide txtSearch fields and add parameter to lv userdata
    if (msPageParams[0] > 0)
    {
        //document.getElementById("tdlabSearch").style.display = "none";
        //document.all.txtSearch.style.display = "none";
    }
    else
    {
        if (msPageParams[2] > 0)
        { }
        else
        {
            document.getElementById("trNavi").style.display = "none";
        }
    }

    // address id
    sData = sData + ';' + msPageParams[0];

    // representative id
    sData = sData + ';' + msPageParams[1];

    // project id
    sData = sData + ';' + msPageParams[2];

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);

    mRefreshPosList('-1');
}

function mOnSetDirty()
{
    if (event.type == 'change' || (event.type=='click' && event.srcElement.id=='chkAllDivisions'))
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL = "";

    if (sColID == "CmdOrder")
    {
        sURL = '../SalesOrdersEdit/SalesOrdersEditMain.aspx' +
                '?ClientID=' + msClientID +
                '&AddressID=' + msPageParams[0] +
                '&Representative=' + msPageParams[1] +
                '&ProjectID=' + msPageParams[2] +
                '&RecordID=' + sItemID;
    }
    else if (sColID == "CmdRef")
    {
        sURL = "../SalesOrderReferenceInfo/SalesOrderReferenceInfoMain.aspx?ClientID=" + msClientID + "&OrderID=" + sItemID;
    }

    document.location = sURL;
}

function mOnListViewClick(sListView, sItemID)
{
    if (sListView == 'lstMain')
        mRefreshPosList(sItemID);
}

function mRefreshPosList(sData)
{
    if (sData == '-1')
        sData = gsListViewGetSelection('lstMain');

    gListViewSetUserData('lstPos', sData);
    gListViewLoadPage('lstPos', 1);
}
