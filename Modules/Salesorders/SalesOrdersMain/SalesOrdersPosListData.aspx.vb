'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrdersPosListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersPosListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow = Nothing

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SalesOrdersPosListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("PosNumber", "Pos.", "30")
            oListView.oCols.oAddCol("ProductNumber", "Artikelnr.", "80")
            oListView.oCols.oAddCol("Name1", "Bezeichnung", "*")

            oListView.oCols.oAddCol("Quantity", "Menge", "50", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("Unit", "ME", "30")
            oListView.oCols.oAddCol("Price", "Einzelpreis", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("PriceS", "Gesamtpreis", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)

            oCol = oListView.oCols.oAddCol("Dummy1", "", "30")
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Dummy2", "", "80")
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Name2", "Bezeichnung 2", "*")
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Dummy3", "", "50")
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Dummy4", "", "30")
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Discount", "Rabatt(%)", "80", , wfEnumAligns.wfAlignRight)
            oCol.lLine = 1
            oCol = oListView.oCols.oAddCol("Dummy5", "", "80")
            oCol.lLine = 1

            '---- Data ---------------------
            Dim lOrderID As Integer
            Dim oOrder As New SalesOrder
            Dim oPos As SalesOrderPos

            lOrderID = glCInt(oListView.sUserData)
            If lOrderID = 0 Then Return True

            If Not oOrder.gbLoad(oClientInfo, lOrderID) Then Return True

            oListView.sTranslatedPrintHeader = oOrder.oMember.sProp("OrderTypeDesc") & " " & oOrder.oMember.sProp("OrderNumber")

            Dim lPosCounter, lPosCounterMax As Integer
            For Each oPos In oOrder.Positions
                If Not oPos.oMember.bProp("PosDeleted") And Not oPos.oMember.bProp("IsAdditional") Then
                    lPosCounterMax = lPosCounterMax + 1
                End If
            Next

            For Each oPos In oOrder.Positions
                lPosCounter = lPosCounter + 1
                If oListView.lPage = -1 Or (lPosCounter <= (oListView.lPage * oListView.lPageSize) _
                    And lPosCounter > ((oListView.lPage - 1) * oListView.lPageSize)) Then

                    If Not oPos.oMember.bProp("PosDeleted") And Not oPos.oMember.bProp("IsAdditional") Then
                        If oPos.oMember.lProp("PosType") = 6 Then
                            If Not oRow Is Nothing Then oRow.bLineBottom = True
                        Else
                            oRow = oListView.oRows.oAddRow(oPos.oMember.sProp("OrderPosID"))

                            oRow.sValue(0) = oPos.oMember.sProp("PosNumber")
                            oRow.sValue(1) = oPos.oMember.sProp("ProductNumber")
                            oRow.sValue(2) = oPos.oMember.sProp("Name1")

                            If oPos.oMember.lProp("PosType") < 4 Then
                                oRow.sValue(3) = gsFormatNumber(oPos.oMember.cProp("Quantity"), oPos.oMember.lProp("QuantityDecimals"))
                                oRow.sValue(4) = oPos.oMember.sProp("Unit")
                                oRow.Value(5) = oPos.oMember.cProp("Price")
                                oRow.Value(6) = oPos.oMember.cProp("PriceS") + oPos.oMember.cProp("DiscountS")

                                oRow.sValue(9) = oPos.oMember.sProp("Name2")
                                If oPos.oMember.cProp("Discount") <> 0 Then
                                    oRow.sValue(12) = oPos.oMember.sProp("Discount")
                                Else
                                    oRow.sValue(12) = "."   'for creating an empty row with defined height
                                End If
                            End If

                            If oPos.oMember.lProp("PosType") = 3 Then
                                oRow.sColor = "gray"
                            End If
                        End If
                    End If
                End If
            Next

            oListView.lRecordCountTotal = lPosCounterMax
            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
