'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrdersMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================


Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad()"

            'from representatives
            Dim lRepresentative As Integer = glCInt(Request.QueryString("Representative"))

            'from projects
            Dim lProjectID As Integer = glCInt(Request.QueryString("ProjectID"))

            'from addresses
            Dim sRecordID = Me.Request.QueryString("RecordID")


            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = sRecordID
            Me.asPageParams(1) = lRepresentative.ToString
            Me.asPageParams(2) = lProjectID.ToString

            If sRecordID <> "" Then
                '------- create navi for addresses ------- 

                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & sRecordID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                            "ModuleName,Edition,PartnerID,ModuleID", _
                            "tsTabModules", _
                            "ParentModule='AddressDetails'  AND ModuleName<>'Address_SalesOrders'", _
                            , "ShowIndex", _
                            wfDataSourceSystem)

                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & sRecordID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString


            ElseIf lProjectID > 0 Then
                '------- create navi for prjects ------- 

                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProjectDetails", "../../../", "ProjectID=" & lProjectID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                            "ModuleName,Edition,PartnerID,ModuleID", _
                            "tsTabModules", _
                            "ParentModule='ProjectDetails'  AND ModuleName <> 'Projects_SalesOrders'", _
                            , "ShowIndex", _
                            wfDataSourceSystem)

                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "ProjectID=" & lProjectID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString
            End If

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
