<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.SalesOrders.SalesOrdersEditMain" CodeFile="SalesOrdersEditMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" width="98%">
        <tr height="30px">
            <td colspan="2">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/SalesOrders/SalesOrdersEdit/dlgSalesOrdersEditmain.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td width="300px">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdMarkAsCurrentOrder" sStyle="width:155px;" sOnClick="OnMarkAsCurrent();"
                                runat="server" sText="Als aktuell markieren">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdEntityAddress" sStyle="width: 155px;" sOnClick="mOnEntityAddress();"
                                runat="server" sText="Zum Kunden" />
                        </td>
                    </tr>
                    <tr>
                    <td>
                        <myfactory:wfButton ID="cmdOrderAsPDF" sStyle="width: 155px;" sOnClick="mOrderAsPDF();" runat="server" sText="Beleg als PDF"></myfactory:wfButton>
                    </td></tr>
                </table>
            </td>
        </tr>
        <tr id="trPos" height="270px">
            <td colspan="3">
                <myfactory:wfListView runat="server" ID="lstPos" sListViewDataPage="SalesOrdersPosListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="100px">
            <td rowspan="2" width="200px" valign="top" id="tdProduct">
                <myfactory:wfXmlDialog runat="server" ID="dlgProduct" sDialog="/tab/modules/SalesOrders/SalesOrdersEdit/dlgProduct.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td rowspan="2" width="*" valign="top" style="margin-right: 20px; padding-right: 20px"
                id="tdProductList">
                <myfactory:wfListView runat="server" ID="lstProducts" sListViewDataPage="ProductsListData.aspx">
                </myfactory:wfListView>
            </td>
            <td width="300px" align="right">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfXmlDialog runat="server" ID="dlgSum" sDialog="/tab/modules/SalesOrders/SalesOrdersEdit/dlgOrderSum.xml"
                                sDataSource="dsoOrderSum">
                            </myfactory:wfXmlDialog>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                                       
                    <myfactory:wfButton runat="server" ID="cmdProcess" sOnClick="mOnProcess()" sText="Verarbeiten">
                    </myfactory:wfButton>
                    <myfactory:wfButton runat="server" ID="cmdMail" sOnClick="mOnMail()" sText="Senden">
                    </myfactory:wfButton>
                    <myfactory:wfButton runat="server" ID="cmdClose" sOnClick="mOnClose()" sText="Schlie�en">
                    </myfactory:wfButton>
                
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</body>
</html>
