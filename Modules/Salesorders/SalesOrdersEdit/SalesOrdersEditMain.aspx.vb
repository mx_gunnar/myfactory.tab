'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersEditMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrdersMain
'--------------------------------------------------------------------------------
' Created:      2/28/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Sales.Orders

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersEditMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lOrderID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lCurrentOderID = ClientValues.glGetClientValue(oClientInfo, "Tab_SalesOrders_CurrentOrderID")
            Dim lCustomerID As Integer
            Dim lAddressID As Integer

            If lOrderID = 0 Then
                ' check for current order in cache
                lOrderID = lCurrentOderID
            End If

            If lOrderID = lCurrentOderID Then
                ' order found! disable the 'MarkAsCurrentOrder' button
                Me.cmdMarkAsCurrentOrder.bDisabled = True
                Me.cmdMarkAsCurrentOrder.sText = "Ist aktueller Beleg"
            End If

            Dim sCurrUnit As String = String.Empty

            Me.sOnLoad = "mOnLoad()"
            Me.lstPos.sUserData = lOrderID.ToString

            ReDim Me.asTerms(5)
            ReDim Me.asTranslatedTerms(1)
            ReDim Me.asPageParams(7)

            Me.asTerms(0) = "M�chten Sie diese Position l�schen?"
            Me.asTerms(1) = "M�chten Sie diesen Beleg verarbeiten und an den Empf�nger senden?"
            Me.asTerms(2) = "Es wurde kein Beleg gefunden."
            Me.asTerms(3) = "Ist aktueller Beleg"

            Me.asPageParams(4) = glCInt(Request.QueryString("AddressID")).ToString
            Me.asPageParams(5) = glCInt(Request.QueryString("Representative")).ToString
            Me.asPageParams(6) = glCInt(Request.QueryString("ProjectID")).ToString


            '----- fill dialog ------
            Dim rs As Recordset = DataMethods.grsGetDBRecordset(oClientInfo, _
                        "OrderNumber,OrderTypeDesc,OrderDate,CustomerNumber,Matchcode,CurrUnit,OrderType,CustomerID,DeliveryDate,ContactID", _
                        "tdSalesOrders", "OrderID=" & lOrderID)

            If Not rs.EOF Then

                sCurrUnit = rs("CurrUnit").sValue

                Me.asPageParams(0) = lOrderID.ToString
                Me.asPageParams(1) = rs("OrderType").sValue
                Me.asPageParams(2) = rs("CustomerID").sValue

                lCustomerID = rs("CustomerID").lValue

                lAddressID = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdCustomers", "CustomerID=" & lCustomerID)

                Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                Me.dlgMain.oDialog.oField("ContactID").oMember.Prop("PARAMS") = lAddressID

            End If


            '----- check readonly -------

            Dim sMsg As String = String.Empty

            Dim bReadOnly As Boolean = SalesOrderInfo.gbGetOrderInfoReadOnly(oClientInfo, lOrderID, sMsg)
            If bReadOnly Then
                Me.asTranslatedTerms(0) = sMsg
                Me.asPageParams(2) = "-1"
                Me.cmdProcess.bHidden = True
            End If


            '----- set curr unit --------
            Me.dlgSum.oDialog.oField("CurrUnit1").oMember.Prop("DESC") = sCurrUnit
            Me.dlgSum.oDialog.oField("CurrUnit2").oMember.Prop("DESC") = sCurrUnit
            Me.dlgSum.oDialog.oField("CurrUnit3").oMember.Prop("DESC") = sCurrUnit


            '---- set Customer-Entity-Default command ---------

            Dim sCmdEntity As String = DataMethods.gsGetDBValue(oClientInfo, "EditCmdTab", "tsEntities", "EntityName='Addresses'", , myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)
            sCmdEntity = sCmdEntity.Replace("$1", lAddressID.ToString)
            Me.asTerms(4) = sCmdEntity


            '----- check if mail is allowed -------
            Dim bMailAllowed As Boolean

            Dim lMailTemplate As Integer = GeneralProperties.glGetGeneralProperty(oClientInfo, "TabSalesMailTemplate")
            If lMailTemplate <> 0 Then
                bMailAllowed = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccessSalesMail", _
                                                               "", "Verkaufsbeleg als Mail senden")
            End If

            If Not bMailAllowed Then Me.cmdMail.bHidden = True

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
