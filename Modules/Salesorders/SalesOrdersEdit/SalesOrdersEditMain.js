﻿// SalesOrdersEditMain.js (c) myfactory

var sCurrentPosID = "";

function mOnLoad()
{
    if (msPageParams[0] == '')
    {
        alert(msTerms[2]);

        sURL = '../SalesOrdersMain/SalesOrdersMain.aspx' +
                '?ClientID=' + msClientID;

        document.location = sURL;
        return;
    }

    if (msTranslatedTerms[0] != '')
        alert(msTranslatedTerms[0]);

    if (msPageParams[2] == '-1')
    {
        document.getElementById('tdProduct').style.visibility = 'hidden';
        document.getElementById('tdProductList').style.visibility = 'hidden';
    }

    mRefreshPosList();
    mRefreshProductList();
}

function mRefreshPosList()
{
    gListViewLoadPage('lstPos', -1);
    mRefreshOrderSum();


    // reselect item
    gListViewItemSelect('lstPos', sCurrentPosID);
    gListViewScollIntoView('lstPos', sCurrentPosID, true);
}

function mRefreshOrderSum()
{
    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?OrderID=' + msPageParams[0] + '&Cmd=GetSums');

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
    var asRes = sRes.split(';');
    document.all.txtOrderSumNet.value = asRes[1];
    document.all.txtTaxSum.value = asRes[2];
    document.all.txtOrderSumGross.value = asRes[3];
}


function mOnListViewClick(sListView, sItemID)
{
    if (sListView == 'lstPos')
    {
        sCurrentPosID = sItemID;
    }
}

function mOnListViewCboChange(sListView, sColID, sItemID, sNewValue)
{
    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=ChangeVariante' +
                        '&OrderID=' + msPageParams[0] +
                        '&PosID=' + sItemID +
                        '&VariantID=' + sNewValue);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }

    mRefreshPosList();
}

function mOnListViewInputChange(sListView, sColID, sItemID, sNewValue)
{
    sCurrentPosID = sItemID;
    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=Input' +
                        '&OrderID=' + msPageParams[0] +
                        '&PosID=' + sItemID +
                        '&ColID=' + sColID +
                        '&Value=' + gsEncodeStringURL(sNewValue));
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
    mRefreshPosList();
}

function mOnClose()
{
    sURL = '../SalesOrdersMain/SalesOrdersMain.aspx' +
                '?RecordID=' + msPageParams[4] +
                '&Representative=' + msPageParams[5] +
                '&ProjectID=' + msPageParams[6] +
                '&ClientID=' + msClientID;

    document.location = sURL;
}

function mOnProcess()
{
    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=Process' +
                        '&OrderID=' + msPageParams[0]);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
    mOnClose();
}

function mOnMail()
{
    if (!window.confirm(msTerms[1]))
        return;

    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=Mail' +
                        '&OrderID=' + msPageParams[0]);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
    alert(sRes.substr(3));
    mOnClose();
}


function mOnSetDirty() {

    if ((event.type == 'change' && event.srcElement.id == 'txtProduct') || (event.type == 'click' && event.srcElement.id == 'chkOnlyOrderdProducts')) {
        mRefreshProductList();

        if (event.srcElement.id == 'txtProduct') {
            var lDirektProductID = gsGetCacheValue('Tab_DirektAddProductToSO');
            if (lDirektProductID && lDirektProductID != '' && lDirektProductID != '0' && lDirektProductID != 'empty') {
                gSetCacheValue('Tab_DirektAddProductToSO', '');

                var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=AddProduct' +
                    '&OrderID=' + msPageParams[0] +
                    '&ProductID=' + lDirektProductID);
                if (sRes.substr(0, 3) != 'OK;') {
                    alert(sRes);
                    return;
                }

                var asRes = sRes.split(";");
                if (asRes.length > 2) {
                    sCurrentPosID = asRes[2];
                }
                document.getElementById("txtProduct").value = "";
                mRefreshPosList();
            }
        }
    } else if ((event.type == 'change' && event.srcElement.id == 'cboContactID')) {
        var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=ChangeContact&OrderID=' + msPageParams[0] + '&ContactID=' + event.srcElement.value);
        if (sRes.substr(0, 3) != 'OK;') {
            alert(sRes);
            return;
        }
    } else if ((event.type == 'change' && event.srcElement.id == 'txtDeliveryDate')) {
        var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=ChangeOrderDeliveryDate&OrderID=' + msPageParams[0] + '&DeliveryDate=' + event.srcElement.value);
        if (sRes.substr(0, 3) != 'OK;') {
            alert(sRes);
            return;
        }
    }
}

function mRefreshProductList()
{
    var sOnlyOrderdProducts = document.all.chkOnlyOrderdProducts.checked;

    gListViewSetUserData('lstProducts', msPageParams[2] + ";" + sOnlyOrderdProducts + ";" + document.all.txtProduct.value);
    gListViewLoadPage('lstProducts', 1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sRes;

    if (sListView == 'lstProducts')
    {
        sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=AddProduct' +
                        '&OrderID=' + msPageParams[0] +
                        '&ProductID=' + sItemID);
        if (sRes.substr(0, 3) != 'OK;')
        {
            alert(sRes);
            return;
        }

        var asRes = sRes.split(";");
        if (asRes.length > 2)
        {
            sCurrentPosID = asRes[2];
        }
        
        mRefreshPosList();
    }
    if (sListView == 'lstPos')
    {
        if (!window.confirm(msTerms[0]))
            return;

        sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=DeletePos' +
                        '&OrderID=' + msPageParams[0] +
                        '&PosID=' + sItemID);
        if (sRes.substr(0, 3) != 'OK;')
        {
            alert(sRes);
            return;
        }
        mRefreshPosList();
    }
}

function OnMarkAsCurrent()
{
    var sRes = gsCallServerMethod('SalesOrdersEditProcess.aspx?Cmd=MarkAsCurrent&OrderID=' + msPageParams[0]);

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }


    document.all.cmdMarkAsCurrentOrder.disabled = true;
    document.all.cmdMarkAsCurrentOrder.innerHTML = msTerms[3];
}



function mOnEntityAddress()
{
    var sPath = msWebPageRoot + "/" + msTerms[4] + "&ClientID=" + msClientID;
    document.location = sPath;
}


function mOrderAsPDF()
{
    var sURL = msWebPageRoot + "/ie50/system/batchprint/order_download_direct.aspx?ClientID=" + msClientID + "&OrderID=" + msPageParams[0] + "&OrderType=Sales&PDFFormat=-1&DirectOpen=-1";
    window.open(sURL, "order_as_pdf", "target: _blank;");

}