'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrdersPosListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		19.09.12,  ABuhleier, Product Variants developed
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrdersEdit

    Partial Class SalesOrdersPosListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow = Nothing
            Dim bVariants As Boolean = True
            Dim lOrderID As Integer
            Dim bReadOnly As Boolean
            Dim bPriceChange As Boolean

            If Not Editions.gbCheckUserEditions(oClientInfo, "WWS4") Then
                bVariants = False
            End If

            If Not GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabSalesOrderProductVariants") Then
                bVariants = False
            End If


            '---- ListView Properties ------

            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SalesOrdersPosListData.aspx"
            oListView.lPageSize = 6
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            lOrderID = glCInt(oListView.sUserData)
            bReadOnly = SalesOrderInfo.gbGetOrderInfoReadOnly(oClientInfo, lOrderID, "")
            bPriceChange = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_SalesOrderPriceChange", "", "Verkaufspreise �ndern")

            '---- Columns ------------------
            oListView.oCols.oAddCol("PosNumber", "Pos.", "30")
            oListView.oCols.oAddCol("ProductNumber", "Artikelnr.", "80")
            oListView.oCols.oAddCol("Name1", "Bezeichnung", "*")

            oCol = oListView.oCols.oAddCol("Quantity", "Menge", "50", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)

            oListView.oCols.oAddCol("Unit", "ME", "30")
            oListView.oCols.oAddCol("Price", "Einzelpreis", "70", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("Discount", "Rabatt(%)", "70", , wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("PriceS", "Gesamtpreis", "70", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)

            oCol = oListView.oCols.oAddCol("cmdDelete", "", "50")
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50px"
            If bReadOnly Then oCol.bHidden = True

            '---- Data ---------------------
            Dim oOrder As New SalesOrder
            Dim oPos As SalesOrderPos
            Dim sVariantSrc As String = ""
            Dim sVariantData As String = ""
            If lOrderID = 0 Then Return True

            If Not oOrder.gbLoad(oClientInfo, lOrderID) Then Return True

            oListView.sTranslatedPrintHeader = oOrder.oMember.sProp("OrderTypeDesc") & " " & oOrder.oMember.sProp("OrderNumber")

            Dim lPosCounter, lPosCounterMax As Integer
            For Each oPos In oOrder.Positions
                If Not oPos.oMember.bProp("PosDeleted") And Not oPos.oMember.bProp("IsAdditional") Then
                    lPosCounterMax = lPosCounterMax + 1
                End If
            Next

            For Each oPos In oOrder.Positions
                lPosCounter = lPosCounter + 1
                If oListView.lPage = -1 Or (lPosCounter <= (oListView.lPage * oListView.lPageSize) _
                    And lPosCounter > ((oListView.lPage - 1) * oListView.lPageSize)) Then

                    If Not oPos.oMember.bProp("PosDeleted") And Not oPos.oMember.bProp("IsAdditional") Then
                        If oPos.oMember.lProp("PosType") = 6 Then
                            If Not oRow Is Nothing Then oRow.bLineBottom = True
                        Else
                            oRow = oListView.oRows.oAddRow(oPos.oMember.sProp("OrderPosID"))

                            oRow.sValue(0) = oPos.oMember.sProp("PosNumber")
                            oRow.sValue(1) = oPos.oMember.sProp("ProductNumber")
                            oRow.sValue(2) = oPos.oMember.sProp("Name1")

                            '--- product variants  ----
                            If bVariants And oPos.oMember.lProp("ProductID") > 0 Then
                                mAddProductVariants(oClientInfo, oListView, oRow, oPos.oMember.lProp("ProductID"), oPos, bReadOnly, bPriceChange)
                            Else
                                oRow.bSetEmpty(3)
                            End If
                            '---------------------

                            If oPos.oMember.lProp("PosType") < 4 Then
                                oRow.sValue(3) = gsFormatNumber(oPos.oMember.cProp("Quantity"), oPos.oMember.lProp("QuantityDecimals"))
                                oRow.sValue(4) = oPos.oMember.sProp("Unit")
                                oRow.Value(5) = oPos.oMember.cProp("Price")
                                oRow.Value(7) = oPos.oMember.cProp("PriceS") + oPos.oMember.cProp("DiscountS")

                                If oPos.oMember.cProp("Discount") <> 0 Then oRow.sValue(6) = oPos.oMember.sProp("Discount")
                            End If

                            If oPos.oMember.lProp("PosType") = 3 Then
                                oRow.sColor = "gray"
                            End If

                            '---- input for quantities
                            If Not bReadOnly Then
                                If oPos.oMember.lProp("PosType") = 1 Then
                                    oRow.lColType(3) = wfEnumAspListViewColTypes.wfColTypeInput
                                    If bPriceChange Then
                                        oRow.lColType(5) = wfEnumAspListViewColTypes.wfColTypeInput
                                        oRow.lColType(6) = wfEnumAspListViewColTypes.wfColTypeInput
                                    End If
                                End If

                            End If
                        End If
                    End If
                End If
            Next

            oListView.lRecordCountTotal = lPosCounterMax
            Return True

        End Function



        Private Sub mAddProductVariants(ByVal oClientInfo As ClientInfo, ByRef oListView As AspListView, _
                                        ByRef oRow As AspListViewRow, _
                                        ByVal lProductId As Integer, ByRef oPos As SalesOrderPos, _
                                        ByVal bReadOnly As Boolean, ByVal bPriceChange As Boolean)

            Dim oVarRow As AspListViewRow
            Dim sData As String = ""
            Dim sSrc As String = ""
            Call mGetProductVariants(oClientInfo, oPos.oMember.lProp("ProductID"), sSrc, sData)

            For Each oVariant As SalesOrderPosVar In oPos.Variants

                'parent row properties
                oRow.bDisabled(3) = True

                'key
                oVarRow = oListView.oRows.oAddRow(oPos.oMember.lProp("OrderPosID") & "_" & oVariant.oMember.sProp("VariantID"))

                'variant combo box
                oVarRow.lColType(2) = wfEnumAspListViewColTypes.wfColTypeListCombo
                oVarRow.sComboSrc(2) = sSrc
                oVarRow.sComboData(2) = sData
                oVarRow.Value(2) = oVariant.oMember.sProp("VariantID")

                'quantity
                oVarRow.sValue(3) = gsFormatNumber(oVariant.oMember.cProp("Quantity"), oPos.oMember.lProp("QuantityDecimals"))

                'set fields editable if not readonly
                If Not bReadOnly Then
                    If oPos.oMember.lProp("PosType") = 1 Then
                        oVarRow.lColType(3) = wfEnumAspListViewColTypes.wfColTypeInput
                    End If
                End If
            Next

            'hase variants? 
            If sData <> "" Then

                'add empty row for new entries
                oVarRow = oListView.oRows.oAddRow(oPos.oMember.lProp("OrderPosID") & "_-1")
                oVarRow.lColType(2) = wfEnumAspListViewColTypes.wfColTypeListCombo
                oVarRow.sComboSrc(2) = sSrc
                oVarRow.sComboData(2) = sData

                ' hide X-Button
                oVarRow.bSetEmpty(8)

                oVarRow.lColType(3) = wfEnumAspListViewColTypes.wfColTypeInput
                oVarRow.bSetEmpty(5)

            End If


        End Sub





        Private Sub mGetProductVariants(ByVal oClientInfo As ClientInfo, ByVal lProductID As Integer, ByRef sSrc As String, ByRef sData As String)

            sSrc = ""
            sData = ""

            myfactory.FrontendSystem.AspTools.AspCombo.gbGetDataComboValues(oClientInfo, "VariantsMain", True, sData, sSrc, lProductID.ToString)


        End Sub


    End Class

End Namespace

'================================================================================
'================================================================================
