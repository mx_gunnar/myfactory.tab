'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersEditProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SalesOrdersEditProcess
'--------------------------------------------------------------------------------
' Created:      2/28/2011 2:45:57 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		19.09.12,  ABuhleier, Product Variants developed
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Email

Imports myfactory.FrontendApp
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.BusinessTasks.Sales.Orders.PublicEnums
Imports myfactory.BusinessTasks.Base.MailTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersEditProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        Private _msDeletePos As String


        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "GetSums"
                    Return msGetSums(oClientInfo, glCInt(Request.QueryString("OrderID")))

                Case "Input"
                    Return msInput(oClientInfo, _
                                   glCInt(Request.QueryString("OrderID")), _
                                   Request.QueryString("PosID"), _
                                   Request.QueryString("ColID"), _
                                   Request.QueryString("Value"))

                Case "Process"
                    Return msProcess(oClientInfo, glCInt(Request.QueryString("OrderID")))

                Case "Mail"
                    Return msMail(oClientInfo, glCInt(Request.QueryString("OrderID")))

                Case "AddProduct"
                    Return msAddProduct(oClientInfo, glCInt(Request.QueryString("OrderID")), glCInt(Request.QueryString("ProductID")))

                Case "AddProductExtern"
                    Return msAddProductExt(oClientInfo, glCInt(Request.QueryString("ProductID")))

                Case "MarkAsCurrent"
                    Return msMarkAsCurrent(oClientInfo, glCInt(Request.QueryString("OrderID")))

                Case "DeletePos"
                    Return msDeletePos(oClientInfo, glCInt(Request.QueryString("OrderID")), Request.QueryString("PosID"))

                Case "ChangeVariante"
                    Return msChangeVariante(oClientInfo, glCInt(Request.QueryString("OrderID")), _
                                                        Request.QueryString("PosID"), _
                                                        glCInt(Request.QueryString("VariantID")))
                Case "ChangeContact"

                    Return msChangeContact(oClientInfo, glCInt(Request.QueryString("OrderID")), glCInt(Request.QueryString("ContactID")))

                Case "ChangeOrderDeliveryDate"

                    Return msChangeOrderDeliveryDate(oClientInfo, glCInt(Request.QueryString("OrderID")), Request.QueryString("DeliveryDate"))

                Case Else
                    Return "Unknown Cmd:" & sCmd
            End Select

        End Function

        Private Function msChangeOrderDeliveryDate(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer, ByVal sDeliveryDate As String) As String
            Dim oSalesOrder As New SalesOrder

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            Dim dtDeliveryDate As Date

            If Not Date.TryParse(sDeliveryDate, dtDeliveryDate) Then
                Return Dictionary.gsTranslate(oClientInfo, "Liefertermin falsch formatiert. Bitte geben Sie ein g�ltiges Datum ein")
            End If

            oSalesOrder.oMember.Prop("DeliveryDate") = dtDeliveryDate

            For Each oPos As SalesOrderPos In oSalesOrder.Positions
                If oPos.oMember.dtProp("DeliveryDate") <> dtDeliveryDate Then
                    If Not SalesOrderPosMethods.gbSetDeliveryDate(oClientInfo, oSalesOrder, oPos, dtDeliveryDate) Then
                        Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen des Liefertermins")
                    End If
                End If
            Next

            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK;"
            Else
                Return sMsg
            End If

        End Function

        Private Function msChangeContact(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer, ByVal lContactID As Integer) As String
            Dim oSalesOrder As New SalesOrder

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            If Not SalesOrderMethods.gbSetContact(oClientInfo, oSalesOrder, lContactID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen des Ansprechpartners")
            End If

            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK;"
            Else
                Return sMsg
            End If

        End Function

        Private Function msChangeVariante(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer, ByVal sPosID As String, ByVal lNewVariantID As Integer) As String

            Dim oSalesOrder As New SalesOrder
            Dim oPos As SalesOrderPos = Nothing
            Dim lPosID As Integer = glCInt(sPosID.Split("_"c)(0))
            Dim lVarID As Integer = glCInt(sPosID.Split("_"c)(1))


            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            '-------------- Get Position ---------------------
            Dim bFound As Boolean

            bFound = False
            For Each oPos In oSalesOrder.Positions
                If oPos.oMember.lProp("OrderPosID") = lPosID Then
                    bFound = True
                    Exit For
                End If
            Next

            If Not bFound Then
                Return (Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Position"))
            End If
            '---------------------------------------------------


            '--- set new variant  ---- 
            For Each oExistingVar As SalesOrderPosVar In oPos.Variants
                If oExistingVar.oMember.lProp("VariantID") = lNewVariantID Then
                    Return Dictionary.gsTranslate(oClientInfo, "Diese Variante wurde bereits f�r diesen Artikel hinzugef�gt.")
                End If
            Next

            If lVarID > 0 Then
                'change variant
                For Each oExistingVar As SalesOrderPosVar In oPos.Variants
                    If oExistingVar.oMember.lProp("VariantID") = lVarID Then
                        If lNewVariantID = 0 Then
                            'if new variant id is 0 -> delete this variant
                            Return msDeletePos(oClientInfo, lOrderID, sPosID)
                        Else
                            oExistingVar.oMember.Prop("VariantID") = lNewVariantID
                        End If
                    End If
                Next
            Else
                'add new variant
                If lNewVariantID > 0 Then

                    Dim oNewVar As New SalesOrderPosVar()
                    Dim cQuat As Decimal = oPos.oMember.cProp("PriceUnit")

                    If oPos.Variants.Count = 0 Then
                        cQuat = oPos.oMember.cProp("Quantity")
                    End If

                    If cQuat = 0 Then
                        cQuat = 1
                    End If

                    oNewVar.oMember.Prop("VariantID") = lNewVariantID
                    oNewVar.oMember.Prop("Quantity") = cQuat
                    oNewVar.oMember.Prop("BaseQuantity") = oNewVar.oMember.cProp("Quantity") * oPos.oMember.cProp("UnitConversion")

                    oPos.Variants.Add(oNewVar)
                End If
            End If

            '--------- set new counts ---------
            mSetPosCounts(oPos)

            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK; variante changed"
            Else
                Return sMsg
            End If

        End Function

        Private Function mbSaveSalesOrder(ByVal oClientInfo As ClientInfo, ByVal oSalesOrder As SalesOrder, ByVal bFinal As Boolean, ByRef sMsg As String) As Boolean
            '-------------- Save Changes ---------------------
            If SalesOrderMethodsT.glSaveSalesOrder(oClientInfo, oSalesOrder, bFinal) = 0 Then
                sMsg = Dictionary.gsTranslate(oClientInfo, "Fehler beim Speichern des Belegs")

                If oSalesOrder.oMember.sProp("Message") <> "" Then
                    sMsg &= vbCrLf() & oSalesOrder.oMember.sProp("Message")
                End If

                Return False
            End If

            Return True

        End Function

        Private Sub mSetPosCounts(ByRef oPos As SalesOrderPos)
            Dim cSum As Decimal = 0D
            For Each oVariante As SalesOrderPosVar In oPos.Variants
                cSum = cSum + oVariante.oMember.cProp("Quantity")
            Next

            oPos.oMember.Prop("Quantity") = cSum
        End Sub


        Private Function msMarkAsCurrent(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer) As String

            If lOrderID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim setzten des aktuellen Belegs")
            End If

            ClientValues.gbSetClientValue(oClientInfo, "Tab_SalesOrders_CurrentOrderID", lOrderID)

            Return "OK; marked as currend"
        End Function

        ''' <summary>
        ''' Get Order Sums
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msGetSums(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer) As String

            Dim oSalesOrder As New SalesOrder
            Dim cTax, cNet, cGross As Decimal

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, True) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            With oSalesOrder.oMember
                cTax = .cProp("TaxSum")
                cGross = .cProp("OrderSumGross")
                cNet = cGross - cTax
            End With

            Return "OK;" & _
                    gsFormatNumber(cNet, 2) & ";" & _
                    gsFormatNumber(cTax, 2) & ";" & _
                    gsFormatNumber(cGross, 2)

        End Function

        ''' <summary>
        ''' handle user input on pos
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <param name="sPosID"></param>
        ''' <param name="sColID"></param>
        ''' <param name="sValue"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msInput(ByVal oClientInfo As ClientInfo, _
                                 ByVal lOrderID As Integer, ByVal sPosID As String, _
                                 ByVal sColID As String, ByVal sValue As String) As String

            Dim oSalesOrder As New SalesOrder
            Dim oPos As SalesOrderPos = Nothing
            Dim lPosID As Integer
            Dim lVarID As Integer

            If sPosID.Contains("_") Then
                lPosID = glCInt(sPosID.Split("_"c)(0))
                lVarID = glCInt(sPosID.Split("_"c)(1))
            Else
                lPosID = glCInt(sPosID)
            End If

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            '-------------- Get Position ---------------------
            Dim bFound As Boolean

            bFound = False
            For Each oPos In oSalesOrder.Positions
                If oPos.oMember.lProp("OrderPosID") = lPosID Then
                    bFound = True
                    Exit For
                End If
            Next

            If Not bFound Then
                Return (Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Position"))
            End If

            Select Case sColID
                Case "Quantity"

                    If lVarID > 0 Then
                        For Each oVar As SalesOrderPosVar In oPos.Variants
                            If oVar.oMember.lProp("VariantID") = lVarID Then
                                oVar.oMember.Prop("Quantity") = gcCCur(sValue)
                                oVar.oMember.Prop("BaseQuantity") = oVar.oMember.cProp("Quantity") * oPos.oMember.cProp("UnitConversion")

                                '--- calculate new sum
                                mSetPosCounts(oPos)

                                Exit For
                            End If
                        Next
                    Else
                        oPos.oMember.Prop("Quantity") = gcCCur(sValue)
                        oPos.oMember.Prop("BaseQuantity") = oPos.oMember.cProp("Quantity") * oPos.oMember.cProp("UnitConversion")
                    End If


                    If oPos.oMember.lProp("ReferencePosID") = 0 And oPos.oMember.lProp("PriceManual") = 0 Then
                        Call SalesOrderPosMethods.gbRefreshProductPrice(oClientInfo, oSalesOrder, oPos)
                    End If

                Case "Price", "Discount"
                    oPos.oMember.Prop(sColID) = gcCCur(sValue)
                    oPos.oMember.Prop("PriceManual") = -1

            End Select


            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK;" & lOrderID & ";" & lPosID
            Else
                Return sMsg
            End If

        End Function

        ''' <summary>
        ''' Process order
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msProcess(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer) As String

            Dim oSalesOrder As New SalesOrder

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, True, sMsg) Then
                Return "OK;" & lOrderID
            Else
                Return sMsg
            End If

        End Function

        ''' <summary>
        ''' Mail order
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msMail(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer) As String

            Dim oSalesOrder As New SalesOrder
            Dim lState As Integer

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            lState = oSalesOrder.oMember.lProp("OrderState")

            If lState <> 1 And lState <> 3 Then
                '-------------- Save  ---------------------
                If SalesOrderMethodsT.glSaveSalesOrder(oClientInfo, oSalesOrder, True) = 0 Then
                    Dim sMsg As String

                    sMsg = Dictionary.gsTranslate(oClientInfo, "Fehler beim Speichern des Belegs")
                    If oSalesOrder.oMember.sProp("Message") <> "" Then
                        sMsg &= vbCrLf() & oSalesOrder.oMember.sProp("Message")
                    End If
                    Return sMsg
                End If
            End If

            '----------- send mail -----------------
            Dim sSubject As String = ""
            Dim sEMailSender As String = ""
            Dim oTo As New EMailAddress
            Dim lTemplateID As Integer
            Dim rs As Recordset

            Dim sTo As String = ""
            Dim sCC As String = ""
            Dim sBCC As String = ""

            oTo.lAddressID = oSalesOrder.oMember.lProp("AddressID")
            oTo.lContactID = oSalesOrder.oMember.lProp("ContactID")
            oTo.SetEMail(oClientInfo)
            oTo.SetEMailAlias(oClientInfo)
            sTo = oTo.sAddress
            If oTo.sEMail = "" Then Return Dictionary.gsTranslate(oClientInfo, "F�r den Empf�nger konnte keine E-Mail-Adresse ermittelt werden.")

            lTemplateID = GeneralProperties.glGetGeneralProperty(oClientInfo, "TabSalesMailTemplate")
            If lTemplateID = 0 Then Return Dictionary.gsTranslate(oClientInfo, "Die Mailvorlage konnte nicht ermittelt werden.")

            Dim sText As String = MailTools.gsGetTemplateText(oClientInfo, lTemplateID, oTo, sSubject, "")

            '---------- get mail addresses --------------
            Dim colTO As New Collection
            Dim colCC As New Collection
            Dim colBCC As New Collection
            Dim oAddress As EMailAddress

            MailTools.gbGetTemplateAdresses(oClientInfo, lTemplateID, colTO, colCC, colBCC)
            For Each oAddress In colTO
                If sTo <> "" Then
                    sTo &= ";"
                End If
                sTo &= oAddress.sAddress
            Next

            MailTools.gbGetTemplateAdresses(oClientInfo, lTemplateID, colTO, colCC, colBCC)
            For Each oAddress In colCC
                If sCC <> "" Then
                    sCC &= ";"
                End If
                sCC &= oAddress.sAddress
            Next

            MailTools.gbGetTemplateAdresses(oClientInfo, lTemplateID, colTO, colCC, colBCC)
            For Each oAddress In colBCC
                If sBCC <> "" Then
                    sBCC &= ";"
                End If
                sBCC &= oAddress.sAddress
            Next

            '--------- get mail sender ---------------------
            sEMailSender = DataMethods.gsGetDBValue(oClientInfo, "MailTemplateFrom", "tdMailTemplates", "MailTemplateID=" & lTemplateID)
            If sEMailSender = "" Then sEMailSender = "info@myfactory.com"

            '--------- salescontactinformations ---------------------
            Dim sContactDesc As String = ""
            Dim sContactType As String = ""
            Dim sContactTopic As String = ""
            Dim sContactResult As String = ""

            rs = DataMethods.grsGetDBRecordset(oClientInfo, "CreateContact,ContactDesc,ContactResult,ContactTopic,ContactType", _
                                               "tdMailTemplates", "MailTemplateID=" & lTemplateID)
            If Not rs.EOF Then
                If rs("CreateContact").bValue Then
                    sContactDesc = rs("ContactDesc").sValue
                    sContactType = rs("ContactType").sValue
                    sContactResult = rs("ContactResult").sValue
                    sContactTopic = rs("ContactTopic").sValue
                End If
            End If

            '--------- order document ----------------------------
            Dim lDocumentID As Integer
            Dim sDocumentID As String = ""

            lDocumentID = myfactory.FrontendApp.Print.ToolFunctions.glCreateOrderDocument(oClientInfo, _
                                    lOrderID, 0, Print.ToolFunctions.wfEnumOrderType.Sales, _
                                    , , , , True)

            If lDocumentID <> 0 Then sDocumentID = lDocumentID.ToString

            '--------- send mail ----------------------------
            Dim bOK As Boolean

            bOK = MailTools.gbSaveMail(oClientInfo, _
                            0, _
                            0, _
                            sEMailSender, _
                            sTo, _
                            sCC, _
                            sBCC, _
                            sSubject, _
                            sText, _
                            0, _
                            True, sContactDesc, sContactType, sContactTopic, sContactResult, _
                            sDocumentID)

            Return "OK;" & Dictionary.gsTranslate(oClientInfo, "Der Beleg wurde an den Empf�nger gesendet.") & " " & oTo.sEMail

        End Function


        Private Function msAddProductExt(ByVal oClientInfo As ClientInfo, ByVal lProductID As Integer) As String

            Dim lOrderId As Integer = ClientValues.glGetClientValue(oClientInfo, "Tab_SalesOrders_CurrentOrderID")
            Dim sResult As String = ""

            If lOrderId <> 0 Then
                sResult = msAddProduct(oClientInfo, lOrderId, lProductID)
            Else
                Return Dictionary.gsTranslate(oClientInfo, "Es wurde kein aktueller Beleg gefunden")
            End If

            If Not sResult.StartsWith("OK;") Then
                Return sResult
            Else
                Return "OK;" & Dictionary.gsTranslate(oClientInfo, "Der Artikel wurde dem aktuellen Beleg hinzugef�gt.")
            End If

        End Function


        ''' <summary>
        ''' add new product to sales order
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <param name="lProductID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msAddProduct(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer, ByVal lProductID As Integer) As String

            Dim oSalesOrder As New SalesOrder
            Dim oPos As SalesOrderPos
            Dim sMsg As String


            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If


            '--- if the parameter: TabAutoIncrementSalesOrderPos is true: check product already exists in positions and increment its quantity (do not create a new own pos for the product)
            If DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdGeneralProperties", "PropertyName='TabAutoIncrementSalesOrderPos'") = -1 Then
                For Each pos As SalesOrderPos In oSalesOrder.Positions
                    If pos.oMember.lProp("ProductID") = lProductID Then

                        If pos.Variants.Count > 0 Then
                            Return Dictionary.gsTranslate(oClientInfo, "Hinzuf�gen nicht m�glich, da dieser Artikel bereits mit einer Variante dem aktuellen Beleg hinzugef�gt wurde.")
                        End If

                        
                        Dim cVal = pos.oMember.cProp("PriceUnit")
                        If cVal = 0 Then
                            cVal = 1
                        End If

                        pos.oMember.Prop("Quantity") = pos.oMember.cProp("Quantity") + cVal
                        If pos.oMember.lProp("ReferencePosID") = 0 And pos.oMember.lProp("PriceManual") = 0 Then
                            Call SalesOrderPosMethods.gbRefreshProductPrice(oClientInfo, oSalesOrder, pos)
                        End If

                        sMsg = Nothing
                        If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                            Return "OK;" & lOrderID & ";" & pos.oMember.lProp("OrderPosID")
                        Else
                            Return sMsg
                        End If

                    End If
                Next
            End If
            '-----------------------------------------------------------


            '--- get Last PosNumber ---
            Dim sLastPosNumber As String = ""

            For Each oPos In oSalesOrder.Positions
                If oPos.oMember.lProp("PosDeleted") = 0 And Not oPos.oMember.bProp("IsInternalPos") Then
                    sLastPosNumber = oPos.oMember.sProp("PosNumber")
                    If sLastPosNumber <> "" And oPos.oMember.lProp("PosType") = 7 Then
                        sLastPosNumber = sLastPosNumber & ".0"
                    End If
                End If
            Next

            '--- create new Position
            oPos = New SalesOrderPos
            oPos.gbNew(oClientInfo, wfEnumSalesPosTypes.wfSalesPosTypeProduct)
            If Not SalesOrderPosMethods.gbSetProduct(oClientInfo, oSalesOrder, oPos, lProductID) Then
                Return (Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen des Artikels") & vbCrLf() & oClientInfo.oErrors.sErrText)
            End If
            oPos.oMember.Prop("PosNumber") = SalesOrderTools.gsGetNextPositionNumber(sLastPosNumber)
            oSalesOrder.Positions.bAdd(oPos)


            sMsg = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK;" & lOrderID & ";" & oPos.oMember.lProp("OrderPosID")
            Else
                Return sMsg
            End If

        End Function

        ''' <summary>
        ''' Delete position from order
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderID"></param>
        ''' <param name="sPosID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msDeletePos(ByVal oClientInfo As ClientInfo, ByVal lOrderID As Integer, ByVal sPosID As String) As String

            Dim oSalesOrder As New SalesOrder
            Dim oPos As SalesOrderPos = Nothing
            Dim oVar As SalesOrderPosVar = Nothing

            Dim lPosId As Integer
            Dim lVarID As Integer

            If sPosID.Contains("_") Then
                lPosId = glCInt(sPosID.Split("_"c)(0))
                lVarID = glCInt(sPosID.Split("_"c)(1))
            Else
                lPosId = glCInt(sPosID)
            End If

            If Not oSalesOrder.gbLoad(oClientInfo, lOrderID, False) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden des Belegs")
            End If

            '-------------- Get Position ---------------------
            Dim bFound As Boolean

            bFound = False
            For Each oPos In oSalesOrder.Positions
                If oPos.oMember.lProp("OrderPosID") = lPosID Then
                    bFound = True
                    Exit For
                End If
            Next

            If Not bFound Then
                Return (Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Position"))
            End If


            '---- if VarID > 0 -> remove variante and do not delete complete pos --- 
            If lVarID > 0 Then
                Dim lVariantIndex = 0
                For Each oVar In oPos.Variants
                    If oVar.oMember.lProp("VariantID") = lVarID Then
                        Exit For
                    End If
                    lVariantIndex = lVariantIndex + 1
                Next

                If Not oVar Is Nothing Then
                    ' remove the found Variant
                    oPos.Variants.Remove(lVariantIndex)
                End If

            Else
                oPos.oMember.Prop("PosDeleted") = -1
            End If


            '--------- set new counts ---------
            mSetPosCounts(oPos)

            Dim sMsg As String = Nothing
            If mbSaveSalesOrder(oClientInfo, oSalesOrder, False, sMsg) Then
                Return "OK;" & lOrderID
            Else
                Return sMsg
            End If

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
