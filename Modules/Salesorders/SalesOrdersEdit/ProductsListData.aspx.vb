'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrdersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrdersPosListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		22.08.2012  A.Buhleier -> new condition developed (OnlyOrderedProducts)
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrdersEdit

    Partial Class ProductsListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow = Nothing
            Dim sSearch As String
            Dim sClause, sOrder, sTables, sFields As String
            Dim rs As Recordset
            Dim bOnlyOrderdProducts As Boolean
            Dim lCustomerID As Integer

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductsListData.aspx"
            oListView.lPageSize = 5
            oListView.bTabletMode = True
            oListView.bAutoHideNavigation = True
            oListView.bTabletScrollMode = True

            lCustomerID = glCInt(oListView.sUserData.Split(";"c)(0))
            bOnlyOrderdProducts = gbCBool(oListView.sUserData.Split(";"c)(1))
            sSearch = oListView.sUserData.Split(";"c)(2)

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProductNumber", "Nr.", "80", , , True)
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True)

            oCol = oListView.oCols.oAddCol("cmdInsert", "Hinzuf�gen", "70")
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "70px"
            oCol.sText = "+"

            '---- Data ---------------------
            If sSearch = "" And Not bOnlyOrderdProducts Then Return True

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "Matchcode"
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            sClause = "InActive=0"
            sSearch = Replace(sSearch, "*", "%")
            sClause = DataTools.gsClauseAnd(sClause, "(Matchcode like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name1 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name2 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR ProductNumber like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR EANNumber = " & sSearch.gs2Sql & _
                                                            ")")

            sFields = "ProductID,ProductNumber,Matchcode,'+'"
            sTables = "tdProducts"

            '---- permissions ----
            Dim sSubClause As String

            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Products")
            If sSubClause <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If

            If bOnlyOrderdProducts Then
                Dim lYears As Integer = DataMethods.glGetDBValue(oClientInfo, "PropertyValue", "tdGeneralProperties", "PropertyName='TabSalesOrderProductLastOrder'")
                lYears = lYears * -1
                Dim sYearClause As String = ""
                If lYears <> 0 Then
                    sYearClause = " SO.OrderDate > " & Date.Now.AddYears(lYears).gs2Sql & " AND "
                End If
                Dim sOnlyOrderdProductsClause As String = " tdProducts.ProductID IN ( SELECT SOP.ProductID from tdSalesOrderPos SOP INNER JOIN tdSalesOrders SO ON SO.OrderID=SOP.OrderID  WHERE " & sYearClause & " SO.CustomerID=" & lCustomerID & ")"

                sClause = sClause.gsClauseAnd(sOnlyOrderdProductsClause)

            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            ' get product id for autoadd to so if searched via EAN or ProductNumber
            If GeneralProperties.gbGetGeneralProperty(oClientInfo, "TabAutoAddProductToSO") Then
                Dim frsDirekt As FastRecordset = DataMethods.gfrsGetFastRecordset(oClientInfo, "ProductID", "tdProducts", "(EANNumber=" & sSearch.gs2Sql & " OR ProductNumber=" & sSearch.gs2Sql & ")".gsClauseAnd(sSubClause))
                If Not frsDirekt.EOF AndAlso frsDirekt.RecordCount = 1 Then
                    ClientValues.gbSetClientValue(oClientInfo, "Tab_DirektAddProductToSO", frsDirekt(0).lValue)
                Else
                    ClientValues.gbClearClientValue(oClientInfo, "Tab_DirektAddProductToSO")
                End If
            End If


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
