﻿

function mOnLoad()
{
    mRefreshOrders();
}


function mRefreshOrders()
{
    gListViewSetUserData('lstOrders', msPageParams[0]);
    gListViewLoadPage('lstOrders', 1);
}

function mOnBack()
{
    document.location = "../SalesOrdersMain/SalesOrdersMain.aspx?ClientID=" + msClientID;
}

function mOnListViewClick(sListView, sItemID)
{
    if (sListView == "lstOrders")
    {
        // show pos
        mRefreshStates(sItemID.split("_")[1], sItemID.split("_")[0]);
    }
}

function mRefreshStates(lProductID, lPurchaseOrderID)
{

    var sRes = gsCallServerMethod("SalesOrderReferenceInfoProcess.aspx?Cmd=GetStates&ProductID=" + lProductID + "&PurchaseOrderID=" + lPurchaseOrderID, "");

    if (sRes.substr(0, 3) == "OK;")
    {
        var asRes = sRes.split(";");
        document.all.txtState1.value = asRes[1];
        document.all.txtState2.value = asRes[2];
        document.all.txtState3.value = asRes[3];
    }
    else
    {
        alert(sRes);
   
    }

}