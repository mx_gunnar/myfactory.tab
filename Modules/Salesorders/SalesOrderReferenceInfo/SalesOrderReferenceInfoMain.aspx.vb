'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderReferenceInfoMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrderReferenceInfoMain
'--------------------------------------------------------------------------------
' Created:      21.10.2013 15:16:35, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrder

    Partial Class SalesOrderReferenceInfoMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lOrderID As Integer = glCInt(Request.QueryString("OrderID"))

            Me.dlgMain.oDialog.oField("OrderInfo").oMember.Prop("VALUE") = DataMethods.gsGetDBValue(oClientInfo, "OrderNumber", "tdSalesOrders", "OrderID=" & lOrderID)

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = lOrderID.ToString

            Me.sOnLoad = "mOnLoad()"


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
