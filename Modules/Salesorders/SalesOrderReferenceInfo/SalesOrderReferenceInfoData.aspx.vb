'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderReferenceInfoData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrderReferenceInfoData
'--------------------------------------------------------------------------------
' Created:      21.10.2013 15:38:27, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.BusinessTasks.Sales.Orders.PublicEnums

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrder

    Partial Class SalesOrderReferenceInfoData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim bFirst As Boolean
            Dim rs As Recordset
            Dim l As Integer

            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sClause As String = ""
            Dim sTables As String
            Dim lCount As Integer = 0

            Dim lOrderID As Integer = glCInt(oListView.sUserData)

            Dim oOrder As myfactory.BusinessTasks.Sales.Orders.SalesOrder
            Dim oPos As SalesOrderPos
            Dim lType As wfEnumSalesPosTypes


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SalesOrderReferenceInfoData.aspx"
            oListView.lPageSize = 999
            oListView.bTabletMode = True
            oListView.bAutoHideNavigation = True
            oListView.bTabletScrollMode = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("PosNumber", "Pos", "40")
            oListView.oCols.oAddCol("ProductNumber", "Artikel", "80")
            oListView.oCols.oAddCol("Name1", "Bezeichnung")
            oListView.oCols.oAddCol("OrderNumber", "Belegnummer", "100")
            oListView.oCols.oAddCol("OrderState", "Status", "80")
            oListView.oCols.oAddCol("Matchcode", "Lieferant", "150")

            oCol = oListView.oCols.oAddCol("CmdDetails", " ", "20")
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)

            oOrder = New myfactory.BusinessTasks.Sales.Orders.SalesOrder
            If Not oOrder.gbLoad(oClientInfo, lOrderID) Then Return True

            For Each oPos In oOrder.Positions
                lType = CType(oPos.oMember.lProp("PosType"), wfEnumSalesPosTypes)

                If (lType = wfEnumSalesPosTypes.wfSalesPosTypeProduct) And _
                    oPos.oMember.lProp("PosDeleted") = 0 _
                Then

                    bFirst = True
                    sClause = "r.ReferenceType=0 AND r.ReferenceID=" & oPos.oMember.lProp("OrderPosID")

                    rs = DataMethods.grsGetDBRecordset(oClientInfo, "p.OrderPosID,p.OrderID,o.OrderNumber,o.Matchcode, POS.StateDesc, P.ProductID", _
                                "tdPurchaseOrderPos p INNER JOIN tdPurchaseOrderPosReferences r ON p.OrderPosID=r.OrderPosID" & _
                                " INNER JOIN tdPurchaseOrders o ON p.OrderID=o.OrderID " & _
                                " INNER JOIN tdPurchaseOrderStates POS ON POS.StateID=o.OrderState ", _
                                sClause, "", _
                                "p.OrderID,p.PosIndex")
                    Do Until rs.EOF
                        l += 1
                        oRow = oListView.oRows.oAddRow(rs(1).lValue & "_" & rs(5).lValue & "_" & l)

                        If bFirst Then
                            oRow.Value(0) = oPos.oMember.sProp("PosNumber")
                            oRow.Value(1) = oPos.oMember.sProp("ProductNumber")
                            oRow.Value(2) = oPos.oMember.sProp("Name1")
                            bFirst = False
                        End If

                        oRow.Value(3) = rs(2).sValue
                        oRow.Value(4) = rs(4).sValue
                        oRow.Value(5) = rs(3).sValue

                        rs.MoveNext()
                    Loop
                End If
            Next

            oListView.lRecordCountTotal = l

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
