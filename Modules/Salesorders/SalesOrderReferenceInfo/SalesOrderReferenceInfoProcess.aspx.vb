'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderReferenceInfoProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SalesOrderReferenceInfoProcess
'--------------------------------------------------------------------------------
' Created:      22.10.2013 11:32:10, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrder

    Partial Class SalesOrderReferenceInfoProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lPurchaseOrderID As Integer = glCInt(Request.QueryString("PurchaseOrderID"))
            Dim lProductID As Integer = glCInt(Request.QueryString("ProductID"))

            Select Case sCmd
                Case "GetStates"
                    Return msGetState(oClientInfo, lProductID, lPurchaseOrderID)
            End Select

            Return "unknown cmd"

        End Function

        Private Function msGetState(ByVal oClientInfo As ClientInfo, ByVal lProductID As Integer, ByVal lPurchaseOrderID As Integer) As String

            If lPurchaseOrderID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim �bergeben der Parameter")
            End If

            Dim sReturn As String = "OK;"

            Dim frs As FastRecordset = DataMethods.gfrsGetFastRecordset(oClientInfo, "SUM(State1), SUM(State2), SUM(State3)", "tdPurchaseOrderMainPos", "ProductID=" & lProductID & " AND InitialOrderID=" & lPurchaseOrderID, "InitialOrderID, ProductID")

            If Not frs.EOF Then
                Do Until frs.EOF
                    sReturn = sReturn & frs(0).lValue & ";" & frs(1).lValue & ";" & frs(2).lValue
                    frs.MoveNext()
                Loop
                frs.Close()
            Else
                sReturn = sReturn & "0;0;0"
            End If

            Return sReturn
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
