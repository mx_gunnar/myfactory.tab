<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.SalesOrder.SalesOrderReferenceInfoMain" CodeFile="SalesOrderReferenceInfoMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr height="10px" valign="top">
            <td width="500px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/TAB/MODULES/Salesorders/SalesOrderReferenceInfo/dlgSalesOrderPosReferenceInfo.xml"></myfactory:wfXmlDialog>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr valign="top">
            <td colspan="2" height="390px">
                <myfactory:wfListView ID="lstOrders" runat="server" sListViewDataPage="SalesOrderReferenceInfoData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="5px">
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr valign="top" height="10px">
            <td colspan="2">
                <myfactory:wfXmlDialog runat="server" ID="dlgState" sDialog="/TAB/MODULES/Salesorders/SalesOrderReferenceInfo/dlgOrderStateInfo.xml"></myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr valign="bottom">
            <td align="right" height="70px" valign="middle" colspan="2">
                <myfactory:wfButton ID="cmdBack" runat="server" sText="Zur�ck" sOnClick="mOnBack();">
                </myfactory:wfButton>
            </td>
        </tr>
    </table>
</body>
</html>
