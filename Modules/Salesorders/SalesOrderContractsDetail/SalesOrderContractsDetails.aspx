<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.SalesOrderContracts.SalesOrderContractsDetails" CodeFile="SalesOrderContractsDetails.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="100%" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="98%">
        <tr valign="top" height="150px">
            <td width="100%">
                <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/tab/modules/SalesOrders/SalesOrderContractsDetail/DlgSalesOrderContracts.xml">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr valign="top" height="40px">
            <td align="left">
                <table width="300px">
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdAttributes" runat="server" sText="Kennzeichen" sOnClick="mShowAttributes();"
                                sStyle="Width: 140px;">
                            </myfactory:wfButton>
                        </td>
                        <td>
                            <myfactory:wfButton ID="cmdDocuments" runat="server" sText="Dokumente" sOnClick="mShowDocuments();"
                                sStyle="Width: 140px;">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top" id="trListDocuments">
            <td>
                <myfactory:wfListView ID="lstDocuments" sListViewDataPage="SalesOrderContractsDocumentsData.aspx"
                    runat="server">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr valign="top" id="trListAttributes">
            <td>
                <myfactory:wfListView ID="lstAttributes" sListViewDataPage="SalesOrderContractsAttributesData.aspx"
                    runat="server">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td align="right" width="*" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
