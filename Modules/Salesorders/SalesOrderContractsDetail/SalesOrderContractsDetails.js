﻿

function mOnLoad() 
{
    mShowAttributes();
}

function mRefreshDocumentList() {

    // show Documents  +  hide Attributes
    document.getElementById('trListAttributes').style.display = "none";
    document.getElementById('trListDocuments').style.display = "";

    gListViewSetUserData('lstDocuments', msPageParams[0]);
    gListViewLoadPage('lstDocuments', -1);
}

function mRefreshAttributeList() {

    // show Attributes  +   hide Documents
    document.getElementById('trListDocuments').style.display = "none";
    document.getElementById('trListAttributes').style.display = "";

    gListViewSetUserData('lstAttributes', msPageParams[0]);
    gListViewLoadPage('lstAttributes', -1);
}

function mShowAttributes() 
{
    mRefreshAttributeList();
}


function mShowDocuments()
{
    mRefreshDocumentList();
}



function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sListView == 'lstDocuments')
    {
        if (sColID == 'CmdMail')
        {
            // open new email with this documant as attachment
            mNavigateToNewMail(sItemID)
        }

        if (sColID == 'CmdDoc')
        {
            mOpenDocument(sItemID);
        }
    }
}

function mNavigateToNewMail(DocID)
{
    var sURL = '../../Email/EMailNew/EMailNewMain.aspx' +
                '?ClientID=' + msClientID + "&MailID=0&DocumentID=" + DocID;

    document.location = sURL;
}

function mOpenDocument(lDocumentID) {
    window.resizeTo(10, 10);

    //------ check permission to read ----------
    if (lDocumentID != '') {
        var sPerm = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' + 'document_Process.aspx?DocumentID=' + lDocumentID + '&Method=CheckPermissionRead', '');
        if (sPerm != ';-1') {
            if (sPerm != ';0')
                alert(sPerm);
            else
                alert(msTerms[3]);
            return;
        };
    }
    else
        return;

    //------ get link and open document --------
    var sLink = gsCallServerMethod(msWebPageRoot + '/IE50/BASE/Documents/' +
					'document_Process.aspx?DocumentID=' + lDocumentID +
					'&Method=GetDocumentLink&LocalMode=-1', '');
    if (sLink.substr(0, 1) != ';') {
        alert(sLink);
        return;
    }
    sLink = sLink.substr(1);
    if (sLink != '')
        window.open(sLink, '', 'menubar=no,fullscreen=no,scrollbars=yes,location=no,toolbar=no,resizable=yes,status=yes,left=50,top=50,width=600,height=400')
    //------------------------------------------	
}