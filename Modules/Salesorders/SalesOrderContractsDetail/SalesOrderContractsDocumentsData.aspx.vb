'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderContractsDocumentsData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for SalesOrderContractsDocumentsData
'--------------------------------------------------------------------------------
' Created:      11.02.2013 11:22:45, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrderContracts

    Partial Class SalesOrderContractsDocumentsData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "SalesOrderContractsDocumentsData.aspx"
            oListView.lPageSize = 4
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True


            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("CreationDate", "Datum", "80", wfEnumDataTypes.wfDataTypeDate, , True)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("DocumentDesc", "Bezeichnung", "*", , , True)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("DocumentExtension", "Typ", "80", , , True)
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("CmdMail", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonEMail)
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDoc", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            Dim sSubClause As String
            Dim lSalesOrderContractID As Integer = glCInt(oListView.sUserData)

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "D.CreationDate"
                oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            If sOrder = "CreationDate" Then
                sOrder &= ", D.DocumentID"
            ElseIf sOrder = "D.CreationDate DESC" Then
                sOrder &= ", D.DocumentID DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "D.DocumentID AS RowID,D.CreationDate,D.DocumentDesc,tsDocumentTypes.DocumentExtension"
            sTables = "tdDocuments D" & _
                        " LEFT JOIN tsDocumentTypes ON D.DocumentType = tsDocumentTypes.DocumentTypeID"

            '---- clause ---------
            sClause = "SystemDoc=0"

            If lSalesOrderContractID > 0 Then
                sTables = sTables & " INNER JOIN tdDocumentReferences DR ON DR.DocumentID=D.DocumentID"
                sClause = sClause.gsClauseAnd("DR.TypeID=7 AND ReferenceID=" & lSalesOrderContractID.gs2Sql)
            End If

            '---- permissions ----
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Documents")
            If sSubClause <> "" Then sClause = DataTools.gsClauseAnd(sClause, sSubClause)


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True


        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
