'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    SalesOrderContracts.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrderContracts
'--------------------------------------------------------------------------------
' Created:      08.02.2013 17:27:51, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Xml

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrderContracts

    Partial Class SalesOrderContractsDetails
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddressID As Integer = glCInt(Page.Request.QueryString("AddressID"))
            Dim lSalesOrderContractID As Integer = glCInt(Page.Request.QueryString("SalesOrderContractID"))

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = Str(lSalesOrderContractID)

            Me.sOnLoad = "mOnLoad();"



            '---------- load xml data -------------
            If lSalesOrderContractID > 0 Then
                Dim rs As Recordset
                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdSalesOrderContracts", "ContractID=" & lSalesOrderContractID.gs2Sql)
                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If
                rs.Close()
            End If
            '--------------------------------------




            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            osHTML.bAppend("<nobr>")
            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAddressID))

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition", _
                                                   "tsTabModules", _
                                                   "ParentModule='AddressDetails' AND ModuleName<>'Addresses_SalesOrderContracts'", _
                                                   , "ShowIndex", _
                                                   wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAddressID))

                frs.MoveNext()
            Loop
            osHTML.bAppend("</nobr>")
            Me.lblNaviSub.sTranslatedText = osHTML.sString


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
