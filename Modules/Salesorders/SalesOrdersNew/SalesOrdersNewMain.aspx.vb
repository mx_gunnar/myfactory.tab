'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    SalesOrdersNewMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for SalesOrdersMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersNewMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.sOnLoad = "mOnLoad()"

            ReDim Me.asTerms(1)
            Me.asTerms(0) = "Bitte w�hlen Sie einen Kunden aus."

            Me.dlgMain.oDialog.oField("OrderType").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_SalesOrderNew_OrderType", False)

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
