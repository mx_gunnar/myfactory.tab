﻿// SalesOrdersNewMain.js (c) myfactory 2011

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    sData = document.all.txtSearch.value;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty()
{
    if (event.type == 'change')
        mRefreshList();
}

function mOnOK()
{
    var lCustomerID = gsListViewGetSelection('lstMain');

    if (lCustomerID == '')
    {
        alert(msTerms[0]);
        return;
    }

    sRes = gsCallServerMethod('SalesOrdersNewProcess.aspx?Cmd=CreateOrder' +
                        '&OrderType=' + document.all.cboOrderType.value +
                        '&CustomerID=' + lCustomerID);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    var lOrderID = sRes.substr(3);

    sURL = '../SalesOrdersEdit/SalesOrdersEditMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + lOrderID;
    document.location = sURL;

}

