'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    SalesOrdersNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for SalesOrdersEditProcess
'--------------------------------------------------------------------------------
' Created:      2/28/2011 2:45:57 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.BusinessTasks.Sales.Orders.PublicEnums

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class SalesOrdersNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        Private _msDeletePos As String


        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "CreateOrder"
                    Return msCreateOrder(oClientInfo, glCInt(Request.QueryString("OrderType")), _
                                         glCInt(Request.QueryString("CustomerID")))

                Case Else
                    Return "Unknown Cmd:" & sCmd
            End Select

        End Function

        ''' <summary>
        ''' Create New Order
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <param name="lOrderType"></param>
        ''' <param name="lCustomerID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msCreateOrder(ByVal oClientInfo As ClientInfo, ByVal lOrderType As Integer, ByVal lCustomerID As Integer) As String

            Dim lOrderID As Integer
            Dim oOrder As New SalesOrder

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_SalesOrderNew_OrderType", lOrderType, False)

            If Not oOrder.gbNew(oClientInfo) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Erstellen des Belegs")
            End If

            If Not oOrder.gbInitOrderType(oClientInfo, lOrderType) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen der Belegart")
            End If

            If Not SalesOrderMethods.gbSetCustomer(oClientInfo, oOrder, lCustomerID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Setzen des Kunden")
            End If

            lOrderID = SalesOrderMethodsT.glSaveSalesOrder(oClientInfo, oOrder, False)
            If lOrderID = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Speichern des Belegs") & " " & oOrder.oMember.sProp("Message")
            End If

            ClientValues.gbSetClientValue(oClientInfo, "Tab_SalesOrders_CurrentOrderID", lOrderID)



            Return "OK;" & lOrderID

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
