<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.SalesOrders.SalesOrdersNewMain" codefile="SalesOrdersNewMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable cellSpacing=2 cellPadding=2 width="400px" style="table-layout:fixed">
<tr height="60px">
	<td width="400px">
		<myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/SalesOrders/SalesOrdersNew/dlgSalesOrdersNewMain.xml"></myfactory:wfXmlDialog>
	</td>
</tr>
<tr height="450px" valign="top">
	<td width="400px" style="height:450px">
		<myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="CustomerListData.aspx"></myfactory:wfListView>
	</td>
</tr>
<tr height="30px">
	<td width="400px" align="right">
        <myfactory:wfButton runat="server" ID="cmdOK" sOnClick="mOnOK()" sText="OK"></myfactory:wfButton>
	</td>
</tr>
</table>

</body>
</HTML>
