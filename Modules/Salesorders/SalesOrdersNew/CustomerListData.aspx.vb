'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Holding GmbH
' Component:    CustomersListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for CustomersListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.SalesOrders

    Partial Class CustomerListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "CustomerListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("CustomerNumber", "Kundenr.", "80", , , True)
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True)

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            Dim sSearch As String = ""

            sSearch = oListView.sUserData

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "Matchcode"
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            sClause = "tdCustomers.InActive=0"
            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "CustomerID AS RowID, CustomerNumber, Matchcode"
            sTables = "tdAddresses" & _
                        " INNER JOIN tdCustomers ON tdAddresses.AddressID=tdCustomers.AddressID "

            '---- clause ---------

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "(Matchcode like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name1 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR Name2 like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            " OR City like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                            ")")
            End If

            '---- permissions ----
            Dim sSubClause As String

            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Addresses")
            If sSubClause <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If

            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers")
            If sSubClause <> "" Then
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)


            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
