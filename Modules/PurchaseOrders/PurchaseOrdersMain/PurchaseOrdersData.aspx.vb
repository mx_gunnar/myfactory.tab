'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    PurchaseOrdersData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for PurchaseOrdersData
'--------------------------------------------------------------------------------
' Created:      17.09.2013 14:17:32, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.PurchaseOrders

    Partial Class PurchaseOrdersData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim bWWS As Boolean = Editions.gbCheckUserEditions(oClientInfo, "WWS")

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "PurchaseOrdersData.aspx"
            oListView.lPageSize = 8
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oListView.oCols.oAddCol("po.OrderDate", "Datum", "80", wfEnumDataTypes.wfDataTypeDate, , True)
            oListView.oCols.oAddCol("sot.TypeDesc", "Belegart", "100")
            oListView.oCols.oAddCol("po.OrderNumber", "Belegnr", "80")
            oListView.oCols.oAddCol("po.Matchcode", "Kurzbezeichnung", "*")
            oListView.oCols.oAddCol("soa.SAName1", "Lieferanschrift", "200")
            oListView.oCols.oAddCol("OrderSumNet", "Nettobetrag", "80", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oListView.oCols.oAddCol("po.CurrUnit", "Wkz", "40")

            oCol = oListView.oCols.oAddCol("CmdOrder", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            If Not bWWS OrElse Not Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_PurchaseOrderEdit") Then
                oCol.bHidden = True
            End If

            '---- Data ---------------------
            Dim sSubClause As String
            Dim asData As String()
            Dim lOrderType As Integer
            Dim dtDateFrom, dtDateTo As Date
            Dim lDivisionNr As Integer
            Dim sSearch As String = ""
            Dim lPriojectID As Integer = 0

            asData = Split(oListView.sUserData & ";;;;;;", ";")
            If asData.Length > 6 Then
                dtDateFrom = gdtCDate(asData(0))
                dtDateTo = gdtCDate(asData(1))
                lOrderType = glCInt(asData(2))
                If Not gbCBool(asData(3)) Then lDivisionNr = oClientInfo.oClientProperties.lDivisionNr
                sSearch = asData(4)
                lPriojectID = glCInt(asData(5))
            End If

            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "po.OrderDate"
                oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            If sOrder = "po.OrderDate" Then
                sOrder &= ", po.OrderID"
            ElseIf sOrder = "po.OrderDate DESC" Then
                sOrder &= ", po.OrderID DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "po.OrderID AS RowID, po.OrderDate, sot.TypeDesc, po.OrderNumber, po.Matchcode, soa.SAName1, (po.OrderSumGross-po.TaxSum) AS OrderSumNet, po.CurrUnit"
            sTables = "tdPurchaseOrders po" & _
                        " INNER JOIN tdPurchaseOrderTypes sot ON po.OrderType = sot.OrderType" & _
                        " LEFT JOIN tdPurchaseOrderAddresses soa ON po.OrderID = soa.OrderID" & _
                        " INNER JOIN tdSuppliers s ON po.SupplierID=s.SupplierID " & _
                        " INNER JOIN tdAddresses a ON po.AddressID=a.AddressID"

            '---- clause ---------
            If gbDate(dtDateFrom) Then sClause = DataTools.gsClauseAnd(sClause, "po.OrderDate>=" & DataTools.gsDate2Sql(dtDateFrom))
            If gbDate(dtDateTo) Then sClause = DataTools.gsClauseAnd(sClause, "po.OrderDate<" & DataTools.gsDate2Sql(dtDateTo.AddDays(1)))
            If lOrderType <> 0 Then sClause = DataTools.gsClauseAnd(sClause, "po.OrderType=" & lOrderType)
            If lDivisionNr <> 0 Then sClause = DataTools.gsClauseAnd(sClause, "po.DivisionNr=" & lDivisionNr)
            If lPriojectID <> 0 Then sClause = sClause.gsClauseAnd("po.ProjectID=" & lPriojectID)

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "(po.Matchcode like " & DataTools.gsStr2Sql(sSearch & "%") & _
                                                        " OR soa.SAName1 like " & DataTools.gsStr2Sql(sSearch & "%") & "  )")
            End If


            '---- permissions ----
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "PurchaseOrders")
            If sSubClause <> "" Then
                sSubClause = Replace(sSubClause, "tdPurchaseOrders.", "po.")
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Suppliers")
            If sSubClause <> "" Then
                sSubClause = Replace(sSubClause, "tdSuppliers.", "s.")
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If
            sSubClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Addresses")
            If sSubClause <> "" Then
                sSubClause = Replace(sSubClause, "tdAddresses.", "a.")
                sClause = DataTools.gsClauseAnd(sClause, sSubClause)
            End If


            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)
                rs.MoveNext()
            Loop
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
