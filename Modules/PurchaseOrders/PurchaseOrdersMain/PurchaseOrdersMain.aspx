<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.PurchaseOrders.PurchaseOrdersMain" codefile="PurchaseOrdersMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
      <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
      <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" width="98%"
        style="table-layout: fixed;">
        <tr valign="top" height="35px">
            <td width="700px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/PurchaseOrders/PurchaseOrdersMain/dlgPurchaseOrders.xml" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="top" height="*">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="PurchaseOrdersData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr id="trPos" valign="top" height="250px">
            <td colspan="2"  >
                <myfactory:wfListView runat="server" ID="lstPos" sListViewDataPage="PurchaseOrderPosData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>        <tr height="130px" valign="bottom" id="trNavi">
            <td align="right" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>

    </table>
</body>
</html>
