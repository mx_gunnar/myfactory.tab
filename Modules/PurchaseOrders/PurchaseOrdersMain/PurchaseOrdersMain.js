﻿




function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    sData = document.all.txtDateFrom.value +
                ';' + document.all.txtDateTo.value +
                ';' + document.all.cboOrderType.value +
                ';' + document.all.chkAllDivisions.checked +
                ';' + document.all.txtSearch.value;

    sData = sData + ';' + msPageParams[0];

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);

}



function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sListView == 'lstMain')
        mRefreshPosList(sItemID);
}

function mOnListViewClick(sListView, sItemID)
{
    if (sListView == 'lstMain')
        mRefreshPosList(sItemID);
}


function mOnSetDirty()
{
    if (event.type == 'change' || (event.type == 'click' && event.srcElement.id == 'chkAllDivisions'))
        mRefreshList();
}


function mRefreshPosList(sData)
{
    if (sData == '-1')
        sData = gsListViewGetSelection('lstMain');

    gListViewSetUserData('lstPos', sData);
    gListViewLoadPage('lstPos', 1);
}
