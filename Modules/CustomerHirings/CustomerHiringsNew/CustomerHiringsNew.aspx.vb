'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringsNew.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for CustomerHiringsNew
'--------------------------------------------------------------------------------
' Created:      15.11.2012 11:20:06, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem.AspMobileControls

Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.CustomerHirings

    Partial Class CustomerHiringsNew
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad
            Dim lEntryID As Integer = glCInt(Request.QueryString("EntryID"))

            ReDim Me.asPageParams(2)
            Me.gAddScriptLink("wfDlgParams.js", True)

            '------------ laod data ----------
            If lEntryID <> 0 Then
                Dim sFields As String
                Dim rs As Recordset
                Dim sClause As String = "EntryID=" & lEntryID.gs2Sql()
                Dim sTables As String
                Dim sValues As String

                sFields = "'' as CustomerName, *"
                sTables = "tdHiringTimes"

                rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause)

                rs("StartTime").Value = gsGetTimeFromDate(rs("StartTime").dtValue)
                rs("EndTime").Value = gsGetTimeFromDate(rs("EndTime").dtValue)

                If rs("RecordID").lValue > 0 Then

                    rs("CustomerName").Value = DataMethods.gsGetDBValue(oClientInfo, "C.CustomerNumber + ' ' + A.Matchcode", _
                                                                        "tdCustomers C INNER JOIN tdAddresses A ON C.AddressID=A.AddressID",
                                                                        "C.CustomerID=" & rs("RecordID").lValue)
                End If

                sValues = DataTools.gsRecord2Xml(rs)

                Me.asPageParams(0) = lEntryID.ToString
                Me.asPageParams(1) = rs("ActivityID").sValue
                Me.dlgMain.sValues = sValues

                msSetAddOnInfoLabel(oClientInfo, lEntryID)
            End If
            '---------------------------------


            Me.sOnLoad = "mOnLoad()"
        End Sub


        Private Sub msSetAddOnInfoLabel(ByVal oClientInfo As ClientInfo, ByVal lEntryID As Integer)

            Dim sFields As String
            Dim sTables As String
            Dim sClause As String
            Dim lCount As Integer = 0
            Dim cSum As Decimal = 0
            Dim lCustomerID As Integer
            Dim rs As Recordset


            lCustomerID = DataMethods.glGetDBValue(oClientInfo, "RecordID", "tdHiringTimes", "EntryID=" & lEntryID.gs2Sql)


            sClause = "HiringTimeEntryID=" & lEntryID.gs2Sql

            sFields = "EntryID AS RowID, " & _
                "HA.AddOnDesc, " & _
                "'' as Price, " & _
                "HTA.Amount, " & _
                "P.BaseUnit, " & _
                "'' AS Total, " & _
                "'' AS WKZ, " & _
                "'x' as CmdDelete, " & _
                "HTA.PriceNew as hiddenPriceNew, " & _
                "HTA.AddOnID as hiddenAddOnID"

            sTables = "tdHiringTimeAddOns HTA INNER JOIN tdHiringAddOns HA ON HA.AddOnID=HTA.AddOnID INNER JOIN tdProducts P ON P.ProductID=HA.ProductID "

            rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause)

            Do Until rs.EOF
                lCount = lCount + 1

                Dim cPrice As Decimal

                If rs("hiddenPriceNew").sValue.Trim = "" Then
                    cPrice = msGetStandardPrice(oClientInfo, lCustomerID, rs("hiddenAddOnID").lValue)
                Else
                    cPrice = rs("hiddenPriceNew").cValue
                End If

                cSum = cSum + (cPrice * rs("Amount").cValue)

                rs.MoveNext()
            Loop


            Me.lblAddOnInfo.sText = lCount.ToString() & " " & Dictionary.gsTranslate(oClientInfo, "Zulagen") & " " & Dictionary.gsTranslate(oClientInfo, "f�r") & " " & gsFormatNumber(cSum, 2) & " " & Currencies.gsCurrUnitInternal(oClientInfo, -1)

            rs.Close()

        End Sub

        Private Function msGetStandardPrice(ByVal oClientInfo As ClientInfo, ByVal lCustomerID As Integer, ByVal lAddOnID As Integer) As Decimal
            Dim cPrice As Decimal = 0D

            Dim lProductID As Integer = DataMethods.glGetDBValue(oClientInfo, "ProductID", "tdHiringAddOns", "AddONID=" & lAddOnID.gs2Sql)

            cPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                    lCustomerID, _
                    lProductID, _
                    1)


            Return cPrice
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
