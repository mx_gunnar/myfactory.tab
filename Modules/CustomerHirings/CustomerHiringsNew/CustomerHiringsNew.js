﻿

var mOiginalValues;


function mOnLoad()
{
    mRefreshActivitys();

    // entry id not empty?
    if (msPageParams[0] != '')
    {
        // set activity id
        if (msPageParams[1] != '')
        {
            gSetCboValue(document.getElementById("cboActivityID"), msPageParams[1]);
        }

        // hold original values
        mOiginalValues = gsXMLDlgParams(frmMain, '', false);
    }
}

function cmdSearchButton_OnClick()
{
    // start customer search
    
    gListViewSetUserData('lstSearch', document.all.txtSearch.value);
    gListViewLoadPage('lstSearch', -1);
}

function cmdSetCustomer_OnClick()
{
    var CustomerID = gsListViewGetSelection('lstSearch');

    var sCustomerText = gsListViewGetItemUserData('lstSearch', CustomerID);
    
    document.all.txtRecordID.value = CustomerID;
    document.all.txtCustomerName.value = sCustomerText;
}

function mOnSetDirty()
{
    if (event.srcElement.id == 'cboTimeTypeID')
    {
        mRefreshActivitys();
    }
}


function mRefreshActivitys()
{
    var TimeTypeValue = document.all.cboTimeTypeID.value;

    gRefreshDataCombo('cboActivityID', 'ProjectActivitiesActive', TimeTypeValue, false, '');
}

function mOnOK()
{
    var sURL = 'CustomerHiringsNewProcess.aspx';
    var sRes;
    var sNewValues;

    if (msPageParams[0] != '')
    {
        sURL = sURL + "?Cmd=Change&&EntryID=" + msPageParams[0];

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, mOiginalValues + ';' + sNewValues);

    }
    else
    {
        sURL = sURL + "?Cmd=New";

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, sNewValues);
    }

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }
    else
    {
        mNavigateToList();
    }
}


function mNavigateToList()
{
    var sURL = '../CustomerHiringsMain/CustomerHiringsMain.aspx?ClientID=' + msClientID;

    document.location = sURL;
}


function mOnAddOnClick() 
{
    var sURLAddOn; 
    var sURL = 'CustomerHiringsNewProcess.aspx';
    var sRes;
    var sNewValues;

    if (msPageParams[0] == '' || msPageParams[0] == '0') 
    {
        // do save
        sURL = sURL + "?Cmd=New";

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, sNewValues);
    }
    else 
    {
        sURL = sURL + "?Cmd=Change&EntryID=" + msPageParams[0];

        sNewValues = gsXMLDlgParams(frmMain, '', false);
        sRes = gsCallServerMethod(sURL, mOiginalValues + ';' + sNewValues);
    }


    if (sRes.substr(0, 3) != 'OK;') 
    {
        alert(sRes);
        return;
    }

    sURLAddOn = '../CustomerHiringsAddOn/CustomerHiringAddOnMain.aspx?ClientID=' + msClientID + '&EntryID=' + sRes.split(';')[1];
    document.location = sURLAddOn;

}