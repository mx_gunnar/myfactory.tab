<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.CustomerHirings.CustomerHiringsNew" CodeFile="CustomerHiringsNew.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="100%" height="97%">
        <tr valign="top" height="400px">
            <td width="50%">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <form id="frmMain" name="frmMain">
                            <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/CustomerHirings/CustomerHiringsNew/dlgCustomerHiringsNew.xml" />
                            </form>
                        </td>
                        
                    </tr>
                    <tr>
                        <td width="180px">
                            <myfactory:wfButton ID="cmdAddOns" runat="server" sStyle="Width: 150px;" sText="Zulagen" sOnClick="mOnAddOnClick();"></myfactory:wfButton>
                        </td>
                        <td align="left">
                            <myfactory:wfLabel ID="lblAddOnInfo" runat="server" sStyle="color:White;text-shadow: black 0.1em 0.1em 0.15em;font-size:12pt;" sText=""></myfactory:wfLabel>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <myfactory:wfXmlDialog runat="server" ID="dlgSearch" sDialog="/tab/modules/CustomerHirings/CustomerHiringsNew/dlgCustomerHiringsNewSearch.xml" />
            </td>
        </tr>
        <tr height="*" valign="bottom">
            <td align="right" colspan="2">
                <table>
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdOK" sOnClick="mOnOK()" runat="server" sText=" OK " />
                        </td>
                        <td>
                            <myfactory:wfButton ID="cmdCancel" sOnClick="mNavigateToList()" runat="server" sText="Abbrechen" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
