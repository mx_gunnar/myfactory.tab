'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringsNewProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for CustomerHiringsNewProcess
'--------------------------------------------------------------------------------
' Created:      15.11.2012 11:20:28, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Xml
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.CustomerHirings

    Partial Class CustomerHiringsNewProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Me.Request.QueryString("Cmd")
            Dim lEntryID As Integer = glCInt(Me.Request.QueryString("EntryID"))
            Dim lProjectID As Integer = glCInt(Me.Request.QueryString("ProjectID"))

            Select Case sCmd
                Case "New"
                    Return msSaveNewEntry(oClientInfo)
                Case "Change"
                    Return msChangeEntry(oClientInfo, lEntryID)
                Case "Delete"
                    Return msDeleteEntry(oClientInfo, lEntryID)
            End Select

            Return "ERR; unknown command"

        End Function

        Private Function msSaveNewEntry(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '------------------------------------



            '-------- correct time values? ----------
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Startzeit ung�ltig")
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Endzeit ung�ltig")

            Dim dtDateStart As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime"))
            Dim dtDateend As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime"))

            If dtDateend <= dtDateStart Then
                Return Dictionary.gsTranslate(oClientInfo, "Die Endzeit muss gr��er als die Startzeit sein")
            End If
            '-----------------------------------



            '------- get additional information for calculation -----------------
            Dim lTimeTypeId As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboTimeTypeID"))

            Dim lCustomerID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtRecordID"))

            If lCustomerID = 0 Then Return Dictionary.gsTranslate(oClientInfo, "Sie m�ssen einen Kunden angeben")

            Dim tsAmount As TimeSpan = New TimeSpan(dtDateend.Ticks - dtDateStart.Ticks)
            '-------------------------------------------------------------------



            '-------------- build insert statement -----------------
            Dim sFields As String = ""
            Dim sValues As String = ""
            Dim sMsg As String = ""
            Dim lNewEntryID As Integer

            If Not TableDefFunctions.gbBuildInsertExpression(oClientInfo, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\hrm\hiring\tableHiringTimes.xml", _
                                                             sFields, sValues, sMsg, True) Then
                Return sMsg
            End If

            sFields = sFields & ", EntryID, Amount, UserInitials, ResourceID, IsRunning,  IsDayToDay, EntryType, EmployeeID, HiringID, EntityID"
            lNewEntryID = RecordID.glGetNextRecordID(oClientInfo, "tdHiringTimes")
            sValues = sValues & ", " & lNewEntryID
            sValues = sValues & ", " & msGetFormatedAmount(oClientInfo, lTimeTypeId, tsAmount)
            sValues = sValues & ", " & oClientInfo.oClientProperties.sCurrentUser.gs2Sql
            sValues = sValues & ", " & DataMethods.glGetDBValue(oClientInfo, "ResourceID", "tdResources", "UserInitials=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql).gs2Sql
            sValues = sValues & ", 2, 0, 1, 0, 0, 1100"


            If DataMethods2.glDBExecute(oClientInfo, "INSERT INTO tdHiringTimes (" & sFields & ") VALUES(" & sValues & ")", myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceData) = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim Speichern")
            End If

            Return "OK;" & lNewEntryID
        End Function


        Private Function msChangeEntry(ByVal oClientInfo As ClientInfo, ByVal lEntryID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '-----------------------------------



            '-------- correct time values? ----------
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Startzeit ung�ltig")
            If Not gbTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime")) Then Return Dictionary.gsTranslate(oClientInfo, "Endzeit ung�ltig")

            Dim dtDateStart As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtStartTime"))
            Dim dtDateend As DateTime = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEndTime"))

            If dtDateend <= dtDateStart Then
                Return Dictionary.gsTranslate(oClientInfo, "Die Endzeit muss gr��er als die Startzeit sein")
            End If
            '----------------------------------------



            '--------- get additional informations for validation and calculation --------
            Dim lTimeTypeId As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboTimeTypeID"))

            Dim tsAmount As TimeSpan = New TimeSpan(dtDateend.Ticks - dtDateStart.Ticks)
            '------------------------------------------------------------------------------



            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\hrm\hiring\tableHiringTimes.xml", sQry, sMsg, True) Then
                Return sMsg
            End If

            'calculate new amount in correct unit!
            If sQry <> "" Then
                Dim sAmount = msGetFormatedAmount(oClientInfo, lTimeTypeId, tsAmount)
                sQry = sQry & ",Amount=" & sAmount.gs2Sql
            End If

            If sQry = "" Then
                Return "OK;" & lEntryID
            End If

            If DataMethods2.glDBUpdate(oClientInfo, "tdHiringTimes", sQry, "EntryID=" & lEntryID) = 0 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim Speichern")
            End If

            Return "OK;" & lEntryID
        End Function


        Private Function msDeleteEntry(ByVal oClientInfo As ClientInfo, ByVal lEntryID As Integer) As String

            DataMethods2.glDBDelete(oClientInfo, "tdHiringTimeAddOns", "HiringTimeEntryID=" & lEntryID)

            If DataMethods2.glDBDelete(oClientInfo, "tdHiringTimes", "EntryID=" & lEntryID) <> 1 Then
                Return Dictionary.gsTranslate(oClientInfo, "ERR; Fehler beim L�schen")
            End If

            Return "OK; delete"
        End Function


        Private Function msGetFormatedAmount(ByVal oClientInfo As ClientInfo, ByVal lTimeTypeID As Integer, ByVal timeSpanAmount As TimeSpan) As String

            Dim cAmount = timeSpanAmount.TotalHours
            Dim lFactor = DataMethods.glGetDBValue(oClientInfo, "UnitFactor", "tdHiringTimeTypes HTT INNER JOIN tdTimeUnits TU ON HTT.Unit=TU.UnitName", "HTT.TimeTypeID=" & lTimeTypeID)

            If lFactor > 0 Then
                cAmount = timeSpanAmount.TotalSeconds / lFactor
            End If

            Return gsFormatNumber(cAmount, 2).Replace(",", ".")
        End Function
    End Class

End Namespace

'================================================================================
'================================================================================
