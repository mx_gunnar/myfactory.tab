'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringsNewSearch.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for CustomerHiringsNewSearch
'--------------------------------------------------------------------------------
' Created:      15.11.2012 12:37:17, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.CustomerHirings

    Partial Class CustomerHiringsNewSearch
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sSearch As String = oListView.sUserData

            '---- ListView Properties ------
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10
            oListView.bPageNavigation = True
            oListView.bAutoHideNavigation = True
            oListView.sListViewDataPage = "CustomerHiringsNewSearch.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("CustomerNumber", "Kundennr", "70", , , False, False)
            oListView.oCols.oAddCol("Matchcode", "Matchcode", "*", , , False, False)

            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "C.CustomerID AS RowID,C.CustomerNumber,A.Matchcode"
            sTables = "tdCustomers C " & _
                 " INNER JOIN tdAddresses A ON A.AddressID=C.AddressID"

            If Not String.IsNullOrEmpty(sSearch) Then
                sClause = sClause.gsClauseAnd("A.Matchcode like '%" & sSearch & "%'")
            End If

            '--- permissions ----
            Dim sPermClause As String
            sPermClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers")
            If sPermClause <> "" Then
                sPermClause = Replace(sPermClause, "tdCustomers.", "c.", False)
                sClause = sClause.gsClauseAnd(sPermClause)
            End If

            sPermClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Addresses")
            If sPermClause <> "" Then
                sPermClause = Replace(sPermClause, "tdAddresses.", "a.", False)
                sClause = sClause.gsClauseAnd(sPermClause)
            End If
            '----------------------

            sClause = sClause.gsClauseAnd("C.InActive=0")

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF

                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)
                oRow.sUserData = rs(1).sValue & " " & rs(2).sValue

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
