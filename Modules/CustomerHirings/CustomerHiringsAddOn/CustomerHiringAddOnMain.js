﻿

function mOnLoad() {

    mRefreshListView();
}

function mRefreshListView() 
{
    gListViewSetUserData('lstAddOns', msPageParams[0]);
    gListViewLoadPage('lstAddOns', 1);
}

function mOnAddAddOn() {
    // navigate to AddOnDetail (with ID=0)
    
    mNavigateTotDetails(0);
}

function mOnCancel() {
    // navigate back

    var sURL = '../CustomerHiringsNew/CustomerHiringsNew.aspx?ClientID=' + msClientID;
    sURL = sURL + '&EntryID=' + msPageParams[0];

    document.location = sURL;
}

function mNavigateTotDetails(sAddOnEntryID) 
{
    var sURL = '../CustomerHiringsAddOnDetail/CustomerHiringAddOnDetail.aspx?ClientID=' + msClientID;
    sURL = sURL + '&AddOnEntryID=' + sAddOnEntryID + '&HiringTimeEntryID=' + msPageParams[0];

    document.location = sURL;
}

function mOnListViewClick(sListView, sItemID) 
{
    mNavigateTotDetails(sItemID);
}

function mOnListViewBtnClick(sListView, sColID, sItemID) 
{
    if (sColID == 'CmdDel') 
    {
        var sURL = '../CustomerHiringsAddOnDetail/CustomerHiringAddOnDetailProcess.aspx?Cmd=Delete&AddOnEntryID=' + sItemID;
        var sRes = gsCallServerMethod(sURL, '');

        if (sRes.substr(0, 3) != 'OK;') 
        {
            alert(sRes.replace('ERR;', ''));
        }
        else
        {
            mRefreshListView();
        }
    }
 }