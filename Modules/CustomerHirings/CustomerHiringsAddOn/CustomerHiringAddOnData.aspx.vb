'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringAddOnData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for CustomerHiringAddOnData
'--------------------------------------------------------------------------------
' Created:      11.02.2013 16:07:11, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'===============================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================


Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Tools.DataTypeFunctions


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.CustomerHirings

    Partial Class CustomerHiringAddOnData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol
            Dim lCustomerHiringEntryID As Integer = glCInt(oListView.sUserData)
            Dim sCurrUnit As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            Dim lCustomerID As Integer

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.lPageSize = 15
            oListView.sListViewDataPage = "CustomerHiringAddOnData.aspx"


            '---- Columns ------------------
            oListView.oCols.oAddCol("AddOnDesc", "Zulage", "*", , , True, False)
            oListView.oCols.oAddCol("Price", "Preis", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("Amount", "Menge", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("BaseUnit", "ME", "50", , , True, False)
            oListView.oCols.oAddCol("Total", "Gesamt", "150", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("WKZ", "WKZ", "50", , , True, False)

            'delete button
            oCol = oListView.oCols.oAddCol("CmdDel", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDelete)
            oCol.sWidth = "50"



            '---- Data ---------------------
            lCustomerID = DataMethods.glGetDBValue(oClientInfo, "RecordID", "tdHiringTimes", "EntryID=" & lCustomerHiringEntryID.gs2Sql)



            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            If lCustomerHiringEntryID > 0 Then
                sClause = sClause.gsClauseAnd("HiringTimeEntryID=" & lCustomerHiringEntryID.gs2Sql)
            End If


            sFields = "EntryID AS RowID, " & _
                "HA.AddOnDesc, " & _
                "'' as Price, " & _
                "HTA.Amount, " & _
                "P.BaseUnit, " & _
                "'' AS Total, " & _
                "'' AS WKZ, " & _
                "'x' as CmdDelete, " & _
                "HTA.PriceNew as hiddenPriceNew, " & _
                "HTA.AddOnID as hiddenAddOnID"

            sTables = "tdHiringTimeAddOns HTA INNER JOIN tdHiringAddOns HA ON HA.AddOnID=HTA.AddOnID INNER JOIN tdProducts P ON P.ProductID=HA.ProductID "

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, , sOrder)

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                Dim cPrice As Decimal

                If rs("hiddenPriceNew").sValue.Trim = "" Then
                    cPrice = msGetStandardPrice(oClientInfo, lCustomerID, rs("hiddenAddOnID").lValue)
                Else
                    cPrice = rs("hiddenPriceNew").cValue
                End If

                oRow.Value(1) = cPrice
                oRow.Value(4) = cPrice * rs("Amount").cValue
                oRow.Value(5) = sCurrUnit

                rs.MoveNext()
            Loop

            rs.Close()
            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function



        Private Function msGetStandardPrice(ByVal oClientInfo As ClientInfo, ByVal lCustomerID As Integer, ByVal lAddOnID As Integer) As Decimal
            Dim cPrice As Decimal = 0D

            Dim lProductID As Integer = DataMethods.glGetDBValue(oClientInfo, "ProductID", "tdHiringAddOns", "AddONID=" & lAddOnID.gs2Sql)

            cPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                    lCustomerID, _
                    lProductID, _
                    1)


            Return cPrice
        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
