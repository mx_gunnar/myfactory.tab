<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.CustomerHirings.CustomerHiringAddOnMain" CodeFile="CustomerHiringAddOnMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="98%" height="97%">
        <tr>
            <td>
                <myfactory:wfListView ID="lstAddOns" runat="server" sListViewDataPage="CustomerHiringAddOnData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr valign="bottom" height="50px;">
            <td align=right>
                <table width="250px" >
                    <tr>
                        <td>
                                        <myfactory:wfButton ID="cmdAdd" sStyle="Width:150px;" sOnClick="mOnAddAddOn();" runat="server" sText="Neue Zulage">
                </myfactory:wfButton>
                        </td>
                        <td>
                                        <myfactory:wfButton ID="cmdCancel" sStyle="Width:100px;" sOnClick="mOnCancel();" runat="server" sText="Zur�ck">
                </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
