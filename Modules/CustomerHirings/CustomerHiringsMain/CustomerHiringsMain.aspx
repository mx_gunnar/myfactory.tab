<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.CustomerHirings.CustomerHiringsMain" codefile="CustomerHiringsMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>


<html>
  <HEAD>
     <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>
<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="WfPageHeader1" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" style="table-layout:fixed;" width="99%" height="97%">
        <tr height="50px" valign="top">
            <td width="700px">
                <myfactory:wfListView ID="lstMain" runat="server" sListViewDataPage="CustomerHiringsData.aspx" />
            </td>
        </tr>
    </table>
</body>
</html>
