﻿

function mOnLoad()
{
    mRefreshLstMain();
}

function mRefreshLstMain()
{
    gListViewLoadPage('lstMain', -1);
}


function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == 'CmdDet')
    {
        mNavigateToDetails(sItemID);
    }
    else if (sColID == 'CmdDel')
    {
        mOnDelete(sItemID);
    }
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mOnDelete(sItemID) {

    if (window.confirm(msTerms[0]) == false)
    {
        return;
    }

    var sURL = '../CustomerHiringsNew/CustomerHiringsNewProcess.aspx';
    var sRes;

    sURL = sURL + "?Cmd=Delete&EntryID=" + sItemID;
    sRes = gsCallServerMethod(sURL, '');

    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
    }
    else
    {
        mRefreshLstMain();
    }
}

function mNavigateToDetails(sEntryID)
{

    var sURL = '../CustomerHiringsNew/CustomerHiringsNew.aspx' +
                '?ClientID=' + msClientID +
                '&EntryID=' + sEntryID;

    document.location = sURL;
}
