<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Base.CustomerHiringAddOnDetail" CodeFile="CustomerHiringAddOnDetail.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="100%" height="97%">
        <tr valign="top" height="175px">
            <td>
                <form id="frmMain" name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/CustomerHirings/CustomerHiringsAddOnDetail/dlgCustomerHiringAddOnDetail.xml" />
                </form>
            </td>
        </tr>
        <tr valign="bottom">
            <td align="right">
                <table width="220px">
                    <tr>
                        <td>
                            <myfactory:wfButton ID="cmdSave" runat="server" sOnClick="mOnOK();" sText="Speichern">
                            </myfactory:wfButton>
                        </td>
                        <td>
                            <myfactory:wfButton ID="cmdCancel" runat="server" sOnClick="mNavigateBack();" sText="Abbrechen">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
