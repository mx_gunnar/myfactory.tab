﻿var mOiginalValues;


function mOnLoad() 
{

    if (msPageParams[0] != '0') 
    {
        mOiginalValues = gsXMLDlgParams(frmMain, '', false);
    }


    mRefreshStandardPrice();
}


function mRefreshStandardPrice() {

    var sPrice = '';

    var AddOnEntryID = document.all.cboAddOnID.value;
    var sURL = 'CustomerHiringAddOnDetailProcess.aspx?Cmd=StandardPrice&HiringTimeEntryID=' + msPageParams[1] + '&AddOnID=' + AddOnEntryID;
    sPrice = gsCallServerMethod(sURL, '');

    document.all.txtStandardPrice.value = sPrice;
}

function mOnSetDirty() 
{
    if ("cboAddOnID" == event.srcElement.id) {
        mRefreshStandardPrice();
    }
}

function mOnOK() {
    var sURL = 'CustomerHiringAddOnDetailProcess.aspx';
    var sRes;
    var sNewValues;

    if (msPageParams[0] == '0') 
    {
        sURL = sURL + "?Cmd=SaveNew&HiringTimeEntryID=" + msPageParams[1];
        sNewValues = gsXMLDlgParams(frmMain, '', false);

        sRes = gsCallServerMethod(sURL, sNewValues);
    }
    else 
    {
        sURL = sURL + "?Cmd=SaveChange&AddOnEntryID=" + msPageParams[0];
        sNewValues = gsXMLDlgParams(frmMain, '', false);

        sRes = gsCallServerMethod(sURL, mOiginalValues + ';' + sNewValues);
    }

    var bOK = mHandleResult(sRes);

    if (bOK) {
        mNavigateBack();
    }
}


function mNavigateBack() {
    var sURL = '../CustomerHiringsAddOn/CustomerHiringAddOnMain.aspx?ClientID=' + msClientID + '&EntryID=' + msPageParams[1];

    document.location = sURL;
}


function mHandleResult(sRes) {

    if (sRes.substr(0, 3) != 'OK;') {
        alert(sRes.replace('ERR;', ''));
        return false;
    }
    return true;
}
