'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringAddOnDetail.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for CustomerHiringAddOnDetail
'--------------------------------------------------------------------------------
' Created:      11.02.2013 17:32:36, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Base

    Partial Class CustomerHiringAddOnDetail
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddOnEntryID As Integer = glCInt(Page.Request.QueryString("AddOnEntryID"))
            Dim lHiringTimeEntryID As Integer = glCInt(Page.Request.QueryString("HiringTimeEntryID"))

            Me.sOnLoad = "mOnLoad();"
            Me.gAddScriptLink("wfDlgParams.js", True)

            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lAddOnEntryID)
            Me.asPageParams(1) = Str(lHiringTimeEntryID)


            If lAddOnEntryID > 0 Then
                '--------- load data -----------

                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tdHiringTimeAddOns", "EntryID=" & lAddOnEntryID.gs2Sql)

                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If

            End If


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
