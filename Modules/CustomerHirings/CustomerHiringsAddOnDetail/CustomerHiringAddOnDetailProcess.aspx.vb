'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    CustomerHiringAddOnDetailProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for CustomerHiringAddOnDetailProcess
'--------------------------------------------------------------------------------
' Created:      11.02.2013 17:32:52, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Base

    Partial Class CustomerHiringAddOnDetailProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")
            Dim lAddOnID As Integer = glCInt(Request.QueryString("AddOnID"))
            Dim lAddOnEntryID As Integer = glCInt(Request.QueryString("AddOnEntryID"))
            Dim lCustomerHiringEntryID As Integer = glCInt(Request.QueryString("HiringTimeEntryID"))


            Select Case sCmd

                Case "SaveNew"
                    Return msSaveAddOn(oClientInfo, lCustomerHiringEntryID)

                Case "SaveChange"
                    Return msSaveChange(oClientInfo, lAddOnEntryID)

                Case "Delete"
                    Return msDeleteAddOn(oClientInfo, lAddOnEntryID)

                Case "StandardPrice"
                    Return msGetStandardPrice(oClientInfo, DataMethods.glGetDBValue(oClientInfo, "RecordID", "tdHiringTimes", "EntryID=" & lCustomerHiringEntryID.gs2Sql), lAddOnID)

            End Select


            Return "ERR; unknown cmd"

        End Function

        Private Function msGetStandardPrice(ByVal oClientInfo As ClientInfo, ByVal lCustomerID As Integer, ByVal lAddOnID As Integer) As String

            Dim cPrice As Decimal
            Dim sCurrUnit As String = Currencies.gsCurrUnitInternal(oClientInfo, -1)

            Dim lProductID As Integer = DataMethods.glGetDBValue(oClientInfo, "ProductID", "tdHiringAddOns", "AddONID=" & lAddOnID.gs2Sql)

            cPrice = myfactory.BusinessTasks.Sales.Prices.SalesPrices.gcGetProductPrice(oClientInfo, _
                    lCustomerID, _
                    lProductID, _
                    1)

            Return gsFormatNumber(cPrice, 2) & " " & sCurrUnit

        End Function


        Private Function msDeleteAddOn(ByVal oClientInfo As ClientInfo, ByVal lAddOnEntryID As Integer) As String

            If DataMethods2.glDBDelete(oClientInfo, "tdHiringTimeAddOns", "EntryID=" & lAddOnEntryID) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Es wurde kein Datensatz gefunden")
            End If

            Return "OK; deleted"
        End Function



        Private Function msSaveChange(ByVal oClientInfo As ClientInfo, ByVal lAddOnEntryID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootOrig, oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(1)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            oRootOrig = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootOrig Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '-----------------------------------


            '------- get additional information for validation -----------------
            Dim lAddOnID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboAddOnID"))
            If lAddOnID = 0 Then Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Es wurde keine Zulage ausgew�hlt")

            Dim cAmount As Decimal = gcCCur(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtAmount"))
            If cAmount = 0 Then Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Es wurde keine Menge angegeben")

            Dim sNewPrice As String = XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtPriceNew")
            '--------------------------------------------------------------------


            '---------- update db -------------
            Dim sMsg As String = ""
            Dim sQry As String = ""

            If Not TableDefFunctions.gbBuildUpdateExpression(oClientInfo, oRootOrig.xml, oRootNew.xml, _
                                                             oClientInfo.sAppPath & "\ie50\hrm\hiring\tableHiringTimeAddOns.xml", sQry, sMsg, True) Then
                Return sMsg
            End If

            If sQry <> "" Then
                If DataMethods2.glDBUpdate(oClientInfo, "tdHiringTimeAddOns", sQry, "EntryID=" & lAddOnEntryID) = 0 Then
                    Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Fehler! Es wurde kein Datensatz ge�ndert")
                End If

                'delete PriceNew if value is string.empty
                If String.IsNullOrEmpty(sNewPrice.Trim) Then
                    DataMethods2.glDBUpdate(oClientInfo, "tdHiringTimeAddOns", "PriceNew=NULL", "EntryID=" & lAddOnEntryID)
                End If
            End If

            Return "OK; changed"
        End Function


        Private Function msSaveAddOn(ByVal oClientInfo As ClientInfo, ByVal lCustomerHiringEntryID As Integer) As String

            Dim oXml As New XmlDocument
            Dim oRootNew As XmlNode


            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRootNew = oXml.documentElement
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRootNew = oXml.documentElement.selectNodes("//DlgParams")(0)
            If oRootNew Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")
            '------------------------------------



            '------- get additional information for validation -----------------
            Dim lAddOnID As Integer = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "cboAddOnID"))
            If lAddOnID = 0 Then Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Es wurde keine Zulage ausgew�hlt")

            Dim cAmount As Decimal = gcCCur(XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtAmount"))
            If cAmount = 0 Then Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Es wurde keine Menge angegeben")

            Dim sPriceNew As String = XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtPriceNew")
            Dim sEntryDesc As String = XmlFunctions.gsGetXMLSubNodeText(oRootNew, "txtEntryDesc")
            '--------------------------------------------------------------------



            '---------------- insert statement --------------------------------
            Dim sQry As String
            sQry = "INSERT INTO tdHiringTimeAddOns (EntryID, HiringTimeEntryID, AddOnID, Amount, EntryDesc, PriceNew) VALUES (" & _
                RecordID.glGetNextRecordID(oClientInfo, "tdHiringTimeAddOns") & ", " & _
                lCustomerHiringEntryID & ", " & _
                lAddOnID & ", " & _
                cAmount.gs2Sql & ", " & _
                sEntryDesc.gs2Sql & ", "

            If Not String.IsNullOrEmpty(sPriceNew) Then
                sQry = sQry & gcCCur(sPriceNew).gs2Sql
            Else
                sQry = sQry & "NULL"
            End If

            sQry = sQry & ")"

            If DataMethods2.glDBExecute(oClientInfo, sQry) = 0 Then
                Return "ERR;" & Dictionary.gsTranslate(oClientInfo, "Beim Speichern ist ein Fehler aufgetreten")
            End If
            '-------------------------------------------------------------------


            Return "OK; saved"
        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
