﻿// TurnOverMain.js (c) myfactory 2011

function mOnLoad() 
{
    mRefreshData();
}

function mRefreshData() 
{
    var sData = document.all.chkAllDivisions.checked;

    gListViewSetUserData('lstTurnover', sData);
    gListViewLoadPage('lstTurnover', -1);
    gChartRefreshData('ctlTurnoverPeriods');
}

function mOnSetDirty() 
{
    mRefreshData();
}