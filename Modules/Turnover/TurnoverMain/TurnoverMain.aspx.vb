'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    Turnover.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for Turnover
'--------------------------------------------------------------------------------
' Created:      1/14/2011 2:04:37 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Main

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab

    Partial Class TurnoverMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            'Me.lblTurnoverChart.sTranslatedText = myfactory.FrontendSystem.AspSystem.InfoViewTools.gsDefaultChartContainer(oClientInfo, "TurnoverPeriods")
            'Me.lblTurnoverList.sTranslatedText = myfactory.FrontendSystem.AspSystem.InfoViewTools.gsDefaultListViewContainer(oClientInfo, "TurnoverOverview")

            Me.gAddScriptLink("wfAspChart.js")

            Me.sOnLoad = "mOnLoad()"
            Me.lstTurnover.sListViewDataPage = "TurnoverOverViewListData.aspx"
        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
