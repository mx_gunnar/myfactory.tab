'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TurnoverPeriods.aspx
'--------------------------------------------------------------------------------
' Purpose:      ChartDataPage for TurnoverPeriods
'--------------------------------------------------------------------------------
' Created:      03/12/2007 13:56:31, mgerlach
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem
Imports myfactory.FrontendSystem.AspSystem

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Turnover

    Partial Class TurnoverPeriodsChartData
        Inherits myfactory.FrontendSystem.AspSystem.ChartDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillChartData
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into Chart
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oChart          - Chart Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillChartData(ByVal oClientInfo As ClientInfo, _
                    ByVal oChart As AspChart.AspChart) As Boolean

            Dim oRow As AspChart.AspChartRow
            Dim oCol As AspChart.AspChartCol
            Dim bPlanning As Boolean
            Dim l, lPeriod, lYear As Integer
            Dim sDesc As String
            Dim sClause As String
            Dim bDivision As Boolean

            Dim bAllDivisions As Boolean = gbCBool(oChart.sUserData)


            '----- Chart Properties ---------
            oChart.oStyle.lChartType = AspChart.wfChartType.wfChartTypeBar
            oChart.oStyle.bShowLegend = True

            If bAllDivisions Then
                bDivision = False
            Else
                bDivision = True
            End If

            bPlanning = True

            '----- Chart Data ---------
            oChart.oRows.bClear()
            oChart.oCols.bClear()

            oCol = New AspChart.AspChartCol
            oCol.sDesc = "Umsatz"
            oChart.oCols.bAdd(oCol)

            oCol = New AspChart.AspChartCol
            oCol.sDesc = "Vorjahr"
            oChart.oCols.bAdd(oCol)

            If bPlanning Then
                oCol = New AspChart.AspChartCol
                oCol.sDesc = "Plan"
                oChart.oCols.bAdd(oCol)
            End If

            lPeriod = oClientInfo.oClientProperties.lCurrentPeriod
            lYear = lPeriod \ 1000
            lPeriod = CInt(lYear & "000")
            For l = 1 To 12
                lPeriod += 1

                oRow = New AspChart.AspChartRow

                sDesc = Period.gsGetPeriodCaption(oClientInfo, lPeriod)
                oRow.sDesc = Left(sDesc, 3)

                sClause = "SalesPeriod=" & lPeriod
                If bDivision Then sClause = DataTools.gsClauseAnd(sClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
                oRow.cValue(0) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", _
                                    "tdStatSalesCustomerGroups", sClause)

                sClause = "SalesPeriod=" & (lPeriod - 1000)
                If bDivision Then sClause = DataTools.gsClauseAnd(sClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
                oRow.cValue(1) = DataMethods.gcGetDBValue(oClientInfo, "SUM(Turnover)", _
                                    "tdStatSalesCustomerGroups", sClause)

                If bPlanning Then
                    sClause = "SalesPeriod=" & lPeriod
                    If bDivision Then sClause = DataTools.gsClauseAnd(sClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
                    oRow.cValue(2) = DataMethods.gcGetDBValue(oClientInfo, "SUM(TurnoverBudget)", _
                                        "tdBudgetSalesDivisions", sClause)
                End If

                oChart.oRows.bAdd(oRow)
            Next

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
