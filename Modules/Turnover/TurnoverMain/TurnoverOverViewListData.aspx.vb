'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TurnoverOverViewListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for TurnoverOverViewListData
'--------------------------------------------------------------------------------
' Created:      03/12/2007 12:30:56, mgerlach
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.FrontendSystem.AspSystem

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Turnover

    Partial Class TurnoverOverViewListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim sClause As String = ""
            Dim oRow As AspListViewRow
            Dim sCurrUnit As String
            Dim sCurrUnitInternal As String
            Dim cValue As Decimal
            Dim dtToday As Date
            Dim dtLast As Date
            Dim dtFirst As Date
            Dim sDivisionClause As String = ""
            Dim oCol As AspListViewCol

            Dim bAllDivisions As Boolean = gbCBool(oListView.sUserData)

            dtToday = Date.Today

            '---- ListView Properties ------
            oListView.bPageNavigation = False
            oListView.bTabletMode = True
            oListView.sListViewDataPage = "TurnoverOverViewListData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("Desc", "Zeitraum", "*")
            oListView.oCols.oAddCol("Turnover", "Umsatz anteilig", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)

            oCol = oListView.oCols.oAddCol("TurnoverComplete", "Umsatz gesamt", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oCol.bHidden = True

            oCol = oListView.oCols.oAddCol("LastYear", "Vorjahr anteilig", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)
            oCol = oListView.oCols.oAddCol("LastYearComplete", "Vorjahr gesamt", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight)

            oListView.oCols.oAddCol("CurrUnit", "Wkz", "40")

            '---- Data ---------------------
            sCurrUnitInternal = Currencies.gsCurrUnitInternal(oClientInfo, -1)
            sCurrUnit = sCurrUnitInternal

            sDivisionClause = "Combinationsmark<2"

            If Not bAllDivisions Then
                sDivisionClause = DataTools.gsClauseAnd(sDivisionClause, "DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)
            End If


            'Today
            dtFirst = dtToday
            dtLast = dtToday.AddDays(1)

            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

            oRow = oListView.oRows.oAddRow("1")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz heute")
            oRow.Value(1) = cValue
            oRow.Value(2) = cValue
            oRow.Value(5) = sCurrUnit


            dtFirst = dtFirst.AddYears(-1)
            dtLast = dtLast.AddYears(-1)
            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)
            oRow.Value(3) = cValue
            oRow.Value(4) = cValue

            'Month
            dtFirst = DateSerial(Year(dtToday), Month(dtToday), 1)
            dtLast = dtToday.AddDays(1)

            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

            oRow = oListView.oRows.oAddRow("2")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz Monat")
            oRow.Value(1) = cValue
            oRow.Value(5) = sCurrUnit


            '---- bLastYear ----
            dtFirst = dtFirst.AddYears(-1)
            dtLast = dtLast.AddYears(-1)
            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                            " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)
            oRow.Value(3) = cValue

            '---- bLastYearComplete ----
            dtLast = DateSerial(dtFirst.Year, dtFirst.Month, 1).AddMonths(1)
            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)
            oRow.Value(4) = cValue

            'year
            Dim dtYearFirstDate As Date
            Dim lYear As Integer = oClientInfo.oClientProperties.lCurrentPeriod \ 1000

            dtYearFirstDate = Period.gdtPeriod2Date(oClientInfo, glCInt(lYear & "001"))


            dtFirst = dtYearFirstDate
            dtLast = dtToday.AddDays(1)

            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)

            oRow = oListView.oRows.oAddRow("3")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Umsatz Jahr")
            oRow.Value(1) = cValue
            oRow.Value(5) = sCurrUnit

            dtFirst = dtFirst.AddYears(-1)
            dtLast = dtLast.AddYears(-1)

            '---- bLastYear ----
            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)
            oRow.Value(3) = cValue

            '---- bLastYearComplete ----
            dtLast = dtFirst.AddYears(1)
            sClause = "SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                        " AND SalesDate<" & DataTools.gsDate2Sql(dtLast)
            sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

            cValue = DataMethods.gcGetDBValue(oClientInfo, _
                        "SUM(Turnover)", "tdStatSalesDetail", sClause)
            cValue = Currencies.gcCurrConvert(oClientInfo, cValue, -1, sCurrUnitInternal, , sCurrUnit)
            oRow.Value(4) = cValue

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
