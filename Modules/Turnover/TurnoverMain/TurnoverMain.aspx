<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.TurnoverMain" codefile="TurnoverMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
	<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server"> 
<table class=borderTable height="98%" cellSpacing=2 cellPadding=2 width="98%">
<tr height="50px">
<td width="300px">
       <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/tab/modules/Turnover/TurnoverMain/DlgTurnoverMain.xml"></myfactory:wfXmlDialog>
</td>
<td>&nbsp;</td>
</tr>
<tr valign="top" height="300px">
	<td colspan="2">
		<myfactory:wfChart runat="server" ID="ctlTurnoverPeriods" sChartDataPage="TurnoverPeriodsChartData.aspx"></myfactory:wfChart>
	</td>
</tr>
<tr valign="top" height="*">
	<td colspan="2">
		<myfactory:wfListView runat="server" ID="lstTurnover" sListViewDataPage="/tab/modules/Turnover/TurnoverMain/TurnoverOverViewListData.aspx"></myfactory:wfListView>
	</td>
</tr>

</table>


</body>
</HTML>
