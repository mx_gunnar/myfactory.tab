<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.MIS.TurnoverInfo.ProductgroupTurnoverInfoMain" codefile="ProductgroupTurnoverInfoMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<HTML>
  <HEAD>
		<link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet" >

	<myfactory:wfpageheader id=ctlPageHeader runat="server"></myfactory:wfpageheader>
  </HEAD>


<body id=ctlBody leftMargin=0 topMargin=0 scroll=no runat="server">
<table class=borderTable height="100%" cellSpacing=1 cellPadding=1 width="98%" style="table-layout:fixed;">
<tr height="25px">
	<td>
        <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/tab/modules/turnover/productgroupturnoverinfo/dlgproductgroupturnoverinfo.xml">
        </myfactory:wfXmlDialog>
	</td>
</tr>
<tr height="95%">
	<td style="padding:10px">
	    <myfactory:wfListView ID="lstMain" runat="server" sListViewDataPage="productgroupturnoverinfodata.aspx">
        </myfactory:wfListView>
	</td>
</tr>
</table>

</body>
</HTML>
