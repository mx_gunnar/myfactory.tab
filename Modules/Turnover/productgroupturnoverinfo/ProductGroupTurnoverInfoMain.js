﻿//ProductGroupTurnoverInfo.js (c) myfactory International 2008

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    var sUserData = document.all.cboYear.value + 
                        ';' + document.all.chkAllDivisions.checked +
                        ';' + document.all.chkSplitSets.checked + 
                        ';0';

    gListViewSetUserData('lstMain', sUserData);
    gListViewLoadPage('lstMain', -1);
    gListViewResize('lstMain');                        
}

function mOnUpdate()
{
    mRefreshList();
}
function mOnSetDirty()
{
    if (event.srcElement.id!='txtCustomerIDExt')
        mRefreshList();
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    var sURL;
    
    sURL = 'ProductGroupTurnoverInfoProcess.aspx' +
                '?Method=ToggleGroup' +
                '&Group=' + gsEncodeStringURL(sItemID);
                
    var sRes = gsCallServerMethod(sURL,'');
    if (sRes=='OK;')
    {
        mRefreshList();
        return;
    }
    gAlert(sRes);
}