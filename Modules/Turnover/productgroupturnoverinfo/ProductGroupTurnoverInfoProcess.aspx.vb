'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductGroupTurnoverInfoProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for ProductGroupTurnoverInfoProcess
'--------------------------------------------------------------------------------
' Created:      6/9/2008 2:47:14 PM, mgerlach
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.MIS.TurnoverInfo

    Partial Class ProductGroupTurnoverInfoProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sMethod As String = Request.QueryString("Method")
            Dim sGroup As String = Request.QueryString("Group")

            If sMethod = "ToggleGroup" Then
                Dim sOpenGroups As String

                sOpenGroups = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "ProductGroupTurnoverInfo", False)
                If InStr(0, sOpenGroups, ";" & sGroup & ";") <> 0 Then
                    sOpenGroups = Replace(sOpenGroups, ";" & sGroup & ";", ";")
                Else
                    If sOpenGroups = "" Then sOpenGroups = ";"
                    sOpenGroups &= sGroup & ";"
                End If

                myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "ProductGroupTurnoverInfo", sOpenGroups, False)

                Return "OK;"
            End If

            Return "Unknown Method:" & sMethod

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
