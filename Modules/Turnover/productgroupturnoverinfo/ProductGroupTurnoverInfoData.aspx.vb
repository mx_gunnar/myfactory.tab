'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductGroupTurnoverInfoData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductGroupTurnoverInfoData
'--------------------------------------------------------------------------------
' Created:      6/6/2008 8:26:21 AM, mgerlach
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.StringFunctions
Imports myfactory.Sys.Tools.BasicFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.MIS.TurnoverInfo

    Partial Class ProductGroupTurnoverInfoData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim rsPeriods As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim oCol As AspListViewCol
            Dim sDesc As String

            Dim asUserData As String()
            Dim lYear, lCustomerID As Integer
            Dim bAllDivisions, bSplitSets As Boolean

            '---- ListView Properties ------
            oListView.bPageNavigation = False
            oListView.bTabletMode = True
            oListView.sListViewDataPage = "ProductGroupTurnoverInfoData.aspx"
            oListView.bTabletScrollMode = True


            asUserData = Split(oListView.sUserData, ";")
            If UBound(asUserData) > 1 Then
                lYear = glCInt(asUserData(0))
                bAllDivisions = gbCBool(asUserData(1))
                bSplitSets = gbCBool(asUserData(2))
                lCustomerID = glCInt(asUserData(3))
            End If

            oListView.bEnablePrint = True
            oListView.sTranslatedPrintHeader = Dictionary.gsTranslate(oClientInfo, _
                                                "Umsatz�bersicht nach Artikelgruppen") & " " & lYear
            If bAllDivisions Then
                oListView.sTranslatedPrintHeader &= " " & Dictionary.gsTranslate(oClientInfo, "Alle Betriebsst�tten")
            End If

            rsPeriods = DataMethods.grsGetDBRecordset(oClientInfo, "Period,StartDate", _
                            "tdPeriods", _
                            "YearEndPeriod=0 AND Period>=" & lYear & "000 AND Period<=" & lYear & "999", _
                            , "Period")

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("cmdOpen", "", "50px")
            oCol = oListView.oCols.oAddCol("ProductGroupDesc", "Gruppe", "*")

            Do Until rsPeriods.EOF
                sDesc = Period.gsGetMonthDesc(oClientInfo, True, rsPeriods(1).dtValue)
                oCol = oListView.oCols.oAddCol(rsPeriods(0).sValue, sDesc, "55", wfEnumDataTypes.wfDataTypeInt, wfEnumAligns.wfAlignRight)

                rsPeriods.MoveNext()
            Loop

            sDesc = "Summe"
            oCol = oListView.oCols.oAddCol("-1", sDesc, "60", wfEnumDataTypes.wfDataTypeInt, wfEnumAligns.wfAlignRight)


            '---- Data ---------------------
            sOrder = "ProductGroupTree"

            rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                    "ProductGroupName,ProductGroupDesc,ProductGroupTree", _
                    "tdProductGroups", _
                    "ProductGroupTree IS NULL OR ProductGroupTree Like '____.'", _
                    , sOrder)
            Do Until rs.EOF

                mAddRow(oClientInfo, oListView, rs, rsPeriods, bAllDivisions, bSplitSets, 0, lCustomerID)

                rs.MoveNext()
            Loop

            mAddRow(oClientInfo, oListView, Nothing, rsPeriods, bAllDivisions, bSplitSets, 0, lCustomerID)

            Return True

        End Function


        Private Sub mAddRow(ByVal oClientInfo As ClientInfo, ByVal oListView As AspListView, _
                        ByVal rs As Recordset, ByVal rsPeriods As Recordset, _
                        ByVal bAllDivisions As Boolean, ByVal bSplitSets As Boolean, _
                        ByVal lLevel As Integer, ByVal lCustomerID As Integer)

            Dim oRow As AspListViewRow
            Dim sFields As String
            Dim rsTurnover As Recordset
            Dim rsGroups As Recordset = Nothing
            Dim l, k As Integer
            Dim sProductGroups As String
            Dim cSum As Decimal
            Dim bSum As Boolean = False
            Dim bSubGroups As Boolean = False
            Dim sPrefix As String = ""

            '---- Get Group ans SubGroups -----
            If rs Is Nothing Then
                sProductGroups = ""
                oRow = oListView.oRows.oAddRow("-1")
                oRow.Value(1) = Dictionary.gsTranslate(oClientInfo, "Summe")
                oRow.bRowBold = False
                oRow.bLineBottom = True
                oRow.bLineTop = True
                bSum = True

            Else
                If lLevel > 0 Then sPrefix = New String("-"c, lLevel)
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.Value(1) = sPrefix & rs(1).sValue
                If lLevel = 0 Then
                    oRow.bRowBold = False
                End If

                If rs("ProductGroupTree").sValue = "" Then
                    sProductGroups = DataTools.gsStr2Sql(rs(0).sValue)
                Else
                    rsGroups = DataMethods.grsGetDBRecordset(oClientInfo, _
                                    "ProductGroupName", _
                                    "tdProductGroups", _
                                    "ProductGroupTree like " & (rs("ProductGroupTree").sValue & "%").gs2Sql, _
                                    , "ProductGroupTree")
                    sProductGroups = ""
                    Do Until rsGroups.EOF
                        If sProductGroups <> "" Then
                            sProductGroups &= ","
                            bSubGroups = True
                        End If
                        sProductGroups &= DataTools.gsStr2Sql(rsGroups(0).sValue)
                        rsGroups.MoveNext()
                    Loop
                End If

                sProductGroups = "(" & sProductGroups & ")"
            End If

            If rsPeriods.RecordCount = 0 Then Return


            '---- Get Turnover ------
            sFields = ""
            rsPeriods.MoveFirst()
            Do Until rsPeriods.EOF
                If sFields <> "" Then sFields &= ","

                sFields &= "(SELECT SUM(Turnover) "
                If lCustomerID = 0 Then
                    sFields &= "FROM tdStatSalesProductGroups "
                Else
                    sFields &= "FROM tdStatSalesDetail "
                End If
                sFields &= "WHERE SalesPeriod=" & rsPeriods(0).lValue

                If sProductGroups <> "" Then sFields &= "  AND ProductGroup IN " & sProductGroups
                If lCustomerID <> 0 Then sFields &= "  AND CustomerID=" & lCustomerID

                If Not bAllDivisions Then sFields &= " AND DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr
                If bSplitSets Then
                    sFields &= " AND (CombinationsMark=0 OR CombinationsMark=2)"
                Else
                    sFields &= " AND (CombinationsMark=0 OR CombinationsMark=1)"
                End If

                sFields &= ")"

                rsPeriods.MoveNext()
            Loop

            rsTurnover = DataMethods.grsGetDBRecordset(oClientInfo, _
                            sFields, "tdProductGroups")
            If Not rsTurnover.EOF Then
                l = 2
                k = 0
                cSum = 0
                rsPeriods.MoveFirst()
                Do Until rsPeriods.EOF
                    oRow.Value(l) = gcRound(rsTurnover(k).cValue, 0, wfEnumRoundMode.wfRoundModeBusiness)
                    cSum += gcRound(rsTurnover(k).cValue, 0, wfEnumRoundMode.wfRoundModeBusiness)
                    l += 1
                    k += 1
                    rsPeriods.MoveNext()
                Loop

                oRow.Value(l) = cSum

            End If

            If bSubGroups Then
                Dim sOpenGroups As String
                Dim bShow As Boolean

                sOpenGroups = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "ProductGroupTurnoverInfo", False)
                bShow = (InStr(0, sOpenGroups, rs(0).sValue) <> 0)

                If bShow Then
                    oRow.lColType(0) = wfEnumAspListViewColTypes.wfColTypeButton
                    oRow.Value(0) = "-"

                    rsGroups = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                        "ProductGroupName,ProductGroupDesc,ProductGroupTree", _
                                                        "tdProductGroups", _
                                                        "ProductGroupTree like " & (rs("ProductGroupTree").sValue & "____.").gs2Sql, _
                                                        , "ProductGroupTree")
                    Do Until rsGroups.EOF
                        mAddRow(oClientInfo, oListView, rsGroups, rsPeriods, bAllDivisions, bSplitSets, lLevel + 1, lCustomerID)
                        rsGroups.MoveNext()
                    Loop
                Else
                    oRow.lColType(0) = wfEnumAspListViewColTypes.wfColTypeButton
                    oRow.Value(0) = "+"
                End If

            End If

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
