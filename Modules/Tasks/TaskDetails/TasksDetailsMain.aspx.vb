'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for TasksDetailsMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.Sys.Main
Imports myfactory.FrontendApp.AspPhoneTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TasksDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))

            Dim sMobilePhonePlatform = ClientValues.gsGetClientValue(oClientInfo, "Tab_MobilePlatformName")


            Dim sTaskCommand As String = ""

            If lRecordID <> 0 Then
                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, _
                                                   "TaskID,TaskDesc,TaskDueDate AS OrigTaskDueDate,UserName,CreatorName,TaskText,TaskDone,'' AS TaskDueDate, '' AS TaskTimeTo, TaskCommand, TaskType, TaskTeam" & _
                                                   ",(SELECT MIN(ReadState) FROM tsTaskReads tr WHERE tr.TaskID=tsTasks.TaskID AND UserName=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql & ") AS IsRead", _
                                                   "tsTasks", "TaskID=" & lRecordID)
                If Not rs.EOF Then
                    rs("TaskDueDate").Value = gsGetDate(rs("OrigTaskDueDate").dtValue)
                    rs("TaskTimeTo").Value = gsGetTimeFromDate(rs("OrigTaskDueDate").dtValue)

                    'bring into correct jQueryMobile-Format
                    If sMobilePhonePlatform = "iPad" Then
                        rs("TaskDueDate").Value = PhoneToolFunctions.gsGetDateInMobileFormat(rs("TaskDueDate").dtValue)
                    Else
                        If Request.Browser.Browser.Contains("Chrome") Then
                            Me.dlgMain.oDialog.oField("TaskTimeTo").oMember.Prop("INPUTTYPE") = ""
                            Me.dlgMain.oDialog.oField("TaskDueDate").oMember.Prop("INPUTTYPE") = ""
                        End If
                    End If


                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)

                    sTaskCommand = rs("TaskCommand").sValue
                End If
            Else
                'bring into correct jQueryMobile-Format
                If sMobilePhonePlatform = "iPad" Then
                    Me.dlgMain.oDialog.oField("TaskDueDate").oMember.Prop("VALUE") = PhoneToolFunctions.gsGetDateInMobileFormat(Date.Now)
                Else
                    If Request.Browser.Browser.Contains("Chrome") Then
                        Me.dlgMain.oDialog.oField("TaskDueDate").oMember.Prop("INPUTTYPE") = ""
                    End If
                End If
            End If

            ReDim Me.asPageParams(3)
            ReDim Me.asTerms(2)

            Me.asPageParams(1) = Str(lRecordID)

            If lAddressID > 0 Then
                Me.asPageParams(0) = Str(lAddressID)

                If lRecordID = 0 Then

                    Me.asTerms(0) = Dictionary.gsTranslate(oClientInfo, "Adresse") & _
                            " " & _
                            DataMethods.gsGetDBValue(oClientInfo, "AddressNumber + ', ' + Matchcode", "tdAddresses", "AddressID=" & lAddressID) & _
                            " " & _
                            Dictionary.gsTranslate(oClientInfo, "bearbeiten")

                End If
            End If

            Me.sOnLoad = "mOnLoad()"
            Me.gAddScriptLink("wfDlgParams.js", True)


            ' ---- set entity button command  ---- 
            Dim sCmdEntity As String = ""

            If Not String.IsNullOrEmpty(sTaskCommand) Then

                Dim sEntityName As String = sTaskCommand.Split(";"c)(0)

                If "Products SalesOrders Addresses".Contains(sEntityName) Then
                    ' entity navigation to product / so / address
                    sCmdEntity = DataMethods.gsGetDBValue(oClientInfo, "EditCmdTab", "tsEntities", "EntityName=" & sEntityName.gs2Sql & "", , myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)
                    sCmdEntity = sCmdEntity.Replace("$1", sTaskCommand.Split(";"c)(1))
                    Me.cmdEntityEdit.sText = DataMethods.gsGetDBValue(oClientInfo, "ItemDescription", "tsEntities", "EntityName=" & sEntityName.gs2Sql, , myfactory.Sys.Data.PublicEnums.wfEnumDataSources.wfDataSourceSystem)

                ElseIf "Plannings".Contains(sEntityName) Then
                    ' task is appointment-request 
                    Me.cmdEntityEdit.sText = Dictionary.gsTranslate(oClientInfo, "Terminanfrage beantworten")
                    Me.cmdEntityEdit.sStyle = "width: 200px;"
                    Me.asPageParams(2) = sTaskCommand
                Else
                    Me.cmdEntityEdit.bHidden = True
                End If

            Else
                Me.cmdEntityEdit.bHidden = True
            End If
            Me.asTerms(1) = sCmdEntity

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
