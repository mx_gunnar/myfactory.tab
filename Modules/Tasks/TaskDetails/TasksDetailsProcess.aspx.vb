'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksDetailsProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for TasksDetailsProcess
'--------------------------------------------------------------------------------
' Created:      1/24/2011 12:39:32 PM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Xml

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.BusinessTasks.Base.General
Imports myfactory.BusinessTasks.Resources.ToolFunctions
Imports myfactory.BusinessTasks.Resources

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TasksDetailsProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Dim lPlanningID As Integer = glCInt(Request.QueryString("PlanningID"))
            Dim lPlanningAllocationID As Integer = glCInt(Request.QueryString("PlanningAllocationID"))
            Dim lTaskID As Integer = glCInt(Request.QueryString("TaskID"))
            Dim sReason As String = Request.QueryString("Reason")

            Select Case sCmd
                Case "Save"
                    Return msSaveTaskData(oClientInfo)
                Case "AllocPlanningAllocationDirect"
                    Return msAllocPlanningAllocationDirect(oClientInfo, lPlanningID, lPlanningAllocationID, lTaskID)
                Case "SetPlanningAllocationDenied"
                    Return msSetPlanningAllocationDenied(oClientInfo, lPlanningID, lPlanningAllocationID, sReason, lTaskID)
                Case "SetPlanningAllocationUnderReservation"
                    Return msSetPlanningAllocationUnderReservation(oClientInfo, lPlanningID, lPlanningAllocationID, sReason)
                Case Else
                    Return "Unknown Cmd: " & sCmd
            End Select

            Return "TODO"

        End Function

        Private Function mbSetTaskDone(ByVal oClientInfo As ClientInfo, ByVal lTaskID As Integer) As Boolean

            Dim oTask As New Task
            Dim bOK As Boolean = False

            If oTask.gbLoad(oClientInfo, lTaskID) Then
                oTask.gbSetTaskDone()

                If oTask.glSave(oClientInfo, , True) > 0 Then
                    bOK = True
                End If
            End If

            Return bOK

        End Function

        Private Function msAllocPlanningAllocationDirect(ByVal oClientInfo As ClientInfo, ByVal lPlanningID As Integer, ByVal lPlanningAllocationID As Integer, ByVal lTaskID As Integer) As String

            Dim oResPlanning As New Planning
            oResPlanning.bInit(oClientInfo, lPlanningID)

            If Not oResPlanning.bTransferDirectToAlloc(oClientInfo, , lPlanningAllocationID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Beim Setzen des Status 'Zusagen' ist ein Fehler aufgetreten")
            End If

            If Not mbSetTaskDone(oClientInfo, lTaskID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Best�tigung erfolgreich, aber die Aufgabe konnte nicht auf erledigt gestzt werden")
            End If

            Return "OK;"

        End Function

        Private Function msSetPlanningAllocationDenied(ByVal oClientInfo As ClientInfo, ByVal lPlanningID As Integer, ByVal lPlanningAllocationID As Integer, ByVal sReason As String, ByVal lTaskID As Integer) As String

            Dim oResPlanning As New Planning
            oResPlanning.bInit(oClientInfo, lPlanningID)

            If Not oResPlanning.bSetState(oClientInfo, Planning.wfEnumPlanningState.Rejected, lPlanningID, lPlanningAllocationID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Beim Setzen des Status 'Ablehnen' ist ein Fehler aufgetreten")
            Else
                oResPlanning.bAddLog(oClientInfo, sReason, lPlanningID, lPlanningAllocationID)
            End If

            If Not mbSetTaskDone(oClientInfo, lTaskID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Ablehnung erfolgreich, aber die Aufgabe konnte nicht auf erledigt gestzt werden")
            End If

            Return "OK;"

        End Function

        Private Function msSetPlanningAllocationUnderReservation(ByVal oClientInfo As ClientInfo, ByVal lPlanningID As Integer, ByVal lPlanningAllocationID As Integer, ByVal sReason As String) As String


            Dim oResPlanning As New Planning
            oResPlanning.bInit(oClientInfo, lPlanningID)

            If Not oResPlanning.bSetState(oClientInfo, Planning.wfEnumPlanningState.UnderReservation, lPlanningID, lPlanningAllocationID) Then
                Return Dictionary.gsTranslate(oClientInfo, "Beim Setzen des Status 'Unter Vorbehalt' ist ein Fehler aufgetreten")
            Else
                oResPlanning.bAddLog(oClientInfo, sReason, lPlanningID, lPlanningAllocationID)
            End If

            Return "OK;"

        End Function

        ''' <summary>
        ''' Save Task Data
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msSaveTaskData(ByVal oClientInfo As ClientInfo) As String

            Dim oXml As New XmlDocument
            Dim oRoot As XmlNode
            Dim lAddressID As Integer = glCInt(Me.Request.QueryString("AddressID"))

            '--------- get dialog params ---------
            If Not oXml.load(Request) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Ermitteln der Parameter")

            oRoot = oXml.documentElement
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Lesen der Parameter")

            oRoot = oRoot.selectSingleNode("//DlgParams")
            If oRoot Is Nothing Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Auswerten der Parameter")

            '--------- check values ----------------
            Dim sDesc As String
            Dim dtTaskDueDate As Date
            Dim lTaskID As Integer
            Dim oTask As New Task

            sDesc = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskDesc")
            If sDesc = "" Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie einen Aufgabenbetreff an.")

            dtTaskDueDate = gdtCDate(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskDueDate"))
            If Not gbDate(dtTaskDueDate) Then Return Dictionary.gsTranslate(oClientInfo, "Bitte geben Sie ein g�ltiges Datum an.")

            lTaskID = glCInt(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskID"))
            If lTaskID = 0 Then
                oTask.gbNew(oClientInfo)
            Else
                If Not oTask.gbLoad(oClientInfo, lTaskID) Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Laden der Aufgabe")
            End If

            If lAddressID > 0 Then
                oTask.oMember.Prop("AddressID") = lAddressID
                oTask.oMember.Prop("TaskType") = 2
                oTask.oMember.Prop("TaskCommand") = "Addresses;" & lAddressID
            End If

            oTask.oMember.Prop("TaskDesc") = sDesc
            oTask.oMember.Prop("TaskText") = XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskText")
            oTask.oMember.Prop("UserName") = XmlFunctions.gsGetXMLSubNodeText(oRoot, "cboUserName")
            oTask.oMember.Prop("TaskTeam") = XmlFunctions.gsGetXMLSubNodeText(oRoot, "cboTaskTeam")

            dtTaskDueDate = gdtAddTime2Date(dtTaskDueDate, gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeTo")))
            oTask.oMember.Prop("TaskDueDate") = dtTaskDueDate
            oTask.oMember.Prop("TaskDate") = dtTaskDueDate
            oTask.oMember.Prop("TaskTimeTo") = gdtTime(XmlFunctions.gsGetXMLSubNodeText(oRoot, "txtTaskTimeTo")).ToShortTimeString

            'Categories
            oTask.oMember.Prop("Category") = XmlFunctions.gsXMLDecode(Request.QueryString("CategoryString"))

            If gbCBool(XmlFunctions.gsGetXMLSubNodeText(oRoot, "chkTaskDone")) Then
                oTask.gbSetTaskDone()
            Else
                oTask.gbSetTaskNotDone()
            End If

            lTaskID = oTask.glSave(oClientInfo)
            If lTaskID = 0 Then Return Dictionary.gsTranslate(oClientInfo, "Fehler beim Speichern der Aufgabe")

            If gbCBool(XmlFunctions.gsGetXMLSubNodeText(oRoot, "chkIsRead")) Then
                Task.gbSetRead(oClientInfo, lTaskID)
            Else
                Task.gbSetUnRead(oClientInfo, lTaskID)
            End If

            Return "OK;"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
