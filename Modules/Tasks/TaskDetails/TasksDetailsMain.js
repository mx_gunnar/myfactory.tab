﻿// TasksMain.js (c) myfactory 2010

function mOnLoad()
{
    if (msTerms[0] != '')
    {
        document.all.txtTaskDesc.value = msTerms[0];
    }

    mRefreshCategoryList();
}

function mRefreshCategoryList() 
{
    gListViewSetUserData('lstCategory', msPageParams[1]);
    gListViewLoadPage('lstCategory', 1);

}

function mOnOK() {

    var sCheckedList = gsListViewGetChecked('lstCategory', 'ChkSelected');
    // XML Encocde
    sCheckedList = gsEncodeStringURL(sCheckedList);

    var bWithAddress = false;
    var sValues = gsXMLDlgParams(frmMain, '', false);
    var sURL = 'TasksDetailsProcess.aspx?Cmd=Save'

    // got an AddressID? 
    if (msPageParams[0] != '')
    {
        bWithAddress = true;
        sURL = sURL + "&AddressID=" + msPageParams[0];
    }

    sURL = sURL + "&CategoryString=" + sCheckedList; 

    var sRes = gsCallServerMethod(sURL, sValues);
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }
    
    mNavigateToList();
}

function mOnCancel()
{
    mNavigateToList();
}

function mNavigateToList() 
{
    var sURL = '../TasksMain/TasksMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + msPageParams[0];

    document.location = sURL;
}

function mOnEntityEdit() 
{
    if (msPageParams[2] && msPageParams[2] != "")
    {
        var sURL = "../TaskAppointmentReply/TaskAppointmentReplyMain.aspx?ClientID=" + msClientID + "&Command=" + msPageParams[2] + "&TaskID=" + msPageParams[1] + "&AddressID=" + msPageParams[0];
        document.location = sURL;
    } 
    else
    {
        var sPath = msWebPageRoot + "/" + msTerms[1] + "&ClientID=" + msClientID;
        document.location = sPath;
    }
}