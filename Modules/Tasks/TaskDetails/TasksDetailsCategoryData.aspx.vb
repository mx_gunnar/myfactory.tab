'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksDetailsCategoryData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for TasksDetailsCategoryData
'--------------------------------------------------------------------------------
' Created:      22.04.2013 10:13:08, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports System.Collections.Generic

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TasksDetailsCategoryData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sClause As String = ""
            Dim sFields As String
            Dim lTaskID As Integer = 0
            Dim sTables As String
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol

            lTaskID = glCInt(oListView.sUserData)

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "TasksDetailsCategoryData.aspx"
            oListView.bTabletMode = True
            oListView.bAutoHideNavigation = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10

            '---- Columns ------------------
            oListView.oCols.oAddCol("Category", "Kategorie", "*", , , False, False)

            oCol = oListView.oCols.oAddCol("ChkSelected", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeCheckBox
            oCol.sWidth = "50"


            '---- Data ---------------------
            sFields = "Category AS RowID, Category"
            sTables = "tsTaskUserCategories"
            sClause = "UserInitials=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , )


            'get existing categories
            Dim sCategoryString As String = DataMethods.gsGetDBValue(oClientInfo, "Category", "tsTasks", "TaskID=" & lTaskID.gs2Sql)
            If Not String.IsNullOrEmpty(sCategoryString) Then
                Dim listCategories As New List(Of String)
                listCategories.AddRange(sCategoryString.Split(";"c))

                Do Until rs.EOF
                    oRow = oListView.oRows.oAddRow(rs(0).sValue)
                    oRow.Value(0) = rs(1).sValue

                    If listCategories.Contains(rs(0).sValue) Then
                        oRow.Value(1) = "-1"
                    Else
                        oRow.Value(1) = "0"
                    End If

                    rs.MoveNext()
                Loop

            Else
                oListView.oRows.SetRecordset(rs, True)
            End If

            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
