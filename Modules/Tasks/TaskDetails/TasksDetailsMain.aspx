<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Tasks.TasksDetailsMain" CodeFile="TasksDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="98%">
        <tr height="200px">
            <td width="500px" valign="top">
                <form name="frmMain">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Tasks/TaskDetails/dlgTaskDetails.xml"
                    sDataSource="dsoData">
                </myfactory:wfXmlDialog>
                </form>
            </td>
            <td width="*" height="200px">
                <myfactory:wfListView runat="server" ID="lstCategory" sListViewDataPage="TasksDetailsCategoryData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr>
        <td>
        <myfactory:wfButton ID="cmdEntityEdit" sStyle="width: 150px;" runat="server" sOnClick="mOnEntityEdit();"></myfactory:wfButton>
        </td></tr>
        <tr height="25px">
            <td width="*" align="right" colspan="2">
                <myfactory:wfButton runat="server" ID="cmdOK" sOnClick="mOnOK()" sText=" OK ">
                </myfactory:wfButton>
                <myfactory:wfButton runat="server" ID="cmdCancel" sOnClick="mOnCancel()" sText="Abbrechen">
                </myfactory:wfButton>
            </td>
        </tr>
    </table>
</body>
</html>
