'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TaskAppointmentReplyMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for TaskAppointmentReplyMain
'--------------------------------------------------------------------------------
' Created:      15.10.2013 17:12:51, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TaskAppointmentReplyMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddressID As Integer = glCInt(Request.QueryString("AddressID"))
            Dim lTaskID As Integer = glCInt(Request.QueryString("TaskID"))
            Dim sCommand As String = Request.QueryString("Command")  'Plannings;866;5  / Plannings;PlanningID;PlanningAlocationID

            Dim lPlanningID As Integer
            Dim lPlanningAllocationID As Integer
            Dim sCreatorName As String = ""
            Dim sXMLValues As String
            Dim rs As Recordset

            lPlanningID = glCInt(sCommand.Split(";"c)(1))
            lPlanningAllocationID = glCInt(sCommand.Split(";"c)(2))

            ReDim Me.asPageParams(4)
            Me.asPageParams(0) = lTaskID.ToString
            Me.asPageParams(1) = lAddressID.ToString
            Me.asPageParams(2) = lPlanningID.ToString
            Me.asPageParams(3) = lPlanningAllocationID.ToString


            rs = DataMethods.grsGetDBRecordsetDirect(oClientInfo, "SELECT * FROM " & _
                        "tsTasks " & _
                        " JOIN tdResourcePlannings ON tsTasks.ResourcePlanningID=tdResourcePlannings.PlanningID" & _
                        " JOIN tdResourcePlanningAllocation ON tdResourcePlanningAllocation.PlanningID=tdResourcePlannings.PlanningID" & _
                        " WHERE tdResourcePlannings.PlanningID=" & lPlanningID)

            If Not rs.EOF Then

                rs("TaskText").Value = myfactory.Sys.Tools.StringFunctions.gsConvertHTML2Plain(rs("TaskText").sValue)
                sXMLValues = myfactory.Sys.Data.DataTools.gsRecord2Xml(rs)

                sCreatorName = DataMethods.gsGetDBValue(oClientInfo, "Username", _
                                    "tsDatabaseUsers", "UserInitials=" & (rs("CreatorName").sValue.gs2Sql))

                Me.dlgMain.sValues = sXMLValues
                Me.dlgMain.oDialog.oField("CreatorDesc").oMember.Prop("VALUE") = sCreatorName
            End If

            Me.sOnLoad = "mOnLoad();"
        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
