﻿



function mOnLoad()
{
    mSetReasonEnable();
    document.getElementById("txtTaskText").disabled = true;
}

function mOnSetDirty()
{

    if (event.type == "change")
    {
        if (event.srcElement.id == "cboAnswer")
        {
            mSetReasonEnable();
        } 
    }
}

function mSetReasonEnable()
{
    var lAnswer = document.getElementById("cboAnswer").value;

    if (lAnswer == "1")
    {
        document.getElementById("txtReason").disabled = true;
    }
    else
    {
        document.getElementById("txtReason").disabled = false;
    }
}


function mOnCancel()
{
    mNavigateToTaskDetails();
}

function mOnOK()
{
    var lAnswer = document.getElementById("cboAnswer").value;
    var sReason = document.getElementById("txtReason").value;
    var sCmd = "";

    if (lAnswer == "1")
    {
        // Zustimmen
        sCmd = "AllocPlanningAllocationDirect"
    }
    else if (lAnswer == "2")
    {
        // Ablehnen
        sCmd = "SetPlanningAllocationDenied"
    }
    else if (lAnswer == "3")
    {
        // Vorbehalt
        sCmd = "SetPlanningAllocationUnderReservation"
    }

    var sURL = "../TaskDetails/TasksDetailsProcess.aspx";
    sURL = sURL + "?Cmd=" + sCmd;
    sURL = sURL + "&TaskID=" + msPageParams[0];
    sURL = sURL + "&PlanningID=" + msPageParams[2];
    sURL = sURL + "&PlanningAllocationID=" + msPageParams[3];
    sURL = sURL + "&Reason=" + gsEncodeStringURL(sReason);

    var sRes = gsCallServerMethod(sURL, '');
    if (sRes.substr(0, 3) != 'OK;')
    {
        alert(sRes);
        return;
    }

    mNavigateToTaskList();
}

function mNavigateToTaskList()
{
    var sURL = "../TasksMain/TasksMain.aspx?ClientID=" + msClientID + "&RecordID=" + msPageParams[1]; // RecordID = AddressID
    document.location = sURL;

}

function mNavigateToTaskDetails()
{
    var sURL = "../TaskDetails/TasksDetailsMain.aspx?ClientID=" + msClientID + "&RecordID=" + msPageParams[0] + "&AddressID=" + msPageParams[1];
    document.location = sURL;
}