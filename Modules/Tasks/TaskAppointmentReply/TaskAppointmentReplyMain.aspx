<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Tasks.TaskAppointmentReplyMain" CodeFile="TaskAppointmentReplyMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../CSS/wfStyleBrowserTable.css" rel="stylesheet" type="text/css" />
    <link href="../../../CSS/wfStylePage.css" rel="stylesheet" type="text/css" />
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="0" cellpadding="0" width="98%"
        style="table-layout: fixed;">
        <tr valign="top">
            <td>
                <myfactory:wfXmlDialog ID="dlgMain" runat="server" sDialog="/TAB/MODULES/TASKS/TaskAppointmentReply/DlgTaskAppointmentReply.xml">
                </myfactory:wfXmlDialog>
            </td>
        </tr>
        <tr height="*">
            <td>
                &nbsp;
            </td>
        </tr>
        <tr valign="bottom" height="120px">
            <td>
                <table width="100%" height="100%">
                    <tr>
                        <td>
                        </td>
                        <td width="120px" align="right">
                            <myfactory:wfButton ID="cmdOK" sOnClick="mOnOK();" sText="OK" runat="server">
                            </myfactory:wfButton>
                        </td>
                        <td width="120px" align="right">
                            <myfactory:wfButton ID="cmdCancel" sOnClick="mOnCancel();" sText="Abbrechen" runat="server">
                            </myfactory:wfButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
