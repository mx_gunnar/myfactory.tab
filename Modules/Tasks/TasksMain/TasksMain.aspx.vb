'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for TasksMain
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:56:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TasksMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lAddressID As Integer = glCInt(Request.QueryString("RecordID"))

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = Str(lAddressID)

            If lAddressID > 0 Then

                '-------- write navi ---------------
                Dim osHTML As New FastString
                Dim frs As FastRecordset
                Dim bShow As Boolean

                osHTML.bAppend("<nobr>")
                osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "AddressDetails", "../../../", "RecordID=" & lAddressID))

                frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                       "ModuleName,Edition,PartnerID,ModuleID", _
                                                       "tsTabModules", _
                                                       "ParentModule='AddressDetails' AND ModuleName<>'Addresses_Tasks'", _
                                                       , "ShowIndex", _
                                                       wfDataSourceSystem)
                Do Until frs.EOF
                    bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                    If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                    If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                    If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lAddressID))

                    frs.MoveNext()
                Loop
                osHTML.bAppend("</nobr>")
                Me.lblNaviSub.sTranslatedText = osHTML.sString
            End If

            Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_Tasks_Filter", False)
            Me.dlgMain.oDialog.oField("Search").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Tasks_Search", False)
            Me.dlgMain.oDialog.oField("Category").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Tasks_Category", False)
            Me.dlgMain.oDialog.oField("GroupByCategory").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_Tasks_GroupByCategory", False)


            Me.sOnLoad = "mOnLoad()"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
