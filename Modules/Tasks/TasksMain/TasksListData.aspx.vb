'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    TasksListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for TasksListData
'--------------------------------------------------------------------------------
' Created:      1/18/2011 10:57:05 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Xml
Imports System.Collections.Generic

'================================================================================
' Class Definition 
'================================================================================

Namespace ASP.Tab.Tasks

    Partial Class TasksListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim sUser As String = oClientInfo.oClientProperties.sCurrentUser

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "TasksListData.aspx"
            oListView.lPageSize = 10
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("TaskDueDate", "F�llig bis", "80", wfEnumDataTypes.wfDataTypeDate, , True)
            oCol.sVerticalAlign = "middle"
            oCol = oListView.oCols.oAddCol("TaskDesc", "Bezeichnung", "*", , , True)
            oCol.sVerticalAlign = "middle"

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"

            '---- Data ---------------------
            Dim asData As String()
            Dim lFilter As Integer
            Dim lAddressID As Integer
            Dim sSearch As String = ""
            Dim sCategorySearch As String = ""
            Dim bGroupByCategory As Boolean = False
            Dim sOhneString As String = myfactory.Sys.Main.Dictionary.gsTranslate(oClientInfo, "<Ohne>")
            asData = Split(oListView.sUserData, ";")
            If UBound(asData) > 3 Then
                lFilter = glCInt(asData(0))
                lAddressID = glCInt(asData(1))
                sSearch = asData(2)
                sCategorySearch = asData(3)
                bGroupByCategory = gbCBool(asData(4))
            End If

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Tasks_Filter", lFilter, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Tasks_Search", sSearch, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Tasks_Category", sCategorySearch, False)
            If bGroupByCategory Then
                myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Tasks_GroupByCategory", -1, False)
            Else
                myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Tasks_GroupByCategory", 0, False)
            End If


            If oListView.sOrderCol = "" Then
                oListView.sOrderCol = "TaskDueDate"
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "TaskID AS RowID,TaskDueDate,TaskDesc,'...' AS Cmd, ISNULL(Category," & sOhneString.gs2Sql & ") AS Category"
            sFields &= ",(SELECT MIN(ReadState) FROM tsTaskReads tr WHERE tr.TaskID=tsTasks.TaskID AND tr.UserName=" & sUser.gs2Sql & ") AS ReadState"
            sFields &= ",UserName,tt.TeamDesc"
            sTables = "tsTasks LEFT JOIN tsTaskTeams tt ON tsTasks.TaskTeam=tt.TeamID"

            '---- clause ---------
            Dim sUserTeamClause As String = ""
            Dim sTaskTeams As String = myfactory.BusinessTasks.Base.General.TaskTools.gsGetUserTaskTeams(oClientInfo, sUser)
            Dim sTeam As String = Dictionary.gsTranslate(oClientInfo, "Team") & ": "

            'show tasks with user=username or if user is one of taskusers and has commited this , so state=2
            If sTaskTeams <> "" Then sUserTeamClause = " OR (tsTasks.UserName='*' AND tsTasks.TaskTeam IN (" & sTaskTeams & ")) "


            sClause = "(UserName=" & sUser.gs2Sql & sUserTeamClause & ")" & _
                        " AND ResourcePlanningID IS NULL "

            If lFilter = 2 Then
                sClause = DataTools.gsClauseAnd(sClause, "TaskDone=0")
                sClause = DataTools.gsClauseAnd(sClause, "TaskDueDate>=" & DataTools.gsDate2Sql(Date.Today))
                sClause = DataTools.gsClauseAnd(sClause, "TaskDueDate<" & DataTools.gsDate2Sql(Date.Today.AddDays(7)))
            ElseIf lFilter = 3 Then
                sClause = DataTools.gsClauseAnd(sClause, "TaskDone=0")
            ElseIf lFilter = 4 Then
                sClause = DataTools.gsClauseAnd(sClause, "TaskDone=-1")
            Else
                sClause = DataTools.gsClauseAnd(sClause, "TaskDone=0")
                sClause = DataTools.gsClauseAnd(sClause, "TaskDueDate<" & DataTools.gsDate2Sql(Date.Today.AddDays(1)))
            End If

            If sSearch <> "" Then
                sSearch = Replace(sSearch, "*", "%")
                sClause = DataTools.gsClauseAnd(sClause, "TaskDesc like " & DataTools.gsStr2Sql(sSearch & "%"))
            End If

            If lAddressID > 0 Then
                sClause = sClause.gsClauseAnd("AddressID=" & lAddressID.gs2Sql)
            End If


            If Not String.IsNullOrEmpty(sCategorySearch) Then
                sClause = sClause.gsClauseAnd("Category like '%" & sCategorySearch & "%'")
            End If


            If Not bGroupByCategory Then
                rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                    sFields, sTables, sClause, , sOrder)

                Do Until rs.EOF
                    oRow = oListView.oRows.oAddRow(rs(0).sValue)
                    If rs("UserName").sValue = "*" AndAlso rs("TeamDesc").sValue <> "" Then rs("TaskDesc").Value = rs("TaskDesc").sValue & " (" & sTeam & rs("TeamDesc").sValue & ")"

                    oRow.SetRecord(rs, True)
                    If rs("ReadState").lValue = 0 Then oRow.bRowBold = True

                    rs.MoveNext()
                Loop
                rs.Close()
            Else
                '---------- get categories ----------
                Dim listCategories As New List(Of String)

                rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                    sFields, sTables, sClause, , "Category desc, TaskDueDate")

                '---- get all (not empty) categories ---- 
                Dim sCatString As String = ""
                Do Until rs.EOF
                    sCatString = rs("Category").sValue
                    For Each sCat As String In sCatString.Split(";"c)
                        If Not listCategories.Contains(sCat) Then
                            listCategories.Add(sCat)
                        End If
                    Next
                    rs.MoveNext()
                Loop


                For Each sCategory In listCategories
                    oRow = oListView.oRows.oAddRow(sCategory)
                    oRow.Value(1) = sCategory
                    oRow.bSetEmpty(2)
                    oRow.bGroupStyle = True
                    oRow.sUserData = "-1"

                    rs.MoveFirst()

                    Do Until rs.EOF
                        If (rs("Category").sValue.Contains(sCategory)) Then
                            oRow = oListView.oRows.oAddRow(rs(0).sValue & "_" & sCategory)

                            If rs("UserName").sValue = "*" AndAlso rs("TeamDesc").sValue <> "" Then rs("TaskDesc").Value = rs("TaskDesc").sValue & " (" & sTeam & rs("TeamDesc").sValue & ")"

                            oRow.SetRecord(rs, True)
                            If rs("ReadState").lValue = 0 Then oRow.bRowBold = True
                        End If
                        rs.MoveNext()
                    Loop
                Next

                rs.Close()

            End If


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
