﻿// TasksMain.js (c) myfactory 2010

function mOnLoad() {
    
    // no address id
    if (msPageParams[0] == '0') 
    {
        document.getElementById("trNavi").style.display = "none";
    }

    if (document.all.cboCategory.value != '') 
    {
        document.all.chkGroupByCategory.checked = false;
        document.all.chkGroupByCategory.disabled = true;
    } 
    
    mRefreshList();
}

function mRefreshList()
{
    var sData;

    var sSearchCategoryValue = document.all.cboCategory.value;
         
    sData = document.all.cboFilter.value +
                ';' + msPageParams[0] +
                ';' + document.all.txtSearch.value +
                ';' + sSearchCategoryValue +
                ';' + document.all.chkGroupByCategory.checked;

    gListViewSetUserData('lstMain', sData);
    gListViewLoadPage('lstMain', 1);
}

function mOnSetDirty() 
{

    if (event.type == 'change' || event.type == 'click')
    {
        if (document.all.cboCategory.value != '') 
        {
            document.all.chkGroupByCategory.checked = false;
            document.all.chkGroupByCategory.disabled = true;
        }
        else 
        {
            document.all.chkGroupByCategory.disabled = false;
        }
        mRefreshList();
    }
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    mOnNavigateToDetails(sItemID);
}

function mOnListViewClick(sListView, sItemID) 
{

    // no group rows
    if (gsListViewGetItemUserData(sListView, sItemID) == "-1") 
    {
        return;
    }

    mOnNavigateToDetails(sItemID);
}

function mOnNavigateToDetails(sItemID) {

    var sURL = '../TaskDetails/TasksDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sItemID +
                '&AddressID=' + msPageParams[0];

    document.location = sURL;
}
