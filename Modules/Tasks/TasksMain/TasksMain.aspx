<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Tasks.TasksMain" CodeFile="TasksMain.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" height="98%" cellspacing="2" cellpadding="2" width="98%">
        <tr height="30px">
            <td width="800px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Tasks/Tasksmain/dlgTasksmain.xml">
                </myfactory:wfXmlDialog>
            </td>
            <td width="*">
                &nbsp;
            </td>
        </tr>
        <tr height="*" valign="top">
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="TasksListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom" id="trNavi">
            <td align="right" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblScroll1" sClass="scrollLeftComplete" sText="<">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblScroll2" sClass="scrollRight" sText=">">
                </myfactory:wfLabel>
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sClass="scrollNaviComplete">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
