﻿

function mOnLoad()
{
    mRefreshLstMain();
}
function mRefreshLstMain()
{
    var sParams;

	if (document.all.cboPriceList.value == '')
    {
        document.all.cboPriceList.value = 1;
    }
	
    sParams = document.all.cboFilter.value;
    sParams = sParams + ';' + document.all.cboProductGroup.value;
    sParams = sParams + ';' + document.all.txtSearch.value;
    sParams = sParams + ';' + document.all.cboPriceList.value;

    gListViewSetUserData('lstMain', sParams);
    gListViewLoadPage('lstMain', -1);
}

function mOnSetDirty()
{
    if (event.type == "change")
        mRefreshLstMain();
}

function mNavigateToDetails(sProductID)
{
    var sURL = '../ProductDetails/ProductDetailsMain.aspx' +
                '?ClientID=' + msClientID +
                '&RecordID=' + sProductID;

    document.location = sURL;
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    if (sColID == 'CmdDet')
    {
        mNavigateToDetails(sItemID);
    }
    else
    {
        mAddToCurrentSalesOrder(sItemID);
    }
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mAddToCurrentSalesOrder(sProductID)
{
    var sRes = gsCallServerMethod('../../Salesorders/SalesOrdersEdit/SalesOrdersEditProcess.aspx?Cmd=AddProductExtern' +
                        '&ProductID=' + sProductID);

    alert(sRes.replace("OK;", ""));
}
