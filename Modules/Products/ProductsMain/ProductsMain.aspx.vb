'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProductsMain
'--------------------------------------------------------------------------------
' Created:      06.07.2012 16:02:05, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On



'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Me.dlgMain.oDialog.oField("Filter").oMember.Prop("Value") = myfactory.Sys.Main.User.glGetUserPreference(oClientInfo, "Tab_Products_Filter", False)
            Me.dlgMain.oDialog.oField("Search").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Products_Search", False)
            Me.dlgMain.oDialog.oField("ProductGroup").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Products_Group", False)
            Me.dlgMain.oDialog.oField("PriceList").oMember.Prop("Value") = myfactory.Sys.Main.User.gsGetUserPreference(oClientInfo, "Tab_Price_List", False)

            Me.sOnLoad = "mOnLoad();"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
