<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Products.ProductsMain" CodeFile="ProductsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" Debug="true" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" style="table-layout:fixed;" cellspacing="2" cellpadding="2" width="99%" height="97%">
        <tr height="50px" valign="top">
            <td width="700px">
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Products/ProductsMain/dlgProductsMain.xml" />
            </td>
            <td width="*">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <myfactory:wfListView runat="server" ID="lstMain" sListViewDataPage="ProductsListData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
    </table>
</body>
</html>
