'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductsListData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductsListData
'--------------------------------------------------------------------------------
' Created:      06.07.2012 16:03:56, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Permissions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductsListData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductsListData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 10

            '---- Columns ------------------
            oListView.oCols.oAddCol("ProductNumber", "Artikelnr", "100", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("Name1", "Name1", "400", , , True, False)
            oListView.oCols.oAddCol("ProductType", "Typ", "50", , , True, False)
            oListView.oCols.oAddCol("Stock", "Bestand", "75", , , True, False)
            oListView.oCols.oAddCol("Price", "Preis", "75", , , True, False)
            oListView.oCols.oAddCol("Scale", "Staffel", "50", , , True, False)

            oCol = oListView.oCols.oAddCol("CmdAddToOrder", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.lType = wfEnumAspListViewColTypes.wfColTypeButton
            oCol.sButtonImage = "Image_ShoppingCartEmpty16x16.png"
            oCol.sWidth = "50"

            oCol = oListView.oCols.oAddCol("CmdDet", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "p.ProductID AS RowID, " & _
                "p.ProductNumber, " & _
                "p.Matchcode, " & _
                "p.Name1, " & _
				"CASE WHEN p.ProductType = 1 THEN 'HA' WHEN p.ProductType = 7 THEN 'HS' ELSE 'Other' END, " & _
                "p.Stock, " & _
                "FORMAT(plp.Price, 'C', 'de-de'), " & _
				"CASE WHEN (SELECT SUM(plp2.Quantity) FROM tdPriceListProducts AS plp2 WHERE (plp2.ProductID = p.ProductID)) > 0 THEN 'JA' ELSE 'NEIN' END, " & _
                "'', " & _
                "''"
            sTables = "tdProducts AS p INNER JOIN tdPriceListProducts AS plp ON p.ProductID = plp.ProductID"

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, msGetClause(oClientInfo, oListView.sUserData), , sOrder)
			
            oListView.oRows.SetRecordset(rs, True)
            rs.Close()


            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, msGetClause(oClientInfo, oListView.sUserData))

            Return True

        End Function


        Private Function msGetClause(ByVal oClientInfo As ClientInfo, ByVal sParams As String) As String

            Dim sClause As String = ""
            Dim sPermissionClause As String
            Dim sSearchText As String
            Dim sFavorite As String
            Dim sProductGroup As String
            Dim sPriceList As String

            Dim asParamas As String() = sParams.Split(";"c)

            If asParamas.Length < 4 Then
                sClause = "1=2"
            End If

            sFavorite = asParamas(0)
            sProductGroup = asParamas(1)
            sSearchText = asParamas(2)
            sPriceList = asParamas(3)

            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Products_Filter", sFavorite, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Products_Group", sProductGroup, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Products_Search", sSearchText, False)
            myfactory.Sys.Main.User.gbSetUserPreference(oClientInfo, "Tab_Price_List", sPriceList, False)

            sClause = sClause.gsClauseAnd("p.InActive=0")
            If sFavorite = "1" Then
                sClause = sClause.gsClauseAnd("p.IsFavorite=-1")
            End If

            If sProductGroup <> "" Then
                sClause = sClause.gsClauseAnd("p.ProductGroup=" & sProductGroup.gs2Sql())
            End If

            If sPriceList <> "" Then
                sClause = sClause.gsClauseAnd("plp.PriceListID=CAST(" & sPriceList & " AS INT)")
                sClause = sClause.gsClauseAnd("plp.VariantID=0")
				sClause = sClause.gsClauseAnd("plp.Quantity=0")
            Else
				sClause = sClause.gsClauseAnd("plp.PriceListID=1")
				sClause = sClause.gsClauseAnd("plp.VariantID=0")
				sClause = sClause.gsClauseAnd("plp.Quantity=0")				
			End If

            If sSearchText <> "" Then
                sSearchText = sSearchText.Replace("*", "%")
                Dim sSearchValue As String = (sSearchText & "%").gs2Sql()
                sClause = sClause.gsClauseAnd("(p.Matchcode like " & sSearchValue & _
                                              " OR p.Name1 like " & sSearchValue & _
                                              " OR p.Name2 like " & sSearchValue & _
                                              " OR p.ProductNumber like " & sSearchValue & ")")
            End If


            '---- permissions ----
            sPermissionClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Products")
            If sPermissionClause <> "" Then
                sClause = sClause.gsClauseAnd(sPermissionClause)
            End If

            '--------------------

            Return sClause
        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
