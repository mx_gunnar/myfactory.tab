'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductTurnoverData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductTurnoverData
'--------------------------------------------------------------------------------
' Created:      09.07.2012 15:41:44, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductTurnoverData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, ByVal oListView As AspListView) As Boolean

            Dim sOrder As String
            Dim sClause As String = ""
            Dim sDivisionClause As String
            Dim sRepresentativeClause As String = ""
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oRow As AspListViewRow
            Dim oCol As AspListViewCol
            Dim bRevenue As Boolean
            Dim lProductDecimals As Integer
            Dim sProductBME As String
            Dim rsProduct As Recordset
            Dim lProductID As Integer
            Dim lType As Integer

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductTurnoverData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.bAutoHideNavigation = True
            oListView.lPageSize = 12

            Dim asParams As String() = (oListView.sUserData & ";;").Split(";"c)
            lProductID = glCInt(asParams(0))
            lType = glCInt(asParams(1))

            If lType = 1 Then
                Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
                Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)
                If lRepresentativeID = 0 Then
                    lRepresentativeID = -1
                End If
                sRepresentativeClause = "S.RepresentativeID=" & lRepresentativeID
            End If

            bRevenue = myfactory.Sys.Permissions.Permissions.gbCheckTaskPermission(oClientInfo, "SalesRevenue")

            '---- Columns ------------------
            oCol = oListView.oCols.oAddCol("Periode", "Periode", "*", , , False, False)

            oListView.oCols.oAddCol("Quantity", "Menge", "100", wfEnumDataTypes.wfDataTypeString, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("Turnover", "Umsatz", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)

            If bRevenue Then
                oListView.oCols.oAddCol("Revenue", "Roherl�s", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            End If

            oListView.oCols.oAddCol("QuantityLY", "Menge VJ", "100", wfEnumDataTypes.wfDataTypeString, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("TurnoverLY", "Umsatz VJ", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)

            If bRevenue Then
                oListView.oCols.oAddCol("RevenueLY", "Roherl�s VJ", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            End If

            oCol = oListView.oCols.oAddCol("CmdDetTurnover", "", "50", , wfEnumAligns.wfAlignCenter)
            oCol.SetDefaultButton(wfEnumAspListViewButtonTypes.wfButtonDetails)
            oCol.sWidth = "50"


            '---- Data ---------------------
            rsProduct = DataMethods.grsGetDBRecordset(oClientInfo, "BaseUnit, BaseDecimals", "tdProducts", "tdProducts.ProductID=" & lProductID)
            sProductBME = rsProduct(0).sValue
            lProductDecimals = rsProduct(1).lValue

            sTables = "tdStatSalesDetail S " & _
                        " INNER JOIN tdProducts P ON S.ProductID=P.ProductID "

            sDivisionClause = "s.Combinationsmark<2"
            sDivisionClause = DataTools.gsClauseAnd(sDivisionClause, "P.ProductID=" & lProductID)
            sDivisionClause = DataTools.gsClauseAnd(sDivisionClause, "S.DivisionNr=" & oClientInfo.oClientProperties.lDivisionNr)

            Dim lYear As Integer = oClientInfo.oClientProperties.lCurrentPeriod \ 1000
            lYear = lYear - (oListView.lPage - 1)
            Dim i, lPeriod As Integer
            Dim sPeriod As String
            Dim dtFirst, dtLast As Date
            Dim cTurnover, cTurnoverLY As Decimal
            Dim cRevenue, cRevenueLY As Decimal
            Dim cSumTurnover, cSumTurnoverLY As Decimal
            Dim cSumRevenue, cSumRevenueLY As Decimal
            Dim rsCurrent As Recordset
            Dim rsLast As Recordset
            Dim cSumQuantity, cSumQuantityLY As Decimal
            Dim l As Integer

            For i = 1 To 12 Step 1
                lPeriod = lYear * 1000 + i
                sPeriod = Period.gsGetPeriodCaption(oClientInfo, lPeriod)

                dtFirst = Period.gdtPeriod2Date(oClientInfo, lPeriod, True)
                dtLast = Period.gdtPeriod2Date(oClientInfo, lPeriod, False).AddDays(1)
                sClause = "S.SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                            " AND S.SalesDate<" & DataTools.gsDate2Sql(dtLast)
                sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

                If lType = 1 Then
                    sClause = sClause.gsClauseAnd(sRepresentativeClause)
                End If

                rsCurrent = DataMethods.grsGetDBRecordset(oClientInfo, "SUM(S.Turnover), SUM(S.Revenue), SUM(S.Quantity)", sTables, sClause)
                cTurnover = rsCurrent(0).cValue
                cRevenue = rsCurrent(1).cValue
                cSumTurnover += cTurnover
                cSumRevenue += cRevenue
                cSumQuantity += rsCurrent(2).cValue

                dtFirst = dtFirst.AddYears(-1)
                dtLast = dtLast.AddYears(-1)
                sClause = "S.SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & _
                            " AND S.SalesDate<" & DataTools.gsDate2Sql(dtLast)
                sClause = DataTools.gsClauseAnd(sClause, sDivisionClause)

                If lType = 1 Then
                    sClause = sClause.gsClauseAnd(sRepresentativeClause)
                End If

                rsLast = DataMethods.grsGetDBRecordset(oClientInfo, "SUM(S.Turnover), SUM(S.Revenue), SUM(S.Quantity)", sTables, sClause)
                cTurnoverLY = rsLast(0).cValue
                cRevenueLY = rsLast(1).cValue
                cSumTurnoverLY += cTurnoverLY
                cSumRevenueLY += cRevenueLY
                cSumQuantityLY += rsLast(2).cValue

                '------- add row ------
                l = 3

                oRow = oListView.oRows.oAddRow(Str(lPeriod))
                oRow.sValue(0) = sPeriod

                If rsCurrent(2).lValue > 0 Then
                    oRow.sValue(1) = gsFormatNumber(rsCurrent(2).cValue, lProductDecimals) & " " & sProductBME
                End If

                oRow.Value(2) = cTurnover
                If bRevenue Then
                    oRow.Value(3) = cRevenue
                    l += 1
                End If

                If rsLast(2).lValue > 0 Then
                    oRow.sValue(l) = gsFormatNumber(rsLast(2).cValue, lProductDecimals) & " " & sProductBME
                End If
                l += 1

                oRow.Value(l) = cTurnoverLY
                l += 1
                If bRevenue Then
                    oRow.Value(l) = cRevenueLY
                End If
            Next

            '------ add sum row -------
            l = 3
            oRow = oListView.oRows.oAddRow("-1")
            oRow.Value(0) = "Summe"
            oRow.Value(1) = gsFormatNumber(cSumQuantity, lProductDecimals) & " " & sProductBME
            oRow.Value(2) = cSumTurnover

            If bRevenue Then
                oRow.Value(3) = cSumRevenue
                l += 1
            End If

            oRow.Value(l) = gsFormatNumber(cSumQuantityLY, lProductDecimals) & " " & sProductBME
            l += 1
            oRow.Value(l) = cSumTurnoverLY
            l += 1

            If bRevenue Then
                oRow.Value(l) = cSumRevenueLY
                l += 1
            End If

            ' hide detail-button in sum-row
            oRow.bSetEmpty(l)
            oRow.bGroupStyle = True

            oListView.lRecordCountTotal = 12 * DataMethods.glGetDBCount(oClientInfo, "tdPeriods", "Period<" & Date.Now.Year & "999", "DISTINCT(Period/1000)")

            Return True

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
