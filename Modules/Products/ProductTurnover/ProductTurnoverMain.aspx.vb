'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductTurnoverMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProductTurnoverMain
'--------------------------------------------------------------------------------
' Created:      09.07.2012 15:40:40, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductTurnoverMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad


            Dim lRecordID As Integer = glCInt(Me.Request.QueryString("RecordID"))
            Dim sType As String = Request.QueryString("Type")

            If String.IsNullOrEmpty(sType) Then
                sType = "0"
            ElseIf sType = "Representative" Then
                sType = "1"
            End If


            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lRecordID)
            Me.asPageParams(1) = sType

            Me.sOnLoad = "mOnLoad();"

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean
            Dim sMenuClause As String = ""
            If sType = "1" Then
                sMenuClause = "ModuleName<>'Products_TurnoverRepresentative'"
            Else
                sMenuClause = "ModuleName<>'Products_Turnover'"
            End If

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ProductDetails' AND " & sMenuClause, _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProductDetails", "../../../", "RecordID=" & lRecordID))

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)

                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
