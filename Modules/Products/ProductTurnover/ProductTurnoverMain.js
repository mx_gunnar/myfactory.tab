﻿


function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    // lstTurnover
    gListViewSetUserData('lstTurnover', msPageParams[0] + ';' + msPageParams[1]);
    gListViewLoadPage('lstTurnover', -1);
}

function mOnListViewBtnClick(sListView, sColID, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mOnListViewClick(sListView, sItemID)
{
    mNavigateToDetails(sItemID);
}

function mNavigateToDetails(sPeriod)
{

    var sParams = "&RecordID=" + msPageParams[0];
    sParams = sParams + "&Period=" + sPeriod;
    sParams = sParams + "&Type=" + msPageParams[1];

    var sURL = '../ProductTurnoverCustomer/ProductTurnoverCustomerMain.aspx' +
                '?ClientID=' + msClientID + sParams;

    document.location = sURL;
}