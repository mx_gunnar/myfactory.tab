<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Products.ProductTurnoverMain" CodeFile="ProductTurnoverMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <form name="frmMain">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="99%" height="97%">
        <tr height="*">
            <td>
                <myfactory:wfListView ID="lstTurnover" sListViewDataPage="ProductTurnoverData.aspx"
                    runat="server">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom">
            <td align="right">
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
