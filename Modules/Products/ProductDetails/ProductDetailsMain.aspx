<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Product.ProductDetailsMain" CodeFile="ProductDetailsMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="100%" height="97%">
        <tr valign="top" height="200px">
            <td>
                <myfactory:wfXmlDialog runat="server" ID="dlgMain" sDialog="/tab/modules/Products/ProductDetails/dlgProductDetails.xml" />
            </td>
        </tr>
        <tr valign="top" height="150px">
            <td>
                <table width="100%">
                    <tr height="150px">
                        <td align="center" valign="middle">
                            <img width="150" height="150" id="imgProduct1" border="0" src="" runat="server" />
                        </td>
                        <td align="center" valign="middle">
                            <img width="150" height="150" id="imgProduct2" border="0" src="" runat="server" />
                        </td>
                        <td align="center" valign="middle">
                            <img width="150" height="150" id="imgProduct3" border="0" src="" runat="server" />
                        </td>
                        <td align="center" valign="middle">
                            <img width="150" height="150" id="imgProduct4" border="0" src="" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="120px" valign="bottom">
            <td align="right">
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
