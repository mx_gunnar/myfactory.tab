﻿

function mOnLoad()
{
    mSetImages();
}

function mSetImages()
{
    for (i = 1; i <= 4; i++)
    {
        var sImgCtlName = "imgProduct" + i;
        
        if (msPageParams[i] == '')
        {
            document.getElementById(sImgCtlName).style.visibility = "hidden"
        }
        else
        {
            document.getElementById(sImgCtlName).src = msPageParams[i];
        }
    }
}

function cmdAddToOrder_OnClick()
{
    var sRes = gsCallServerMethod('../../Salesorders/SalesOrdersEdit/SalesOrdersEditProcess.aspx?Cmd=AddProductExtern' +
                    '&ProductID=' + msPageParams[0]);

    alert(sRes.replace("OK;", ""));
}