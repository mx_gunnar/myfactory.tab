'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductDetailsMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProductDetailsMain
'--------------------------------------------------------------------------------
' Created:      06.07.2012 17:02:36, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Product

    Partial Class ProductDetailsMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Request.QueryString("RecordID"))
            ReDim Me.asPageParams(5)
            Me.asPageParams(0) = lRecordID.ToString

            '------------- load details -----------------
            If lRecordID <> 0 Then

                Dim sFields As String
                Dim sProductGrpSubSelect, sProductTypeSubSelect, sMainSupplierSubSelect, sManufacturerNamer As String

                sProductGrpSubSelect = "(Select ProductGroupDesc from tdProductGroups where ProductGroupName=tdProducts.ProductGroup) AS ProductGroup, "
                sProductTypeSubSelect = "(Select ProductTypeDesc from tdProductTypes where tdProductTypes.ProductTypeID=tdProducts.ProductType) AS ProductType,"
                sMainSupplierSubSelect = "(Select A.Matchcode from tdSuppliers S INNER JOIN tdAddresses A on A.AddressID=S.AddressID where S.SupplierID=tdProducts.MainSupplier) AS MainSupplier, "
                sManufacturerNamer = "(Select ManufacturerName from tdManufacturers M INNER JOIN tdProducts P ON P.ManufacturerID=M.ManufacturerID WHERE P.ProductID=tdProducts.ProductID) AS Manufacturer,"

                sFields = "ProductID, " & _
                    "Matchcode, " & _
                    sProductGrpSubSelect & _
                    sProductTypeSubSelect & _
                    "ProductNumber, " & _
                    "Name1, " & _
                    "Name2, " & _
                    "EANNumber, " & _
                    sMainSupplierSubSelect & _
                    "ProductWidth, " & _
                    "ProductLength, " & _
                    "ProductWeight, " & _
                    "ProductHeight, " & _
                    "WeightUnit, " & _
                    sManufacturerNamer & _
                    "ManufacturerNumber, " & _
                    "ProcurementTime"

                Dim rs As Recordset

                rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, "tdProducts", "ProductID=" & lRecordID)

                Do Until rs.EOF
                    If rs("ProductHeight").lValue = 0 Then
                        rs("ProductHeight").Value = ""
                    End If
                    If rs("ProductLength").lValue = 0 Then
                        rs("ProductLength").Value = ""
                    End If
                    If rs("ProductWidth").lValue = 0 Then
                        rs("ProductWidth").Value = ""
                    End If
                    If rs("ProductWeight").lValue = 0 Then
                        rs("ProductWeight").Value = ""
                    End If
                    If rs("ProcurementTime").lValue = 0 Then
                        rs("ProcurementTime").Value = ""
                    End If
                    rs.MoveNext()
                Loop

                rs.MoveFirst()

                If Not rs.EOF Then
                    Me.dlgMain.sValues = DataTools.gsRecord2Xml(rs)
                End If

            End If

            '---------- load image links ---------
            Dim rsImages As Recordset
            Dim sTablesDoc As String = "tdDocumentReferences DR" & _
                                        " INNER JOIN tdDocuments D ON D.DocumentID=DR.DocumentID" & _
                                        " INNER JOIN tsDocumentTypes DT ON DT.DocumentTypeID=D.DocumentType"
            'DR.TypeID=2  for Products
            rsImages = DataMethods.grsGetDBRecordsetPage(oClientInfo, 1, 4, "DR.DocumentID", sTablesDoc, _
                                                         "DR.ReferenceID=" & lRecordID.gs2Sql & " AND DR.TypeID=2 AND DT.DocumentType='Image'", , "D.CreationDate")

            For i As Integer = 1 To rsImages.RecordCount
                Me.asPageParams(i) = myfactory.Sys.Document.DocDocumentsT.gsGetDocumentLink(oClientInfo, False, rsImages("DocumentID").lValue)
                rsImages.MoveNext()
            Next
            rsImages.Close()

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ProductDetails'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString

            '------------------------------------


            Me.sOnLoad = "mOnLoad()"

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
