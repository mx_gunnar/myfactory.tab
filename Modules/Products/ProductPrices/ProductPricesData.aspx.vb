'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductPricesData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductPricesData
'--------------------------------------------------------------------------------
' Created:      09.07.2012 11:20:42, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.FrontendSystem.AspTools


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductPricesData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
			Dim sPermissionClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim lProductID As Integer = glCInt(oListView.sUserData)

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 15
            oListView.sListViewDataPage = "ProductPricesData.aspx"

            '---- Columns ------------------
            oListView.oCols.oAddCol("tdPriceLists.PriceListID", "Preisliste", "*", , , False, False)
            oListView.oCols.oAddCol("Quantity", "ab Menge", "100", , wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("BaseUnit", "BME", "60", , wfEnumAligns.wfAlignLeft, False, False)
            oListView.oCols.oAddCol("Price", "Preis", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("WKZ", "Wkz", "50", , wfEnumAligns.wfAlignLeft, False, False)


            '---- Data ---------------------
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "tdPriceLists.PriceListID, Quantity, Price"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "PriceID AS RowID," & _
                    "tdPriceLists.PriceListDesc," & _
                    "Quantity," & _
                    "tdProducts.BaseUnit," & _
                    "Price, " & _
                    "tdPriceLists.CurrUnit"

            sTables = "tdPriceListProducts" & _
                " INNER JOIN tdPriceLists ON tdPriceLists.PriceListID=tdPriceListProducts.PriceListID" & _
                " INNER JOIN tdProducts ON tdProducts.ProductID=tdPriceListProducts.ProductID"

            sClause = "tdPriceListProducts.ProductID=" & lProductID.gs2Sql()
            sClause = sClause.gsClauseAnd("(tdPriceLists.ValidFrom IS NULL OR tdPriceLists.ValidFrom <= " & Date.Now.gs2Sql() & ")")
            sClause = sClause.gsClauseAnd("(tdPriceLists.ValidTo IS NULL OR tdPriceLists.ValidTo >= " & Date.Now.gs2Sql() & ")")
            sClause = sClause.gsClauseAnd("tdPriceLists.Inactive=0")

            '---- permissions ----
            sPermissionClause = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "PriceLists")
            If sPermissionClause <> "" Then
                sClause = sClause.gsClauseAnd(sPermissionClause)
            End If
            '--------------------			
			
            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)

            Dim sListName As String = ""

            Do Until rs.EOF
                oRow = oListView.oRows.oAddRow(rs(0).sValue)
                oRow.SetRecord(rs, True)

                If sListName = "" Then
                    sListName = rs("PriceListDesc").sValue
                Else
                    If sListName = rs("PriceListDesc").sValue Then
                        oRow.bSetEmpty(0)
                    Else
                        sListName = rs("PriceListDesc").sValue
                    End If
                End If

                rs.MoveNext()
            Loop

            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
