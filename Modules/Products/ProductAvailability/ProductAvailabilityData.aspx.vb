'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductAvailabilityData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductAvailabilityData
'--------------------------------------------------------------------------------
' Created:      09.07.2012 17:23:36, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		02.10.2012 ABuhleier        Product Variants developed
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================


Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Main
Imports myfactory.BusinessTasks.Stock.StockMain
Imports myfactory.BusinessTasks.Stock.StockReservation
Imports System.Collections.Generic


'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductAvailabilityData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sGroup As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim oCol As AspListViewCol
            Dim oRow As AspListViewRow
            Dim lProductID As Integer
            Dim lVariantID As Integer
            Dim sBME As String
            Dim lDivisionNr, lDivisionNrAvail As Integer
            Dim cSum, cSumVariante, cNoDispo As Decimal
            Dim lRecordCount As Integer
            Dim cCurrValue As Decimal
            Dim bBreakAdded As Boolean
            Dim l As Integer

            Dim bVariants As Boolean = Editions.gbCheckUserEditions(oClientInfo, "WWS4")
            Dim lstVariantIDs As List(Of Integer) = Nothing


            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductAvailabilityData.aspx"
            oListView.bTabletScrollMode = True
            oListView.bTabletMode = True
            oListView.lPageSize = 999999
            oListView.bAutoHideNavigation = True


            '---- Columns ------------------
            oListView.oCols.oAddCol("Stock", "", "*", , , False, False)
            oListView.oCols.oAddCol("Quantity", "Menge", "200", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, False, False)
            oListView.oCols.oAddCol("BME", "BME", "50", , , False, False)


            '---- Data ---------------------
            lProductID = glCInt(oListView.sUserData.Split(";"c)(0))
            lVariantID = glCInt(oListView.sUserData.Split(";"c)(1))


            If GeneralProperties.gbGetGeneralProperty(oClientInfo, "SalesShowStockDivisionOnly") Then
                lDivisionNr = oClientInfo.oClientProperties.lDivisionNr
            End If
            If GeneralProperties.gbGetGeneralProperty(oClientInfo, "SalesShowAvailableDivisionOnly") Then
                lDivisionNrAvail = oClientInfo.oClientProperties.lDivisionNr
            End If

            sBME = DataMethods.gsGetDBValue(oClientInfo, "BaseUnit", "tdProducts", "ProductID=" & lProductID)
            sClause = "tdProductStockInfo.ProductID=" & lProductID


            '----- get variant ids -------
            If bVariants Then
                lstVariantIDs = New List(Of Integer)
                Dim rsVariants As Recordset
                rsVariants = DataMethods.grsGetDBRecordset(oClientInfo, "distinct MainVariantID", "tdProductVariants", "ProductID=" & lProductID & " AND MainVariantID > 0")

                Do Until rsVariants.EOF
                    lstVariantIDs.Add(rsVariants(0).lValue)
                    rsVariants.MoveNext()
                Loop
            End If


            '================== stock sum ================
            If lVariantID <> 0 Then
                cSum = DataMethods.gcGetDBValue(oClientInfo, "SUM(Stock)", "tdProductStockInfo", sClause & " AND DivisionNr=" & lDivisionNr & " AND VariantID=" & lVariantID)

                oRow = oListView.oRows.oAddRow("-1_" & lVariantID)
                oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Lagerbestand:") & " " & DataMethods.gsGetDBValue(oClientInfo, "VariantDesc", "tdProductVariants", "MainVariantID= " & lVariantID & " AND ProductID= " & lProductID)
                oRow.Value(1) = gsFormatNumber(cSum, 2)
                oRow.sValue(2) = sBME

                lRecordCount += 1
            Else
                cSum = DataMethods.gcGetDBValue(oClientInfo, "SUM(Stock)", "tdProductStockInfo", sClause & " AND DivisionNr=" & lDivisionNr & " AND VariantID=" & lVariantID)

                oRow = oListView.oRows.oAddRow("-1")
                oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Lagerbestand")
                oRow.Value(1) = gsFormatNumber(cSum, 2)
                oRow.sValue(2) = sBME

                lRecordCount += 1

            End If


            '================== no dispo ================
            If lVariantID <> 0 Then
                cNoDispo = StockProductInfo.gcGetWarehouseStockNoDispo(oClientInfo, lProductID, lVariantID, lDivisionNr)
            Else
                cNoDispo = StockProductInfo.gcGetWarehouseStockNoDispo(oClientInfo, lProductID, 0, lDivisionNr)
            End If

            If cNoDispo <> 0 Then
                cSum -= cNoDispo

                oRow = oListView.oRows.oAddRow("-2")
                oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Gesperrt")
                oRow.Value(1) = gsFormatNumber(-cNoDispo, 2)
                oRow.sValue(2) = sBME
                lRecordCount += 1

            End If

            '================== external dispo ================
            If lVariantID <> 0 Then
                cNoDispo = StockProductInfo.gcGetWarehouseStockExternalDispo(oClientInfo, lProductID, lVariantID, lDivisionNr)
            Else
                cNoDispo = StockProductInfo.gcGetWarehouseStockExternalDispo(oClientInfo, lProductID, 0, lDivisionNr)
            End If

            If cNoDispo <> 0 Then
                cSum += cNoDispo

                oRow = oListView.oRows.oAddRow("-3")
                oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Fremdfertigung zugewiesen")
                oRow.Value(1) = gsFormatNumber(cNoDispo, 2)
                oRow.sValue(2) = sBME
                lRecordCount += 1

            End If

            '================== reserved ================
            If lVariantID <> 0 Then
                cNoDispo = ReservationFunctions.gcAssignedQuantity(oClientInfo, lProductID, lVariantID, 0, 0, True, True)
            Else
                cNoDispo = ReservationFunctions.gcAssignedQuantity(oClientInfo, lProductID, 0, 0, 0, True, True)
            End If

            If cNoDispo <> 0 Then
                cSum -= cNoDispo

                oRow = oListView.oRows.oAddRow("-4")
                oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "Reserviert")
                oRow.Value(1) = gsFormatNumber(-cNoDispo, 2)
                oRow.sValue(2) = sBME
                lRecordCount += 1

            End If




            '================== anything else...  ================
            sFields = "SUM(PP.Quantity) AS Quantity, " & _
                    "PPT.TypeName AS TypeName"

            sTables = "tdProductPlanning PP" & _
                    " INNER JOIN tdProductPlanningTypes PPT ON PPT.TypeID=PP.PlanningType"

            sOrder = "Quantity desc"
            sGroup = "TypeName"

            sClause = "PP.ProductID=" & lProductID

            If lVariantID <> 0 Then
                sClause = sClause.gsClauseAnd("PP.VariantID=" & lVariantID)
            End If

            If lDivisionNrAvail <> 0 Then
                sClause = DataTools.gsClauseAnd(sClause, "DivisionNr=" & lDivisionNrAvail)
            End If

            rs = DataMethods.grsGetDBRecordset(oClientInfo, sFields, sTables, sClause, sGroup, sOrder)

            Do Until rs.EOF

                cCurrValue = rs("Quantity").cValue
                cCurrValue = cCurrValue * -1
                cSum += cCurrValue

                '============= now availibil ============
                If (cCurrValue > 0) Then
                    If Not bBreakAdded Then

                        oRow = oListView.oRows.oAddRow("-5")
                        oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "jetzt verf�gbar")
                        oRow.Value(1) = gsFormatNumber(cSum - cCurrValue, 2)
                        oRow.sValue(2) = sBME
                        oRow.bGroupStyle = True
                        lRecordCount += 1

                        bBreakAdded = True
                    End If
                End If

                l += 1
                oRow = oListView.oRows.oAddRow(CStr(l))
                oRow.sValue(0) = rs("TypeName").sValue
                oRow.Value(1) = gsFormatNumber(cCurrValue, 2)
                oRow.sValue(2) = sBME
                lRecordCount += 1

                rs.MoveNext()
            Loop



            ' ================= total availabil sum row ===============
            oRow = oListView.oRows.oAddRow("-6")
            oRow.sValue(0) = Dictionary.gsTranslate(oClientInfo, "verf�gbar")
            oRow.Value(1) = gsFormatNumber(cSum, 2)
            oRow.sValue(2) = sBME
            oRow.bGroupStyle = True
            lRecordCount += 1

            oListView.lRecordCountTotal = -1

            Return True

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
