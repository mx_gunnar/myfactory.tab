﻿

function mOnLoad()
{
    mRefreshList();
}

function mRefreshList()
{
    // lstAvailability
    var lVarID = 0;
    if (msPageParams[1] == 'true')
    {
        lVarID = document.all.cboVariants.value;
    }

    gListViewSetUserData('lstAvailability', msPageParams[0] + ';' + lVarID);
    gListViewLoadPage('lstAvailability', -1);
}

function mOnSetDirty()
{
    if (event.srcElement.id == 'cboVariants')
    {
        mRefreshList();        
    }
}