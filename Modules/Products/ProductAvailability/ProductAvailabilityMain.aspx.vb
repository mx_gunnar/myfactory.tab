'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductAvailability.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProductAvailability
'--------------------------------------------------------------------------------
' Created:      09.07.2012 17:21:55, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================


Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Tools.DataTypeFunctions

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductAvailabilityMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim lRecordID As Integer = glCInt(Me.Request.QueryString("RecordID"))
            ReDim Me.asPageParams(2)
            Me.asPageParams(0) = Str(lRecordID)

            '-------- write navi ---------------
            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ProductDetails' AND ModuleName<>'Products_Availability'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProductDetails", "../../../", "RecordID=" & lRecordID))

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)

                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & lRecordID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString

            '------------------------------------

            Me.sOnLoad = "mOnLoad();"

            'set variant drop down
            If Editions.gbCheckUserEditions(oClientInfo, "WWS4") And DataMethods.glGetDBCount(oClientInfo, "tdProductVariants", "ProductID=" & lRecordID & " AND MainVariantID > 0") > 0 Then
                Me.asPageParams(1) = "true"
                Me.cboVariants.sSource = "VariantsMain"
                Me.cboVariants.bNullEntry = True
                Me.cboVariants.sParams = lRecordID.ToString
            Else
                Me.cboVariants.Visible = False
                Me.lblVariante.Visible = False
            End If


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
