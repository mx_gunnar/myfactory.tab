<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls" Assembly="wfAspControls" %>
<%@ Page inherits="ASP.Tab.Products.ProductTurnoverCustomerMain" codefile="ProductTurnoverCustomerMain.aspx.vb" EnableViewState="false" AutoEventWireup="false" Language="vb" %>


<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="98%" height="97%">
        <tr>
            <td>
                <myfactory:wfListView runat="server" ID="lstTurnoverCustomer" sListViewDataPage="ProductTurnoverCustomerData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
                <tr height="120px" valign="bottom">
            <td align="right">
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
