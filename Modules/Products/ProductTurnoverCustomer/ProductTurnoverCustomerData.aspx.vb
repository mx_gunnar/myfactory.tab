'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductTurnoverCustomerData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductTurnoverCustomerData
'--------------------------------------------------------------------------------
' Created:      10.07.2012 11:52:59, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools
Imports myfactory.Sys.Main

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductTurnoverCustomerData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean

            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sRepresentativeClause As String = ""
            Dim sFields As String
            Dim dtFirst, dtLast As Date
            Dim sTables As String
            Dim oFilter As AspListViewFilter
            Dim bRevenue As Boolean

            Dim lProductID As Integer
            Dim lPeriod As Integer
            Dim lType As Integer

            Dim asParams As String() = (oListView.sUserData & ";;;").Split(";"c)
            lProductID = glCInt(asParams(0))
            lPeriod = glCInt(asParams(1))
            lType = glCInt(asParams(2))

            If lType = 1 Then 'Representative only
                Dim lAddressID As Integer = DataMethods.glGetDBValue(oClientInfo, "AddressID", "tdEmployees", "UserAccount=" & oClientInfo.oClientProperties.sCurrentUser.gs2Sql)
                Dim lRepresentativeID As Integer = DataMethods.glGetDBValue(oClientInfo, "RepresentativeID", "tdRepresentatives", "AddressID=" & lAddressID)
                If lRepresentativeID = 0 Then
                    lRepresentativeID = -1
                End If
                sRepresentativeClause = "S.RepresentativeID=" & lRepresentativeID
            End If



            bRevenue = myfactory.Sys.Permissions.Permissions.gbCheckTaskPermission(oClientInfo, "SalesRevenue")

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductTurnoverCustomerData.aspx"
            oListView.bTabletMode = True
            oListView.bAutoHideNavigation = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 15

            '---- Columns ------------------
            oListView.oCols.oAddCol("CustomerNumber", "Kundennr", "95", , , True, False)
            oListView.oCols.oAddCol("Matchcode", "Kurzbezeichnung", "*", , , True, False)
            oListView.oCols.oAddCol("Date", "Datum", "90", wfEnumDataTypes.wfDataTypeDate, , True, False)
            oListView.oCols.oAddCol("Quant", "Menge", "100", , wfEnumAligns.wfAlignRight, True, False)
            oListView.oCols.oAddCol("BME", "BME", "50", , , False, False)
            oListView.oCols.oAddCol("Turno", "Umsatz", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            If bRevenue Then
                oListView.oCols.oAddCol("Reve", "Roherl�s", "100", wfEnumDataTypes.wfDataTypeNumber, wfEnumAligns.wfAlignRight, True, False)
            End If


            '---- Data ---------------------    
            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "S.SalesDate desc"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "S.OrderPosId AS RowId, " & _
                        "C.CustomerNumber AS CustomerNumber," & _
                        "A.Matchcode AS Matchcode, " & _
                        "S.SalesDate AS Date, " & _
                        "S.Quantity AS Quant, " & _
                        "P.BaseUnit AS BME, " & _
                        "S.Turnover AS Turno "
            If bRevenue Then
                sFields = sFields & ", S.Revenue AS Reve"
            End If


            sTables = "tdStatSalesDetail S " & _
                        " INNER JOIN tdProducts P ON S.ProductID=P.ProductID " & _
                        " INNER JOIN tdCustomers C ON C.CustomerID=S.CustomerID " & _
                        " INNER JOIN tdAddresses A ON A.AddressID=C.AddressID "

            dtFirst = Period.gdtPeriod2Date(oClientInfo, lPeriod, True)
            dtLast = Period.gdtPeriod2Date(oClientInfo, lPeriod, False).AddDays(1)

            sClause = "P.ProductID=" & lProductID & " AND "
            sClause = sClause & "S.SalesDate>=" & DataTools.gsDate2Sql(dtFirst) & " AND S.SalesDate<" & DataTools.gsDate2Sql(dtLast)

            If lType = 1 Then
                sClause = sClause.gsClauseAnd(sRepresentativeClause)
            End If

            'PermissionClause Customer
            Dim sPermissionClauseCustomer As String = EntityPermissions.gsGetEntityPermissionClause(oClientInfo, "Customers")
            sPermissionClauseCustomer = sPermissionClauseCustomer.Replace("tdCustomers.", "C.")
            sClause = sClause.gsClauseAnd(sPermissionClauseCustomer)

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, sFields, sTables, sClause, , sOrder)

            oListView.oRows.SetRecordset(rs, True)
            rs.Close()

            oListView.lRecordCountTotal = DataMethods.glGetDBCount(oClientInfo, sTables, sClause)

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================

