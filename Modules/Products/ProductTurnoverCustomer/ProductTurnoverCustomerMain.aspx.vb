'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductTurnoverCustomerMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ProductTurnoverCustomerMain
'--------------------------------------------------------------------------------
' Created:      10.07.2012 11:52:23, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductTurnoverCustomerMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad

            Dim sProductID As String = Me.Request.QueryString("RecordID")
            Dim sPeriod As String = Me.Request.QueryString("Period")
            Dim lType As Integer = glCInt(Me.Request.QueryString("Type"))

            ReDim Me.asPageParams(3)
            Me.asPageParams(0) = sProductID
            Me.asPageParams(1) = sPeriod
            Me.asPageParams(2) = lType.ToString


            Me.sOnLoad = "mOnLoad();"





            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean

            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                        "ModuleName,Edition,PartnerID,ModuleID", _
                        "tsTabModules", _
                        "ParentModule='ProductDetails'", _
                        , "ShowIndex", _
                        wfDataSourceSystem)

            osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, "ProductDetails", "../../../", "RecordID=" & sProductID))

            Do Until frs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)

                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteSubNaviLink(oClientInfo, frs(0).sValue, "../../../", "RecordID=" & sProductID))

                frs.MoveNext()
            Loop

            Me.lblNaviSub.sTranslatedText = osHTML.sString
        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
