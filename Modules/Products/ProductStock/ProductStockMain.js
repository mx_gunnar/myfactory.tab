﻿

function mOnLoad()
{
    mRefreshListOwn();
    mRefreshListAll();
}

function mRefreshListOwn()
{
    var lVarID = 0;
    if (msPageParams[1] == 'true')
    {
        lVarID = document.all.cboVariante.value;
    }

    gListViewSetUserData('lstStockOwnDiv', msPageParams[0] + ';own;' + lVarID + ";" + document.all.cboShowUnit.value);
    gListViewLoadPage('lstStockOwnDiv', -1);
}

function mRefreshListAll()
{
    var lVarID = 0;
    if (msPageParams[1] == 'true')
    {
        lVarID = document.all.cboVariante.value;
    }

    gListViewSetUserData('lstStockAllDiv', msPageParams[0] + ';all;' + lVarID + ";" + document.all.cboShowUnit.value);
    gListViewLoadPage('lstStockAllDiv', -1);
}

function mOnSetDirty()
{
    mOnLoad();
}