<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Products.ProductStockMain" CodeFile="ProductStockMain.aspx.vb"
    EnableViewState="false" AutoEventWireup="false" Language="vb" %>

<html>
<head>
    <link href="../../../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="2" cellpadding="2" width="98%" height="97%">
        <%--<tr height="25px" valign="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td width="50px" align="left" valign="middle">
                            <myfactory:wfLabel runat="server" sText="Variante: " ID="lblVariante">
                            </myfactory:wfLabel>
                        </td>
                        <td align="left">
                            <myfactory:wfDataCombo ID="cboVariants" runat="server" sOnChange="mOnSetDirty();">
                            </myfactory:wfDataCombo>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>--%>
        <tr  height="20px" valign="top">
            <td colspan="2">
                <myfactory:wfXmlDialog ID="dlgSettings" runat="server" sDialog="/tab/modules/Products/ProductStock/dlgProductStockSettings.xml" />
            </td>
        </tr>
        <tr height="20px" valign="top">
            <td width="50%">
                <myfactory:wfXmlDialog ID="dlgOwn" runat="server" sDialog="/tab/modules/Products/ProductStock/dlgProductStockOwn.xml" />
            </td>
            <td width="50%">
                <myfactory:wfXmlDialog ID="dlgAll" runat="server" sDialog="/tab/modules/Products/ProductStock/dlgProductStockAll.xml" />
            </td>
        </tr>
        <tr height="*">
            <td width="50%">
                <myfactory:wfListView runat="server" ID="lstStockOwnDiv" sListViewDataPage="ProductStockData.aspx">
                </myfactory:wfListView>
            </td>
            <td width="50%">
                <myfactory:wfListView runat="server" ID="lstStockAllDiv" sListViewDataPage="ProductStockData.aspx">
                </myfactory:wfListView>
            </td>
        </tr>
        <tr height="120px" valign="bottom">
            <td align="right" colspan="2">
                <myfactory:wfLabel runat="server" ID="lblNaviSub" sStyle="margin-bottom:30px">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
