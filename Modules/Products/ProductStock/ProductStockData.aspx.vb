'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ProductStockOwnDivData.aspx
'--------------------------------------------------------------------------------
' Purpose:      ListDataPage for ProductStockOwnDivData
'--------------------------------------------------------------------------------
' Created:      09.07.2012 14:10:22, ABuhleier
'--------------------------------------------------------------------------------
' Changed:		02.10.2012 - ABuhleier      - product variants implemented
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Permissions
Imports myfactory.Sys.Tools.DataTypeFunctions
Imports myfactory.Sys.Tools.BasicFunctions

Imports myfactory.BusinessTasks.Sales.Orders
Imports myfactory.FrontendSystem.AspTools
Imports myfactory.Sys.Main
Imports myfactory.BusinessTasks.Base.ProductUnits

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab.Products

    Partial Class ProductStockData
        Inherits myfactory.FrontendSystem.AspSystem.ListViewDataPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       bFillListView
        '--------------------------------------------------------------------------------
        ' Purpose:      Fill Data into ListView
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo     - ClientInfo
        '               oListView       - ListView Object to fill data into
        '--------------------------------------------------------------------------------
        ' Return:       true/false
        '================================================================================

        Public Overrides Function bFillListView(ByVal oClientInfo As ClientInfo, _
                    ByVal oListView As AspListView) As Boolean


            Dim rs As Recordset
            Dim sOrder As String
            Dim sClause As String = ""
            Dim sFields As String
            Dim sTables As String
            Dim sGroup As String
            Dim oFilter As AspListViewFilter
            Dim lShowUnit As Integer

            Dim lProductID As Integer
            Dim sDivisions As String = ""

            Dim lVariantID As Integer
            Dim bVariants As Boolean = False
            Dim sVariantIDs As String = ""

            Dim asParams As String() = (oListView.sUserData & ";;;;;;").Split(";"c)

            If asParams.Length < 3 Then
                sClause = " 1=2 "
            Else
                lProductID = glCInt(asParams(0))
                sDivisions = asParams(1)
                lVariantID = glCInt(asParams(2))
                lShowUnit = glCInt(asParams(3))
            End If

            If Editions.gbCheckUserEditions(oClientInfo, "WWS4") Then
                If DataMethods.glGetDBCount(oClientInfo, "tdProductVariants", "ProductID=" & lProductID & " AND MainVariantID > 0") > 0 Then
                    bVariants = True
                End If
            End If

            '---- ListView Properties ------
            oListView.bPageNavigation = True
            oListView.sListViewDataPage = "ProductStockData.aspx"
            oListView.bTabletMode = True
            oListView.bTabletScrollMode = True
            oListView.lPageSize = 12
            oListView.bAutoHideNavigation = False

            '---- Columns ------------------
            oListView.oCols.oAddCol("HouseName", "Lager", "*", , , True, False)
            oListView.oCols.oAddCol("Quantity", "Menge", "100", , wfEnumAligns.wfAlignRight, True, False)

            If lShowUnit = 0 Then
                oListView.oCols.oAddCol("ShowUnit", "BME", "60", , , False, False)
            Else
                oListView.oCols.oAddCol("ShowUnit", "LME", "60", , , False, False)
            End If



            '---- Data ---------------------

            If bVariants Then
                Dim rsVariants As Recordset
                rsVariants = DataMethods.grsGetDBRecordset(oClientInfo, "distinct MainVariantID", "tdProductVariants", "ProductID=" & lProductID)

                Do Until rsVariants.EOF
                    If Not String.IsNullOrEmpty(sVariantIDs) Then
                        sVariantIDs = sVariantIDs & ","
                    End If
                    sVariantIDs = sVariantIDs & rsVariants(0).sValue
                    rsVariants.MoveNext()
                Loop
            End If

            sOrder = oListView.sOrderCol
            If sOrder <> "" Then
                If oListView.lOrderType = wfEnumAspListViewOrderTypes.wfOrderTypeDesc Then sOrder = sOrder & " DESC"
            Else
                sOrder = "WHouse.WarehouseDesc"
            End If

            For Each oFilter In oListView.oFilters
                sClause = DataTools.gsClauseAnd(sClause, oFilter.sClause)
            Next

            sFields = "WHouse.WarehouseID AS RowID, " & _
                    "WHouse.WarehouseDesc AS HouseName, " & _
                    "SUM(SWH.WarehouseQuantity) AS Quantity,"

            If lShowUnit = 0 Then
                sFields = sFields & "MIN(Prod.BaseUnit) AS ShowUnit"
            Else
                sFields = sFields & "MIN(ISNULL(Prod.StockUnit,Prod.BaseUnit)) AS ShowUnit"
            End If

            sFields = sFields & ",MIN(Prod.BaseUnit) AS hiddenBaseUnit"

            sTables = "tdStockMain SMain " & _
                        "INNER JOIN tdStockwarehouses SWH on SMain.stockmainid = SWH.stockmainid " & _
                        "INNER JOIN tdWarehouses WHouse ON WHouse.WarehouseID=SWH.WareHouseId " & _
                        "INNER JOIN tdProducts Prod ON Prod.ProductID=SMain.ProductID "

            sGroup = "WHouse.WarehouseID, WHouse.WarehouseDesc "

            sClause = sClause.gsClauseAnd("(SMain.ProductID=" & lProductID & ") ")

            If lVariantID <> 0 Then
                sClause = sClause.gsClauseAnd("SWH.VariantID=" & lVariantID)
            End If

            '---------- division clause ---------
            If sDivisions = "own" Then
                sClause = DataTools.gsClauseAnd(sClause, "EXISTS (SELECT WHD.DivisionID FROM tdWareHouseDivisions WHD WHERE " & _
                "WHD.DivisionID=" & oClientInfo.oClientProperties.lDivisionNr & " AND " & _
                "(WHD.WareHouseID=SWH.WareHouseID OR WHD.WareHouseID=WHouse.MainParentID)) ")
            End If

            If bVariants And Not String.IsNullOrEmpty(sVariantIDs) Then
                sTables = sTables & " INNER JOIN tdProductVariants PV ON PV.MainVariantID=SWH.VariantID "
                sClause = sClause.gsClauseAnd("SWH.VariantID IN (" & sVariantIDs & ")")
                sFields = sFields & " ,PV.VariantDesc, PV.MainVariantID "
                sGroup = sGroup & ", PV.VariantDesc, PV.MainVariantID "
                sOrder = sOrder & ", PV.MainVariantID "
            End If

            rs = DataMethods.grsGetDBRecordsetPage(oClientInfo, oListView.lPage, oListView.lPageSize, _
                                sFields, sTables, sClause, sGroup, sOrder)

            '------------ sum row data --------------
            Dim lSum As Decimal
            Dim sShowUnit As String = ""
            Dim oRow As AspListViewRow
            Dim sKey As String
            Dim sPrevHouseName As String = ""
            Dim cQuant As Decimal

            Do Until rs.EOF

                If lShowUnit = 1 Then
                    ProductUnit.gbConvertToTargetUnit(oClientInfo, rs("Quantity").cValue, rs("ShowUnit").sValue, lProductID, cQuant)
                    rs("Quantity").Value = cQuant
                End If


                lSum = lSum + rs("Quantity").cValue
                sShowUnit = rs("ShowUnit").sValue

                If bVariants Then
                    sKey = rs("RowID").sValue & "V" & rs("MainVariantID").sValue
                Else
                    sKey = rs("RowID").sValue
                End If

                If bVariants Then
                    If sPrevHouseName <> rs("HouseName").sValue Then
                        sPrevHouseName = rs("HouseName").sValue

                        oRow = oListView.oRows.oAddRow(sKey & "_" & rs("RowID").sValue)
                        oRow.Value(0) = sPrevHouseName
                        oRow.bGroupStyle = True

                    End If

                    oRow = oListView.oRows.oAddRow(sKey)
                    oRow.Value(0) = "   " & rs("VariantDesc").sValue
                    oRow.Value(1) = rs("Quantity").sValue
                    oRow.Value(2) = rs("ShowUnit").sValue

                Else
                    oRow = oListView.oRows.oAddRow(sKey)
                    oRow.SetRecord(rs, True)
                End If


                rs.MoveNext()
            Loop

            rs.Close()

            '----------- add sum row ----------

            oRow = oListView.oRows.oAddRow("-1")
            oRow.Value(0) = Dictionary.gsTranslate(oClientInfo, "Summe")

            If lShowUnit = 1 Then
                ProductUnit.gbConvertToTargetUnit(oClientInfo, DataMethods.gcGetDBValue(oClientInfo, "SUM(SWH.WarehouseQuantity)", sTables, sClause), sShowUnit, lProductID, cQuant)
                oRow.Value(1) = gsFormatNumber(cQuant, 0)
            Else
                oRow.Value(1) = DataMethods.glGetDBValue(oClientInfo, "SUM(SWH.WarehouseQuantity)", sTables, sClause)
            End If
            oRow.Value(2) = sShowUnit

            '------- sum row style ----------
            oRow.bGroupStyle = True


            Dim lCount As Integer
            lCount = DataMethods.glGetDBCount(oClientInfo, sTables, sClause, "distinct WHouse.WarehouseID ")
            If bVariants Then
                lCount = lCount + DataMethods.glGetDBCount(oClientInfo, sTables, sClause, "distinct SWH.VariantID")
            End If

            oListView.lRecordCountTotal = lCount

            Return True

        End Function


    End Class

End Namespace

'================================================================================
'================================================================================
