﻿// Home.js (c) myfactory 2011




function mOnLoad()
{
  
    mResizeGroups();

    addEvent(window, "resize", mResizeGroups);
}

function mResizeGroups()
{
    var oMainDiv = document.getElementById("divBackground");
    if (!oMainDiv) return;

    var oContentDiv = document.getElementById("divMain");
    if (!oContentDiv) return;
    
    var lScreenWidth = oMainDiv.offsetWidth;
    var lScreenHeight = oMainDiv.offsetHeight - 60; // 60 = GroupOffset (überschriften und so)

    var sDefinition = msPageParams[0];
    var asGroupDefinitions = sDefinition.split(";");
    var sDivGroup;
    var lGroupModuleCount;

    // ---- fix def heighs (with margins!) in px ---- 
    var lModuleHeigh = 180;
    var lModuleWidth = 160;
    // 

    var lNewMainDivWidth = 0;

    for (iGroupDef = 0; iGroupDef < asGroupDefinitions.length; iGroupDef++)
    {
        sDivGroup = asGroupDefinitions[iGroupDef].split(",")[0];
        lGroupModuleCount = parseInt(asGroupDefinitions[iGroupDef].split(",")[1]);

        // calculate column-count
        var lColumns = lScreenHeight / lModuleHeigh;
        lColumns = parseInt(lColumns); // cut of decimals
        lColumns = lGroupModuleCount / lColumns;
        lColumns = Math.ceil(lColumns); // round up


        var lNewGroupWith = lColumns  * lModuleWidth;
        var oModuleGroup = document.getElementById(sDivGroup);
        if (oModuleGroup)
        {
            oModuleGroup.style.width = lNewGroupWith;
            lNewMainDivWidth = lNewMainDivWidth + lNewGroupWith + 40; // 40 = margin right
        }
    }

    oContentDiv.style.width = lNewMainDivWidth + 40;  // 40 = offset für bars und 10 margin left

}



function mOnLogoff()
{
    if (!window.confirm(msTerms[0]))
        return;

    var sURL = 'HomeProcess.aspx?Cmd=Logoff'
    var sRes = gsCallServerMethod(sURL, '');

    window.location = '../Default.aspx';
}


var addEvent = function (elem, type, eventHandle)
{
    if (elem == null || typeof (elem) == 'undefined') return;
    if (elem.addEventListener)
    {
        elem.addEventListener(type, eventHandle, false);
    } else if (elem.attachEvent)
    {
        elem.attachEvent("on" + type, eventHandle);
    } else
    {
        elem["on" + type] = eventHandle;
    }
};