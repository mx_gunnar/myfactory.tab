﻿<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.ModuleMain" CodeFile="ModuleMain.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>

    <!--[if IE]>
        <link href="../css/wfStylePageIE.css" type="text/css" rel="stylesheet">
        
    <![endif]-->

    <!--[if !IE]> -->
    
        <link href="../css/wfStylePage.css" type="text/css" rel="stylesheet">
    <!-- <![endif]-->

    
    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <table class="borderTable" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%">
        <tr id="trModuleTop" style="height:50px;">
            <td valign="top">
                <div id="divModuleHead" class="divModuleHead">
                    <div class="divMainHome" id="divMainHome" runat="server" ></div>
                    <myfactory:wfLabel runat="server" ID="lblHead" sClass="lblModuleDesc"></myfactory:wfLabel>
                    <myfactory:wfLabel runat="server" ID="lblNav" sClass="divModuleNavi"></myfactory:wfLabel>
                    <myfactory:wfButton runat="server" ID="lblEnlarge"   stext=" &darr; " sOnClick="mOnEnlarge();"  sClass="btnEnlarge"></myfactory:wfButton>
                </div>
            </td>
        </tr>
        <tr valign="top" height="*">
            <td>
                <myfactory:wfIFrame runat="server" ID="fraModule" sStyle="width:100%;height:100%;margin-top:1px;">
                </myfactory:wfIFrame>
            </td>
        </tr>
    </table>
</body>
</html>
