﻿// Home.js (c) myfactory 2011

function mOnLogoff()
{
    if (!window.confirm(msTerms[0]))
        return;

    var sURL = 'HomeProcess.aspx?Cmd=Logoff'
    var sRes = gsCallServerMethod(sURL, '');

    window.location = '../Default.aspx';
}

function isTouchDevice()
{
    try
    {
        document.createEvent("TouchEvent");
        return true;
    } catch (e)
    {
        return false;
    }
}
function touchScrollX(id)
{
    if (isTouchDevice())
    { //if touch events exist...
        var el = document.getElementById(id);
        var scrollStartPos = 0;

        document.getElementById(id).addEventListener("touchstart", function (event)
        {
            scrollStartPos = this.scrollLeft + event.touches[0].pageX;
        }, false);

        document.getElementById(id).addEventListener("touchmove", function (event)
        {
            this.scrollLeft = scrollStartPos - event.touches[0].pageX;
            event.preventDefault();
        }, false);
    }
}


var bToggle = false;
function mOnEnlarge()
{
    if (bToggle == false)
    {
        document.getElementById("divModuleHead").style.height = "100px";

        document.getElementById("lblHead").style.fontSize = "45px";
        document.getElementById("lblHead").style.marginTop = "25px";

        document.getElementById("lblEnlarge").innerHTML = "&nbsp;&uarr;&nbsp;"

        try
        {
            // change all txtNavi displays to a visible style
            var els = document.getElementsByClassName("txtNavi");
            for (iEle = 0; iEle < els.length; iEle++)
            {
                var oEle = els[iEle];
                if (oEle)
                {
                    oEle.style.display = 'block';

                }
            }

            // chang all divNavi widths to a larger style
            els = document.getElementsByClassName("divNavi");
            for (iEle = 0; iEle < els.length; iEle++)
            {
                var oEle = els[iEle];
                if (oEle)
                {
                    oEle.style.width = '120px';

                }
            }

        } catch (e)
        {

        }

        bToggle = true;
    }
    else
    {
        document.getElementById("divModuleHead").style.height = "50px";


        document.getElementById("lblHead").style.marginTop = "5px";
        document.getElementById("lblHead").style.fontSize = "35px";

        document.getElementById("lblEnlarge").innerHTML = "&nbsp;&darr;&nbsp;"

        try
        {
            // change all txtNavi displays to a invisible style
            var els = document.getElementsByClassName("txtNavi");
            for (iEle = 0; iEle < els.length; iEle++)
            {
                var oEle = els[iEle];
                if (oEle)
                {
                    oEle.style.display = 'none';

                }
            }
            // chang all divNavi widths to a larger style
            els = document.getElementsByClassName("divNavi");
            for (iEle = 0; iEle < els.length; iEle++)
            {
                var oEle = els[iEle];
                if (oEle)
                {
                    oEle.style.width = '80px';

                }
            }

        } catch(e) {}


        bToggle = false;
    }
    
}