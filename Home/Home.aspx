<%@ Register TagPrefix="myfactory" Namespace="myfactory.FrontendSystem.AspControls"
    Assembly="wfAspControls" %>

<%@ Page Inherits="ASP.Tab.Home" CodeFile="Home.aspx.vb" EnableViewState="false"
    AutoEventWireup="false" Language="vb" %>

<html>
<head>


    <link href="../CSS/wfStyleHome.css" rel="stylesheet" />


    <myfactory:wfPageHeader ID="ctlPageHeader" runat="server">
    </myfactory:wfPageHeader>
</head>
<body id="ctlBody" leftmargin="0" topmargin="0" scroll="no" runat="server">
    <div class="divBackground" id="divBackground">
    </div>
    <table class="borderTable" height="100%" cellspacing="0" cellpadding="0" width="100%">
        <tr height="30px">
            <td class="tdHead" valign="top">
                <div style="width: 100%;"> 
                    <div class="divHeadLbl">
                        
                        <myfactory:wfLabel sClass="lblHeadLeft" ID="lblMf" runat="server" sText="my<strong>factory</strong>"></myfactory:wfLabel>
                    </div>
                    
                    <div class="divHeadLbl">
                        <myfactory:wfLabel runat="server" ID="lblHead" sClass="lblHeadMid"></myfactory:wfLabel>
                    </div>
                    <div class="divHeadLbl">
                        <button runat="server" id="cmdLogOut" class="btnHeadRight" onclick="mOnLogoff();"></button>
                    </div>
                </div>
            </td>
        </tr>
        <tr valign="top" >
            <td>
                <myfactory:wfLabel runat="server" sClass="divMain" ID="divMain">
                </myfactory:wfLabel>
            </td>
        </tr>
    </table>
</body>
</html>
