'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    HomeProcess.aspx
'--------------------------------------------------------------------------------
' Purpose:      Process page for HomeProcess
'--------------------------------------------------------------------------------
' Created:      2/8/2011 10:23:41 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab

    Partial Class HomeProcess
        Inherits myfactory.FrontendSystem.AspSystem.ProcessPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       sResponseText
        '--------------------------------------------------------------------------------
        ' Purpose:      Return Response Text
        '--------------------------------------------------------------------------------
        ' Parameter:    oClientInfo         - ClientInfo
        '               sClientID           - ClientID
        '               lContentType (out)  - ContentType
        '--------------------------------------------------------------------------------
        ' Return:       string  - ResponseText
        '================================================================================

        Public Overrides Function sResponseText(ByVal oClientInfo As ClientInfo, ByVal sClientID As String, _
                    ByRef lContentType As myfactory.FrontendSystem.AspSystem.wfEnumContentTypes) As String

            Dim sCmd As String = Request.QueryString("Cmd")

            Select Case sCmd
                Case "Logoff"
                    Return msLogoff(oClientInfo)

                Case Else
                    Return "Unknown Cmd:" & sCmd
            End Select


        End Function

        ''' <summary>
        ''' log off this client 
        ''' </summary>
        ''' <param name="oClientInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function msLogoff(ByVal oClientInfo As ClientInfo) As String

            ClientLogin.gbLogoff(oClientInfo)
            Return "OK;"

        End Function

    End Class

End Namespace

'================================================================================
'================================================================================
