'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    Home.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for Home
'--------------------------------------------------------------------------------
' Created:      1/14/2011 9:36:46 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Tools
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions

Imports myfactory.FrontendSystem.AspTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab

    Partial Class Home
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad
            Me.ctlPageHeader.sIEVersionMetaTag = "<meta http-equiv=""X-UA-Compatible"" content=""IE=edge""/>"

            Dim osHTML As New FastString

            'mWriteModules()
            Dim sGroupDefinitionString As String = msDrawGroups(osHTML)

            Me.divMain.sTranslatedText = osHTML.sString

            Dim sDesc As String
            sDesc = DataMethods.gsGetDBValue(oClientInfo, "DatabaseDesc", "tsDatabases", _
                                             "DatabaseName=" & DataTools.gsStr2Sql(oClientInfo.oClientProperties.sCurrentDatabase), _
                                             , wfDataSourceGlobal)

            Me.lblHead.sTranslatedText = sDesc
            Me.cmdLogOut.InnerText = Dictionary.gsTranslate(oClientInfo, "Abmelden")

            ReDim Me.asTerms(1)
            Me.asTerms(0) = "M�chten Sie sich abmelden?"

            ReDim Me.asPageParams(1)
            Me.asPageParams(0) = sGroupDefinitionString

            Me.sOnLoad = "mOnLoad();"
        End Sub


        Private Sub msGetModuleInfos(ByVal sModule As String, ByRef sInfo1 As String, ByRef sInfo2 As String)

            Dim oDyn As New DynamicObject
            If SysEvents.gbRaiseBeforeEventObject(oClientInfo, "Tablet", "GetModuleInfo", sModule, oDyn) Then
                sInfo1 = oDyn.sProp("Info1")
                sInfo2 = oDyn.sProp("Info2")
            End If


        End Sub


        Private Function msDrawGroups(ByVal osHTML As FastString) As String

            Dim sDefinition As String = String.Empty

            Dim frsGroups As FastRecordset
            frsGroups = DataMethods.gfrsGetFastRecordset(oClientInfo, "GroupName, GroupTitle,GroupDesc", "tsTabModuleGroups", , , "SortIndex", wfDataSourceSystem)


            Dim lGroupModuleCount As Integer
            Dim osGroup As FastString

            Do Until frsGroups.EOF
                lGroupModuleCount = 0
                osGroup = New FastString

                osGroup.bAppend("<div class=""divModuleGroup"" ")
                osGroup.bAppend(" id=""divModuleGroup_" & frsGroups(0).sValue & """ ")
                osGroup.bAppend(">")
                osGroup.bAppend("<div class=""divModuleGroupDesc"">")
                osGroup.bAppend(frsGroups(1).sValue)
                osGroup.bAppend("</div>")

                lGroupModuleCount = mlWriteModules(osGroup, frsGroups(0).sValue)

                osGroup.bAppend("</div>")

                If lGroupModuleCount > 0 Then
                    sDefinition = sDefinition & ";" & "divModuleGroup_" & frsGroups(0).sValue & "," & lGroupModuleCount
                    osHTML.bAppend(osGroup.sString)
                End If

                frsGroups.MoveNext()
            Loop

            ' draw ungrouped modules
            If DataMethods.gbDBExists(oClientInfo, "*", "tsTabModules", "ParentModule IS NULL AND (ModuleGroup IS NULL OR ModuleGroup = '')", wfDataSourceSystem) Then

                lGroupModuleCount = 0
                osGroup = New FastString

                osGroup.bAppend("<div class=""divModuleGroup"" ")
                osGroup.bAppend(" id=""divModuleGroup_ungrouped"" ")
                osGroup.bAppend(">")
                osGroup.bAppend("<div class=""divModuleGroupDesc"">")
                osGroup.bAppend(Dictionary.gsTranslate(oClientInfo, "Sonstiges"))
                osGroup.bAppend("</div>")

                lGroupModuleCount = mlWriteModules(osGroup, "")

                osGroup.bAppend("</div>")

                If lGroupModuleCount > 0 Then
                    sDefinition = sDefinition & ";" & "divModuleGroup_ungrouped" & "," & lGroupModuleCount
                    osHTML.bAppend(osGroup.sString)
                End If

            End If


            sDefinition = sDefinition.TrimStart(";"c)
            Return sDefinition

        End Function




        ''' <summary>
        ''' write module boxes
        ''' </summary>
        ''' <remarks></remarks>
        Private Function mlWriteModules(ByVal osHTML As FastString, ByVal sGroupName As String) As Integer

            Dim rs As Recordset
            Dim bShow As Boolean
            Dim sGroup As String = sGroupName
            Dim lCounter As Integer = 0

            If String.IsNullOrEmpty(sGroup) Then
                sGroup = "'' OR ModuleGroup IS NULL"
            Else
                sGroup = sGroup.gs2Sql
            End If

            rs = DataMethods.grsGetDBRecordset(oClientInfo, "*", "tsTabModules", "ParentModule IS NULL AND (ModuleGroup=" & sGroup & ")", , "ShowIndex", wfDataSourceSystem)
            Do Until rs.EOF
                bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & rs("ModuleName").sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, rs("Edition").sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, rs("PartnerID").sValue, rs("ModuleID").sValue)

                If bShow Then
                    lCounter = lCounter + 1
                    mWriteModule(osHTML, rs)
                End If

                rs.MoveNext()
            Loop

            Me.divMain.sText = ""
            Me.divMain.sTranslatedText = osHTML.sString

            Return lCounter

        End Function


        'write single module box
        Private Sub mWriteModule(ByVal osHTML As FastString, ByVal rs As Recordset)


            Dim sBackgorundImage As String = rs("BGImageName").sValue
            Dim sInlineStyle As String

            Dim sPath As String = "ModuleMain.aspx"
            sPath &= "?ClientID=" & oClientInfo.sClientID
            sPath &= "&Module=" & rs("ModuleName").sValue


            osHTML.bAppend("<a style=""text-decoration:none;"" href=""" & sPath & """ ")

            'lWindowOpenType:  0 = iFrame;  1 = new Window;
            Dim lWindowOpenType As Integer = rs("WindowOpenType").lValue


            If lWindowOpenType = 1 Then
                osHTML.bAppend("target=""_blank""")
            End If
            osHTML.bAppend(">")


            sInlineStyle = "style=""background-color:" & rs("BGColor").sValue & ";"""
            If Not String.IsNullOrEmpty(sBackgorundImage) Then
                sInlineStyle = "style=""background-color:" & rs("BGColor").sValue & "; background-image: url('" & "../IMG/HOME/" & sBackgorundImage & "');"""
            End If

            osHTML.bAppend("<div " & sInlineStyle & " class=""divModule"">" & vbCrLf)

            If String.IsNullOrEmpty(sBackgorundImage) Then
                osHTML.bAppend("<img class=""imgModule"" src=""../img/home/" & rs("ImageName").sValue & """ />" & vbCrLf)
            End If


            'text
            osHTML.bAppend("<div class=""divModuleText"">" & Dictionary.gsTranslate(oClientInfo, rs("ModuleDesc").sValue) & "</div>" & vbCrLf)



            Dim sInfo1 As String = String.Empty
            Dim sInfo2 As String = String.Empty
            msGetModuleInfos(rs("ModuleName").sValue, sInfo1, sInfo2)

            If Not String.IsNullOrEmpty(sInfo1) Then
                osHTML.bAppend("<div class=""divModuleInfo1"">")
                osHTML.bAppend(sInfo1)
                osHTML.bAppend("</div>")
            End If

            If Not String.IsNullOrEmpty(sInfo2) Then
                osHTML.bAppend("<div class=""divModuleInfo2"">")
                osHTML.bAppend(sInfo2)
                osHTML.bAppend("</div>")
            End If

            osHTML.bAppend("</div>" & vbCrLf)

            osHTML.bAppend("</a>")

        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
