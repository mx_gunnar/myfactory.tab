'================================================================================
' Project:      myfactory.BusinessWorld
' Copyright:    myfactory Software GmbH
' Component:    ModuleMain.aspx
'--------------------------------------------------------------------------------
' Purpose:      Info/Edit page for ModuleMain
'--------------------------------------------------------------------------------
' Created:      1/17/2011 11:51:28 AM, Administrator
'--------------------------------------------------------------------------------
' Changed:		7/10/2014, JSchweighart: check if module has parent module
'================================================================================

Option Strict On
Option Explicit On

'================================================================================
' Imports
'================================================================================

Imports myfactory.Sys.Tools
Imports myfactory.Sys.Data
Imports myfactory.Sys.Data.PublicEnums.wfEnumDataSources
Imports myfactory.Sys.Main
Imports myfactory.Sys.Permissions
Imports myfactory.FrontendSystem.AspTools

Imports myfactory.FrontendApp.AspTabletTools

'================================================================================
' Class Definition
'================================================================================

Namespace ASP.Tab

    Partial Class ModuleMain
        Inherits myfactory.FrontendSystem.AspSystem.DialogPage

        '================================================================================
        ' Page Members
        '================================================================================

#Region " Vom Web Form Designer generierter Code "

        'Dieser Aufruf ist f�r den Web Form-Designer erforderlich.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: Diese Methode ist f�r den Web Form-Designer erforderlich
            'Verwenden Sie nicht den Code-Editor zur Bearbeitung.
            InitializeComponent()
        End Sub

#End Region

        '================================================================================
        ' Private Members
        '================================================================================

        '================================================================================
        ' Functions
        '================================================================================

        '================================================================================
        ' Method:       PageLoad
        '--------------------------------------------------------------------------------
        ' Purpose:      Inits Page
        '--------------------------------------------------------------------------------
        ' Parameter:
        '--------------------------------------------------------------------------------
        ' Return:       
        '================================================================================

        Private Sub Page_PageLoad() Handles MyBase.PageLoad
            Me.ctlPageHeader.sIEVersionMetaTag = "<meta http-equiv=""X-UA-Compatible"" content=""IE=edge""/>"

            Dim sModule As String = Request.QueryString("Module")
            Dim sTitle As String

            sTitle = DataMethods.gsGetDBValue(oClientInfo, "ModuleDesc", "tsTabModules", "ModuleName=" & DataTools.gsStr2Sql(sModule), , wfDataSourceSystem)
            sTitle = Dictionary.gsTranslate(oClientInfo, sTitle)

            '  Me.cmdLogOut.InnerText = Dictionary.gsTranslate(oClientInfo, "Abmelden")

            Me.sTranslatedTitle = sTitle
            Me.lblHead.sTranslatedText = sTitle

            '-------- write navi ---------------
            Dim osHTML As New FastString
            Dim frs As FastRecordset
            Dim bShow As Boolean
            Dim sParentModule As String
            Dim sPathExt As String = String.Empty

            sParentModule = DataMethods.gsGetDBValue(oClientInfo, "ParentModule", "tsTabModules", "ModuleName=" & DataTools.gsStr2Sql(sModule), , wfDataSourceSystem)
            If sParentModule = "" Then sParentModule = sModule

            Me.divMainHome.InnerHtml = PageTools.gsWriteHomeLink(oClientInfo)
            osHTML.bAppend(PageTools.gsWriteModuleSubLink(oClientInfo, sParentModule))


            frs = DataMethods.gfrsGetFastRecordset(oClientInfo, _
                                                   "ModuleName,Edition,PartnerID,ModuleID,HideInNavigation", _
                                                   "tsTabModules", _
                                                   "ParentModule=" & DataTools.gsStr2Sql(sParentModule), _
                                                   , "ShowIndex", _
                                                   wfDataSourceSystem)
            Do Until frs.EOF
                bShow = Not frs(4).bValue
                If bShow Then bShow = Permissions.gbCheckTaskPermission(oClientInfo, "TabAccess_" & frs(0).sValue)
                If bShow Then bShow = Editions.gbCheckUserEditions(oClientInfo, frs(1).sValue)
                If bShow Then bShow = AddInModules.gbModulePermission(oClientInfo, frs(2).sValue, frs(3).sValue)

                If bShow Then osHTML.bAppend(PageTools.gsWriteModuleSubLink(oClientInfo, frs(0).sValue))

                frs.MoveNext()
            Loop

            Me.lblNav.sTranslatedText = osHTML.sString

            '-------- write content ---------------
            Dim sPath As String

            sPath = DataMethods.gsGetDBValue(oClientInfo, "PagePath", "tsTabModules", "ModuleName=" & DataTools.gsStr2Sql(sModule), , wfDataSourceSystem)
            If sPath = "" Then sPath = "TODO/TODOMain.aspx"

            'check if module has parent module
            If sParentModule.Equals(sModule) Then sPathExt = "&HasParentModule=False"

            sPath = "/tab/" & sPath & "?ClientID=" & sClientID & sPathExt
            Me.fraModule.sSrc = sPath

            '-------- page data ---------------
            ReDim Me.asTerms(1)
            Me.asTerms(0) = "M�chten Sie sich abmelden?"


        End Sub

    End Class

End Namespace

'================================================================================
'================================================================================
