<%@ Page language="VB"%>
<%@ Import Namespace="myfactory.sys.xml" %>
<%@ Import Namespace="myfactory.sys.tools.datatypefunctions" %>
<%
    '================================================================================
    ' Project:      myfactory.BusinessWorld
	' Copyright:    myfactory Holding GmbH
	' Component:    Default.aspx
    '--------------------------------------------------------------------------------
	' Purpose:      Start Page for myfactory tablet app
    '--------------------------------------------------------------------------------
	' Created:      14.01.11, M.Gerlach
    '--------------------------------------------------------------------------------
    ' Changed:
    '================================================================================

    Dim sRedirect As String = ""
    
    Response.Expires = -1

    If UCase(Request.ServerVariables.Item("HTTPS")) <> "ON" Then
        If myfactory.Sys.Data.SysOptions.sOption("AllowTabWithoutSSL", Application("AppName").ToString) <> "-1" Then
            sRedirect = Request.Url.ToString
            
            If Left(sRedirect, 7) = "http://" Then
                sRedirect = Replace(sRedirect, "http://", "https://")
            Else
                sRedirect = ""
            End If
        End If
    End If

    '----------- get remembered data ----------------
    Dim sUser As String = ""
    Dim sPwd As String = ""
    Dim sChecked As String = ""
    Dim sRemember As String
    Dim sDB As String
    Dim sBS As String
    Dim sBadBrowserInfo As String
    
    sBadBrowserInfo = Request.Browser.Browser
    
    If sBadBrowserInfo = "IE" Then
        sBadBrowserInfo = "Diese Seiten sind f�r die Darstellung in Google Chrome und Apple Safari optimiert.<br />Bei Verwendung des Internet Explorers kann es zu einer abweichenden Darstellung kommen.<br /><br />"
    Else
        sBadBrowserInfo = ""
    End If
    
    Dim bUseRemeber As Boolean

    bUseRemeber = Not gbCBool(myfactory.Sys.Data.SysOptions.sOption("DontRememberMobileUser", Application("AppName").ToString))
    If bUseRemeber Then
    
        On Error Resume Next
        sRemember = Request.Cookies("wfTabRemember").Value
        If sRemember = "true" And sRedirect = "" Then

            Dim sKey As String = "wT34!Qd+12#k"

            sChecked = " checked "
            sUser = Request.Cookies("wfTabUser").Value
            If Left(sUser, 7) <> "<wfXML>" Then sUser = "<wfXML>" & sUser & "</wfXML>"
            sPwd = Request.Cookies("wfTabPwd").Value
            If Left(sPwd, 7) <> "<wfXML>" Then sPwd = "<wfXML>" & sPwd & "</wfXML>"

            sUser = XmlFunctions.gsXMLDecrypt(sUser, sKey)
            sPwd = XmlFunctions.gsXMLDecrypt(sPwd, sKey)

            sDB = Request.Cookies("wfTabDB").Value
            sBS = Request.Cookies("wfTabBS").Value
        End If
        On Error GoTo 0
    End If
    
%>
<html>
<head>
    <link href="css/wfStyleLogin.css" type="text/css" rel="stylesheet" />
  
	<link rel="apple-touch-icon" href="myfactory_icon.png" />
    <script LANGUAGE="JavaScript" SRC="../ie50/js/wfDlgParams.js"></script>

	<title>myfactory.Start</title>

    <script language="javascript">
        var msDB = '<%=sDB%>';
        var msBS = '<%=sBS%>';

        if ('<%=sRedirect%>' != '')
        {
            window.location = '<%=sRedirect%>';
        }

        function mOnLogin()
        {
            if (document.all.txtUser.value != '<%=sUser%>')
            {
                msDB = '';
                msBS = '';
            }
            if (document.all.cboDatabases.value != '')
                msDB = document.all.cboDatabases.value;

            if (document.all.cboDivisions.value != '')
                msBS = document.all.cboDivisions.value;
            
            var sChecked = false;
            if (document.all.chkRemember) sChecked = document.all.chkRemember.checked;

            var sURL = 'LoginProcess.aspx?Cmd=Login' +
                        '&User=' + msEncodeStringURL(document.all.txtUser.value) +
                        '&Pwd=' + msEncodeStringURL(document.all.txtPwd.value) +
                        '&Database=' + msEncodeStringURL(msDB) +
                        '&Division=' + msEncodeStringURL(msBS) +
                        '&Remember=' + sChecked +
                        '&MobilePlatform=' + navigator.platform;

            var oSMthHttp = new XMLHttpRequest();
            oSMthHttp.open('GET', sURL, false);
            oSMthHttp.send('');
            var sResponse = oSMthHttp.responseText;

            if (sResponse.substr(0, 3) == 'OK;')
            {
                window.location = 'Home/Home.aspx?ClientID=' + sResponse.substr(3);
            }
            else if (sResponse.substr(0, 4) == 'PWD;')
            {
                var asResponse = sResponse.split(';');
                alert(asResponse[2]);
                window.location = 'Home/Home.aspx?ClientID=' + asResponse[1];
            }
            else if (sResponse.substr(0, 3) == 'DB$')
            {
                document.all.trDatabases.style.display = '';
                gClearCombobox('cboDatabases');
                gAddCombobox('cboDatabases', sResponse.split('$')[1],sResponse.split('$')[2]);
            }
            else if (sResponse.substr(0, 3) == 'BS$')
            {
                document.all.trDivisions.style.display = '';
                gClearCombobox('cboDivisions');
                gAddCombobox('cboDivisions', sResponse.split('$')[1], sResponse.split('$')[2]);
            }
            else
                alert(sResponse);
        }
        function mRememberClick()
        {
            msDB = '';
            msBS = '';
        }

        function msEncodeStringURL(sValue)
        {
            var sURL, sNew;

            sURL = sValue + '';
            if (sURL == '')
                return ('');


            sURL = sURL.replace(/%/gi, '%25')

            sURL = sURL.replace(/\*/gi, '%2A')
            sURL = sURL.replace(/\+/gi, '%2B')
            sURL = sURL.replace(/\./gi, '%2E')
            sURL = sURL.replace(/\//gi, '%2F')

            sURL = sURL.replace(/"/gi, '%22')
            sURL = sURL.replace(/#/gi, '%23')
            sURL = sURL.replace(/&/gi, '%26')
            sURL = sURL.replace(/'/gi, '%27')
            sURL = sURL.replace(/,/gi, '%2C')
            sURL = sURL.replace(/:/gi, '%3A')

            sURL = sURL.replace(/ /gi, '+')

            sURL = sURL.replace(/\r\n/g, "%5Cn");
            sURL = sURL.replace(/\n/g, "%5Cn");

            sNew = "";
            for (var n = 0; n < sURL.length; n++)
            {
                var c = sURL.charCodeAt(n);

                if (c < 128)
                    sNew += String.fromCharCode(c);
                else if ((c > 127) && (c < 2048))
                {
                    sNew += '%' + ((c >> 6) | 192).toString(16);
                    sNew += '%' + ((c & 63) | 128).toString(16);
                }
                else
                {
                    sNew += '%' + ((c >> 12) | 224).toString(16);
                    sNew += '%' + (((c >> 6) & 63) | 128).toString(16);
                    sNew += '%' + ((c & 63) | 128).toString(16);
                }
            }

            return (sNew);
        }
    </script>
</head>
<body leftMargin=0 topMargin=0> 
    <div class="divBackground">
<img class="imgSplashMain" src="img/splash_3.png" />
        </div>
<table class=borderTable height="30%" cellSpacing=0 cellPadding=0 width="100%">
<tr height="25px">
	<td class="tdHead">
		<div>myfactory - Tablet - Login</div>
	</td>
</tr>	   


<tr valign="middle">
	<td align="center">
		<div >
		<form name="frmLogin" action="LoginProcess.aspx" method="post">
			<br />
            <label id="lblBadBrowserInfo"><%=sBadBrowserInfo%></label>

            <table class="inputTable" cellspacing="0" cellpadding="0">
                <tr class="trLoginTile">
                    <td width="150px" class="tdLoginTile">
                            <div class="lblLogin">Benutzer:</div> 
                    </td>
                    <td class="tdLoginTile">
                        <input type="text" class="txtLogin" name="txtUser" value="<%=sUser%>" /> 
                    </td>
                </tr>
                <tr class="trLoginTile">
                    <td class="tdLoginTile"> 
                        <div class="lblLogin">Kennwort:</div> 
                    </td>
                    <td class="tdLoginTile">
                        <input type="password" class="txtLogin" name="txtPwd" value="<%=sPwd%>" /> 
                    </td>
                </tr>



                <tr id="trDatabases" class="trLoginTile" style="display:none;">
                    <td class="tdLoginTile">
                        <div class="lblLogin" id="lblDatabases">Datenbank:</div> 
                    </td>
                    <td class="tdLoginTile">
                        <select id="cboDatabases"  style="width:200px;" class="txtLogin"   ></select>
                    </td>
                </tr>

                <tr id="trDivisions" class="trLoginTile" style="display:none;">
                    <td class="tdLoginTile">
                        <div class="lblLogin" id="lblDivisions"  >Betriebsst�tte:</div>  
                    </td>
                    <td class="tdLoginTile">
                        <select id="cboDivisions"   style="width:200px;"" class="txtLogin"  ></select>
                    </td>
                </tr>
                <% If bUseRemeber Then%>
                <tr>
                    <td >&nbsp;</td>
                    <td >
            			<input type="checkbox" class="loginCheckbox"  name="chkRemember" <%=sChecked%> onclick="mRememberClick()"/>
                        <div class="lblLoginCbo" style="display:inline;">&nbsp;Anmeldung&nbsp;merken</div>
                    </td>
                </tr>
<% End If%>
            </table>

			<input style="margin-top:30px;" class="cmdLogin" type="Button" value="Anmelden" onclick="mOnLogin()" />

		</form>

		</div>
	</td>
</tr>
</table>

</body>
</html>
