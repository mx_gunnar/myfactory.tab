<%@ Page language="VB"%>
<%
    '================================================================================
    ' Project:      myfactory.BusinessWorld
	' Copyright:    myfactory Holding GmbH
	' Component:    ErrPage.aspx
    '--------------------------------------------------------------------------------
    ' Purpose:      Error Page for myfactory tab app
    '--------------------------------------------------------------------------------
	' Created:      14.01.11, M.Gerlach
    '--------------------------------------------------------------------------------
    ' Changed:
    '================================================================================

    Dim sTarget As String
    
    sTarget = Application("WebPageRoot").ToString & "/tab/default.aspx"
        
    Response.Expires = -1
    'Response.Redirect()
    'Response.End()

%>

<html>
<head>
</head>
<script language="javascript">

    var oWin = window;
    var oParent = oWin;

    do
    {
        oWin = oParent;
        oParent = oWin.parent;
    } while (oParent && oParent!=oWin)
    
    if (oWin)
        oWin.location = '<%=sTarget%>';

</script>
<body>
</body>
</html>
